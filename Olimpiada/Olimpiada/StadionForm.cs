﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class StadionForm : Form
    {
        private GraphClient client;
        private Stadion stadion;
        private Klub klub;
        private Disciplina disciplina;
        private Takmicenje selektovanoTakmicenje;

        public StadionForm(Stadion s, Klub k, Disciplina d)
        {
            InitializeComponent();
            stadion = s;
            klub = k;
            disciplina = d;
        }


        public void popuniKontrole()
        {
            textBoxIme.Text = stadion.Ime_stadiona;
            textBoxGrad.Text = stadion.Grad;
            textBoxLokacija.Text = stadion.Lokacija;
            textBoxKapacitet.Text = stadion.Kapacitet;
            textBoxGodIzgradnje.Text = stadion.Godina_izgradnje;



        }
        public void obrisiKontrole()
        {
            textBoxIme.Text = String.Empty;
            textBoxGrad.Text = String.Empty;
            textBoxLokacija.Text = String.Empty;
            textBoxKapacitet.Text = String.Empty;
            textBoxGodIzgradnje.Text = String.Empty;

        }


        public void popuniTakmicenja()
        {
            if (disciplina.Tip == "Pojedinacna")
            {
                
                List<Takmicenje> takm;

                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("NazivDisciplina", disciplina.Naziv);
                var query = new CypherQuery("start d=node(*) match  (d)-[r:POSEDUJE]->(t) where d.Naziv=~{NazivDisciplina}  return t",
                   queryDict, CypherResultMode.Set);
                takm = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();

                if (comboBoxtakmicenje.Items.Count > 0)
                    comboBoxtakmicenje.Items.Clear();
                foreach (Takmicenje ds in takm)
                {
                    comboBoxtakmicenje.Items.Add(ds);
                    comboBoxtakmicenje.DisplayMember = "Naziv";
                }
            }

        }
        


        private int getMaxId()
        {


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Stadion) RETURN n.ID",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            List<String> maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList();


            List<int> id = maxId.Select(s => int.Parse(s)).ToList();
            int max = id.Max();

            return max;
        }


        private Stadion dodajStadion()
        {
            Stadion a = new Stadion();
            
            a.Ime_stadiona = textBoxIme.Text;
            a.Grad = textBoxGrad.Text;
            a.Lokacija = textBoxLokacija.Text;
            a.Kapacitet = textBoxKapacitet.Text;
            a.Godina_izgradnje = textBoxGodIzgradnje.Text;
            

            return a;
        }

        
        private void buttonIzmeni_Click(object sender, EventArgs e)
        {
            if (textBoxIme.Text != "" && textBoxGrad.Text != "" && textBoxLokacija.Text != "" && textBoxKapacitet.Text != "" && textBoxGodIzgradnje.Text != "")
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Stadion) and n.ID = " + "'" + stadion.ID + "'" +
                                                            " set n.Ime_stadiona = " + "'" + textBoxIme.Text + "'" +
                                                            ", n.Grad= " + "'" + textBoxGrad.Text + "'" +
                                                             ", n.Lokacija= " + "'" + textBoxLokacija.Text + "'" +
                                                               ", n.Kapacitet= " + "'" + textBoxKapacitet.Text + "'" +
                                                               ", n.Godina_izgradnje= " + "'" + textBoxGodIzgradnje.Text + "'" +
                                                            " return n",
                                                new Dictionary<string, object>(), CypherResultMode.Set);


                ((IRawGraphClient)client).ExecuteCypher(query);
                MessageBox.Show("Uspesno ste izmenili stadion.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();
        }

        private void buttonDodaj_Click(object sender, EventArgs e)
        {

            if (textBoxIme.Text != "" || textBoxGrad.Text != "" || textBoxLokacija.Text != "" || textBoxKapacitet.Text != "" || textBoxGodIzgradnje.Text != "")
            {
                Stadion s = dodajStadion();
                int maxId = getMaxId();

                try
                {
                    // int mId = Int32.Parse(maxId);
                    s.ID = (++maxId);
                }
                catch (Exception exception)
                {
                    s.ID = 0;
                }


                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("Ime_stadiona", s.Ime_stadiona);
                queryDict.Add("Grad", s.Grad);
                queryDict.Add("Lokacija", s.Lokacija);
                queryDict.Add("Kapacitet", s.Kapacitet);
                queryDict.Add("Godina_izgradnje", s.Godina_izgradnje);



                var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Stadion {ID:'" + s.ID + "', Ime_stadiona:'" + s.Ime_stadiona
                                                                + "', Grad:'" + s.Grad + "', Lokacija:'" + s.Lokacija
                                                                + "', Kapacitet:'" + s.Kapacitet
                                                                + "', Godina_izgradnje:'" + s.Godina_izgradnje

                                                                + "'}) return n",
                                                                queryDict, CypherResultMode.Set);

                List<Stadion> sp = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).ToList();


                //ako je tip discipline ekipna kreira vezu stadion-[odigrava]->klub
                if (disciplina.Tip == "Ekipna")
                {
                    var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Stadion),(b:Klub)"
                                                    + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + klub.ID + "'"
                                                    + " CREATE (a)-[r:ODIGRAVA]->(b)"
                                                    + " return a,r,b",
                                                  new Dictionary<string, object>(), CypherResultMode.Projection);
                    ((IRawGraphClient)client).ExecuteCypher(query1);


                    MessageBox.Show("Uspesno ste dodali stadion za klub " + klub.Naziv, "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Stadion),(b:Takmicenje)"
                                                    + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + selektovanoTakmicenje.ID + "'"
                                                    + " CREATE (a)-[r:ODRZAVA]->(b)"
                                                    + " return a,r,b",
                                                  new Dictionary<string, object>(), CypherResultMode.Projection);
                    ((IRawGraphClient)client).ExecuteCypher(query2);
                    MessageBox.Show("Uspesno ste dodali stadion na takmicenju " + selektovanoTakmicenje.Naziv
                        + " u " + selektovanoTakmicenje.Zemlja, "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

                this.Close();
            }
            else
                MessageBox.Show("Morate popuniti sva polja ", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);


        }

        private void radioButtonkreirajStadion_CheckedChanged(object sender, EventArgs e)
        {
            obrisiKontrole();
            stadion = null;
            buttonIzmeni.Enabled = false;

        }

        private void comboBoxtakmicenje_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovanoTakmicenje = (Takmicenje)comboBoxtakmicenje.SelectedItem;

        }

        private void StadionForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            if (disciplina.Tip == "Pojedinacna")
            {
                comboBoxtakmicenje.Enabled = true;
                popuniTakmicenja();
            }
            else
                comboBoxtakmicenje.Enabled = false;

            comboBoxtakmicenje.Text = "--Izaberite takmicenje--";


            if (stadion != null)
                popuniKontrole();
        }
    }
}
