﻿namespace Olimpiada
{
    partial class DisciplinaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxDisciplina = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.buttonIzmeniDisciplinu = new System.Windows.Forms.Button();
            this.buttonKreirajDisciplinu = new System.Windows.Forms.Button();
            this.radioButtonZenski = new System.Windows.Forms.RadioButton();
            this.radioButtonMuski = new System.Windows.Forms.RadioButton();
            this.textBoxNazivDiscipline = new System.Windows.Forms.TextBox();
            this.labelOdaberiteDisciplinu = new System.Windows.Forms.Label();
            this.labelTipDiscipline = new System.Windows.Forms.Label();
            this.labelPol = new System.Windows.Forms.Label();
            this.labelNazivDiscipline = new System.Windows.Forms.Label();
            this.comboBoxOdaberiteDisciplinu = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxTakmicenjaVeza = new System.Windows.Forms.ComboBox();
            this.comboBoxDisciplineVeza = new System.Windows.Forms.ComboBox();
            this.buttonIzbrisiVezu = new System.Windows.Forms.Button();
            this.buttonDodajVezu = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxTip = new System.Windows.Forms.ComboBox();
            this.groupBoxDisciplina.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxDisciplina
            // 
            this.groupBoxDisciplina.Controls.Add(this.comboBoxTip);
            this.groupBoxDisciplina.Controls.Add(this.button1);
            this.groupBoxDisciplina.Controls.Add(this.buttonIzmeniDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.buttonKreirajDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.radioButtonZenski);
            this.groupBoxDisciplina.Controls.Add(this.radioButtonMuski);
            this.groupBoxDisciplina.Controls.Add(this.textBoxNazivDiscipline);
            this.groupBoxDisciplina.Controls.Add(this.labelOdaberiteDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.labelTipDiscipline);
            this.groupBoxDisciplina.Controls.Add(this.labelPol);
            this.groupBoxDisciplina.Controls.Add(this.labelNazivDiscipline);
            this.groupBoxDisciplina.Controls.Add(this.comboBoxOdaberiteDisciplinu);
            this.groupBoxDisciplina.Location = new System.Drawing.Point(10, 11);
            this.groupBoxDisciplina.Name = "groupBoxDisciplina";
            this.groupBoxDisciplina.Size = new System.Drawing.Size(339, 208);
            this.groupBoxDisciplina.TabIndex = 6;
            this.groupBoxDisciplina.TabStop = false;
            this.groupBoxDisciplina.Text = "Disciplina";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(216, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(71, 38);
            this.button1.TabIndex = 12;
            this.button1.Text = "Obrisi disciplinu";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // buttonIzmeniDisciplinu
            // 
            this.buttonIzmeniDisciplinu.Location = new System.Drawing.Point(126, 164);
            this.buttonIzmeniDisciplinu.Name = "buttonIzmeniDisciplinu";
            this.buttonIzmeniDisciplinu.Size = new System.Drawing.Size(71, 38);
            this.buttonIzmeniDisciplinu.TabIndex = 11;
            this.buttonIzmeniDisciplinu.Text = "Izmeni disciplinu";
            this.buttonIzmeniDisciplinu.UseVisualStyleBackColor = true;
            this.buttonIzmeniDisciplinu.Click += new System.EventHandler(this.buttonIzmeniDisciplinu_Click);
            // 
            // buttonKreirajDisciplinu
            // 
            this.buttonKreirajDisciplinu.Location = new System.Drawing.Point(34, 164);
            this.buttonKreirajDisciplinu.Name = "buttonKreirajDisciplinu";
            this.buttonKreirajDisciplinu.Size = new System.Drawing.Size(71, 38);
            this.buttonKreirajDisciplinu.TabIndex = 9;
            this.buttonKreirajDisciplinu.Text = "Kreiraj disciplinu";
            this.buttonKreirajDisciplinu.UseVisualStyleBackColor = true;
            this.buttonKreirajDisciplinu.Click += new System.EventHandler(this.buttonKreirajDisciplinu_Click);
            // 
            // radioButtonZenski
            // 
            this.radioButtonZenski.AutoSize = true;
            this.radioButtonZenski.Location = new System.Drawing.Point(186, 89);
            this.radioButtonZenski.Name = "radioButtonZenski";
            this.radioButtonZenski.Size = new System.Drawing.Size(32, 17);
            this.radioButtonZenski.TabIndex = 8;
            this.radioButtonZenski.TabStop = true;
            this.radioButtonZenski.Text = "Z";
            this.radioButtonZenski.UseVisualStyleBackColor = true;
            // 
            // radioButtonMuski
            // 
            this.radioButtonMuski.AutoSize = true;
            this.radioButtonMuski.Location = new System.Drawing.Point(146, 89);
            this.radioButtonMuski.Name = "radioButtonMuski";
            this.radioButtonMuski.Size = new System.Drawing.Size(34, 17);
            this.radioButtonMuski.TabIndex = 7;
            this.radioButtonMuski.TabStop = true;
            this.radioButtonMuski.Text = "M";
            this.radioButtonMuski.UseVisualStyleBackColor = true;
            // 
            // textBoxNazivDiscipline
            // 
            this.textBoxNazivDiscipline.Location = new System.Drawing.Point(126, 59);
            this.textBoxNazivDiscipline.Name = "textBoxNazivDiscipline";
            this.textBoxNazivDiscipline.Size = new System.Drawing.Size(121, 20);
            this.textBoxNazivDiscipline.TabIndex = 5;
            // 
            // labelOdaberiteDisciplinu
            // 
            this.labelOdaberiteDisciplinu.AutoSize = true;
            this.labelOdaberiteDisciplinu.Location = new System.Drawing.Point(6, 21);
            this.labelOdaberiteDisciplinu.Name = "labelOdaberiteDisciplinu";
            this.labelOdaberiteDisciplinu.Size = new System.Drawing.Size(99, 13);
            this.labelOdaberiteDisciplinu.TabIndex = 4;
            this.labelOdaberiteDisciplinu.Text = "Odaberite disciplinu";
            // 
            // labelTipDiscipline
            // 
            this.labelTipDiscipline.AutoSize = true;
            this.labelTipDiscipline.Location = new System.Drawing.Point(6, 119);
            this.labelTipDiscipline.Name = "labelTipDiscipline";
            this.labelTipDiscipline.Size = new System.Drawing.Size(22, 13);
            this.labelTipDiscipline.TabIndex = 3;
            this.labelTipDiscipline.Text = "Tip";
            // 
            // labelPol
            // 
            this.labelPol.AutoSize = true;
            this.labelPol.Location = new System.Drawing.Point(6, 89);
            this.labelPol.Name = "labelPol";
            this.labelPol.Size = new System.Drawing.Size(22, 13);
            this.labelPol.TabIndex = 2;
            this.labelPol.Text = "Pol";
            // 
            // labelNazivDiscipline
            // 
            this.labelNazivDiscipline.AutoSize = true;
            this.labelNazivDiscipline.Location = new System.Drawing.Point(6, 59);
            this.labelNazivDiscipline.Name = "labelNazivDiscipline";
            this.labelNazivDiscipline.Size = new System.Drawing.Size(34, 13);
            this.labelNazivDiscipline.TabIndex = 1;
            this.labelNazivDiscipline.Text = "Naziv";
            // 
            // comboBoxOdaberiteDisciplinu
            // 
            this.comboBoxOdaberiteDisciplinu.FormattingEnabled = true;
            this.comboBoxOdaberiteDisciplinu.Location = new System.Drawing.Point(126, 19);
            this.comboBoxOdaberiteDisciplinu.Name = "comboBoxOdaberiteDisciplinu";
            this.comboBoxOdaberiteDisciplinu.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiteDisciplinu.TabIndex = 0;
            this.comboBoxOdaberiteDisciplinu.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteDisciplinu_SelectedIndexChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxTakmicenjaVeza);
            this.groupBox1.Controls.Add(this.comboBoxDisciplineVeza);
            this.groupBox1.Controls.Add(this.buttonIzbrisiVezu);
            this.groupBox1.Controls.Add(this.buttonDodajVezu);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(368, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 235);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Povezivanje  discipline i takmicenja";
            // 
            // comboBoxTakmicenjaVeza
            // 
            this.comboBoxTakmicenjaVeza.FormattingEnabled = true;
            this.comboBoxTakmicenjaVeza.Location = new System.Drawing.Point(125, 67);
            this.comboBoxTakmicenjaVeza.Name = "comboBoxTakmicenjaVeza";
            this.comboBoxTakmicenjaVeza.Size = new System.Drawing.Size(140, 21);
            this.comboBoxTakmicenjaVeza.TabIndex = 11;
            // 
            // comboBoxDisciplineVeza
            // 
            this.comboBoxDisciplineVeza.FormattingEnabled = true;
            this.comboBoxDisciplineVeza.Location = new System.Drawing.Point(125, 26);
            this.comboBoxDisciplineVeza.Name = "comboBoxDisciplineVeza";
            this.comboBoxDisciplineVeza.Size = new System.Drawing.Size(140, 21);
            this.comboBoxDisciplineVeza.TabIndex = 10;
            // 
            // buttonIzbrisiVezu
            // 
            this.buttonIzbrisiVezu.Location = new System.Drawing.Point(158, 142);
            this.buttonIzbrisiVezu.Name = "buttonIzbrisiVezu";
            this.buttonIzbrisiVezu.Size = new System.Drawing.Size(75, 23);
            this.buttonIzbrisiVezu.TabIndex = 6;
            this.buttonIzbrisiVezu.Text = "Izbrisi vezu";
            this.buttonIzbrisiVezu.UseVisualStyleBackColor = true;
            this.buttonIzbrisiVezu.Click += new System.EventHandler(this.buttonIzbrisiVezu_Click);
            // 
            // buttonDodajVezu
            // 
            this.buttonDodajVezu.Location = new System.Drawing.Point(45, 142);
            this.buttonDodajVezu.Name = "buttonDodajVezu";
            this.buttonDodajVezu.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajVezu.TabIndex = 1;
            this.buttonDodajVezu.Text = "Dodaj vezu";
            this.buttonDodajVezu.UseVisualStyleBackColor = true;
            this.buttonDodajVezu.Click += new System.EventHandler(this.buttonDodajVezu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 75);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(107, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Odaberite takmicenje";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 34);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Discipline";
            // 
            // comboBoxTip
            // 
            this.comboBoxTip.FormattingEnabled = true;
            this.comboBoxTip.Items.AddRange(new object[] {
            "Pojedinacna",
            "Ekipna"});
            this.comboBoxTip.Location = new System.Drawing.Point(126, 119);
            this.comboBoxTip.Name = "comboBoxTip";
            this.comboBoxTip.Size = new System.Drawing.Size(121, 21);
            this.comboBoxTip.TabIndex = 13;
            // 
            // DisciplinaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 277);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxDisciplina);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "DisciplinaForm";
            this.Text = "DisciplinaForm";
            this.Load += new System.EventHandler(this.DisciplinaForm_Load);
            this.groupBoxDisciplina.ResumeLayout(false);
            this.groupBoxDisciplina.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxDisciplina;
        private System.Windows.Forms.Button buttonIzmeniDisciplinu;
        private System.Windows.Forms.Button buttonKreirajDisciplinu;
        private System.Windows.Forms.RadioButton radioButtonZenski;
        private System.Windows.Forms.RadioButton radioButtonMuski;
        private System.Windows.Forms.TextBox textBoxNazivDiscipline;
        private System.Windows.Forms.Label labelOdaberiteDisciplinu;
        private System.Windows.Forms.Label labelTipDiscipline;
        private System.Windows.Forms.Label labelPol;
        private System.Windows.Forms.Label labelNazivDiscipline;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteDisciplinu;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxTakmicenjaVeza;
        private System.Windows.Forms.ComboBox comboBoxDisciplineVeza;
        private System.Windows.Forms.Button buttonIzbrisiVezu;
        private System.Windows.Forms.Button buttonDodajVezu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxTip;
    }
}