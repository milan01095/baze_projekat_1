﻿namespace Olimpiada
{
    partial class KlubForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxKlub = new System.Windows.Forms.GroupBox();
            this.textBoxGodina = new System.Windows.Forms.TextBox();
            this.buttonIzmeniKlub = new System.Windows.Forms.Button();
            this.buttonKreirajKlub = new System.Windows.Forms.Button();
            this.textBoxSponzor = new System.Windows.Forms.TextBox();
            this.numericUpDownBrojTitula = new System.Windows.Forms.NumericUpDown();
            this.textBoxSajt = new System.Windows.Forms.TextBox();
            this.textBoxGrad = new System.Windows.Forms.TextBox();
            this.textBoxMenadzer = new System.Windows.Forms.TextBox();
            this.textBoxTrener = new System.Windows.Forms.TextBox();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.labelBrojOsvijenihTitula = new System.Windows.Forms.Label();
            this.labelSponzor = new System.Windows.Forms.Label();
            this.labelSajt = new System.Windows.Forms.Label();
            this.labelGrad = new System.Windows.Forms.Label();
            this.labelMenadzer = new System.Windows.Forms.Label();
            this.labelDatumOsnivanja = new System.Windows.Forms.Label();
            this.labelTrener = new System.Windows.Forms.Label();
            this.labelNaizv = new System.Windows.Forms.Label();
            this.groupBoxKlub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojTitula)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBoxKlub
            // 
            this.groupBoxKlub.Controls.Add(this.textBoxGodina);
            this.groupBoxKlub.Controls.Add(this.buttonIzmeniKlub);
            this.groupBoxKlub.Controls.Add(this.buttonKreirajKlub);
            this.groupBoxKlub.Controls.Add(this.textBoxSponzor);
            this.groupBoxKlub.Controls.Add(this.numericUpDownBrojTitula);
            this.groupBoxKlub.Controls.Add(this.textBoxSajt);
            this.groupBoxKlub.Controls.Add(this.textBoxGrad);
            this.groupBoxKlub.Controls.Add(this.textBoxMenadzer);
            this.groupBoxKlub.Controls.Add(this.textBoxTrener);
            this.groupBoxKlub.Controls.Add(this.textBoxNaziv);
            this.groupBoxKlub.Controls.Add(this.labelBrojOsvijenihTitula);
            this.groupBoxKlub.Controls.Add(this.labelSponzor);
            this.groupBoxKlub.Controls.Add(this.labelSajt);
            this.groupBoxKlub.Controls.Add(this.labelGrad);
            this.groupBoxKlub.Controls.Add(this.labelMenadzer);
            this.groupBoxKlub.Controls.Add(this.labelDatumOsnivanja);
            this.groupBoxKlub.Controls.Add(this.labelTrener);
            this.groupBoxKlub.Controls.Add(this.labelNaizv);
            this.groupBoxKlub.Location = new System.Drawing.Point(10, 11);
            this.groupBoxKlub.Name = "groupBoxKlub";
            this.groupBoxKlub.Size = new System.Drawing.Size(346, 356);
            this.groupBoxKlub.TabIndex = 0;
            this.groupBoxKlub.TabStop = false;
            this.groupBoxKlub.Text = "Klub";
            // 
            // textBoxGodina
            // 
            this.textBoxGodina.Location = new System.Drawing.Point(137, 106);
            this.textBoxGodina.Name = "textBoxGodina";
            this.textBoxGodina.Size = new System.Drawing.Size(121, 20);
            this.textBoxGodina.TabIndex = 5;
            // 
            // buttonIzmeniKlub
            // 
            this.buttonIzmeniKlub.Location = new System.Drawing.Point(182, 317);
            this.buttonIzmeniKlub.Name = "buttonIzmeniKlub";
            this.buttonIzmeniKlub.Size = new System.Drawing.Size(75, 23);
            this.buttonIzmeniKlub.TabIndex = 17;
            this.buttonIzmeniKlub.Text = "Izmeni klub";
            this.buttonIzmeniKlub.UseVisualStyleBackColor = true;
            this.buttonIzmeniKlub.Click += new System.EventHandler(this.buttonIzmeniKlub_Click);
            // 
            // buttonKreirajKlub
            // 
            this.buttonKreirajKlub.Location = new System.Drawing.Point(40, 317);
            this.buttonKreirajKlub.Name = "buttonKreirajKlub";
            this.buttonKreirajKlub.Size = new System.Drawing.Size(75, 23);
            this.buttonKreirajKlub.TabIndex = 16;
            this.buttonKreirajKlub.Text = "Kreiraj klub";
            this.buttonKreirajKlub.UseVisualStyleBackColor = true;
            this.buttonKreirajKlub.Click += new System.EventHandler(this.buttonKreirajKlub_Click);
            // 
            // textBoxSponzor
            // 
            this.textBoxSponzor.Location = new System.Drawing.Point(137, 241);
            this.textBoxSponzor.Name = "textBoxSponzor";
            this.textBoxSponzor.Size = new System.Drawing.Size(120, 20);
            this.textBoxSponzor.TabIndex = 13;
            // 
            // numericUpDownBrojTitula
            // 
            this.numericUpDownBrojTitula.Location = new System.Drawing.Point(220, 272);
            this.numericUpDownBrojTitula.Name = "numericUpDownBrojTitula";
            this.numericUpDownBrojTitula.Size = new System.Drawing.Size(37, 20);
            this.numericUpDownBrojTitula.TabIndex = 15;
            // 
            // textBoxSajt
            // 
            this.textBoxSajt.Location = new System.Drawing.Point(137, 207);
            this.textBoxSajt.Name = "textBoxSajt";
            this.textBoxSajt.Size = new System.Drawing.Size(121, 20);
            this.textBoxSajt.TabIndex = 11;
            // 
            // textBoxGrad
            // 
            this.textBoxGrad.Location = new System.Drawing.Point(137, 172);
            this.textBoxGrad.Name = "textBoxGrad";
            this.textBoxGrad.Size = new System.Drawing.Size(121, 20);
            this.textBoxGrad.TabIndex = 9;
            // 
            // textBoxMenadzer
            // 
            this.textBoxMenadzer.Location = new System.Drawing.Point(137, 142);
            this.textBoxMenadzer.Name = "textBoxMenadzer";
            this.textBoxMenadzer.Size = new System.Drawing.Size(121, 20);
            this.textBoxMenadzer.TabIndex = 7;
            // 
            // textBoxTrener
            // 
            this.textBoxTrener.Location = new System.Drawing.Point(137, 76);
            this.textBoxTrener.Name = "textBoxTrener";
            this.textBoxTrener.Size = new System.Drawing.Size(121, 20);
            this.textBoxTrener.TabIndex = 3;
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(137, 46);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(121, 20);
            this.textBoxNaziv.TabIndex = 1;
            // 
            // labelBrojOsvijenihTitula
            // 
            this.labelBrojOsvijenihTitula.AutoSize = true;
            this.labelBrojOsvijenihTitula.Location = new System.Drawing.Point(-3, 272);
            this.labelBrojOsvijenihTitula.Name = "labelBrojOsvijenihTitula";
            this.labelBrojOsvijenihTitula.Size = new System.Drawing.Size(98, 13);
            this.labelBrojOsvijenihTitula.TabIndex = 14;
            this.labelBrojOsvijenihTitula.Text = "Broj osvojenih titula";
            // 
            // labelSponzor
            // 
            this.labelSponzor.AutoSize = true;
            this.labelSponzor.Location = new System.Drawing.Point(-3, 241);
            this.labelSponzor.Name = "labelSponzor";
            this.labelSponzor.Size = new System.Drawing.Size(46, 13);
            this.labelSponzor.TabIndex = 12;
            this.labelSponzor.Text = "Sponzor";
            // 
            // labelSajt
            // 
            this.labelSajt.AutoSize = true;
            this.labelSajt.Location = new System.Drawing.Point(-3, 207);
            this.labelSajt.Name = "labelSajt";
            this.labelSajt.Size = new System.Drawing.Size(25, 13);
            this.labelSajt.TabIndex = 10;
            this.labelSajt.Text = "Sajt";
            // 
            // labelGrad
            // 
            this.labelGrad.AutoSize = true;
            this.labelGrad.Location = new System.Drawing.Point(-3, 172);
            this.labelGrad.Name = "labelGrad";
            this.labelGrad.Size = new System.Drawing.Size(30, 13);
            this.labelGrad.TabIndex = 8;
            this.labelGrad.Text = "Grad";
            // 
            // labelMenadzer
            // 
            this.labelMenadzer.AutoSize = true;
            this.labelMenadzer.Location = new System.Drawing.Point(-3, 142);
            this.labelMenadzer.Name = "labelMenadzer";
            this.labelMenadzer.Size = new System.Drawing.Size(54, 13);
            this.labelMenadzer.TabIndex = 6;
            this.labelMenadzer.Text = "Menadzer";
            // 
            // labelDatumOsnivanja
            // 
            this.labelDatumOsnivanja.AutoSize = true;
            this.labelDatumOsnivanja.Location = new System.Drawing.Point(-3, 106);
            this.labelDatumOsnivanja.Name = "labelDatumOsnivanja";
            this.labelDatumOsnivanja.Size = new System.Drawing.Size(89, 13);
            this.labelDatumOsnivanja.TabIndex = 4;
            this.labelDatumOsnivanja.Text = "Godina osnivanja";
            // 
            // labelTrener
            // 
            this.labelTrener.AutoSize = true;
            this.labelTrener.Location = new System.Drawing.Point(-3, 76);
            this.labelTrener.Name = "labelTrener";
            this.labelTrener.Size = new System.Drawing.Size(38, 13);
            this.labelTrener.TabIndex = 2;
            this.labelTrener.Text = "Trener";
            // 
            // labelNaizv
            // 
            this.labelNaizv.AutoSize = true;
            this.labelNaizv.Location = new System.Drawing.Point(-3, 46);
            this.labelNaizv.Name = "labelNaizv";
            this.labelNaizv.Size = new System.Drawing.Size(34, 13);
            this.labelNaizv.TabIndex = 0;
            this.labelNaizv.Text = "Naziv";
            // 
            // KlubForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 388);
            this.Controls.Add(this.groupBoxKlub);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "KlubForm";
            this.Text = "KlubForm";
            this.Load += new System.EventHandler(this.KlubForm_Load);
            this.groupBoxKlub.ResumeLayout(false);
            this.groupBoxKlub.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojTitula)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxKlub;
        private System.Windows.Forms.Button buttonIzmeniKlub;
        private System.Windows.Forms.Button buttonKreirajKlub;
        private System.Windows.Forms.TextBox textBoxSponzor;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojTitula;
        private System.Windows.Forms.TextBox textBoxSajt;
        private System.Windows.Forms.TextBox textBoxGrad;
        private System.Windows.Forms.TextBox textBoxMenadzer;
        private System.Windows.Forms.TextBox textBoxTrener;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.Label labelBrojOsvijenihTitula;
        private System.Windows.Forms.Label labelSponzor;
        private System.Windows.Forms.Label labelSajt;
        private System.Windows.Forms.Label labelGrad;
        private System.Windows.Forms.Label labelMenadzer;
        private System.Windows.Forms.Label labelDatumOsnivanja;
        private System.Windows.Forms.Label labelTrener;
        private System.Windows.Forms.Label labelNaizv;
        private System.Windows.Forms.TextBox textBoxGodina;
    }
}