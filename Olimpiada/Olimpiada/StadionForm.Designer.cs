﻿namespace Olimpiada
{
    partial class StadionForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxStadion = new System.Windows.Forms.GroupBox();
            this.radioButtonkreirajStadion = new System.Windows.Forms.RadioButton();
            this.buttonDodaj = new System.Windows.Forms.Button();
            this.buttonIzmeni = new System.Windows.Forms.Button();
            this.textBoxGodIzgradnje = new System.Windows.Forms.TextBox();
            this.labelGodIzgradnje = new System.Windows.Forms.Label();
            this.comboBoxtakmicenje = new System.Windows.Forms.ComboBox();
            this.labelTakmicenje = new System.Windows.Forms.Label();
            this.textBoxKapacitet = new System.Windows.Forms.TextBox();
            this.textBoxGrad = new System.Windows.Forms.TextBox();
            this.textBoxLokacija = new System.Windows.Forms.TextBox();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.labelKapacitet = new System.Windows.Forms.Label();
            this.labelGrad = new System.Windows.Forms.Label();
            this.labelLokacija = new System.Windows.Forms.Label();
            this.labelIme = new System.Windows.Forms.Label();
            this.groupBoxStadion.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxStadion
            // 
            this.groupBoxStadion.Controls.Add(this.radioButtonkreirajStadion);
            this.groupBoxStadion.Controls.Add(this.buttonDodaj);
            this.groupBoxStadion.Controls.Add(this.buttonIzmeni);
            this.groupBoxStadion.Controls.Add(this.textBoxGodIzgradnje);
            this.groupBoxStadion.Controls.Add(this.labelGodIzgradnje);
            this.groupBoxStadion.Controls.Add(this.comboBoxtakmicenje);
            this.groupBoxStadion.Controls.Add(this.labelTakmicenje);
            this.groupBoxStadion.Controls.Add(this.textBoxKapacitet);
            this.groupBoxStadion.Controls.Add(this.textBoxGrad);
            this.groupBoxStadion.Controls.Add(this.textBoxLokacija);
            this.groupBoxStadion.Controls.Add(this.textBoxIme);
            this.groupBoxStadion.Controls.Add(this.labelKapacitet);
            this.groupBoxStadion.Controls.Add(this.labelGrad);
            this.groupBoxStadion.Controls.Add(this.labelLokacija);
            this.groupBoxStadion.Controls.Add(this.labelIme);
            this.groupBoxStadion.Location = new System.Drawing.Point(12, 12);
            this.groupBoxStadion.Name = "groupBoxStadion";
            this.groupBoxStadion.Size = new System.Drawing.Size(268, 361);
            this.groupBoxStadion.TabIndex = 30;
            this.groupBoxStadion.TabStop = false;
            this.groupBoxStadion.Text = "Stadion";
            // 
            // radioButtonkreirajStadion
            // 
            this.radioButtonkreirajStadion.AutoSize = true;
            this.radioButtonkreirajStadion.Location = new System.Drawing.Point(9, 73);
            this.radioButtonkreirajStadion.Name = "radioButtonkreirajStadion";
            this.radioButtonkreirajStadion.Size = new System.Drawing.Size(91, 17);
            this.radioButtonkreirajStadion.TabIndex = 26;
            this.radioButtonkreirajStadion.TabStop = true;
            this.radioButtonkreirajStadion.Text = "Kreiraj stadion";
            this.radioButtonkreirajStadion.UseVisualStyleBackColor = true;
            this.radioButtonkreirajStadion.CheckedChanged += new System.EventHandler(this.radioButtonkreirajStadion_CheckedChanged);
            // 
            // buttonDodaj
            // 
            this.buttonDodaj.Location = new System.Drawing.Point(7, 321);
            this.buttonDodaj.Name = "buttonDodaj";
            this.buttonDodaj.Size = new System.Drawing.Size(85, 23);
            this.buttonDodaj.TabIndex = 25;
            this.buttonDodaj.Text = "Dodaj stadion";
            this.buttonDodaj.UseVisualStyleBackColor = true;
            this.buttonDodaj.Click += new System.EventHandler(this.buttonDodaj_Click);
            // 
            // buttonIzmeni
            // 
            this.buttonIzmeni.Location = new System.Drawing.Point(171, 321);
            this.buttonIzmeni.Name = "buttonIzmeni";
            this.buttonIzmeni.Size = new System.Drawing.Size(86, 23);
            this.buttonIzmeni.TabIndex = 24;
            this.buttonIzmeni.Text = "Izmeni stadion";
            this.buttonIzmeni.UseVisualStyleBackColor = true;
            this.buttonIzmeni.Click += new System.EventHandler(this.buttonIzmeni_Click);
            // 
            // textBoxGodIzgradnje
            // 
            this.textBoxGodIzgradnje.Location = new System.Drawing.Point(127, 270);
            this.textBoxGodIzgradnje.Name = "textBoxGodIzgradnje";
            this.textBoxGodIzgradnje.Size = new System.Drawing.Size(130, 20);
            this.textBoxGodIzgradnje.TabIndex = 23;
            // 
            // labelGodIzgradnje
            // 
            this.labelGodIzgradnje.AutoSize = true;
            this.labelGodIzgradnje.Location = new System.Drawing.Point(6, 273);
            this.labelGodIzgradnje.Name = "labelGodIzgradnje";
            this.labelGodIzgradnje.Size = new System.Drawing.Size(89, 13);
            this.labelGodIzgradnje.TabIndex = 22;
            this.labelGodIzgradnje.Text = "Godina izgradnje:";
            // 
            // comboBoxtakmicenje
            // 
            this.comboBoxtakmicenje.FormattingEnabled = true;
            this.comboBoxtakmicenje.Location = new System.Drawing.Point(9, 37);
            this.comboBoxtakmicenje.Name = "comboBoxtakmicenje";
            this.comboBoxtakmicenje.Size = new System.Drawing.Size(157, 21);
            this.comboBoxtakmicenje.TabIndex = 20;
            this.comboBoxtakmicenje.SelectedIndexChanged += new System.EventHandler(this.comboBoxtakmicenje_SelectedIndexChanged);
            // 
            // labelTakmicenje
            // 
            this.labelTakmicenje.AutoSize = true;
            this.labelTakmicenje.Location = new System.Drawing.Point(5, 21);
            this.labelTakmicenje.Name = "labelTakmicenje";
            this.labelTakmicenje.Size = new System.Drawing.Size(231, 13);
            this.labelTakmicenje.TabIndex = 19;
            this.labelTakmicenje.Text = "Odaberite takmicenje za pojedinacnu disciplinu:";
            // 
            // textBoxKapacitet
            // 
            this.textBoxKapacitet.Location = new System.Drawing.Point(127, 231);
            this.textBoxKapacitet.Name = "textBoxKapacitet";
            this.textBoxKapacitet.Size = new System.Drawing.Size(130, 20);
            this.textBoxKapacitet.TabIndex = 17;
            // 
            // textBoxGrad
            // 
            this.textBoxGrad.Location = new System.Drawing.Point(127, 191);
            this.textBoxGrad.Name = "textBoxGrad";
            this.textBoxGrad.Size = new System.Drawing.Size(130, 20);
            this.textBoxGrad.TabIndex = 16;
            // 
            // textBoxLokacija
            // 
            this.textBoxLokacija.Location = new System.Drawing.Point(127, 151);
            this.textBoxLokacija.Name = "textBoxLokacija";
            this.textBoxLokacija.Size = new System.Drawing.Size(130, 20);
            this.textBoxLokacija.TabIndex = 15;
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(127, 116);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(130, 20);
            this.textBoxIme.TabIndex = 14;
            // 
            // labelKapacitet
            // 
            this.labelKapacitet.AutoSize = true;
            this.labelKapacitet.Location = new System.Drawing.Point(6, 234);
            this.labelKapacitet.Name = "labelKapacitet";
            this.labelKapacitet.Size = new System.Drawing.Size(55, 13);
            this.labelKapacitet.TabIndex = 13;
            this.labelKapacitet.Text = "Kapacitet:";
            // 
            // labelGrad
            // 
            this.labelGrad.AutoSize = true;
            this.labelGrad.Location = new System.Drawing.Point(6, 194);
            this.labelGrad.Name = "labelGrad";
            this.labelGrad.Size = new System.Drawing.Size(33, 13);
            this.labelGrad.TabIndex = 12;
            this.labelGrad.Text = "Grad:";
            // 
            // labelLokacija
            // 
            this.labelLokacija.AutoSize = true;
            this.labelLokacija.Location = new System.Drawing.Point(6, 154);
            this.labelLokacija.Name = "labelLokacija";
            this.labelLokacija.Size = new System.Drawing.Size(50, 13);
            this.labelLokacija.TabIndex = 11;
            this.labelLokacija.Text = "Lokacija:";
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(6, 119);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(27, 13);
            this.labelIme.TabIndex = 10;
            this.labelIme.Text = "Ime:";
            // 
            // StadionForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(337, 414);
            this.Controls.Add(this.groupBoxStadion);
            this.Name = "StadionForm";
            this.Text = "StadionForm";
            this.Load += new System.EventHandler(this.StadionForm_Load);
            this.groupBoxStadion.ResumeLayout(false);
            this.groupBoxStadion.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxStadion;
        private System.Windows.Forms.RadioButton radioButtonkreirajStadion;
        private System.Windows.Forms.Button buttonDodaj;
        private System.Windows.Forms.Button buttonIzmeni;
        private System.Windows.Forms.TextBox textBoxGodIzgradnje;
        private System.Windows.Forms.Label labelGodIzgradnje;
        private System.Windows.Forms.ComboBox comboBoxtakmicenje;
        private System.Windows.Forms.Label labelTakmicenje;
        private System.Windows.Forms.TextBox textBoxKapacitet;
        private System.Windows.Forms.TextBox textBoxGrad;
        private System.Windows.Forms.TextBox textBoxLokacija;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.Label labelKapacitet;
        private System.Windows.Forms.Label labelGrad;
        private System.Windows.Forms.Label labelLokacija;
        private System.Windows.Forms.Label labelIme;
    }
}