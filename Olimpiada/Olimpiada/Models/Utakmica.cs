﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Utakmica
    {
        public int ID { get; set; }
        public String Vreme { get; set; }

        public String Rezultat { get; set; }

        public String Datum { get; set; }

        public String Pobednik { get; set; }

        public Klub Gost { get; set; }

        public Klub Domacin { get; set; }

        public Sudija Sudija { get; set; }

        public Kolo Kolo { get; set; }

        public Kolo GetKolo()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:PRIPADA]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Kolo = ((IRawGraphClient)client).ExecuteGetCypherResults<Kolo>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Kolo;
        }

        public Sudija GetSudija()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:SUDI]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Sudija = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Sudija;
        }

        public Klub GetDomacin()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:DOMACIN]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Domacin = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Domacin;
        }

        public Klub GetGost()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:GOST]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Gost = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Gost;
        }

        public static Utakmica Create(GraphClient client, String vreme, String rezultat, String datum, String pobednik)
        {
            int ID = getMaxId(client);
            Utakmica utakmica = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Utakmica {ID: '" + ID + "', Vreme: '" + vreme + "', Rezultat: '" + rezultat + "', Datum: '" + datum + "', " +
                    " Pobednik: '" + pobednik + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                utakmica = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).First();
            }
            catch (Exception ex) { }

            return utakmica;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Utakmica {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Utakmica Modify(GraphClient client, String vreme, String rezultat, String datum, String pobednik)
        {
            Utakmica utakmica = null;
            try
            {
                var query = new CypherQuery("match (n:Utakmica) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Vreme: '" + vreme + "', Rezultat: '" + rezultat + "', Datum: '" + datum + "', " +
                    " Pobednik: '" + pobednik + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                utakmica = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).First();
            }
            catch (Exception ex) { }

            return utakmica;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateUtakmicaSudija(GraphClient client, Utakmica utakmica, Sudija sudija)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija), (b:Utakmica) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "CREATE (n)-[r:SUDI]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteSudijaUtakmica(GraphClient client, Utakmica utakmica, Sudija sudija)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija)-[r:SUDI]->(b:Utakmica) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateUtakmicaDomacin(GraphClient client, Utakmica utakmica, Klub domacin)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Klub), (b:Utakmica) WHERE n.ID= '" + domacin.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "CREATE (n)-[r:DOMACIN]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteUtakmicaDomacin(GraphClient client, Utakmica utakmica, Klub domacin)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Klub)-[r:DOMACIN]->(b:Utakmica) WHERE n.ID= '" + domacin.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateUtakmicaGost(GraphClient client, Utakmica utakmica, Klub gost)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Klub), (b:Utakmica) WHERE n.ID= '" + gost.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "CREATE (n)-[r:GOST]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteUtakmicaGost(GraphClient client, Utakmica utakmica, Klub gost)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Klub)-[r:GOST]->(b:Utakmica) WHERE n.ID= '" + gost.ID + "' AND b.ID = '" + utakmica.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }



    }
}
