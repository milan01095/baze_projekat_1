﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Sportista
    {
        public int ID { get; set; }
        public String Ime { get; set; }

        public String Prezime { get; set; }  

        public String Datum_rodjenja { get; set; }

        public int Broj_dresa { get; set; }

        public String Broj_tel { get; set; }

        public String Drzava { get; set; }

        public String Visina { get; set; }

        public String Tezina { get; set; }

        public Klub Klub{ get; set; }

        public List<Takmici> Takmicenja { get; set; }

        public Klub GetKlub()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:UCESTVUJE]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Klub = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Klub;
        }

        public List<Takmici> GetTakmicenja()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:TAKMICI]->(d) where n.ID =~ '" + ID + "' return r",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Takmicenja = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmici>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicenja;
        }

        public static Sportista Create(GraphClient client, String ime, String prezime, String datum_rodjenja, String broj_dresa, String broj_tel, String drava, String visina, String tezina)
        {
            int ID = getMaxId(client);
            Sportista sportista = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Sportista {ID: '" + ID + "', Ime: '" + ime + "', Prezime: '" + prezime + "', Datum_rodjenja: '"+datum_rodjenja+"', " +
                    " Broj_dresa: '"+broj_dresa+"', Broj_tel: '"+broj_tel+"', Drzava: '"+drava+"', Visina: '"+visina+"', Tezina: '"+tezina+"'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sportista = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).First();
            }
            catch (Exception ex) { }

            return sportista;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Sportista {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Sportista Modify(GraphClient client, String ime, String prezime, String datum_rodjenja, String broj_dresa, String broj_tel, String drava, String visina, String tezina)
        {
            Sportista sportista = null;
            try
            {
                var query = new CypherQuery("match (n:Sportista) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Ime: '" + ime + "', Prezime: '" + prezime + "', Datum_rodjenja: '" + datum_rodjenja + "'," +
                    " Broj_dresa: '" + broj_dresa + "', Broj_tel: '" + broj_tel + "', Drzava: '" + drava + "', Visina: '" + visina + "', Tezina: '"+tezina+"'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sportista = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).First();
            }
            catch (Exception ex) { }

            return sportista;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }
    }
}
