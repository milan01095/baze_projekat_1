﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Disciplina
    {
        public int ID { get; set; }
        public String Naziv { get; set; }

        public String Pol { get; set; }

        public String Tip { get; set; }

        public List<Takmicenje> Takmicenja { get; set; }
       
        public List<Takmicenje> GetTakmicenja()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:POSEDUJE]->(d) where n.ID =~'" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Takmicenja = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicenja;
        }

        public static Disciplina Create(GraphClient client, String naziv, String pol, String tip)
        {
            int ID = getMaxId(client);
            Disciplina disciplina = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Disciplina {ID: '" + ID + "', Naziv: '" + naziv + "', Pol: '" + pol + "', Tip: '"+tip+"'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                disciplina = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query).First();
            }
            catch (Exception ex) { }

            return disciplina;
        }

        public bool Delete(GraphClient client)
        {     
            try
            {
                var query = new CypherQuery("MATCH (n:Disciplina {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Disciplina Modify(GraphClient client, String naziv, String pol, String tip)
        {
            Disciplina disciplina = null;
            try
            {
                var query = new CypherQuery("match (n:Disciplina) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Naziv: '" + naziv + "', Pol: '" + pol + "' Tip: '" + tip + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                disciplina = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query).First();
            }
            catch (Exception ex) { }

            return disciplina;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateDisciplinaTakmicenje(GraphClient client, Disciplina disciplina, Takmicenje takmicenje)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Disciplina), (b:Takmicenje) WHERE n.ID= '" + disciplina.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "CREATE (n)-[r:POSEDUJE]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeletSportDisciplina(GraphClient client, Disciplina disciplina, Takmicenje takmicenje)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Disciplina)-[r:POSEDUJE]->(b:Takmicenje) WHERE n.ID= '" + disciplina.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
