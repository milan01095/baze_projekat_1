﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Takmicenje
    {
        public int ID { get; set; }
       public String Naziv { get; set; }
         
        public String Zemlja { get; set; }

        public String Datum_osnivanja { get; set; }

        public String Domaci_kup { get; set; }

        public String Sponzor { get; set; }

        public List<Klub> Klubovi { get; set; }

        public Tabela Tabela { get; set; }

        public Disciplina Disciplina { get; set; }

        public List<Takmici> Takmicari { get; set; }

        public Sudija Sudija { get; set; }

        public Stadion Stadion { get; set; }

        public List<Klub> GetKlubovi()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:SADRZI]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Klubovi = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Klubovi;
        }

        public Tabela GetTabela()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n = node(*) match (n)-[r:REZULTATI]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Tabela = ((IRawGraphClient)client).ExecuteGetCypherResults<Tabela>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Tabela;
        }

        public Disciplina GetDisciplina()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n = node(*) match (n)-[r:POSEDUJE]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Disciplina = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Disciplina;
        }

        public List<Takmici> GetTakmicari()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n = node(*) match (n)-[r:TAKMICI]-(d) where n.ID =~ '" + ID + "' return r",
                  new Dictionary<string, object>(), CypherResultMode.Set);
             
                Takmicari = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmici>(query).OrderBy(o => o.Mesto).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicari;
        }

        public Sudija GetSudija()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n = node(*) match (n)-[r:KOMANDUJE]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Sudija = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Sudija;
        }

        public Stadion GetStadion()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n = node(*) match (n)-[r:ODRZAVA]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Stadion;
        }

        public static Takmicenje Create(GraphClient client, String naziv, String zemlja, String datum_osnivanja, String domaci_kup, String sponzor)
        {
            int ID = getMaxId(client);
            Takmicenje takmicenje = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Takmicenje {ID: '" + ID + "', Naziv: '" + naziv + "', Zemlja: '" + zemlja + "', Datum_osnivanja: '" + datum_osnivanja + "', " +
                    " Domaci_kup: '" + domaci_kup + "', Sponzor: '" + sponzor + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                takmicenje = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).First();
            }
            catch (Exception ex) { }

            return takmicenje;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Takmicenje {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Takmicenje Modify(GraphClient client, String naziv, String zemlja, String datum_osnivanja, String domaci_kup, String sponzor)
        {
            Takmicenje takmicenje = null;
            try
            {
                var query = new CypherQuery("match (n:Takmicenje) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Naziv: '" + naziv + "', Zemlja: '" + zemlja + "', Datum_osnivanja: '" + datum_osnivanja + "', " +
                    " Domaci_kup: '" + domaci_kup + "', Sponzor: '" + sponzor + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                takmicenje = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).First();
            }
            catch (Exception ex) { }

            return takmicenje;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateTakmicenjeTabela(GraphClient client, Takmicenje takmicenje, Tabela tabela)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Takmicenje), (b:Tabela) WHERE n.ID= '" + takmicenje.ID + "' AND b.ID = '" + tabela.ID + "' " +
                    "CREATE (n)-[r:REZULTATI]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTakmicenjeTabela(GraphClient client, Takmicenje takmicenje, Tabela tabela)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Takmicenje)-[r:REZULTATI]->(b:Tabela) WHERE n.ID= '" + takmicenje.ID + "' AND b.ID = '" + tabela.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateTakmicenjeKlub(GraphClient client, Takmicenje takmicenje, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Takmicenje), (b:Klub) WHERE n.ID= '" + takmicenje.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "CREATE (n)-[r:SADRZI]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTakmicenjeKlub(GraphClient client, Takmicenje takmicenje, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Takmicenje)-[r:SADRZI]->(b:Klub) WHERE n.ID= '" + takmicenje.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateTakmicenjeStadion(GraphClient client, Takmicenje takmicenje, Stadion stadion)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Stadion), (b:Takmicenje) WHERE n.ID= '" + stadion.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "CREATE (n)-[r:ODRZAVA]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTakmicenjeStadion(GraphClient client, Takmicenje takmicenje, Stadion stadion)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Stadion)-[r:ODRZAVA]->(b:Takmicenje) WHERE n.ID= '" + stadion.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateTakmicenjeSudija(GraphClient client, Takmicenje takmicenje, Sudija sudija)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija), (b:Takmicenje) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "CREATE (n)-[r:KOMANDUJE]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTakmicenjeSudija(GraphClient client, Takmicenje takmicenje, Sudija sudija)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Takmicenje)-[r:KOMANDUJE]->(b:Sudija) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateTakmicenjeSportista(GraphClient client, Takmicenje takmicenje, Sportista sportista, int Mesto, String rezultat)
        {
            try
            {
                //(`38`)-[:`TAKMICI` { Mesto: "1",Rezultat: "10.50",IDSportista: "14",IDTakmicenja: "22"}]->(`43`),
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sportista), (b:Takmicenje) WHERE n.ID= '" + sportista.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "CREATE (n)-[r:TAKMICI { Mesto: '"+Mesto+"',Rezultat: '"+rezultat+"',IDSportista: '"+sportista.ID+"',IDTakmicenja: '"+takmicenje.ID+"'}]->(b) return r",
                    new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTakmicenjeSportista(GraphClient client, Takmicenje takmicenje, Sportista sportista)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sportista)-[r:TAKMICI]->(b:Takmicenje) WHERE n.ID= '" + sportista.ID + "' AND b.ID = '" + takmicenje.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
