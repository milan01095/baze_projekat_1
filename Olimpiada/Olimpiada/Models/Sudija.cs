﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Sudija
    {
        public String ID { get; set; }
        public String Ime { get; set; }

        public String Prezime { get; set; }

        public String Datum_rodjenja { get; set; }

        public String Godina_sudjenja { get; set; }

        public String Drzava { get; set; }

        public String Grad { get; set; }

        public Sport Sport { get; set; }

        public List<Utakmica> Utakmice { get; set; }

        public List<Takmicenje> Takmicenja { get; set; }

        public Sport GetSport()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:`IZ SPORTA`]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Sport = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Sport;
        }
        
        public List<Utakmica> GetUtakmice()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:SUDI]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Utakmice = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Utakmice;
        }

        public List<Takmicenje> GetTakmicenja()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r1:KOMANDUJE]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Takmicenja = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicenja;
        }

        public static Sudija Create(GraphClient client, String ime, String prezime, String datum_rodjenja, String godina_sudjenja, String drzava, String grad)
        {
            int ID = getMaxId(client);
            Sudija sudija = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Sudija {ID: '" + ID + "', Ime: '" + ime + "', Prezime: '" + prezime + "', Datum_rodjenja: '" + datum_rodjenja + "', " +
                    " Godina_sudjenja: '" + godina_sudjenja + "', Grad: '" + grad + "', Drzava: '" + drzava + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sudija = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).First();
            }
            catch (Exception ex) { }

            return sudija;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Sudija {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Sudija Modify(GraphClient client, String ime, String prezime, String datum_rodjenja, String godina_sudjenja, String drzava, String grad)
        {
            Sudija sudija = null;
            try
            {
                var query = new CypherQuery("match (n:Sudija) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Ime: '" + ime + "', Prezime: '" + prezime + "', Datum_rodjenja: '" + datum_rodjenja + "', " +
                    " Godina_sudjenja: '" + godina_sudjenja + "', Grad: '" + grad + "', Drzava: '" + drzava + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sudija = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).First();
            }
            catch (Exception ex) { }

            return sudija;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateSudijaSport(GraphClient client, Sudija sudija, Sport sport)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija), (b:Sport) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + sport.ID + "' " +
                    "CREATE (n)-[r:`IZ SPORTA`]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteSudijaSport(GraphClient client, Sudija sudija, Sport sport)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija)-[r:`IZ SPORTA`]->(b:Sport) WHERE n.ID= '" + sudija.ID + "' AND b.ID = '" + sport.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
