﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Kolo
    {
        public int ID { get; set; }
        public int Redni_broj { get; set; }

        public String Datum_pocetka { get; set; }

        public String Datum_zavrsetka { get; set; }

        public List<Utakmica> Utakmice { get; set; }

        public List<Tabela> Tabela { get; set; }

        public List<Utakmica> GetUtakmice()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:PRIPADA]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Utakmice = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Utakmice;
        }

        public List<Tabela> GetTabela()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:SADRZI]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Tabela = ((IRawGraphClient)client).ExecuteGetCypherResults<Tabela>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Tabela;
        }

        public static Kolo Create(GraphClient client, String redni_broj, String datum_pocetka, String datum_zavrsetka)
        {
            int ID = getMaxId(client);
            Kolo kolo = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Kolo {ID: '" + ID + "', Redni_broj: '" + redni_broj + "', Datum_pocetka: '" + datum_pocetka + "', Datum_zavrsetka: '"+datum_zavrsetka+"'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                kolo = ((IRawGraphClient)client).ExecuteGetCypherResults<Kolo>(query).First();
            }
            catch (Exception ex) { }

            return kolo;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Kolo {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Kolo Modify(GraphClient client, String redni_broj, String datum_pocetka, String datum_zavrsetka)
        {
            Kolo kolo = null;
            try
            {
                var query = new CypherQuery("match (n:Admin) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Redni_broj: '" + redni_broj + "', Datum_pocetka: '" + datum_pocetka + "', Datum_zavrsetka: '" + datum_zavrsetka + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                kolo = ((IRawGraphClient)client).ExecuteGetCypherResults<Kolo>(query).First();
            }
            catch (Exception ex) { }

            return kolo;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateKoloUtakmica(GraphClient client, Kolo kolo, Utakmica utakmica)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Utakmica), (b:Kolo) WHERE n.ID= '" + utakmica.ID + "' AND b.ID = '" + kolo.ID + "' " +
                    "CREATE (n)-[r:PRIPADA]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteKoloUtakmica(GraphClient client, Kolo kolo, Utakmica utakmica)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Utakmica)-[r:PRIPADA]->(b:Kolo) WHERE n.ID= '" + utakmica.ID + "' AND b.ID = '" + kolo.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
