﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Neo4jClient;
using Neo4jClient.Cypher;

namespace Olimpiada.Models
{
    public class Sport
    {     
        public int ID { get; set; }
        public String Naziv { get; set; }

        public String Sportska_asociacija{ get; set; }

        public List<Disciplina> Discipline { get; set; }

        public List<Sudija> Sudije { get; set; }

        public List<Disciplina> GetDiscipline()
        {
           GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:DISCIPLINE]->(d) where n.ID =~ '"+ID+"' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Discipline = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query).ToList();
                client.Dispose();
            }
            catch(Exception ex) { }

            return Discipline;
        }

        public List<Sudija> GetSudije()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)<-[r:`IZ SPORTA`]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Sudije = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Sudije;
        }

        public static Sport Create(GraphClient client, String naziv, String sportska_asociacija)
        {
            int ID = getMaxId(client);
            Sport sport = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Sport {ID: '" + ID + "', Naziv: '" + naziv + "', Sportska_asociacija: '" + sportska_asociacija + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sport = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).First();
            }
            catch (Exception ex) { }

            return sport;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Sport {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Sport Modify(GraphClient client, String naziv, String sportska_asociacija)
        {
            Sport sport = null;
            try
            {
                var query = new CypherQuery("match (n:Sport) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Naziv: '" + naziv + "', Sportska_asociacija: '" + sportska_asociacija + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                sport = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).First();
            }
            catch (Exception ex) { }

            return sport;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateSportDisciplina(GraphClient client, Sport sport, Disciplina disciplina)
        {
            try
            {               
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sport), (b:Disciplina) WHERE n.ID= '" + sport.ID + "' AND b.ID = '" + disciplina.ID + "' " +
                    "CREATE (n)-[r:DISCIPLINE]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
       
        public static bool DeletSportDisciplina(GraphClient client, Sport sport, Disciplina disciplina)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sport)-[r:DISCIPLINE]->(b:Disciplina) WHERE n.ID= '" + sport.ID + "' AND b.ID = '" + disciplina.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
