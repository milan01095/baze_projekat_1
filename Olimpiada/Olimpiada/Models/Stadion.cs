﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Stadion
    {
        public int ID { get; set; }
        public String Ime_stadiona { get; set; }

        public String Grad { get; set; }

        public String Lokacija { get; set; }

        public String Kapacitet { get; set; }

        public String Godina_izgradnje { get; set; }

        public Klub Klub { get; set; }

        public List<Utakmica> Utakmice { get; set; }

        public List<Takmicenje> TakmicenjaPojedinacna { get; set; }

        public Klub GetKlub()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:ODIGRAVA]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Klub = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Klub;
        }

        public List<Utakmica>  GetUtakmice()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r1:ODIGRAVA]->(d)-[r2:DOMACIN]->(p) where n.ID =~ '" + ID + "' return p",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Utakmice = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Utakmice;
        }

        public List<Takmicenje> GetTakmicenja()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r1:ODRZAVA]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                TakmicenjaPojedinacna = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();

                client.Dispose();
            }
            catch (Exception ex) { }

            return TakmicenjaPojedinacna;
        }

        public static Stadion Create(GraphClient client, String ime_stadiona, String grad, String lokacija, String kapacitet, String godina_izgradnje)
        {
            int ID = getMaxId(client);
            Stadion stadion = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Stadion {ID: '" + ID + "', Ime_stadiona: '" + ime_stadiona + "', Grad: '" + grad + "', Lokacija: '" + lokacija + "', " +
                    " Kapacitet: '" + kapacitet + "', Godina_izgradnje: '" + godina_izgradnje + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).First();
            }
            catch (Exception ex) { }

            return stadion;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Stadion {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Stadion Modify(GraphClient client, String ime_stadiona, String grad, String lokacija, String kapacitet, String godina_izgradnje)
        {
            Stadion stadion = null;
            try
            {
                var query = new CypherQuery("match (n:Stadion) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Ime_stadiona: '" + ime_stadiona + "', Grad: '" + grad + "', Lokacija: '" + lokacija + "', " +
                    " Kapacitet: '" + kapacitet + "', Godina_izgradnje: '" + godina_izgradnje + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).First();
            }
            catch (Exception ex) { }

            return stadion;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

    }
}
