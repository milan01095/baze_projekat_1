﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Takmici
    {
        public int IDSportista { get; set; }

        public int IDTakmicenje { get; set; }

        public string Rezultat { get; set; }

        public int Mesto { get; set; }

        public Sportista Sportista { get; set; }

        public Takmicenje Takmicenje { get; set; }

        public Sportista GetSportista()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("match (n) where n.ID =~ '" + IDSportista + "' return n",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Sportista = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Sportista;
        }

        public Takmicenje GetTakmicenje()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("match (n) where n.ID =~ '" + IDTakmicenje + "' return n",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Takmicenje = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicenje;
        }
      
    }
}
