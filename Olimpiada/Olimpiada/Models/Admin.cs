﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    class Admin
    {
        public int ID { get; set; }

        public String Korisnicko { get; set; }

        public String Sifra { get; set; }

        public static Admin Create(GraphClient client, String korisnicko, String sifa)
        {         
            int ID = getMaxId(client);
            Admin admin= null;
            try
            {
                var query = new CypherQuery("CREATE (n:Admin {ID: '" + ID + "', Korisnicko: '" + korisnicko + "', Sifra: '" + sifa + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                admin = ((IRawGraphClient)client).ExecuteGetCypherResults<Admin>(query).First();             
            }
            catch (Exception ex) { }

            return admin;
        }

        public bool Delete(GraphClient client)
        {          
            try
            {
                var query = new CypherQuery("MATCH (n:Admin {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

             ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public  Admin Modify(GraphClient client, String korisnicko, String sifra)
        {           
            Admin admin = null;
            try
            {
                var query = new CypherQuery("match (n:Admin) where n.ID = '"+ID+"' set n = {ID: '" + ID + "', Korisnicko: '" + korisnicko + "', Sifra: '" + sifra + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                admin = ((IRawGraphClient)client).ExecuteGetCypherResults<Admin>(query).First();
            }
            catch (Exception ex) { }

            return admin;
        }

        public static int getMaxId(GraphClient client)
        {         
            int maxId=-1;
            try
            {             
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",
                
                    new Dictionary<string, object>(), CypherResultMode.Set);

                 maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();                          
            }
            catch (Exception ex) { }

            return maxId + 1;
        }      
    }
}
