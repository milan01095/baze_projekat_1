﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Tabela
    {
        public int ID { get; set; }
        public int Trenutno_kolo { get; set; }

        public int Broj_kola { get; set; }

        public List<Takmicenje> Takmicenje { get; set; }

        public List<Kolo> Kola { get; set; }

        public List<Takmicenje> GetTakmicenje()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:REZULTATI]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Takmicenje = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Takmicenje;
        }

        public List<Kolo> GetKola()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:IGRA]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Kola = ((IRawGraphClient)client).ExecuteGetCypherResults<Kolo>(query).OrderBy(o =>o.Redni_broj).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Kola;
        }

        public static Tabela Create(GraphClient client, String trenutno_kolo, String broj_kola)
        {
            int ID = getMaxId(client);
            Tabela tabela = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Tabela {ID: '" + ID + "', Trenutno_kolo: '" + trenutno_kolo + "', Broj_kola: '" + broj_kola + "'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                tabela = ((IRawGraphClient)client).ExecuteGetCypherResults<Tabela>(query).First();
            }
            catch (Exception ex) { }

            return tabela;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Tabela {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Tabela Modify(GraphClient client, String trenutno_kolo, String broj_kola)
        {
            Tabela tabela = null;
            try
            {
                var query = new CypherQuery("match (n:Tabela) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Trenutno_kolo: '" + trenutno_kolo + "', Broj_kola: '" + broj_kola + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                tabela = ((IRawGraphClient)client).ExecuteGetCypherResults<Tabela>(query).First();
            }
            catch (Exception ex) { }

            return tabela;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateTabelaKolo(GraphClient client, Tabela tabela, Kolo kolo)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Tabela), (b:Kolo) WHERE n.ID= '" + tabela.ID + "' AND b.ID = '" + kolo.ID + "' " +
                    "CREATE (n)-[r:IGRA]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteTabelaKolo(GraphClient client, Tabela tabela, Kolo kolo)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Tabela)-[r:IGRA]->(b:Kolo) WHERE n.ID= '" + tabela.ID + "' AND b.ID = '" + kolo.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }
    }
}
