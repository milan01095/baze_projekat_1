﻿using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Olimpiada.Models
{
    public class Klub
    {
       public int ID { get; set; }
       public String Naziv { get; set; }

       public String Datum_osnivanja { get; set; }

       public String Grad { get; set; }

       public String Menadzer { get; set; }

        public String Trener { get; set; }

        public int Broj_titula { get; set; }

        public String Sajt { get; set; }

        public String Sponzor { get; set; }

        public List<Sportista> Igraci { get; set; }

        public Stadion Stadion { get; set; }

        public List<Utakmica> Utakmice { get; set; }

        public List<Sportista> GetIgaci()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:UCESTVUJE]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Igraci = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).ToList();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Igraci;
        }

        public Stadion GetStadion()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query = new CypherQuery("start n=node(*) match (n)-[r:ODIGRAVA]-(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                Stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).First();
                client.Dispose();
            }
            catch (Exception ex) { }

            return Stadion;
        }

        public List<Utakmica> GetUtakmice()
        {
            GraphClient client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");

            try
            {
                client.Connect();
                var query1 = new CypherQuery("start n=node(*) match (n)-[r:DOMACIN]->(d) where n.ID =~ '" + ID + "' return d",
                  new Dictionary<string, object>(), CypherResultMode.Set);

                var query2 = new CypherQuery("start n=node(*) match (n)-[r:GOST]->(d) where n.ID =~ '" + ID + "' return d",
                 new Dictionary<string, object>(), CypherResultMode.Set);


                Utakmice = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query1).OrderBy(o=>o.ID).ToList();
                List<Utakmica> utakmicee = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query2).OrderBy(o=>o.ID).ToList();
                if(utakmicee != null)
                {
                    for (int i = 0; i < utakmicee.Count; i++)
                        Utakmice.Add(utakmicee[i]);                  
                }
                client.Dispose();
            }
            catch (Exception ex) { }

            return Utakmice;
        }

        public static Klub Create(GraphClient client, String naziv, String datum_osnivanje, String grad, String menadzer, String trener, String broj_titula, String sajt, String sponzor)
        {
            int ID = getMaxId(client);
            Klub klub = null;
            try
            {
                var query = new CypherQuery("CREATE (n:Klub {ID: '" + ID + "', Naziv: '" + naziv + "', Datum_osnivanja: '" + datum_osnivanje + "', Grad: '" +grad+
                    "', Menadzer:'"+menadzer+"', Trener: '"+trener+"', Broj_titula: '"+broj_titula+"', Sajt: '"+sajt+"', Sponzor: '"+sponzor+"'}) return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                klub = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
            }
            catch (Exception ex) { }

            return klub;
        }

        public bool Delete(GraphClient client)
        {
            try
            {
                var query = new CypherQuery("MATCH (n:Admin {ID: '" + ID + "'}) DETACH DELETE n ",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public Klub Modify(GraphClient client, String naziv, String datum_osnivanje, String grad, String menadzer, String trener, String broj_titula, String sajt, String sponzor)
        {
            Klub klub = null;
            try
            {
                var query = new CypherQuery("match (n:Klub) where n.ID = '" + ID + "' set n = {ID: '" + ID + "', Naziv: '" + naziv + "', Datum_osnivanja: '" + datum_osnivanje + "', Grad: '" + grad +
                    "', Menadzer:'" + menadzer + "', Trener: '" + trener + "', Broj_titula: '" + broj_titula + "', Sajt: '" + sajt + "', Sponzor: '" + sponzor + "'} return n",
                    new Dictionary<string, object>(), CypherResultMode.Set);

                klub = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(query).First();
            }
            catch (Exception ex) { }

            return klub;
        }

        public static int getMaxId(GraphClient client)
        {
            int maxId = -1;
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*)  return n.ID",

                    new Dictionary<string, object>(), CypherResultMode.Set);

                maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).OrderBy(p => p).Last();
            }
            catch (Exception ex) { }

            return maxId + 1;
        }

        public static bool CreateSportistaKlub(GraphClient client, Sportista sportista, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sportista), (b:Klub) WHERE n.ID= '" + sportista.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "CREATE (n)-[r:UCESTVUJE]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeletSportistaKlub(GraphClient client, Sportista sportista, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sportista)-[r:UCESTVUJE]->(b:Klub) WHERE n.ID= '" + sportista.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool CreateStadionKlub(GraphClient client, Stadion stadion, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Stadion), (b:Klub) WHERE n.ID= '" + stadion.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "CREATE (n)-[r:ODIGRAVA]->(b) return r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

        public static bool DeleteStadionKlub(GraphClient client, Stadion stadion, Klub klub)
        {
            try
            {
                var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Stadion)-[r:ODIGRAVA]->(b:Klub) WHERE n.ID= '" + stadion.ID + "' AND b.ID = '" + klub.ID + "' " +
                    "DELETE r", new Dictionary<string, object>(), CypherResultMode.Projection);

                ((IRawGraphClient)client).ExecuteCypher(query);
            }
            catch (Exception ex) { return false; }

            return true;
        }

    }
}
