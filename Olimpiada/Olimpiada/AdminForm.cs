﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class AdminForm : Form
    {
        private GraphClient client;

        List<Admin> administratori = null;
        List<Sport> sports;
        List<Disciplina> discipline= null; 
        List<Takmicenje> takmicenja = null;
        List<Utakmica> utakmice = null;
        List<Sportista> sportista = null;
        List<Klub> klubovi = null;
        List<Kolo> kola = null;
        Klub selektovanKlub = null;
        Sport selektovanSport = null;
        Disciplina selektovanaDisciplina = null;
        Sportista selektovanSportista = null;
        Sudija selektovanSudija = null;
        Takmicenje selektovanoTakmicenje;
        Tabela selektovanaTabela;
        Kolo selektovanoKolo;
        Stadion selektovanStadion;
        Utakmica selektovanaUtakmica;
       
        public AdminForm()
        {
            InitializeComponent();

        }

        private void AdminForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            var query = new Neo4jClient.Cypher.CypherQuery(" match(n:Admin) return n",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);

            administratori = ((IRawGraphClient)client).ExecuteGetCypherResults<Admin>(query).ToList();
            foreach (Admin admin in administratori)
            {
                listBoxAdministratorskiNalozi.Items.Add(admin.Korisnicko);
            }

            comboBoxOdaberiteSport.Text = "--Izaberite sport--";
            comboBoxOdaberiteDisciplinu.Text = "--Izaberite disciplinu--";
            comboBoxOdaberiteSportistu.Text = "--Izaberite sportistu--";
            comboBoxKlub.Text = "--Izaberite klub--";
            comboBoxOdaberiteSudiju.Text = "--Izaberite sudiju--";
            comboBoxOdaberiteStadion.Text = "--Izaberite stadion--";
            comboBox2.Text = "--Izaberite utakmicu--";
            comboBoxOdaberiTakmicenje.Text = "--Izaberite takmicenje--";
            comboBoxKola.Text = "--Izaberite kolo--";

            popuniSportove();
        }





        #region Admin i korisnik
        private void buttonDodajPrivilegije_Click(object sender, EventArgs e)
        {
            if ((!textBoxUsername.Equals("")) && (!textBoxPassword.Equals("")))
            {
                Admin a = Admin.Create(client, textBoxUsername.Text, textBoxPassword.Text);
                listBoxAdministratorskiNalozi.Items.Add(a.Korisnicko);
            }
        }

        private void buttonObrisiNalog_Click(object sender, EventArgs e)
        {
            if (listBoxAdministratorskiNalozi.SelectedIndex != -1)
            {
                administratori[listBoxAdministratorskiNalozi.SelectedIndex].Delete(client);
                listBoxAdministratorskiNalozi.Items.RemoveAt(listBoxAdministratorskiNalozi.SelectedIndex);
            }
        }

        #endregion

        #region Sport
        private void buttonObrisiSport_Click(object sender, EventArgs e)
        {
            if (selektovanSport != null)
            {
                int i = 0;
                int indeks = 0;
                foreach (Sport sp in sports)
                {
                    if (sp.ID == selektovanSport.ID)
                        i = indeks;
                    indeks++;
                }

                selektovanSport.Delete(client);
                selektovanSport = null;
                comboBoxOdaberiteSport.Items.RemoveAt(i);
                comboBoxOdaberiteSport.SelectedIndex = -1;
            }
        }

        private void buttonKreirajSport_Click(object sender, EventArgs e)
        {
            SportForm sp = new SportForm();
            sp.ShowDialog();
        }
#endregion
       
        #region Disciplina
        private void buttonKreirajDisciplinu_Click(object sender, EventArgs e)
        {
            if (comboBoxOdaberiteSport.SelectedIndex != -1)
            {
                DisciplinaForm df = new DisciplinaForm(selektovanSport);
                df.ShowDialog();
            }
        }

        private void buttonObrisiDisciplinu_Click(object sender, EventArgs e)
        {
            if(selektovanaDisciplina != null)
            {
                int i = 0;
                int indeks = 0;
                foreach (Disciplina sp in discipline)
                {
                    if (sp.ID == selektovanaDisciplina.ID)
                        i = indeks;
                    indeks++;
                }

                selektovanaDisciplina.Delete(client);
                selektovanaDisciplina = null;
                comboBoxOdaberiteDisciplinu.Items.RemoveAt(i);
                comboBoxOdaberiteDisciplinu.SelectedIndex = -1;
            }
        }
        #endregion

        #region Takmicenje
        private void buttonKreirajIzmeniTakmicenje_Click(object sender, EventArgs e)
        {
           if(comboBoxOdaberiteDisciplinu.SelectedIndex != -1)
            {
                if (selektovanaDisciplina.Tip.Equals("Ekipna"))
                {
                    if (comboBoxOdaberiTakmicenje.SelectedIndex != -1)
                    {
                        TakmicenjeEkipnoForm frm = new TakmicenjeEkipnoForm(selektovanaDisciplina, selektovanoTakmicenje);
                        frm.ShowDialog();
                    }
                    else
                    {
                        TakmicenjeEkipnoForm fmr = new TakmicenjeEkipnoForm(selektovanaDisciplina, null);
                        fmr.ShowDialog();
                    }
                }
                else
                {
                    if (comboBoxOdaberiTakmicenje.SelectedIndex != -1)
                    {
                        TakmicenjePojedinacno fmr = new TakmicenjePojedinacno(selektovanSport, selektovanaDisciplina, selektovanoTakmicenje);
                        fmr.ShowDialog();
                    }
                    else
                    {
                        TakmicenjePojedinacno fmr = new TakmicenjePojedinacno(selektovanSport, selektovanaDisciplina, null);
                        fmr.ShowDialog();
                    }
                }
            }
        }

        private void buttonObrisiTakmicenje_Click(object sender, EventArgs e)
        {
            if (selektovanoTakmicenje != null)
            {
                int i = 0;
                int indeks = 0;
                foreach (Takmicenje tak in takmicenja)
                {
                    if (tak.ID == selektovanoTakmicenje.ID)
                        i = indeks;
                    indeks++;
                }
                selektovanoTakmicenje.Delete(client);
                selektovanoTakmicenje = null;
                comboBoxOdaberiTakmicenje.Items.RemoveAt(i);
                comboBoxOdaberiTakmicenje.SelectedIndex = -1;
            }
        }
        #endregion

        #region Klub
                private void buttonKreirajKlub_Click(object sender, EventArgs e)
                {
                    if(comboBoxOdaberiTakmicenje.SelectedIndex != -1 && selektovanaDisciplina.Tip.Equals("Ekipna"))
                    {
                       if(comboBoxKlub.SelectedIndex != -1)
                        {
                            KlubForm frm = new KlubForm(selektovanoTakmicenje, selektovanKlub);
                            frm.ShowDialog();
                        }
                        else
                        {
                            KlubForm frm = new KlubForm(selektovanoTakmicenje, null);
                            frm.ShowDialog();
                        }
                    }
            popuniKlub();
            comboBoxKlub.Text = "--Izaberite klub--";
                }

                private void buttonObrisiKlub_Click(object sender, EventArgs e)
                {
                    if (selektovanKlub != null)
                    {
                        int i = 0;
                        int indeks = 0;
                        foreach (Klub tak in klubovi)
                        {
                            if (tak.ID == selektovanKlub.ID)
                                i = indeks;
                            indeks++;
                        }
                        selektovanKlub.Delete(client);
                        selektovanKlub = null;
                        comboBoxKlub.Items.RemoveAt(i);
                        comboBoxKlub.SelectedIndex = -1;
                
                    }
            popuniKlub();
            comboBoxKlub.Text = "--Izaberite klub--";
        }
        #endregion

        #region Tabela
        private void buttonKreirajIzmeniTabelu_Click(object sender, EventArgs e)
        {
            if(comboBoxOdaberiTakmicenje.SelectedIndex != -1 && comboBoxOdaberiteDisciplinu.SelectedIndex != -1 && selektovanaDisciplina.Tip.Equals("Ekipna"))
            {
                FormTabela tb = new FormTabela(selektovanoTakmicenje, selektovanaTabela);
                tb.ShowDialog();
            }
        }

        private void buttonObrisiTabelu_Click(object sender, EventArgs e)
        {
            if (comboBoxOdaberiTakmicenje.SelectedIndex != -1 && comboBoxOdaberiteDisciplinu.SelectedIndex != -1 && selektovanaDisciplina.Tip.Equals("Ekipna") && selektovanaTabela != null)
            {
                selektovanaTabela.Delete(client);
                selektovanaTabela = null;
            }
        }

        #endregion

        #region Kolo
                private void buttonKreirajIzmeniKolo_Click(object sender, EventArgs e)
                {
                    if(comboBoxKola.SelectedIndex != -1 && selektovanaTabela != null)
                    {
                        KoloForm frm = new KoloForm(selektovanaTabela, selektovanoKolo, kola, selektovanoTakmicenje, selektovanSport);
                        frm.ShowDialog();             
                    }
                }

                private void buttonObrisiKolo_Click(object sender, EventArgs e)
                {

                }
        #endregion
        

        #region Sportista
        private void buttonKreirajSportistu_Click(object sender, EventArgs e)
        {
            if (selektovanSport != null && selektovanaDisciplina != null)
            {
                Form f = new SportistaForm(selektovanSportista, selektovanKlub, selektovanaDisciplina);
                f.BringToFront();
                f.ShowDialog();

                if (selektovanaDisciplina.Tip == "Pojedinacna")
                    popuniSportistuPojedinacnaDisciplina();
                else
                    popuniSportistuEkipnaDisciplina();

                comboBoxOdaberiteSportistu.Text = "--Izaberite sportistu--";

            }
            else
                MessageBox.Show("Morate prvo odbarati sport i disciplinu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonObrisiSportistu_Click(object sender, EventArgs e)
        {
            if (selektovanSportista != null)
            {
                
                if (selektovanaDisciplina.Tip == "Ekipna")
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:Sportista)-[r:UCESTVUJE]->(d) where" +
                                                                   " d.Naziv =" + "'" + selektovanKlub.Naziv + "'" + " and n.Ime=" + "'" + selektovanSportista.Ime + "'" +
                                                                   " delete n,r",

                                    new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteCypher(query);

                    MessageBox.Show("Uspesno ste obrisali sportistu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    popuniSportistuEkipnaDisciplina();
                    comboBoxOdaberiteSportistu.Text = "--Izaberite sportistu--";
                }
                else
                {
                    
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:Sportista)-[r:TAKMICI]->(d) where" +
                                                                  " n.Ime=" + "'" + selektovanSportista.Ime + "'" +
                                                                   " delete n,r",
                                    new Dictionary<string, object>(), CypherResultMode.Projection);
                    List<Sportista> s = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).ToList();
                    MessageBox.Show("Uspesno ste obrisali sportistu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    popuniSportistuPojedinacnaDisciplina();
                    comboBoxOdaberiteSportistu.Text = "--Izaberite sportistu--";
                }
            }
            else
                MessageBox.Show("Morate prvo odbarati sportistu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

           
        }
#endregion

      
        #region Sudija
        private void buttonKreirajSudiju_Click(object sender, EventArgs e)
        {
            if (selektovanSport != null && selektovanaDisciplina != null)
            {
                 Form f = new SudijaForm(selektovanSudija, selektovanSport, selektovanaDisciplina);
                 f.BringToFront();
                 f.ShowDialog();
                popuniSudiju();
               

            }
            else MessageBox.Show("Morate prvo odbarati sport i disciplinu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void buttonObrisiSudiju_Click(object sender, EventArgs e)
        {
            if (selektovanSudija != null )
            {
                if(selektovanaDisciplina.Tip=="Pojedinacna")
                {

                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:Sudija)-[r]->(d) where" +
                                                                       " n.ID=" + "'" + selektovanSudija.ID + "'" + "and (d:Sport)" +
                                                                       " delete n,r",

                                        new Dictionary<string, object>(), CypherResultMode.Projection);

                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Sudija)-[r:KOMANDUJE]->(d) where" +
                                                               " n.ID=" + "'" + selektovanSudija.ID + "'" +
                                                               " delete r",

                                new Dictionary<string, object>(), CypherResultMode.Projection);


                    ((IRawGraphClient)client).ExecuteCypher(query1);
                    ((IRawGraphClient)client).ExecuteCypher(query);
                }
                else
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("match (n:Sudija)-[r]->(d) where" +
                                                                   " n.ID=" + "'" + selektovanSudija.ID + "'" + "and (d:Sport)" +
                                                                   " delete n,r",

                                    new Dictionary<string, object>(), CypherResultMode.Projection);

                    var query1 = new Neo4jClient.Cypher.CypherQuery("match (n:Sudija)-[r:SUDI]->(d) where" +
                                                               " n.ID=" + "'" + selektovanSudija.ID + "'" +
                                                               " delete r",

                                new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteCypher(query1);
                    ((IRawGraphClient)client).ExecuteCypher(query);

                }

                


                MessageBox.Show("Uspesno ste obrisali sudiju!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Morate prvo odbarati sudiju!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            popuniSudiju();
            comboBoxOdaberiteSudiju.Text = "--Izaberite sudiju--";

            
        }

    #endregion


        #region comboBoxSelectIndexEvent
        private void comboBoxOdaberiteSport_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanSport = (Sport)comboBoxOdaberiteSport.SelectedItem;           
                    popuniDiscipline();
                    //popuniKlub();
                    popuniSudiju();
                }

            private void comboBoxOdaberiteDisciplinu_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanaDisciplina = (Disciplina)comboBoxOdaberiteDisciplinu.SelectedItem;
                    if (selektovanaDisciplina.Tip == "Pojedinacna")
                        comboBoxKlub.Enabled = false;
                    else
                        comboBoxKlub.Enabled = true;

                    if (selektovanaDisciplina.Tip == "Pojedinacna")
                        popuniSportistuPojedinacnaDisciplina();


                    if (selektovanaDisciplina != null)
                        popuniStadionPojedinacnaTakm();

                    popuniTakmicenja();
                }

            private void comboBoxOdaberiTakmicenje_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanoTakmicenje = takmicenja[comboBoxOdaberiTakmicenje.SelectedIndex];
                    comboBoxOdaberiTakmicenje.Text = comboBoxOdaberiTakmicenje.Items[comboBoxOdaberiTakmicenje.SelectedIndex].ToString();
                    if (selektovanaDisciplina.Tip.Equals("Ekipna"))
                    {
                        popuniKlub();
                        popuniTabelu();
                        popuniKolo();
                    }
                    else
                    {

                    }
                }

            private void comboBoxKlub_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanKlub = klubovi[comboBoxKlub.SelectedIndex];
                    comboBoxKlub.Text = klubovi[comboBoxKlub.SelectedIndex].Naziv;
                    popuniSportistuEkipnaDisciplina();

                    if (selektovanKlub != null && selektovanaDisciplina != null)
                        popuniStadionEkipnaTakm();
                }

            private void comboBoxKola_SelectedIndexChanged(object sender, EventArgs e)
                {
            
                    selektovanoKolo = null;
                    for (int i = 0; i < kola.Count; i++)
                        if (Convert.ToInt32(comboBoxKola.Text) == kola[i].Redni_broj)
                        {
                            selektovanoKolo = kola[i];                  
                        }

                    if (selektovanoKolo != null)
                        popuniUtakmice();
                }

            private void comboBoxOdaberiteSportistu_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanSportista = (Sportista)comboBoxOdaberiteSportistu.SelectedItem;
                }

            private void comboBoxOdaberiteSudiju_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanSudija = (Sudija)comboBoxOdaberiteSudiju.SelectedItem;
                }

            private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
                {
                    selektovanaUtakmica = utakmice[comboBox2.SelectedIndex];
                }

            private void comboBoxOdaberiteStadion_SelectedIndexChanged(object sender, EventArgs e)
                 {
                     selektovanStadion = (Stadion)comboBoxOdaberiteStadion.SelectedItem;
                }

        #endregion


        #region fje za popunjavanje comboBox

        public void popuniSportove()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match(n: Sport) return n",
                                                          new Dictionary<string, object>(), CypherResultMode.Set);

            sports = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).ToList();

           
            foreach (Sport s in sports)
            {
                
                comboBoxOdaberiteSport.Items.Add(s);
                comboBoxOdaberiteSport.DisplayMember = "Naziv";
            }

        }

        public void popuniDiscipline()
        {
            List<Disciplina> disciplina;
           
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("NazivSporta", selektovanSport.Naziv);
            var disciplinequery = new CypherQuery("start n=node(*) match (n)-[r:DISCIPLINE]->(d) where n.Naziv =~ {NazivSporta} return d",
                queryDict, CypherResultMode.Set);

            disciplina = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(disciplinequery).ToList();


            if (comboBoxOdaberiteDisciplinu.Items.Count > 0)
                comboBoxOdaberiteDisciplinu.Items.Clear();
            foreach (Disciplina ds in disciplina)
            {
                comboBoxOdaberiteDisciplinu.Items.Add(ds);
                comboBoxOdaberiteDisciplinu.DisplayMember = "Naziv";
            }


            


        }

        public void popuniTakmicenja()
        {
            takmicenja = selektovanaDisciplina.GetTakmicenja();
            comboBoxOdaberiTakmicenje.Items.Clear();
            foreach (Takmicenje tak in takmicenja)
            {
                comboBoxOdaberiTakmicenje.Items.Add(tak.Naziv);
            }
        }

        public void popuniKlub()
        {
           
            if (klubovi != null)
                klubovi.Clear();

            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("NazivSporta", selektovanSport.Naziv);
            var klubquery = new CypherQuery("start y=node(*) match (y)-[u:SADRZI]->(m) where  y.ID = '" + selektovanoTakmicenje.ID + "' return m",
                queryDict, CypherResultMode.Set);

            klubovi = ((IRawGraphClient)client).ExecuteGetCypherResults<Klub>(klubquery).ToList();

            if (comboBoxKlub.Items.Count > 0)
                comboBoxKlub.Items.Clear();
            foreach (Klub ds in klubovi)
            {
                comboBoxKlub.Items.Add(ds);
                comboBoxKlub.DisplayMember = "Naziv";
            }

            
        }

        public void popuniTabelu()
        {
            if (comboBoxOdaberiTakmicenje.SelectedIndex != -1 && comboBoxOdaberiteDisciplinu.SelectedIndex != -1 && selektovanaDisciplina.Tip.Equals("Ekipna"))
            {
                selektovanaTabela = selektovanoTakmicenje.GetTabela();
                if (selektovanaTabela != null)
                {
                    for (int i = 1; i <= selektovanaTabela.Broj_kola; i++)
                        comboBoxKola.Items.Add(i.ToString());
                }
            }
        }

        public void popuniKolo()
        {
            kola = selektovanaTabela.GetKola();
            selektovanoKolo = null;
            foreach (Kolo kolo in kola)
            {
                if (selektovanaTabela.Trenutno_kolo == kolo.Redni_broj)
                    selektovanoKolo = kolo;
            }
            comboBoxKola.SelectedIndex = selektovanaTabela.Trenutno_kolo - 1;
        }

        public void popuniSportistuPojedinacnaDisciplina()
        {

            Dictionary<string, object> queryDict1 = new Dictionary<string, object>();
            queryDict1.Add("nazivDiscipline", selektovanaDisciplina.Naziv);
            var klubquery = new CypherQuery("match(n:Sportista) match (n)-[r:TAKMICI]->(h)<-[p:POSEDUJE]-(d) where d.Naziv =~ {nazivDiscipline}  return n",
              queryDict1, CypherResultMode.Set);
            sportista = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(klubquery).ToList();


            if (comboBoxOdaberiteSportistu.Items.Count > 0)
                comboBoxOdaberiteSportistu.Items.Clear();

            foreach (Sportista ds in sportista)
            {
                comboBoxOdaberiteSportistu.Items.Add(ds);

                comboBoxOdaberiteSportistu.DisplayMember = "Ime";
            }

        }

        public void popuniSportistuEkipnaDisciplina()
        {
            
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("NazivKluba", selektovanKlub.Naziv);
            var klubquery = new CypherQuery("match(n:Sportista) match (n)-[r:UCESTVUJE]->(d) where d.Naziv =~ {NazivKluba} return n",
               queryDict, CypherResultMode.Set);
            sportista = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(klubquery).ToList();
            
            if (comboBoxOdaberiteSportistu.Items.Count > 0)
                comboBoxOdaberiteSportistu.Items.Clear();
            foreach (Sportista ds in sportista)
            {
                comboBoxOdaberiteSportistu.Items.Add(ds);
                comboBoxOdaberiteSportistu.DisplayMember = "Ime";
            }
        }

        private void popuniSudiju()
        {
            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("NazivSporta", selektovanSport.Naziv);
            var query = new CypherQuery("match(n:Sudija) match (n)-[r:`IZ SPORTA`]->(d) where d.Naziv =~ {NazivSporta} return n",
               queryDict, CypherResultMode.Set);
            List<Sudija> sudije = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).ToList();

            
            if (comboBoxOdaberiteSudiju.Items.Count > 0)
                comboBoxOdaberiteSudiju.Items.Clear();
            foreach (Sudija s in sudije)
            {
                comboBoxOdaberiteSudiju.Items.Add(s);
                comboBoxOdaberiteSudiju.DisplayMember = "Prezime";
            }
        }


        public void popuniStadionPojedinacnaTakm()
        {



            List<Stadion> stadion = new List<Stadion>();
            if (selektovanaDisciplina.Tip == "Pojedinacna")
            {
                Dictionary<string, object> queryDict = new Dictionary<string, object>();
                queryDict.Add("IDDiscipline", selektovanaDisciplina.ID.ToString());
                var query = new CypherQuery("start n=node(*) match  (n)-[r:ODRZAVA]->(t)<-[p:POSEDUJE]-(d) where d.ID=~ {IDDiscipline}  return n  ",
                   queryDict, CypherResultMode.Set);
                stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).ToList();

            }


            if (comboBoxOdaberiteStadion.Items.Count > 0)
                comboBoxOdaberiteStadion.Items.Clear();
            foreach (Stadion ds in stadion)
            {
                comboBoxOdaberiteStadion.Items.Add(ds);
                comboBoxOdaberiteStadion.DisplayMember = "Ime_stadiona";
            }

            

        }


        public void popuniUtakmice()
        {

            utakmice = selektovanoKolo.GetUtakmice();
            comboBox2.Items.Clear();
            foreach (Utakmica tak in utakmice)
            {
                tak.GetDomacin();
                tak.GetGost();
                comboBox2.Items.Add(tak.Domacin.Naziv + " vs " + tak.Gost.Naziv);
            }



        }


        public void popuniStadionEkipnaTakm()
        {
            List<Stadion> stadion;

            // Dictionary<string, object> queryDict = new Dictionary<string, object>();
            // queryDict.Add("NazivKluba", selektovanKlub.Naziv);
            var query = new CypherQuery("match(n:Stadion) match (n)-[r:ODIGRAVA]->(d) where d.ID =" + "'" + selektovanKlub.ID+"'" +
                                " return n ",
               new Dictionary<string, object>(), CypherResultMode.Set);
            stadion = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).ToList();

            if (comboBoxOdaberiteStadion.Items.Count > 0)
                comboBoxOdaberiteStadion.Items.Clear();
            foreach (Stadion ds in stadion)
            {
                comboBoxOdaberiteStadion.Items.Add(ds);
                comboBoxOdaberiteStadion.DisplayMember = "Ime_stadiona";
            }



        }
        #endregion


        #region Stadion
        private void buttonObrisiStadion_Click(object sender, EventArgs e)
        {
            //start n=node(*) match  (n)-[r:ODRZAVA]->(t)<-[p:POSEDUJE]-(d) where n.Ime_stadiona='Cair'  delete r,n
            if (selektovanStadion != null)
            {

                if (selektovanaDisciplina.Tip == "Pojedinacna")
                {
                    var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match  (n)-[r:ODRZAVA]->(t)<-[p:POSEDUJE]-(d) where" +
                                                   " n.ID=" + "'" + selektovanStadion.ID + "'" +
                                                   " delete n,r",

                    new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteCypher(query);

                    MessageBox.Show("Uspesno ste obrisali stadion!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    popuniStadionPojedinacnaTakm();
                    comboBoxOdaberiteStadion.Text = "--Izaberite stadion--";
                }
                else
                {//match(n:Stadion) match (n)-[r:ODIGRAVA]->(d) where d.Naziv ={NazivKluba} return n
                    var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) match  (n)-[r:ODIGRAVA]->(d) where" +
                                                   " n.ID=" + "'" + selektovanStadion.ID + "'" +
                                                   " delete n,r",

                    new Dictionary<string, object>(), CypherResultMode.Projection);

                    ((IRawGraphClient)client).ExecuteCypher(query);

                    MessageBox.Show("Uspesno ste obrisali stadion!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    popuniStadionEkipnaTakm();
                    comboBoxOdaberiteStadion.Text = "--Izaberite stadion--";
                }
            }
            else
                MessageBox.Show("Morate prvo odbarati stadion!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            
        }
        
        private void buttonKreirajIzmeniStadion_Click(object sender, EventArgs e)
        {
            Form f = new StadionForm(selektovanStadion, selektovanKlub, selektovanaDisciplina);
            f.BringToFront();
            f.ShowDialog();

            if (selektovanaDisciplina.Tip == "Pojedinacna")      
                popuniStadionPojedinacnaTakm();
                            
            else
                popuniStadionEkipnaTakm();

            comboBoxOdaberiteStadion.Text = "--Izaberite stadion--";
        }


        #endregion Stadion


        #region Urakmica
                private void button3_Click(object sender, EventArgs e)
                {
                    //birsanje utakmice u zavisnosti od kola
                    

                    if (selektovanaUtakmica != null)
                    {
                        int i = 0;
                        int indeks = 0;
                        foreach (Utakmica ut in utakmice)
                        {
                            if (ut.ID == selektovanaUtakmica.ID)
                                i = indeks;
                            indeks++;
                        }
                        selektovanaUtakmica.Delete(client);
                        selektovanaUtakmica = null;
                        comboBox2.Items.RemoveAt(i);
                        comboBox2.SelectedIndex = -1;
                    }
                    else
                        MessageBox.Show("Morate prvo odbarati utakmicu!", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
        #endregion



    }
}
