﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class TakmicenjePojedinacno : Form
    {
        GraphClient client;
        Takmicenje selektovanoTakmicenje = null;
        Disciplina selektovanaDisciplina = null;
        Stadion selektovaniStadion = null;
        Sudija selektovaniSudija = null;
        Sport selektovaniSport = null;
        List<Stadion> stadioni = null;
        List<Sudija> sudije = null;
        List<Sportista> sportisti = null;
        List<Takmici> takmicari = null;
        Sportista prvi = null, drugi = null, treci = null, cetvrti = null, peti = null, sesti = null, sedmi = null, osmi = null;
        
        public TakmicenjePojedinacno(Sport sport, Disciplina disciplina, Takmicenje takmicenje)
        {
            InitializeComponent();
            selektovaniSport = sport;
            selektovanoTakmicenje = takmicenje;
            selektovanaDisciplina = disciplina;
        }

        private void TakmicenjePojedinacno_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            if (selektovanoTakmicenje != null)
            {
                selektovaniStadion = selektovanoTakmicenje.GetStadion();
                selektovaniSudija = selektovanoTakmicenje.GetSudija();
                textBoxNaziv.Text = selektovanoTakmicenje.Naziv;
                textBoxZemlja.Text = selektovanoTakmicenje.Zemlja;               
                textBoxSponzor.Text = selektovanoTakmicenje.Sponzor;
                comboBoxStadion.Text = selektovaniStadion.Ime_stadiona;
                comboBoxSudija.Text = selektovaniSudija.Ime + " " + selektovaniSudija.Prezime;

                takmicari = selektovanoTakmicenje.GetTakmicari();
                foreach (Takmici tk in takmicari)
                    Popuni(tk);
            }
            else
            {
                dateTimePickerDatumRodjenja.Value = DateTime.Now;
                buttonIzmeni.Enabled = false;
                groupBox1.Visible = false;
            }
            
            var query = new CypherQuery("match (n:Stadion) return n", new Dictionary<string, object>(), CypherResultMode.Set);
            var query1 = new CypherQuery("match (n:Sportista) where not (n)-[:UCESTVUJE]-() return n", new Dictionary<string, object>(), CypherResultMode.Set);

            stadioni = ((IRawGraphClient)client).ExecuteGetCypherResults<Stadion>(query).ToList();
            sportisti = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query1).ToList();

            sudije = selektovaniSport.GetSudije();

            foreach (Stadion stadion in stadioni)
                comboBoxStadion.Items.Add(stadion.Ime_stadiona);

            foreach (Sudija sudija in sudije)
                comboBoxSudija.Items.Add(sudija.Ime + " " + sudija.Prezime);  
            
            foreach(Sportista sp in sportisti)
            {
                comboBoxA1.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA2.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA3.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA4.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA5.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA6.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA7.Items.Add(sp.Ime + "" + sp.Prezime);
                comboBoxA8.Items.Add(sp.Ime + "" + sp.Prezime);
            }
        }

        private void comboBoxStadion_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovaniStadion = stadioni[comboBoxStadion.SelectedIndex];
        }

        private void comboBoxSudija_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovaniSudija = sudije[comboBoxSudija.SelectedIndex];
        }

        private void buttonKreiraj_Click(object sender, EventArgs e)
        {
            if (textBoxNaziv.Text != "" && textBoxZemlja.Text != "" && textBoxNaziv.Text != "" && textBoxSponzor.Text != "" && selektovaniSudija != null && selektovaniStadion != null)
            {
                selektovanoTakmicenje = Takmicenje.Create(client, textBoxNaziv.Text, textBoxZemlja.Text, dateTimePickerDatumRodjenja.Value.ToString("dd.MM.yyyy"), "", textBoxSponzor.Text);
                Disciplina.CreateDisciplinaTakmicenje(client, selektovanaDisciplina, selektovanoTakmicenje);
                Takmicenje.CreateTakmicenjeStadion(client, selektovanoTakmicenje, selektovaniStadion);
                Takmicenje.CreateTakmicenjeSudija(client, selektovanoTakmicenje, selektovaniSudija);
                buttonIzmeni.Enabled = true;
                groupBox1.Visible = true;
            }
        }

        private void buttonIzmeni_Click(object sender, EventArgs e)
        {
            if (textBoxNaziv.Text != "" && textBoxZemlja.Text != "" && textBoxNaziv.Text != "" && textBoxSponzor.Text != "" && selektovaniSudija != null && selektovaniStadion != null)
            {
                Takmicenje.DeleteTakmicenjeStadion(client, selektovanoTakmicenje, selektovanoTakmicenje.GetStadion());
                Takmicenje.DeleteTakmicenjeSudija(client, selektovanoTakmicenje, selektovanoTakmicenje.GetSudija());
                selektovanoTakmicenje.Modify(client, textBoxNaziv.Text, textBoxZemlja.Text, dateTimePickerDatumRodjenja.Value.ToString("dd.MM.yyyy"), "", textBoxSponzor.Text);
                Takmicenje.CreateTakmicenjeStadion(client, selektovanoTakmicenje, selektovaniStadion);
                Takmicenje.CreateTakmicenjeSudija(client, selektovanoTakmicenje, selektovaniSudija);
            }
        }

        public void Popuni(Takmici takmci)
        {
            switch (takmci.Mesto)
            {
                case 1:  prvi = takmci.GetSportista();
                    comboBoxA1.Text = prvi.Ime + " " + prvi.Prezime;
                    textBoxA1.Text = takmci.Rezultat;
                    break;

                case 2:  drugi  = takmci.GetSportista();
                    comboBoxA2.Text = drugi.Ime + " " + drugi.Prezime;
                    textBoxA2.Text = takmci.Rezultat;
                    break;

                case 3: treci = takmci.GetSportista();
                    comboBoxA3.Text = treci.Ime + " " + treci.Prezime;
                    textBoxA3.Text = takmci.Rezultat;
                    break;

                case 4: cetvrti= takmci.GetSportista();
                    comboBoxA4.Text = cetvrti.Ime + " " + cetvrti.Prezime;
                    textBoxA4.Text = takmci.Rezultat;
                    break;

                case 5: peti = takmci.GetSportista();
                    comboBoxA5.Text = peti.Ime + " " + peti.Prezime;
                    textBoxA5.Text = takmci.Rezultat;
                    break;

                case 6: sesti = takmci.GetSportista();
                    comboBoxA6.Text = sesti.Ime + " " + sesti.Prezime;
                    textBoxA6.Text = takmci.Rezultat;
                    break;

                case 7: sedmi = takmci.GetSportista();
                    comboBoxA7.Text = sedmi.Ime + " " + sedmi.Prezime;
                    textBoxA7.Text = takmci.Rezultat;
                    break;

                case 8:  osmi = takmci.GetSportista();
                    comboBoxA8.Text = osmi.Ime + " " + osmi.Prezime;
                    textBoxA8.Text = takmci.Rezultat;
                    break;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBoxA1.SelectedIndex != -1 && textBoxA1.Text != "")
            {                          
                if (takmicari != null && takmicari.Count == 0)
                {                 
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA1.SelectedIndex], 1, textBoxA1.Text);                   
                    button1.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, prvi);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA1.SelectedIndex], 1, textBoxA1.Text);
                    button1.Enabled = false;
                }
            }
        }
    
        private void button2_Click(object sender, EventArgs e)
        {
            if ( comboBoxA2.SelectedIndex != -1 && textBoxA2.Text != "")
            {
                if (takmicari != null && takmicari.Count < 2)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA2.SelectedIndex], 2, textBoxA2.Text);
                    button2.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, drugi);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA2.SelectedIndex], 2, textBoxA2.Text);
                    button2.Enabled = false;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBoxA3.SelectedIndex != -1 && textBoxA3.Text != "")
            {
                if (takmicari != null && takmicari.Count < 3)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA3.SelectedIndex], 3, textBoxA3.Text);
                    button3.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, treci);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA3.SelectedIndex], 3, textBoxA3.Text);
                    button3.Enabled = false;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBoxA4.SelectedIndex != -1 && textBoxA4.Text != "")
            {
                if (takmicari != null && takmicari.Count <  4)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA4.SelectedIndex], 4, textBoxA4.Text);
                    button4.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, cetvrti);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA4.SelectedIndex], 4, textBoxA4.Text);
                    button4.Enabled = false;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (comboBoxA5.SelectedIndex != -1 && textBoxA5.Text != "")
            {
                if (takmicari != null && takmicari.Count <  5)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA5.SelectedIndex], 5, textBoxA5.Text);
                    button5.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, peti);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA5.SelectedIndex], 5, textBoxA5.Text);
                    button5.Enabled = false;
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (comboBoxA6.SelectedIndex != -1 && textBoxA6.Text != "")
            {
                if (takmicari != null && takmicari.Count < 6)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA6.SelectedIndex], 6, textBoxA6.Text);
                    button6.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, sesti);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA6.SelectedIndex], 6, textBoxA6.Text);
                    button6.Enabled = false;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBoxA7.SelectedIndex != -1 && textBoxA7.Text != "")
            {
                if (takmicari != null && takmicari.Count < 7)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA7.SelectedIndex], 7, textBoxA7.Text);
                    button7.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, sedmi);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA7.SelectedIndex], 7, textBoxA7.Text);
                    button7.Enabled = false;
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (comboBoxA8.SelectedIndex != -1 && textBoxA8.Text != "")
            {
                if (takmicari != null && takmicari.Count < 8)
                {
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA8.SelectedIndex], 8, textBoxA8.Text);
                    button8.Enabled = false;
                }
                else
                {
                    Takmicenje.DeleteTakmicenjeSportista(client, selektovanoTakmicenje, osmi);
                    Takmicenje.CreateTakmicenjeSportista(client, selektovanoTakmicenje, sportisti[comboBoxA8.SelectedIndex], 8, textBoxA8.Text);
                    button8.Enabled = false;
                }
            }
        }      
    }
}
