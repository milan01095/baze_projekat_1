﻿namespace Olimpiada
{
    partial class Pocetna
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.adminGroupbox = new System.Windows.Forms.GroupBox();
            this.buttonLoginAdmin = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.navBarControl1 = new DevExpress.XtraNavBar.NavBarControl();
            this.panelStadion = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.listStadionTakmicenj = new System.Windows.Forms.ListView();
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonXStadion = new System.Windows.Forms.Button();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.groupStadionTabela = new System.Windows.Forms.GroupBox();
            this.listStadionLiga = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.StadionID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonStadionX = new System.Windows.Forms.Button();
            this.groupStadionInformacije = new System.Windows.Forms.GroupBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.labelStadionGodIzgradnje = new System.Windows.Forms.Label();
            this.labelStadionKapacitet = new System.Windows.Forms.Label();
            this.labelStadionLokacija = new System.Windows.Forms.Label();
            this.labelStadionGrad = new System.Windows.Forms.Label();
            this.labelStadionNaziv = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.panelTakmicenje = new System.Windows.Forms.Panel();
            this.groupTrenutnoKolo = new System.Windows.Forms.GroupBox();
            this.listTakmicenjeSvaKola = new System.Windows.Forms.ListView();
            this.Kolo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Datum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Domacin_Kola = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Domacin_Rez = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gost_Rez = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gost_Kola = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.KoloID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonXTakmicenje = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.listTakmicenjeTabela = new System.Windows.Forms.ListView();
            this.R_br = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Naziv = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.OM = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.P = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.N = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.I = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.DG = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PG = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.GR = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.PO = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.ID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupNaredno_kolo = new System.Windows.Forms.GroupBox();
            this.listView2 = new System.Windows.Forms.ListView();
            this.Domacin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Br_gol_Domacin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Br_gol_Gost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Gost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupTakmicenjeInformacije = new System.Windows.Forms.GroupBox();
            this.label_Sponzor = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.comboBox_Kolo = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label_Trenutno_kolo = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label_Tip = new System.Windows.Forms.Label();
            this.label_Pol = new System.Windows.Forms.Label();
            this.label_Kup = new System.Windows.Forms.Label();
            this.label_Osnovana = new System.Windows.Forms.Label();
            this.label_Zemlja = new System.Windows.Forms.Label();
            this.label_Asociacija = new System.Windows.Forms.Label();
            this.label_Sport = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelSudija = new System.Windows.Forms.Panel();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.buttonXSudija = new System.Windows.Forms.Button();
            this.groupInformacijeSudija = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.labelSudijaGrad = new System.Windows.Forms.Label();
            this.labelSudijaDrzava = new System.Windows.Forms.Label();
            this.labelSudijaGodSudjenja = new System.Windows.Forms.Label();
            this.labelSudijaSport = new System.Windows.Forms.Label();
            this.labelSudijaDatRodjenja = new System.Windows.Forms.Label();
            this.labelSudijaPrezime = new System.Windows.Forms.Label();
            this.labelSudijaIme = new System.Windows.Forms.Label();
            this.label9943 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupSudijaTablica = new System.Windows.Forms.GroupBox();
            this.listSudijaPojedinacni = new System.Windows.Forms.ListView();
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.listSudijaEkipni = new System.Windows.Forms.ListView();
            this.SudijaDatum = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SudijaVreme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SudijaDomacin = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SudijaDomacinBrGol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SudijaGostBrGol = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.SudijaGost = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader22 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelKlub = new System.Windows.Forms.Panel();
            this.groupKlubSvaKola = new System.Windows.Forms.GroupBox();
            this.listKlubKola = new System.Windows.Forms.ListView();
            this.KlubKolo = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.KlubKolaID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.buttonXKlub = new System.Windows.Forms.Button();
            this.groupInformacijeKlub = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.linkLabelKlubStadion = new System.Windows.Forms.LinkLabel();
            this.labelKlubSajt = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.labelKlubMenadzer = new System.Windows.Forms.Label();
            this.labelKlubTrener = new System.Windows.Forms.Label();
            this.labelKlubGlavniSponzor = new System.Windows.Forms.Label();
            this.labelKlubDatOsnivanja = new System.Windows.Forms.Label();
            this.labelKlubBrTitula = new System.Windows.Forms.Label();
            this.labelKlubGrad = new System.Windows.Forms.Label();
            this.labelKlubNaziv = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupInformacijeIgraci = new System.Windows.Forms.GroupBox();
            this.listViewKlubIgraci = new System.Windows.Forms.ListView();
            this.Broj_dresa = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Ime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.Prezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.KlubSportistaID = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panelSportista = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.groupSportistaInformacije = new System.Windows.Forms.GroupBox();
            this.labelSportistaBrTel = new System.Windows.Forms.Label();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label13 = new System.Windows.Forms.Label();
            this.labelSportistaVisina = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.labelSportistaTezina = new System.Windows.Forms.Label();
            this.labelSportistaDrzava = new System.Windows.Forms.Label();
            this.labelSportistaDres = new System.Windows.Forms.Label();
            this.labelSportistaKlub = new System.Windows.Forms.Label();
            this.labelSportistaDatRodjenja = new System.Windows.Forms.Label();
            this.labelSportistaPrezime = new System.Windows.Forms.Label();
            this.labelSportistaIme = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.buttonXSportista = new System.Windows.Forms.Button();
            this.panelTakmicenjePojedinacno = new System.Windows.Forms.Panel();
            this.buttonXTakmicenjePojedinacno = new System.Windows.Forms.Button();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.groupTakmicenjePojedinacnoInformacije = new System.Windows.Forms.GroupBox();
            this.linkLabelStadion = new System.Windows.Forms.LinkLabel();
            this.label14 = new System.Windows.Forms.Label();
            this.linkLabelTakmicenjePojedinacnoSudija = new System.Windows.Forms.LinkLabel();
            this.labelTakmicenjePojedinacnoSponzor = new System.Windows.Forms.Label();
            this.label55 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoBrUcesnika = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoTip = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoPol = new System.Windows.Forms.Label();
            this.labelTAkmicenjePojedinacnoDisciplina = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoDatum = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoZemlja = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoAsociacija = new System.Windows.Forms.Label();
            this.labelTakmicenjePojedinacnoSport = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.groupTakmicenjePojedinacnoRezultati = new System.Windows.Forms.GroupBox();
            this.listTakmicenjePojedinacno = new System.Windows.Forms.ListView();
            this.TakmicenjePojedinacnoMesto = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TakmicenjePojedinacnoIme = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TakmicenjePojedinacnoPrezime = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.TakmicenjePojedinacnoRezultat = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader23 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panelUtakmica = new System.Windows.Forms.Panel();
            this.buttonXUtakmicaa = new System.Windows.Forms.Button();
            this.buttonXUtakmica = new System.Windows.Forms.Button();
            this.groupUtakmicaInformacije = new System.Windows.Forms.GroupBox();
            this.linkLabelUtakmicaSudija = new System.Windows.Forms.LinkLabel();
            this.label21 = new System.Windows.Forms.Label();
            this.linkLabelGost = new System.Windows.Forms.LinkLabel();
            this.label20 = new System.Windows.Forms.Label();
            this.linkLabelDomacin = new System.Windows.Forms.LinkLabel();
            this.label18 = new System.Windows.Forms.Label();
            this.linkLabelUtakmicaStadion = new System.Windows.Forms.LinkLabel();
            this.label17 = new System.Windows.Forms.Label();
            this.labelUtakmicaRezultat = new System.Windows.Forms.Label();
            this.labelUtakmicaKolo = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.labelUtakmicaVreme = new System.Windows.Forms.Label();
            this.labelUtakmicaDatum = new System.Windows.Forms.Label();
            this.labelUtakmicaLokacija = new System.Windows.Forms.Label();
            this.labelUtakmicaGrad = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.adminGroupbox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).BeginInit();
            this.panelStadion.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            this.groupStadionTabela.SuspendLayout();
            this.groupStadionInformacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.panelTakmicenje.SuspendLayout();
            this.groupTrenutnoKolo.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupNaredno_kolo.SuspendLayout();
            this.groupTakmicenjeInformacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panelSudija.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupInformacijeSudija.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.groupSudijaTablica.SuspendLayout();
            this.panelKlub.SuspendLayout();
            this.groupKlubSvaKola.SuspendLayout();
            this.groupInformacijeKlub.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupInformacijeIgraci.SuspendLayout();
            this.panelSportista.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupSportistaInformacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.panelTakmicenjePojedinacno.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            this.groupTakmicenjePojedinacnoInformacije.SuspendLayout();
            this.groupTakmicenjePojedinacnoRezultati.SuspendLayout();
            this.panelUtakmica.SuspendLayout();
            this.groupUtakmicaInformacije.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            this.SuspendLayout();
            // 
            // adminGroupbox
            // 
            this.adminGroupbox.Controls.Add(this.buttonLoginAdmin);
            this.adminGroupbox.Controls.Add(this.label2);
            this.adminGroupbox.Controls.Add(this.textBoxPassword);
            this.adminGroupbox.Controls.Add(this.textBoxUsername);
            this.adminGroupbox.Controls.Add(this.label1);
            this.adminGroupbox.Location = new System.Drawing.Point(252, 128);
            this.adminGroupbox.Name = "adminGroupbox";
            this.adminGroupbox.Size = new System.Drawing.Size(182, 160);
            this.adminGroupbox.TabIndex = 3;
            this.adminGroupbox.TabStop = false;
            this.adminGroupbox.Text = "Admin";
            // 
            // buttonLoginAdmin
            // 
            this.buttonLoginAdmin.Location = new System.Drawing.Point(19, 130);
            this.buttonLoginAdmin.Name = "buttonLoginAdmin";
            this.buttonLoginAdmin.Size = new System.Drawing.Size(81, 23);
            this.buttonLoginAdmin.TabIndex = 4;
            this.buttonLoginAdmin.Text = "Login";
            this.buttonLoginAdmin.UseVisualStyleBackColor = true;
            this.buttonLoginAdmin.Click += new System.EventHandler(this.buttonLoginAdmin_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Password";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(19, 98);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(145, 20);
            this.textBoxPassword.TabIndex = 2;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(19, 43);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(145, 20);
            this.textBoxUsername.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 26);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username";
            // 
            // navBarControl1
            // 
            this.navBarControl1.ActiveGroup = null;
            this.navBarControl1.Dock = System.Windows.Forms.DockStyle.Left;
            this.navBarControl1.Location = new System.Drawing.Point(0, 0);
            this.navBarControl1.Name = "navBarControl1";
            this.navBarControl1.OptionsNavPane.ExpandedWidth = 140;
            this.navBarControl1.Size = new System.Drawing.Size(140, 749);
            this.navBarControl1.TabIndex = 5;
            this.navBarControl1.Text = "navBarControl1";
            // 
            // panelStadion
            // 
            this.panelStadion.AutoScroll = true;
            this.panelStadion.Controls.Add(this.groupBox1);
            this.panelStadion.Controls.Add(this.buttonXStadion);
            this.panelStadion.Controls.Add(this.pictureBox12);
            this.panelStadion.Controls.Add(this.groupStadionTabela);
            this.panelStadion.Controls.Add(this.buttonStadionX);
            this.panelStadion.Controls.Add(this.groupStadionInformacije);
            this.panelStadion.Controls.Add(this.pictureBox8);
            this.panelStadion.Location = new System.Drawing.Point(557, 368);
            this.panelStadion.Name = "panelStadion";
            this.panelStadion.Size = new System.Drawing.Size(131, 154);
            this.panelStadion.TabIndex = 12;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.listStadionTakmicenj);
            this.groupBox1.Location = new System.Drawing.Point(10, 538);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(425, 257);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Takmicenja";
            // 
            // listStadionTakmicenj
            // 
            this.listStadionTakmicenj.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader7,
            this.columnHeader13,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16});
            this.listStadionTakmicenj.FullRowSelect = true;
            this.listStadionTakmicenj.Location = new System.Drawing.Point(4, 21);
            this.listStadionTakmicenj.Name = "listStadionTakmicenj";
            this.listStadionTakmicenj.Size = new System.Drawing.Size(415, 225);
            this.listStadionTakmicenj.TabIndex = 0;
            this.listStadionTakmicenj.UseCompatibleStateImageBehavior = false;
            this.listStadionTakmicenj.View = System.Windows.Forms.View.Details;
            this.listStadionTakmicenj.DoubleClick += new System.EventHandler(this.listStadionTakmicenj_DoubleClick);
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Datum";
            this.columnHeader7.Width = 91;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Naziv takmicenja";
            this.columnHeader13.Width = 202;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Disciplina";
            this.columnHeader14.Width = 74;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Pol";
            this.columnHeader15.Width = 46;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "ID";
            this.columnHeader16.Width = 43;
            // 
            // buttonXStadion
            // 
            this.buttonXStadion.Location = new System.Drawing.Point(414, 3);
            this.buttonXStadion.Name = "buttonXStadion";
            this.buttonXStadion.Size = new System.Drawing.Size(20, 20);
            this.buttonXStadion.TabIndex = 20;
            this.buttonXStadion.Text = "X";
            this.buttonXStadion.UseVisualStyleBackColor = true;
            this.buttonXStadion.Click += new System.EventHandler(this.buttonXStadion_Click_1);
            // 
            // pictureBox12
            // 
            this.pictureBox12.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox12.Location = new System.Drawing.Point(77, 0);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(291, 68);
            this.pictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox12.TabIndex = 19;
            this.pictureBox12.TabStop = false;
            // 
            // groupStadionTabela
            // 
            this.groupStadionTabela.Controls.Add(this.listStadionLiga);
            this.groupStadionTabela.Location = new System.Drawing.Point(10, 280);
            this.groupStadionTabela.Name = "groupStadionTabela";
            this.groupStadionTabela.Size = new System.Drawing.Size(425, 257);
            this.groupStadionTabela.TabIndex = 18;
            this.groupStadionTabela.TabStop = false;
            this.groupStadionTabela.Text = "Dogadjaji u ligi";
            // 
            // listStadionLiga
            // 
            this.listStadionLiga.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.StadionID});
            this.listStadionLiga.FullRowSelect = true;
            this.listStadionLiga.Location = new System.Drawing.Point(4, 21);
            this.listStadionLiga.Name = "listStadionLiga";
            this.listStadionLiga.Size = new System.Drawing.Size(415, 225);
            this.listStadionLiga.TabIndex = 0;
            this.listStadionLiga.UseCompatibleStateImageBehavior = false;
            this.listStadionLiga.View = System.Windows.Forms.View.Details;
            this.listStadionLiga.DoubleClick += new System.EventHandler(this.listStadionLiga_DoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Datum";
            this.columnHeader1.Width = 77;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vreme";
            this.columnHeader2.Width = 42;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Domacin";
            this.columnHeader3.Width = 96;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Br_gol";
            this.columnHeader4.Width = 42;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Br_gol";
            this.columnHeader5.Width = 42;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Gost";
            this.columnHeader6.Width = 121;
            // 
            // StadionID
            // 
            this.StadionID.Text = "ID";
            // 
            // buttonStadionX
            // 
            this.buttonStadionX.Location = new System.Drawing.Point(328, -186);
            this.buttonStadionX.Name = "buttonStadionX";
            this.buttonStadionX.Size = new System.Drawing.Size(20, 20);
            this.buttonStadionX.TabIndex = 5;
            this.buttonStadionX.Text = "X";
            this.buttonStadionX.UseVisualStyleBackColor = true;
            // 
            // groupStadionInformacije
            // 
            this.groupStadionInformacije.Controls.Add(this.pictureBox7);
            this.groupStadionInformacije.Controls.Add(this.labelStadionGodIzgradnje);
            this.groupStadionInformacije.Controls.Add(this.labelStadionKapacitet);
            this.groupStadionInformacije.Controls.Add(this.labelStadionLokacija);
            this.groupStadionInformacije.Controls.Add(this.labelStadionGrad);
            this.groupStadionInformacije.Controls.Add(this.labelStadionNaziv);
            this.groupStadionInformacije.Controls.Add(this.label40);
            this.groupStadionInformacije.Controls.Add(this.label41);
            this.groupStadionInformacije.Controls.Add(this.label42);
            this.groupStadionInformacije.Controls.Add(this.label49);
            this.groupStadionInformacije.Controls.Add(this.label50);
            this.groupStadionInformacije.Location = new System.Drawing.Point(10, 70);
            this.groupStadionInformacije.Name = "groupStadionInformacije";
            this.groupStadionInformacije.Size = new System.Drawing.Size(425, 205);
            this.groupStadionInformacije.TabIndex = 4;
            this.groupStadionInformacije.TabStop = false;
            this.groupStadionInformacije.Text = "Informacije o stadionu";
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = global::Olimpiada.Properties.Resources.stadium_icon;
            this.pictureBox7.Location = new System.Drawing.Point(26, 40);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(163, 151);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 17;
            this.pictureBox7.TabStop = false;
            // 
            // labelStadionGodIzgradnje
            // 
            this.labelStadionGodIzgradnje.AutoSize = true;
            this.labelStadionGodIzgradnje.Location = new System.Drawing.Point(323, 169);
            this.labelStadionGodIzgradnje.Name = "labelStadionGodIzgradnje";
            this.labelStadionGodIzgradnje.Size = new System.Drawing.Size(41, 13);
            this.labelStadionGodIzgradnje.TabIndex = 11;
            this.labelStadionGodIzgradnje.Text = "label19";
            // 
            // labelStadionKapacitet
            // 
            this.labelStadionKapacitet.AutoSize = true;
            this.labelStadionKapacitet.Location = new System.Drawing.Point(323, 135);
            this.labelStadionKapacitet.Name = "labelStadionKapacitet";
            this.labelStadionKapacitet.Size = new System.Drawing.Size(35, 13);
            this.labelStadionKapacitet.TabIndex = 10;
            this.labelStadionKapacitet.Text = "label8";
            // 
            // labelStadionLokacija
            // 
            this.labelStadionLokacija.AutoSize = true;
            this.labelStadionLokacija.Location = new System.Drawing.Point(323, 103);
            this.labelStadionLokacija.Name = "labelStadionLokacija";
            this.labelStadionLokacija.Size = new System.Drawing.Size(35, 13);
            this.labelStadionLokacija.TabIndex = 9;
            this.labelStadionLokacija.Text = "label8";
            // 
            // labelStadionGrad
            // 
            this.labelStadionGrad.AutoSize = true;
            this.labelStadionGrad.Location = new System.Drawing.Point(323, 70);
            this.labelStadionGrad.Name = "labelStadionGrad";
            this.labelStadionGrad.Size = new System.Drawing.Size(35, 13);
            this.labelStadionGrad.TabIndex = 8;
            this.labelStadionGrad.Text = "label8";
            // 
            // labelStadionNaziv
            // 
            this.labelStadionNaziv.AutoSize = true;
            this.labelStadionNaziv.Location = new System.Drawing.Point(323, 40);
            this.labelStadionNaziv.Name = "labelStadionNaziv";
            this.labelStadionNaziv.Size = new System.Drawing.Size(35, 13);
            this.labelStadionNaziv.TabIndex = 7;
            this.labelStadionNaziv.Text = "label8";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(215, 169);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(89, 13);
            this.label40.TabIndex = 4;
            this.label40.Text = "Godina izgradnje:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(215, 135);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(55, 13);
            this.label41.TabIndex = 3;
            this.label41.Text = "Kapacitet:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(215, 103);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(50, 13);
            this.label42.TabIndex = 2;
            this.label42.Text = "Lokacija:";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(215, 70);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(33, 13);
            this.label49.TabIndex = 1;
            this.label49.Text = "Grad:";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(215, 40);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(80, 13);
            this.label50.TabIndex = 0;
            this.label50.Text = "Naziv stadiona:";
            // 
            // pictureBox8
            // 
            this.pictureBox8.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox8.Location = new System.Drawing.Point(-11, -191);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(291, 68);
            this.pictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox8.TabIndex = 3;
            this.pictureBox8.TabStop = false;
            // 
            // panelTakmicenje
            // 
            this.panelTakmicenje.AutoScroll = true;
            this.panelTakmicenje.Controls.Add(this.groupTrenutnoKolo);
            this.panelTakmicenje.Controls.Add(this.buttonXTakmicenje);
            this.panelTakmicenje.Controls.Add(this.groupBox2);
            this.panelTakmicenje.Controls.Add(this.groupTakmicenjeInformacije);
            this.panelTakmicenje.Controls.Add(this.pictureBox1);
            this.panelTakmicenje.Location = new System.Drawing.Point(814, 654);
            this.panelTakmicenje.Name = "panelTakmicenje";
            this.panelTakmicenje.Size = new System.Drawing.Size(117, 73);
            this.panelTakmicenje.TabIndex = 13;
            // 
            // groupTrenutnoKolo
            // 
            this.groupTrenutnoKolo.Controls.Add(this.listTakmicenjeSvaKola);
            this.groupTrenutnoKolo.Location = new System.Drawing.Point(10, 494);
            this.groupTrenutnoKolo.Name = "groupTrenutnoKolo";
            this.groupTrenutnoKolo.Size = new System.Drawing.Size(417, 248);
            this.groupTrenutnoKolo.TabIndex = 8;
            this.groupTrenutnoKolo.TabStop = false;
            this.groupTrenutnoKolo.Text = "Trenutno kolo";
            // 
            // listTakmicenjeSvaKola
            // 
            this.listTakmicenjeSvaKola.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Kolo,
            this.Datum,
            this.Domacin_Kola,
            this.Domacin_Rez,
            this.Gost_Rez,
            this.Gost_Kola,
            this.KoloID});
            this.listTakmicenjeSvaKola.FullRowSelect = true;
            this.listTakmicenjeSvaKola.Location = new System.Drawing.Point(0, 20);
            this.listTakmicenjeSvaKola.Name = "listTakmicenjeSvaKola";
            this.listTakmicenjeSvaKola.Size = new System.Drawing.Size(411, 222);
            this.listTakmicenjeSvaKola.TabIndex = 0;
            this.listTakmicenjeSvaKola.UseCompatibleStateImageBehavior = false;
            this.listTakmicenjeSvaKola.View = System.Windows.Forms.View.Details;
            this.listTakmicenjeSvaKola.DoubleClick += new System.EventHandler(this.listTakmicenjeSvaKola_DoubleClick);
            // 
            // Kolo
            // 
            this.Kolo.Text = "Kolo";
            this.Kolo.Width = 41;
            // 
            // Datum
            // 
            this.Datum.Text = "Datum";
            this.Datum.Width = 73;
            // 
            // Domacin_Kola
            // 
            this.Domacin_Kola.Text = "Domacin";
            this.Domacin_Kola.Width = 96;
            // 
            // Domacin_Rez
            // 
            this.Domacin_Rez.Text = "Br_gol";
            this.Domacin_Rez.Width = 42;
            // 
            // Gost_Rez
            // 
            this.Gost_Rez.Text = "Br_gol";
            this.Gost_Rez.Width = 44;
            // 
            // Gost_Kola
            // 
            this.Gost_Kola.Text = "Gost";
            this.Gost_Kola.Width = 121;
            // 
            // KoloID
            // 
            this.KoloID.Text = "ID";
            // 
            // buttonXTakmicenje
            // 
            this.buttonXTakmicenje.Location = new System.Drawing.Point(401, 3);
            this.buttonXTakmicenje.Name = "buttonXTakmicenje";
            this.buttonXTakmicenje.Size = new System.Drawing.Size(20, 20);
            this.buttonXTakmicenje.TabIndex = 9;
            this.buttonXTakmicenje.Text = "X";
            this.buttonXTakmicenje.UseVisualStyleBackColor = true;
            this.buttonXTakmicenje.Click += new System.EventHandler(this.buttonXTakmicenje_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.listTakmicenjeTabela);
            this.groupBox2.Controls.Add(this.groupNaredno_kolo);
            this.groupBox2.Location = new System.Drawing.Point(4, 250);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(426, 238);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Tabela";
            // 
            // listTakmicenjeTabela
            // 
            this.listTakmicenjeTabela.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.R_br,
            this.Naziv,
            this.OM,
            this.P,
            this.N,
            this.I,
            this.DG,
            this.PG,
            this.GR,
            this.PO,
            this.ID});
            this.listTakmicenjeTabela.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listTakmicenjeTabela.FullRowSelect = true;
            this.listTakmicenjeTabela.Location = new System.Drawing.Point(3, 16);
            this.listTakmicenjeTabela.Name = "listTakmicenjeTabela";
            this.listTakmicenjeTabela.Size = new System.Drawing.Size(420, 219);
            this.listTakmicenjeTabela.TabIndex = 0;
            this.listTakmicenjeTabela.UseCompatibleStateImageBehavior = false;
            this.listTakmicenjeTabela.View = System.Windows.Forms.View.Details;
            this.listTakmicenjeTabela.DoubleClick += new System.EventHandler(this.listTakmicenjeTabela_DoubleClick);
            // 
            // R_br
            // 
            this.R_br.Text = "R_br";
            this.R_br.Width = 42;
            // 
            // Naziv
            // 
            this.Naziv.Text = "Naziv tima";
            this.Naziv.Width = 152;
            // 
            // OM
            // 
            this.OM.Text = "OM";
            this.OM.Width = 31;
            // 
            // P
            // 
            this.P.Text = "P";
            this.P.Width = 19;
            // 
            // N
            // 
            this.N.Text = "N";
            this.N.Width = 21;
            // 
            // I
            // 
            this.I.Text = "I";
            this.I.Width = 23;
            // 
            // DG
            // 
            this.DG.Text = "DG";
            this.DG.Width = 29;
            // 
            // PG
            // 
            this.PG.Text = "PG";
            this.PG.Width = 28;
            // 
            // GR
            // 
            this.GR.Text = "GR";
            this.GR.Width = 30;
            // 
            // PO
            // 
            this.PO.Text = "PO";
            this.PO.Width = 31;
            // 
            // ID
            // 
            this.ID.Text = "ID";
            // 
            // groupNaredno_kolo
            // 
            this.groupNaredno_kolo.Controls.Add(this.listView2);
            this.groupNaredno_kolo.Location = new System.Drawing.Point(418, 164);
            this.groupNaredno_kolo.Name = "groupNaredno_kolo";
            this.groupNaredno_kolo.Size = new System.Drawing.Size(417, 244);
            this.groupNaredno_kolo.TabIndex = 3;
            this.groupNaredno_kolo.TabStop = false;
            this.groupNaredno_kolo.Text = "Naredno kolo";
            // 
            // listView2
            // 
            this.listView2.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Domacin,
            this.Br_gol_Domacin,
            this.Br_gol_Gost,
            this.Gost});
            this.listView2.Location = new System.Drawing.Point(10, 18);
            this.listView2.Name = "listView2";
            this.listView2.Size = new System.Drawing.Size(401, 220);
            this.listView2.TabIndex = 0;
            this.listView2.UseCompatibleStateImageBehavior = false;
            this.listView2.View = System.Windows.Forms.View.Details;
            // 
            // Domacin
            // 
            this.Domacin.Text = "Domacin";
            this.Domacin.Width = 152;
            // 
            // Br_gol_Domacin
            // 
            this.Br_gol_Domacin.Text = "Br_gol";
            this.Br_gol_Domacin.Width = 47;
            // 
            // Br_gol_Gost
            // 
            this.Br_gol_Gost.Text = "Br_gol";
            this.Br_gol_Gost.Width = 50;
            // 
            // Gost
            // 
            this.Gost.Text = "Gost";
            this.Gost.Width = 150;
            // 
            // groupTakmicenjeInformacije
            // 
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Sponzor);
            this.groupTakmicenjeInformacije.Controls.Add(this.label51);
            this.groupTakmicenjeInformacije.Controls.Add(this.comboBox_Kolo);
            this.groupTakmicenjeInformacije.Controls.Add(this.label12);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Trenutno_kolo);
            this.groupTakmicenjeInformacije.Controls.Add(this.label10);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Tip);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Pol);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Kup);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Osnovana);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Zemlja);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Asociacija);
            this.groupTakmicenjeInformacije.Controls.Add(this.label_Sport);
            this.groupTakmicenjeInformacije.Controls.Add(this.label7);
            this.groupTakmicenjeInformacije.Controls.Add(this.label6);
            this.groupTakmicenjeInformacije.Controls.Add(this.label5);
            this.groupTakmicenjeInformacije.Controls.Add(this.label4);
            this.groupTakmicenjeInformacije.Controls.Add(this.label3);
            this.groupTakmicenjeInformacije.Controls.Add(this.label9);
            this.groupTakmicenjeInformacije.Controls.Add(this.label11);
            this.groupTakmicenjeInformacije.Location = new System.Drawing.Point(-3, 74);
            this.groupTakmicenjeInformacije.Name = "groupTakmicenjeInformacije";
            this.groupTakmicenjeInformacije.Size = new System.Drawing.Size(438, 172);
            this.groupTakmicenjeInformacije.TabIndex = 6;
            this.groupTakmicenjeInformacije.TabStop = false;
            this.groupTakmicenjeInformacije.Text = "Informacije o takmicenju";
            // 
            // label_Sponzor
            // 
            this.label_Sponzor.AutoSize = true;
            this.label_Sponzor.Location = new System.Drawing.Point(346, 90);
            this.label_Sponzor.Name = "label_Sponzor";
            this.label_Sponzor.Size = new System.Drawing.Size(35, 13);
            this.label_Sponzor.TabIndex = 19;
            this.label_Sponzor.Text = "label8";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(260, 90);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(49, 13);
            this.label51.TabIndex = 18;
            this.label51.Text = "Sponzor:";
            // 
            // comboBox_Kolo
            // 
            this.comboBox_Kolo.FormattingEnabled = true;
            this.comboBox_Kolo.Location = new System.Drawing.Point(349, 149);
            this.comboBox_Kolo.Name = "comboBox_Kolo";
            this.comboBox_Kolo.Size = new System.Drawing.Size(45, 21);
            this.comboBox_Kolo.TabIndex = 17;
            this.comboBox_Kolo.SelectedIndexChanged += new System.EventHandler(this.comboBox_Kolo_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(263, 152);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = " Kolo:";
            // 
            // label_Trenutno_kolo
            // 
            this.label_Trenutno_kolo.AutoSize = true;
            this.label_Trenutno_kolo.Location = new System.Drawing.Point(346, 120);
            this.label_Trenutno_kolo.Name = "label_Trenutno_kolo";
            this.label_Trenutno_kolo.Size = new System.Drawing.Size(35, 13);
            this.label_Trenutno_kolo.TabIndex = 15;
            this.label_Trenutno_kolo.Text = "label9";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(263, 120);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(76, 13);
            this.label10.TabIndex = 14;
            this.label10.Text = "Trenutno kolo:";
            // 
            // label_Tip
            // 
            this.label_Tip.AutoSize = true;
            this.label_Tip.Location = new System.Drawing.Point(346, 57);
            this.label_Tip.Name = "label_Tip";
            this.label_Tip.Size = new System.Drawing.Size(35, 13);
            this.label_Tip.TabIndex = 13;
            this.label_Tip.Text = "label9";
            // 
            // label_Pol
            // 
            this.label_Pol.AutoSize = true;
            this.label_Pol.Location = new System.Drawing.Point(346, 27);
            this.label_Pol.Name = "label_Pol";
            this.label_Pol.Size = new System.Drawing.Size(35, 13);
            this.label_Pol.TabIndex = 12;
            this.label_Pol.Text = "label9";
            // 
            // label_Kup
            // 
            this.label_Kup.AutoSize = true;
            this.label_Kup.Location = new System.Drawing.Point(126, 156);
            this.label_Kup.Name = "label_Kup";
            this.label_Kup.Size = new System.Drawing.Size(35, 13);
            this.label_Kup.TabIndex = 11;
            this.label_Kup.Text = "label8";
            // 
            // label_Osnovana
            // 
            this.label_Osnovana.AutoSize = true;
            this.label_Osnovana.Location = new System.Drawing.Point(126, 122);
            this.label_Osnovana.Name = "label_Osnovana";
            this.label_Osnovana.Size = new System.Drawing.Size(35, 13);
            this.label_Osnovana.TabIndex = 10;
            this.label_Osnovana.Text = "label8";
            // 
            // label_Zemlja
            // 
            this.label_Zemlja.AutoSize = true;
            this.label_Zemlja.Location = new System.Drawing.Point(126, 90);
            this.label_Zemlja.Name = "label_Zemlja";
            this.label_Zemlja.Size = new System.Drawing.Size(35, 13);
            this.label_Zemlja.TabIndex = 9;
            this.label_Zemlja.Text = "label8";
            // 
            // label_Asociacija
            // 
            this.label_Asociacija.AutoSize = true;
            this.label_Asociacija.Location = new System.Drawing.Point(126, 57);
            this.label_Asociacija.Name = "label_Asociacija";
            this.label_Asociacija.Size = new System.Drawing.Size(35, 13);
            this.label_Asociacija.TabIndex = 8;
            this.label_Asociacija.Text = "label8";
            // 
            // label_Sport
            // 
            this.label_Sport.AutoSize = true;
            this.label_Sport.Location = new System.Drawing.Point(126, 27);
            this.label_Sport.Name = "label_Sport";
            this.label_Sport.Size = new System.Drawing.Size(35, 13);
            this.label_Sport.TabIndex = 7;
            this.label_Sport.Text = "label8";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(263, 57);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(25, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "Tip:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(263, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Pol:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(18, 156);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Kup:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Osnovana:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Zemlja:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(18, 57);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(102, 13);
            this.label9.TabIndex = 1;
            this.label9.Text = "Sportska asociacija:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 27);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(35, 13);
            this.label11.TabIndex = 0;
            this.label11.Text = "Sport:";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox1.Location = new System.Drawing.Point(70, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(291, 68);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // panelSudija
            // 
            this.panelSudija.AutoScroll = true;
            this.panelSudija.Controls.Add(this.pictureBox6);
            this.panelSudija.Controls.Add(this.buttonXSudija);
            this.panelSudija.Controls.Add(this.groupInformacijeSudija);
            this.panelSudija.Controls.Add(this.panel2);
            this.panelSudija.Controls.Add(this.groupSudijaTablica);
            this.panelSudija.Location = new System.Drawing.Point(1004, 654);
            this.panelSudija.Name = "panelSudija";
            this.panelSudija.Size = new System.Drawing.Size(104, 54);
            this.panelSudija.TabIndex = 14;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox6.Location = new System.Drawing.Point(92, 1);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(291, 68);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 25;
            this.pictureBox6.TabStop = false;
            // 
            // buttonXSudija
            // 
            this.buttonXSudija.Location = new System.Drawing.Point(418, 3);
            this.buttonXSudija.Name = "buttonXSudija";
            this.buttonXSudija.Size = new System.Drawing.Size(20, 20);
            this.buttonXSudija.TabIndex = 22;
            this.buttonXSudija.Text = "X";
            this.buttonXSudija.UseVisualStyleBackColor = true;
            this.buttonXSudija.Click += new System.EventHandler(this.buttonXSudija_Click);
            // 
            // groupInformacijeSudija
            // 
            this.groupInformacijeSudija.Controls.Add(this.pictureBox5);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaGrad);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaDrzava);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaGodSudjenja);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaSport);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaDatRodjenja);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaPrezime);
            this.groupInformacijeSudija.Controls.Add(this.labelSudijaIme);
            this.groupInformacijeSudija.Controls.Add(this.label9943);
            this.groupInformacijeSudija.Controls.Add(this.label43);
            this.groupInformacijeSudija.Controls.Add(this.label44);
            this.groupInformacijeSudija.Controls.Add(this.label45);
            this.groupInformacijeSudija.Controls.Add(this.label46);
            this.groupInformacijeSudija.Controls.Add(this.label47);
            this.groupInformacijeSudija.Controls.Add(this.label48);
            this.groupInformacijeSudija.Location = new System.Drawing.Point(3, 70);
            this.groupInformacijeSudija.Name = "groupInformacijeSudija";
            this.groupInformacijeSudija.Size = new System.Drawing.Size(426, 260);
            this.groupInformacijeSudija.TabIndex = 21;
            this.groupInformacijeSudija.TabStop = false;
            this.groupInformacijeSudija.Text = "Informacije o sudiji";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = global::Olimpiada.Properties.Resources.Judge_icon;
            this.pictureBox5.Location = new System.Drawing.Point(15, 48);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(163, 151);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 17;
            this.pictureBox5.TabStop = false;
            // 
            // labelSudijaGrad
            // 
            this.labelSudijaGrad.AutoSize = true;
            this.labelSudijaGrad.Location = new System.Drawing.Point(323, 235);
            this.labelSudijaGrad.Name = "labelSudijaGrad";
            this.labelSudijaGrad.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaGrad.TabIndex = 13;
            this.labelSudijaGrad.Text = "label9";
            // 
            // labelSudijaDrzava
            // 
            this.labelSudijaDrzava.AutoSize = true;
            this.labelSudijaDrzava.Location = new System.Drawing.Point(323, 202);
            this.labelSudijaDrzava.Name = "labelSudijaDrzava";
            this.labelSudijaDrzava.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaDrzava.TabIndex = 12;
            this.labelSudijaDrzava.Text = "label9";
            // 
            // labelSudijaGodSudjenja
            // 
            this.labelSudijaGodSudjenja.AutoSize = true;
            this.labelSudijaGodSudjenja.Location = new System.Drawing.Point(323, 169);
            this.labelSudijaGodSudjenja.Name = "labelSudijaGodSudjenja";
            this.labelSudijaGodSudjenja.Size = new System.Drawing.Size(41, 13);
            this.labelSudijaGodSudjenja.TabIndex = 11;
            this.labelSudijaGodSudjenja.Text = "label19";
            // 
            // labelSudijaSport
            // 
            this.labelSudijaSport.AutoSize = true;
            this.labelSudijaSport.Location = new System.Drawing.Point(323, 135);
            this.labelSudijaSport.Name = "labelSudijaSport";
            this.labelSudijaSport.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaSport.TabIndex = 10;
            this.labelSudijaSport.Text = "label8";
            // 
            // labelSudijaDatRodjenja
            // 
            this.labelSudijaDatRodjenja.AutoSize = true;
            this.labelSudijaDatRodjenja.Location = new System.Drawing.Point(323, 103);
            this.labelSudijaDatRodjenja.Name = "labelSudijaDatRodjenja";
            this.labelSudijaDatRodjenja.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaDatRodjenja.TabIndex = 9;
            this.labelSudijaDatRodjenja.Text = "label8";
            // 
            // labelSudijaPrezime
            // 
            this.labelSudijaPrezime.AutoSize = true;
            this.labelSudijaPrezime.Location = new System.Drawing.Point(323, 70);
            this.labelSudijaPrezime.Name = "labelSudijaPrezime";
            this.labelSudijaPrezime.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaPrezime.TabIndex = 8;
            this.labelSudijaPrezime.Text = "label8";
            // 
            // labelSudijaIme
            // 
            this.labelSudijaIme.AutoSize = true;
            this.labelSudijaIme.Location = new System.Drawing.Point(323, 40);
            this.labelSudijaIme.Name = "labelSudijaIme";
            this.labelSudijaIme.Size = new System.Drawing.Size(35, 13);
            this.labelSudijaIme.TabIndex = 7;
            this.labelSudijaIme.Text = "label8";
            // 
            // label9943
            // 
            this.label9943.AutoSize = true;
            this.label9943.Location = new System.Drawing.Point(215, 235);
            this.label9943.Name = "label9943";
            this.label9943.Size = new System.Drawing.Size(33, 13);
            this.label9943.TabIndex = 6;
            this.label9943.Text = "Grad:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(215, 205);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(44, 13);
            this.label43.TabIndex = 5;
            this.label43.Text = "Drzava:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(215, 169);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(86, 13);
            this.label44.TabIndex = 4;
            this.label44.Text = "Godina sudjenja:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(215, 135);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(35, 13);
            this.label45.TabIndex = 3;
            this.label45.Text = "Sport:";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(215, 103);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(81, 13);
            this.label46.TabIndex = 2;
            this.label46.Text = "Datum rodjenja:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(215, 70);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(47, 13);
            this.label47.TabIndex = 1;
            this.label47.Text = "Prezime:";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(215, 40);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(27, 13);
            this.label48.TabIndex = 0;
            this.label48.Text = "Ime:";
            // 
            // panel2
            // 
            this.panel2.Location = new System.Drawing.Point(674, 47);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(551, 641);
            this.panel2.TabIndex = 23;
            // 
            // groupSudijaTablica
            // 
            this.groupSudijaTablica.Controls.Add(this.listSudijaPojedinacni);
            this.groupSudijaTablica.Controls.Add(this.listSudijaEkipni);
            this.groupSudijaTablica.Location = new System.Drawing.Point(3, 336);
            this.groupSudijaTablica.Name = "groupSudijaTablica";
            this.groupSudijaTablica.Size = new System.Drawing.Size(426, 322);
            this.groupSudijaTablica.TabIndex = 24;
            this.groupSudijaTablica.TabStop = false;
            this.groupSudijaTablica.Text = "Istorija sudjenja";
            // 
            // listSudijaPojedinacni
            // 
            this.listSudijaPojedinacni.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21});
            this.listSudijaPojedinacni.FullRowSelect = true;
            this.listSudijaPojedinacni.Location = new System.Drawing.Point(0, 20);
            this.listSudijaPojedinacni.Name = "listSudijaPojedinacni";
            this.listSudijaPojedinacni.Size = new System.Drawing.Size(426, 262);
            this.listSudijaPojedinacni.TabIndex = 1;
            this.listSudijaPojedinacni.UseCompatibleStateImageBehavior = false;
            this.listSudijaPojedinacni.View = System.Windows.Forms.View.Details;
            this.listSudijaPojedinacni.DoubleClick += new System.EventHandler(this.listSudijaPojedinacni_DoubleClick);
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Datum";
            this.columnHeader17.Width = 91;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Naziv takmicenja";
            this.columnHeader18.Width = 202;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Disciplina";
            this.columnHeader19.Width = 74;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Pol";
            this.columnHeader20.Width = 46;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "ID";
            this.columnHeader21.Width = 43;
            // 
            // listSudijaEkipni
            // 
            this.listSudijaEkipni.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.SudijaDatum,
            this.SudijaVreme,
            this.SudijaDomacin,
            this.SudijaDomacinBrGol,
            this.SudijaGostBrGol,
            this.SudijaGost,
            this.columnHeader22});
            this.listSudijaEkipni.FullRowSelect = true;
            this.listSudijaEkipni.Location = new System.Drawing.Point(0, 20);
            this.listSudijaEkipni.Name = "listSudijaEkipni";
            this.listSudijaEkipni.Size = new System.Drawing.Size(426, 294);
            this.listSudijaEkipni.TabIndex = 0;
            this.listSudijaEkipni.UseCompatibleStateImageBehavior = false;
            this.listSudijaEkipni.View = System.Windows.Forms.View.Details;
            this.listSudijaEkipni.DoubleClick += new System.EventHandler(this.listSudijaEkipni_DoubleClick);
            // 
            // SudijaDatum
            // 
            this.SudijaDatum.Text = "Datum";
            this.SudijaDatum.Width = 77;
            // 
            // SudijaVreme
            // 
            this.SudijaVreme.Text = "Vreme";
            this.SudijaVreme.Width = 42;
            // 
            // SudijaDomacin
            // 
            this.SudijaDomacin.Text = "Domacin";
            this.SudijaDomacin.Width = 96;
            // 
            // SudijaDomacinBrGol
            // 
            this.SudijaDomacinBrGol.Text = "Br_gol";
            this.SudijaDomacinBrGol.Width = 42;
            // 
            // SudijaGostBrGol
            // 
            this.SudijaGostBrGol.Text = "Br_gol";
            this.SudijaGostBrGol.Width = 43;
            // 
            // SudijaGost
            // 
            this.SudijaGost.Text = "Gost";
            this.SudijaGost.Width = 121;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "ID";
            // 
            // panelKlub
            // 
            this.panelKlub.AutoScroll = true;
            this.panelKlub.Controls.Add(this.groupKlubSvaKola);
            this.panelKlub.Controls.Add(this.buttonXKlub);
            this.panelKlub.Controls.Add(this.groupInformacijeKlub);
            this.panelKlub.Controls.Add(this.pictureBox2);
            this.panelKlub.Controls.Add(this.groupInformacijeIgraci);
            this.panelKlub.Location = new System.Drawing.Point(503, 661);
            this.panelKlub.Name = "panelKlub";
            this.panelKlub.Size = new System.Drawing.Size(109, 70);
            this.panelKlub.TabIndex = 15;
            // 
            // groupKlubSvaKola
            // 
            this.groupKlubSvaKola.Controls.Add(this.listKlubKola);
            this.groupKlubSvaKola.Location = new System.Drawing.Point(12, 500);
            this.groupKlubSvaKola.Name = "groupKlubSvaKola";
            this.groupKlubSvaKola.Size = new System.Drawing.Size(417, 248);
            this.groupKlubSvaKola.TabIndex = 20;
            this.groupKlubSvaKola.TabStop = false;
            this.groupKlubSvaKola.Text = "Sva kola";
            // 
            // listKlubKola
            // 
            this.listKlubKola.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.KlubKolo,
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.KlubKolaID});
            this.listKlubKola.FullRowSelect = true;
            this.listKlubKola.Location = new System.Drawing.Point(0, 20);
            this.listKlubKola.Name = "listKlubKola";
            this.listKlubKola.Size = new System.Drawing.Size(411, 222);
            this.listKlubKola.TabIndex = 0;
            this.listKlubKola.UseCompatibleStateImageBehavior = false;
            this.listKlubKola.View = System.Windows.Forms.View.Details;
            this.listKlubKola.DoubleClick += new System.EventHandler(this.listKlubKola_DoubleClick);
            // 
            // KlubKolo
            // 
            this.KlubKolo.Text = "Kolo";
            this.KlubKolo.Width = 41;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Datum";
            this.columnHeader8.Width = 72;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Domacin";
            this.columnHeader9.Width = 96;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Br_gol";
            this.columnHeader10.Width = 42;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Br_gol";
            this.columnHeader11.Width = 44;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Gost";
            this.columnHeader12.Width = 121;
            // 
            // KlubKolaID
            // 
            this.KlubKolaID.Text = "ID";
            // 
            // buttonXKlub
            // 
            this.buttonXKlub.Location = new System.Drawing.Point(411, 3);
            this.buttonXKlub.Name = "buttonXKlub";
            this.buttonXKlub.Size = new System.Drawing.Size(20, 20);
            this.buttonXKlub.TabIndex = 19;
            this.buttonXKlub.Text = "X";
            this.buttonXKlub.UseVisualStyleBackColor = true;
            this.buttonXKlub.Click += new System.EventHandler(this.buttonXKlub_Click);
            // 
            // groupInformacijeKlub
            // 
            this.groupInformacijeKlub.Controls.Add(this.label8);
            this.groupInformacijeKlub.Controls.Add(this.linkLabelKlubStadion);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubSajt);
            this.groupInformacijeKlub.Controls.Add(this.label15);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubMenadzer);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubTrener);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubGlavniSponzor);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubDatOsnivanja);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubBrTitula);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubGrad);
            this.groupInformacijeKlub.Controls.Add(this.labelKlubNaziv);
            this.groupInformacijeKlub.Controls.Add(this.label23);
            this.groupInformacijeKlub.Controls.Add(this.label24);
            this.groupInformacijeKlub.Controls.Add(this.label25);
            this.groupInformacijeKlub.Controls.Add(this.label26);
            this.groupInformacijeKlub.Controls.Add(this.label27);
            this.groupInformacijeKlub.Controls.Add(this.label28);
            this.groupInformacijeKlub.Controls.Add(this.label29);
            this.groupInformacijeKlub.Location = new System.Drawing.Point(5, 72);
            this.groupInformacijeKlub.Name = "groupInformacijeKlub";
            this.groupInformacijeKlub.Size = new System.Drawing.Size(426, 172);
            this.groupInformacijeKlub.TabIndex = 17;
            this.groupInformacijeKlub.TabStop = false;
            this.groupInformacijeKlub.Text = "Informacije o klubu";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(263, 123);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(46, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Stadion:";
            // 
            // linkLabelKlubStadion
            // 
            this.linkLabelKlubStadion.AutoSize = true;
            this.linkLabelKlubStadion.Location = new System.Drawing.Point(346, 121);
            this.linkLabelKlubStadion.Name = "linkLabelKlubStadion";
            this.linkLabelKlubStadion.Size = new System.Drawing.Size(55, 13);
            this.linkLabelKlubStadion.TabIndex = 16;
            this.linkLabelKlubStadion.TabStop = true;
            this.linkLabelKlubStadion.Text = "linkLabel1";
            this.linkLabelKlubStadion.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelKlubStadion_LinkClicked);
            // 
            // labelKlubSajt
            // 
            this.labelKlubSajt.AutoSize = true;
            this.labelKlubSajt.Location = new System.Drawing.Point(346, 90);
            this.labelKlubSajt.Name = "labelKlubSajt";
            this.labelKlubSajt.Size = new System.Drawing.Size(35, 13);
            this.labelKlubSajt.TabIndex = 15;
            this.labelKlubSajt.Text = "label9";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(263, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(28, 13);
            this.label15.TabIndex = 14;
            this.label15.Text = "Sajt:";
            // 
            // labelKlubMenadzer
            // 
            this.labelKlubMenadzer.AutoSize = true;
            this.labelKlubMenadzer.Location = new System.Drawing.Point(346, 57);
            this.labelKlubMenadzer.Name = "labelKlubMenadzer";
            this.labelKlubMenadzer.Size = new System.Drawing.Size(35, 13);
            this.labelKlubMenadzer.TabIndex = 13;
            this.labelKlubMenadzer.Text = "label9";
            // 
            // labelKlubTrener
            // 
            this.labelKlubTrener.AutoSize = true;
            this.labelKlubTrener.Location = new System.Drawing.Point(346, 27);
            this.labelKlubTrener.Name = "labelKlubTrener";
            this.labelKlubTrener.Size = new System.Drawing.Size(35, 13);
            this.labelKlubTrener.TabIndex = 12;
            this.labelKlubTrener.Text = "label9";
            // 
            // labelKlubGlavniSponzor
            // 
            this.labelKlubGlavniSponzor.AutoSize = true;
            this.labelKlubGlavniSponzor.Location = new System.Drawing.Point(126, 156);
            this.labelKlubGlavniSponzor.Name = "labelKlubGlavniSponzor";
            this.labelKlubGlavniSponzor.Size = new System.Drawing.Size(41, 13);
            this.labelKlubGlavniSponzor.TabIndex = 11;
            this.labelKlubGlavniSponzor.Text = "label18";
            // 
            // labelKlubDatOsnivanja
            // 
            this.labelKlubDatOsnivanja.AutoSize = true;
            this.labelKlubDatOsnivanja.Location = new System.Drawing.Point(126, 122);
            this.labelKlubDatOsnivanja.Name = "labelKlubDatOsnivanja";
            this.labelKlubDatOsnivanja.Size = new System.Drawing.Size(35, 13);
            this.labelKlubDatOsnivanja.TabIndex = 10;
            this.labelKlubDatOsnivanja.Text = "label8";
            // 
            // labelKlubBrTitula
            // 
            this.labelKlubBrTitula.AutoSize = true;
            this.labelKlubBrTitula.Location = new System.Drawing.Point(126, 90);
            this.labelKlubBrTitula.Name = "labelKlubBrTitula";
            this.labelKlubBrTitula.Size = new System.Drawing.Size(35, 13);
            this.labelKlubBrTitula.TabIndex = 9;
            this.labelKlubBrTitula.Text = "label8";
            // 
            // labelKlubGrad
            // 
            this.labelKlubGrad.AutoSize = true;
            this.labelKlubGrad.Location = new System.Drawing.Point(126, 57);
            this.labelKlubGrad.Name = "labelKlubGrad";
            this.labelKlubGrad.Size = new System.Drawing.Size(35, 13);
            this.labelKlubGrad.TabIndex = 8;
            this.labelKlubGrad.Text = "label8";
            // 
            // labelKlubNaziv
            // 
            this.labelKlubNaziv.AutoSize = true;
            this.labelKlubNaziv.Location = new System.Drawing.Point(126, 27);
            this.labelKlubNaziv.Name = "labelKlubNaziv";
            this.labelKlubNaziv.Size = new System.Drawing.Size(35, 13);
            this.labelKlubNaziv.TabIndex = 7;
            this.labelKlubNaziv.Text = "label8";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(263, 57);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 6;
            this.label23.Text = "Menadzer:";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(263, 27);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(41, 13);
            this.label24.TabIndex = 5;
            this.label24.Text = "Trener:";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(18, 156);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Glavni sponzor:";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(18, 122);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Datum osnivanja:";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(18, 90);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(53, 13);
            this.label27.TabIndex = 2;
            this.label27.Text = "Broj titula:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(18, 57);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(33, 13);
            this.label28.TabIndex = 1;
            this.label28.Text = "Grad:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(18, 27);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(37, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "Naziv:";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox2.Location = new System.Drawing.Point(69, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(291, 68);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 16;
            this.pictureBox2.TabStop = false;
            // 
            // groupInformacijeIgraci
            // 
            this.groupInformacijeIgraci.Controls.Add(this.listViewKlubIgraci);
            this.groupInformacijeIgraci.Location = new System.Drawing.Point(3, 250);
            this.groupInformacijeIgraci.Name = "groupInformacijeIgraci";
            this.groupInformacijeIgraci.Size = new System.Drawing.Size(426, 247);
            this.groupInformacijeIgraci.TabIndex = 18;
            this.groupInformacijeIgraci.TabStop = false;
            this.groupInformacijeIgraci.Text = "Informacije o igracima";
            // 
            // listViewKlubIgraci
            // 
            this.listViewKlubIgraci.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.Broj_dresa,
            this.Ime,
            this.Prezime,
            this.KlubSportistaID});
            this.listViewKlubIgraci.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewKlubIgraci.FullRowSelect = true;
            this.listViewKlubIgraci.Location = new System.Drawing.Point(3, 16);
            this.listViewKlubIgraci.Name = "listViewKlubIgraci";
            this.listViewKlubIgraci.Size = new System.Drawing.Size(420, 228);
            this.listViewKlubIgraci.TabIndex = 0;
            this.listViewKlubIgraci.UseCompatibleStateImageBehavior = false;
            this.listViewKlubIgraci.View = System.Windows.Forms.View.Details;
            this.listViewKlubIgraci.DoubleClick += new System.EventHandler(this.listViewKlubIgraci_DoubleClick);
            // 
            // Broj_dresa
            // 
            this.Broj_dresa.Text = "Broj_dresa";
            this.Broj_dresa.Width = 79;
            // 
            // Ime
            // 
            this.Ime.Text = "Ime";
            this.Ime.Width = 141;
            // 
            // Prezime
            // 
            this.Prezime.Text = "Prezime";
            this.Prezime.Width = 188;
            // 
            // KlubSportistaID
            // 
            this.KlubSportistaID.Text = "ID";
            // 
            // panelSportista
            // 
            this.panelSportista.AutoScroll = true;
            this.panelSportista.Controls.Add(this.pictureBox3);
            this.panelSportista.Controls.Add(this.groupSportistaInformacije);
            this.panelSportista.Controls.Add(this.buttonXSportista);
            this.panelSportista.Location = new System.Drawing.Point(395, 658);
            this.panelSportista.Name = "panelSportista";
            this.panelSportista.Size = new System.Drawing.Size(60, 79);
            this.panelSportista.TabIndex = 16;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox3.Location = new System.Drawing.Point(76, 3);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(291, 68);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 17;
            this.pictureBox3.TabStop = false;
            // 
            // groupSportistaInformacije
            // 
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaBrTel);
            this.groupSportistaInformacije.Controls.Add(this.pictureBox4);
            this.groupSportistaInformacije.Controls.Add(this.label13);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaVisina);
            this.groupSportistaInformacije.Controls.Add(this.label16);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaTezina);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaDrzava);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaDres);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaKlub);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaDatRodjenja);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaPrezime);
            this.groupSportistaInformacije.Controls.Add(this.labelSportistaIme);
            this.groupSportistaInformacije.Controls.Add(this.label31);
            this.groupSportistaInformacije.Controls.Add(this.label32);
            this.groupSportistaInformacije.Controls.Add(this.label33);
            this.groupSportistaInformacije.Controls.Add(this.label34);
            this.groupSportistaInformacije.Controls.Add(this.label35);
            this.groupSportistaInformacije.Controls.Add(this.label36);
            this.groupSportistaInformacije.Controls.Add(this.label37);
            this.groupSportistaInformacije.Location = new System.Drawing.Point(3, 77);
            this.groupSportistaInformacije.Name = "groupSportistaInformacije";
            this.groupSportistaInformacije.Size = new System.Drawing.Size(427, 447);
            this.groupSportistaInformacije.TabIndex = 18;
            this.groupSportistaInformacije.TabStop = false;
            this.groupSportistaInformacije.Text = "Informacije o sportisti";
            // 
            // labelSportistaBrTel
            // 
            this.labelSportistaBrTel.AutoSize = true;
            this.labelSportistaBrTel.Location = new System.Drawing.Point(323, 300);
            this.labelSportistaBrTel.Name = "labelSportistaBrTel";
            this.labelSportistaBrTel.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaBrTel.TabIndex = 18;
            this.labelSportistaBrTel.Text = "label9";
            // 
            // pictureBox4
            // 
            this.pictureBox4.Image = global::Olimpiada.Properties.Resources.User_blue_icon;
            this.pictureBox4.Location = new System.Drawing.Point(15, 48);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(163, 151);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 17;
            this.pictureBox4.TabStop = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(215, 300);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 13);
            this.label13.TabIndex = 16;
            this.label13.Text = "Broj telefona:";
            // 
            // labelSportistaVisina
            // 
            this.labelSportistaVisina.AutoSize = true;
            this.labelSportistaVisina.Location = new System.Drawing.Point(323, 268);
            this.labelSportistaVisina.Name = "labelSportistaVisina";
            this.labelSportistaVisina.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaVisina.TabIndex = 15;
            this.labelSportistaVisina.Text = "label9";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(215, 268);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(38, 13);
            this.label16.TabIndex = 14;
            this.label16.Text = "Visina:";
            // 
            // labelSportistaTezina
            // 
            this.labelSportistaTezina.AutoSize = true;
            this.labelSportistaTezina.Location = new System.Drawing.Point(323, 235);
            this.labelSportistaTezina.Name = "labelSportistaTezina";
            this.labelSportistaTezina.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaTezina.TabIndex = 13;
            this.labelSportistaTezina.Text = "label9";
            // 
            // labelSportistaDrzava
            // 
            this.labelSportistaDrzava.AutoSize = true;
            this.labelSportistaDrzava.Location = new System.Drawing.Point(323, 202);
            this.labelSportistaDrzava.Name = "labelSportistaDrzava";
            this.labelSportistaDrzava.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaDrzava.TabIndex = 12;
            this.labelSportistaDrzava.Text = "label9";
            // 
            // labelSportistaDres
            // 
            this.labelSportistaDres.AutoSize = true;
            this.labelSportistaDres.Location = new System.Drawing.Point(323, 169);
            this.labelSportistaDres.Name = "labelSportistaDres";
            this.labelSportistaDres.Size = new System.Drawing.Size(41, 13);
            this.labelSportistaDres.TabIndex = 11;
            this.labelSportistaDres.Text = "label19";
            // 
            // labelSportistaKlub
            // 
            this.labelSportistaKlub.AutoSize = true;
            this.labelSportistaKlub.Location = new System.Drawing.Point(323, 135);
            this.labelSportistaKlub.Name = "labelSportistaKlub";
            this.labelSportistaKlub.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaKlub.TabIndex = 10;
            this.labelSportistaKlub.Text = "label8";
            // 
            // labelSportistaDatRodjenja
            // 
            this.labelSportistaDatRodjenja.AutoSize = true;
            this.labelSportistaDatRodjenja.Location = new System.Drawing.Point(323, 103);
            this.labelSportistaDatRodjenja.Name = "labelSportistaDatRodjenja";
            this.labelSportistaDatRodjenja.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaDatRodjenja.TabIndex = 9;
            this.labelSportistaDatRodjenja.Text = "label8";
            // 
            // labelSportistaPrezime
            // 
            this.labelSportistaPrezime.AutoSize = true;
            this.labelSportistaPrezime.Location = new System.Drawing.Point(323, 70);
            this.labelSportistaPrezime.Name = "labelSportistaPrezime";
            this.labelSportistaPrezime.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaPrezime.TabIndex = 8;
            this.labelSportistaPrezime.Text = "label8";
            // 
            // labelSportistaIme
            // 
            this.labelSportistaIme.AutoSize = true;
            this.labelSportistaIme.Location = new System.Drawing.Point(323, 40);
            this.labelSportistaIme.Name = "labelSportistaIme";
            this.labelSportistaIme.Size = new System.Drawing.Size(35, 13);
            this.labelSportistaIme.TabIndex = 7;
            this.labelSportistaIme.Text = "label8";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(215, 235);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(42, 13);
            this.label31.TabIndex = 6;
            this.label31.Text = "Tezina:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(215, 205);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(44, 13);
            this.label32.TabIndex = 5;
            this.label32.Text = "Drzava:";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(215, 169);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(57, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Broj dresa:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(215, 135);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(31, 13);
            this.label34.TabIndex = 3;
            this.label34.Text = "Klub:";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(215, 103);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(81, 13);
            this.label35.TabIndex = 2;
            this.label35.Text = "Datum rodjenja:";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(215, 70);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(47, 13);
            this.label36.TabIndex = 1;
            this.label36.Text = "Prezime:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(215, 40);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(27, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "Ime:";
            // 
            // buttonXSportista
            // 
            this.buttonXSportista.Location = new System.Drawing.Point(410, 10);
            this.buttonXSportista.Name = "buttonXSportista";
            this.buttonXSportista.Size = new System.Drawing.Size(20, 20);
            this.buttonXSportista.TabIndex = 19;
            this.buttonXSportista.Text = "X";
            this.buttonXSportista.UseVisualStyleBackColor = true;
            this.buttonXSportista.Click += new System.EventHandler(this.buttonXSportista_Click);
            // 
            // panelTakmicenjePojedinacno
            // 
            this.panelTakmicenjePojedinacno.AutoScroll = true;
            this.panelTakmicenjePojedinacno.Controls.Add(this.buttonXTakmicenjePojedinacno);
            this.panelTakmicenjePojedinacno.Controls.Add(this.pictureBox11);
            this.panelTakmicenjePojedinacno.Controls.Add(this.groupTakmicenjePojedinacnoInformacije);
            this.panelTakmicenjePojedinacno.Controls.Add(this.groupTakmicenjePojedinacnoRezultati);
            this.panelTakmicenjePojedinacno.Controls.Add(this.panel3);
            this.panelTakmicenjePojedinacno.Location = new System.Drawing.Point(548, 226);
            this.panelTakmicenjePojedinacno.Name = "panelTakmicenjePojedinacno";
            this.panelTakmicenjePojedinacno.Size = new System.Drawing.Size(170, 118);
            this.panelTakmicenjePojedinacno.TabIndex = 17;
            // 
            // buttonXTakmicenjePojedinacno
            // 
            this.buttonXTakmicenjePojedinacno.Location = new System.Drawing.Point(403, 26);
            this.buttonXTakmicenjePojedinacno.Name = "buttonXTakmicenjePojedinacno";
            this.buttonXTakmicenjePojedinacno.Size = new System.Drawing.Size(20, 20);
            this.buttonXTakmicenjePojedinacno.TabIndex = 22;
            this.buttonXTakmicenjePojedinacno.Text = "X";
            this.buttonXTakmicenjePojedinacno.UseVisualStyleBackColor = true;
            this.buttonXTakmicenjePojedinacno.Click += new System.EventHandler(this.buttonXTakmicenjePojedinacno_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox11.Location = new System.Drawing.Point(81, 3);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(291, 68);
            this.pictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox11.TabIndex = 18;
            this.pictureBox11.TabStop = false;
            // 
            // groupTakmicenjePojedinacnoInformacije
            // 
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.linkLabelStadion);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label14);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.linkLabelTakmicenjePojedinacnoSudija);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoSponzor);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label55);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label52);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoBrUcesnika);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label53);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoTip);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoPol);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTAkmicenjePojedinacnoDisciplina);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoDatum);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoZemlja);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoAsociacija);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.labelTakmicenjePojedinacnoSport);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label61);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label62);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label63);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label64);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label65);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label66);
            this.groupTakmicenjePojedinacnoInformacije.Controls.Add(this.label67);
            this.groupTakmicenjePojedinacnoInformacije.Location = new System.Drawing.Point(14, 77);
            this.groupTakmicenjePojedinacnoInformacije.Name = "groupTakmicenjePojedinacnoInformacije";
            this.groupTakmicenjePojedinacnoInformacije.Size = new System.Drawing.Size(420, 198);
            this.groupTakmicenjePojedinacnoInformacije.TabIndex = 19;
            this.groupTakmicenjePojedinacnoInformacije.TabStop = false;
            this.groupTakmicenjePojedinacnoInformacije.Text = "Informacije o takmicenju";
            // 
            // linkLabelStadion
            // 
            this.linkLabelStadion.AutoSize = true;
            this.linkLabelStadion.Location = new System.Drawing.Point(346, 149);
            this.linkLabelStadion.Name = "linkLabelStadion";
            this.linkLabelStadion.Size = new System.Drawing.Size(55, 13);
            this.linkLabelStadion.TabIndex = 22;
            this.linkLabelStadion.TabStop = true;
            this.linkLabelStadion.Text = "linkLabel1";
            this.linkLabelStadion.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelStadion_LinkClicked);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(263, 149);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(46, 13);
            this.label14.TabIndex = 21;
            this.label14.Text = "Stadion:";
            // 
            // linkLabelTakmicenjePojedinacnoSudija
            // 
            this.linkLabelTakmicenjePojedinacnoSudija.AutoSize = true;
            this.linkLabelTakmicenjePojedinacnoSudija.Location = new System.Drawing.Point(346, 117);
            this.linkLabelTakmicenjePojedinacnoSudija.Name = "linkLabelTakmicenjePojedinacnoSudija";
            this.linkLabelTakmicenjePojedinacnoSudija.Size = new System.Drawing.Size(55, 13);
            this.linkLabelTakmicenjePojedinacnoSudija.TabIndex = 20;
            this.linkLabelTakmicenjePojedinacnoSudija.TabStop = true;
            this.linkLabelTakmicenjePojedinacnoSudija.Text = "linkLabel1";
            this.linkLabelTakmicenjePojedinacnoSudija.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelTakmicenjePojedinacnoSudija_LinkClicked);
            // 
            // labelTakmicenjePojedinacnoSponzor
            // 
            this.labelTakmicenjePojedinacnoSponzor.AutoSize = true;
            this.labelTakmicenjePojedinacnoSponzor.Location = new System.Drawing.Point(346, 86);
            this.labelTakmicenjePojedinacnoSponzor.Name = "labelTakmicenjePojedinacnoSponzor";
            this.labelTakmicenjePojedinacnoSponzor.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoSponzor.TabIndex = 19;
            this.labelTakmicenjePojedinacnoSponzor.Text = "label9";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(263, 88);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(80, 13);
            this.label55.TabIndex = 18;
            this.label55.Text = "Glavni sponzor:";
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(263, 117);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(39, 13);
            this.label52.TabIndex = 16;
            this.label52.Text = "Sudija:";
            // 
            // labelTakmicenjePojedinacnoBrUcesnika
            // 
            this.labelTakmicenjePojedinacnoBrUcesnika.AutoSize = true;
            this.labelTakmicenjePojedinacnoBrUcesnika.Location = new System.Drawing.Point(126, 182);
            this.labelTakmicenjePojedinacnoBrUcesnika.Name = "labelTakmicenjePojedinacnoBrUcesnika";
            this.labelTakmicenjePojedinacnoBrUcesnika.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoBrUcesnika.TabIndex = 15;
            this.labelTakmicenjePojedinacnoBrUcesnika.Text = "label9";
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(18, 182);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(74, 13);
            this.label53.TabIndex = 14;
            this.label53.Text = "Broj ucesnika:";
            // 
            // labelTakmicenjePojedinacnoTip
            // 
            this.labelTakmicenjePojedinacnoTip.AutoSize = true;
            this.labelTakmicenjePojedinacnoTip.Location = new System.Drawing.Point(346, 56);
            this.labelTakmicenjePojedinacnoTip.Name = "labelTakmicenjePojedinacnoTip";
            this.labelTakmicenjePojedinacnoTip.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoTip.TabIndex = 13;
            this.labelTakmicenjePojedinacnoTip.Text = "label9";
            // 
            // labelTakmicenjePojedinacnoPol
            // 
            this.labelTakmicenjePojedinacnoPol.AutoSize = true;
            this.labelTakmicenjePojedinacnoPol.Location = new System.Drawing.Point(346, 27);
            this.labelTakmicenjePojedinacnoPol.Name = "labelTakmicenjePojedinacnoPol";
            this.labelTakmicenjePojedinacnoPol.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoPol.TabIndex = 12;
            this.labelTakmicenjePojedinacnoPol.Text = "label9";
            // 
            // labelTAkmicenjePojedinacnoDisciplina
            // 
            this.labelTAkmicenjePojedinacnoDisciplina.AutoSize = true;
            this.labelTAkmicenjePojedinacnoDisciplina.Location = new System.Drawing.Point(126, 56);
            this.labelTAkmicenjePojedinacnoDisciplina.Name = "labelTAkmicenjePojedinacnoDisciplina";
            this.labelTAkmicenjePojedinacnoDisciplina.Size = new System.Drawing.Size(41, 13);
            this.labelTAkmicenjePojedinacnoDisciplina.TabIndex = 11;
            this.labelTAkmicenjePojedinacnoDisciplina.Text = "label56";
            // 
            // labelTakmicenjePojedinacnoDatum
            // 
            this.labelTakmicenjePojedinacnoDatum.AutoSize = true;
            this.labelTakmicenjePojedinacnoDatum.Location = new System.Drawing.Point(126, 149);
            this.labelTakmicenjePojedinacnoDatum.Name = "labelTakmicenjePojedinacnoDatum";
            this.labelTakmicenjePojedinacnoDatum.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoDatum.TabIndex = 10;
            this.labelTakmicenjePojedinacnoDatum.Text = "label8";
            // 
            // labelTakmicenjePojedinacnoZemlja
            // 
            this.labelTakmicenjePojedinacnoZemlja.AutoSize = true;
            this.labelTakmicenjePojedinacnoZemlja.Location = new System.Drawing.Point(126, 117);
            this.labelTakmicenjePojedinacnoZemlja.Name = "labelTakmicenjePojedinacnoZemlja";
            this.labelTakmicenjePojedinacnoZemlja.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoZemlja.TabIndex = 9;
            this.labelTakmicenjePojedinacnoZemlja.Text = "label8";
            // 
            // labelTakmicenjePojedinacnoAsociacija
            // 
            this.labelTakmicenjePojedinacnoAsociacija.AutoSize = true;
            this.labelTakmicenjePojedinacnoAsociacija.Location = new System.Drawing.Point(126, 84);
            this.labelTakmicenjePojedinacnoAsociacija.Name = "labelTakmicenjePojedinacnoAsociacija";
            this.labelTakmicenjePojedinacnoAsociacija.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoAsociacija.TabIndex = 8;
            this.labelTakmicenjePojedinacnoAsociacija.Text = "label8";
            // 
            // labelTakmicenjePojedinacnoSport
            // 
            this.labelTakmicenjePojedinacnoSport.AutoSize = true;
            this.labelTakmicenjePojedinacnoSport.Location = new System.Drawing.Point(126, 27);
            this.labelTakmicenjePojedinacnoSport.Name = "labelTakmicenjePojedinacnoSport";
            this.labelTakmicenjePojedinacnoSport.Size = new System.Drawing.Size(35, 13);
            this.labelTakmicenjePojedinacnoSport.TabIndex = 7;
            this.labelTakmicenjePojedinacnoSport.Text = "label8";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(263, 56);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(25, 13);
            this.label61.TabIndex = 6;
            this.label61.Text = "Tip:";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(263, 27);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(25, 13);
            this.label62.TabIndex = 5;
            this.label62.Text = "Pol:";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(18, 56);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(55, 13);
            this.label63.TabIndex = 4;
            this.label63.Text = "Disciplina:";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(18, 149);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(41, 13);
            this.label64.TabIndex = 3;
            this.label64.Text = "Datum:";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(18, 117);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(41, 13);
            this.label65.TabIndex = 2;
            this.label65.Text = "Zemlja:";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(18, 84);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(102, 13);
            this.label66.TabIndex = 1;
            this.label66.Text = "Sportska asociacija:";
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(18, 27);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(35, 13);
            this.label67.TabIndex = 0;
            this.label67.Text = "Sport:";
            // 
            // groupTakmicenjePojedinacnoRezultati
            // 
            this.groupTakmicenjePojedinacnoRezultati.Controls.Add(this.listTakmicenjePojedinacno);
            this.groupTakmicenjePojedinacnoRezultati.Location = new System.Drawing.Point(3, 284);
            this.groupTakmicenjePojedinacnoRezultati.Name = "groupTakmicenjePojedinacnoRezultati";
            this.groupTakmicenjePojedinacnoRezultati.Size = new System.Drawing.Size(438, 269);
            this.groupTakmicenjePojedinacnoRezultati.TabIndex = 20;
            this.groupTakmicenjePojedinacnoRezultati.TabStop = false;
            this.groupTakmicenjePojedinacnoRezultati.Text = "Rezultati";
            // 
            // listTakmicenjePojedinacno
            // 
            this.listTakmicenjePojedinacno.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.TakmicenjePojedinacnoMesto,
            this.TakmicenjePojedinacnoIme,
            this.TakmicenjePojedinacnoPrezime,
            this.TakmicenjePojedinacnoRezultat,
            this.columnHeader23});
            this.listTakmicenjePojedinacno.FullRowSelect = true;
            this.listTakmicenjePojedinacno.Location = new System.Drawing.Point(11, 18);
            this.listTakmicenjePojedinacno.Name = "listTakmicenjePojedinacno";
            this.listTakmicenjePojedinacno.Size = new System.Drawing.Size(420, 251);
            this.listTakmicenjePojedinacno.TabIndex = 0;
            this.listTakmicenjePojedinacno.UseCompatibleStateImageBehavior = false;
            this.listTakmicenjePojedinacno.View = System.Windows.Forms.View.Details;
            this.listTakmicenjePojedinacno.DoubleClick += new System.EventHandler(this.listTakmicenjePojedinacno_DoubleClick);
            // 
            // TakmicenjePojedinacnoMesto
            // 
            this.TakmicenjePojedinacnoMesto.Text = "Mesto";
            this.TakmicenjePojedinacnoMesto.Width = 56;
            // 
            // TakmicenjePojedinacnoIme
            // 
            this.TakmicenjePojedinacnoIme.Text = "Ime";
            this.TakmicenjePojedinacnoIme.Width = 137;
            // 
            // TakmicenjePojedinacnoPrezime
            // 
            this.TakmicenjePojedinacnoPrezime.Text = "Prezime";
            this.TakmicenjePojedinacnoPrezime.Width = 143;
            // 
            // TakmicenjePojedinacnoRezultat
            // 
            this.TakmicenjePojedinacnoRezultat.Text = "Rezultat";
            this.TakmicenjePojedinacnoRezultat.Width = 150;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "ID";
            // 
            // panel3
            // 
            this.panel3.Location = new System.Drawing.Point(664, 81);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(524, 459);
            this.panel3.TabIndex = 21;
            // 
            // panelUtakmica
            // 
            this.panelUtakmica.AutoScroll = true;
            this.panelUtakmica.Controls.Add(this.buttonXUtakmicaa);
            this.panelUtakmica.Controls.Add(this.buttonXUtakmica);
            this.panelUtakmica.Controls.Add(this.groupUtakmicaInformacije);
            this.panelUtakmica.Controls.Add(this.pictureBox10);
            this.panelUtakmica.Location = new System.Drawing.Point(650, 664);
            this.panelUtakmica.Name = "panelUtakmica";
            this.panelUtakmica.Size = new System.Drawing.Size(106, 67);
            this.panelUtakmica.TabIndex = 18;
            // 
            // buttonXUtakmicaa
            // 
            this.buttonXUtakmicaa.Location = new System.Drawing.Point(410, 3);
            this.buttonXUtakmicaa.Name = "buttonXUtakmicaa";
            this.buttonXUtakmicaa.Size = new System.Drawing.Size(20, 20);
            this.buttonXUtakmicaa.TabIndex = 25;
            this.buttonXUtakmicaa.Text = "X";
            this.buttonXUtakmicaa.UseVisualStyleBackColor = true;
            this.buttonXUtakmicaa.Click += new System.EventHandler(this.buttonXStadion_Click);
            // 
            // buttonXUtakmica
            // 
            this.buttonXUtakmica.Location = new System.Drawing.Point(114, -65);
            this.buttonXUtakmica.Name = "buttonXUtakmica";
            this.buttonXUtakmica.Size = new System.Drawing.Size(20, 20);
            this.buttonXUtakmica.TabIndex = 24;
            this.buttonXUtakmica.Text = "X";
            this.buttonXUtakmica.UseVisualStyleBackColor = true;
            // 
            // groupUtakmicaInformacije
            // 
            this.groupUtakmicaInformacije.Controls.Add(this.linkLabelUtakmicaSudija);
            this.groupUtakmicaInformacije.Controls.Add(this.label21);
            this.groupUtakmicaInformacije.Controls.Add(this.linkLabelGost);
            this.groupUtakmicaInformacije.Controls.Add(this.label20);
            this.groupUtakmicaInformacije.Controls.Add(this.linkLabelDomacin);
            this.groupUtakmicaInformacije.Controls.Add(this.label18);
            this.groupUtakmicaInformacije.Controls.Add(this.linkLabelUtakmicaStadion);
            this.groupUtakmicaInformacije.Controls.Add(this.label17);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaRezultat);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaKolo);
            this.groupUtakmicaInformacije.Controls.Add(this.label19);
            this.groupUtakmicaInformacije.Controls.Add(this.pictureBox9);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaVreme);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaDatum);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaLokacija);
            this.groupUtakmicaInformacije.Controls.Add(this.labelUtakmicaGrad);
            this.groupUtakmicaInformacije.Controls.Add(this.label22);
            this.groupUtakmicaInformacije.Controls.Add(this.label30);
            this.groupUtakmicaInformacije.Controls.Add(this.label38);
            this.groupUtakmicaInformacije.Controls.Add(this.label39);
            this.groupUtakmicaInformacije.Location = new System.Drawing.Point(9, 77);
            this.groupUtakmicaInformacije.Name = "groupUtakmicaInformacije";
            this.groupUtakmicaInformacije.Size = new System.Drawing.Size(420, 461);
            this.groupUtakmicaInformacije.TabIndex = 23;
            this.groupUtakmicaInformacije.TabStop = false;
            this.groupUtakmicaInformacije.Text = "Informacije o utakmici";
            // 
            // linkLabelUtakmicaSudija
            // 
            this.linkLabelUtakmicaSudija.AutoSize = true;
            this.linkLabelUtakmicaSudija.Location = new System.Drawing.Point(321, 328);
            this.linkLabelUtakmicaSudija.Name = "linkLabelUtakmicaSudija";
            this.linkLabelUtakmicaSudija.Size = new System.Drawing.Size(55, 13);
            this.linkLabelUtakmicaSudija.TabIndex = 29;
            this.linkLabelUtakmicaSudija.TabStop = true;
            this.linkLabelUtakmicaSudija.Text = "linkLabel1";
            this.linkLabelUtakmicaSudija.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelUtakmicaSudija_LinkClicked);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(216, 328);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(39, 13);
            this.label21.TabIndex = 28;
            this.label21.Text = "Sudija:";
            // 
            // linkLabelGost
            // 
            this.linkLabelGost.AutoSize = true;
            this.linkLabelGost.Location = new System.Drawing.Point(99, 328);
            this.linkLabelGost.Name = "linkLabelGost";
            this.linkLabelGost.Size = new System.Drawing.Size(55, 13);
            this.linkLabelGost.TabIndex = 27;
            this.linkLabelGost.TabStop = true;
            this.linkLabelGost.Text = "linkLabel2";
            this.linkLabelGost.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelGost_LinkClicked);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(27, 328);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(32, 13);
            this.label20.TabIndex = 26;
            this.label20.Text = "Gost:";
            // 
            // linkLabelDomacin
            // 
            this.linkLabelDomacin.AutoSize = true;
            this.linkLabelDomacin.Location = new System.Drawing.Point(101, 292);
            this.linkLabelDomacin.Name = "linkLabelDomacin";
            this.linkLabelDomacin.Size = new System.Drawing.Size(55, 13);
            this.linkLabelDomacin.TabIndex = 25;
            this.linkLabelDomacin.TabStop = true;
            this.linkLabelDomacin.Text = "linkLabel1";
            this.linkLabelDomacin.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelDomacin_LinkClicked);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(27, 292);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 24;
            this.label18.Text = "Domacin:";
            // 
            // linkLabelUtakmicaStadion
            // 
            this.linkLabelUtakmicaStadion.AutoSize = true;
            this.linkLabelUtakmicaStadion.Location = new System.Drawing.Point(321, 292);
            this.linkLabelUtakmicaStadion.Name = "linkLabelUtakmicaStadion";
            this.linkLabelUtakmicaStadion.Size = new System.Drawing.Size(55, 13);
            this.linkLabelUtakmicaStadion.TabIndex = 23;
            this.linkLabelUtakmicaStadion.TabStop = true;
            this.linkLabelUtakmicaStadion.Text = "linkLabel1";
            this.linkLabelUtakmicaStadion.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabelUtakmicaStadion_LinkClicked);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(216, 292);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(80, 13);
            this.label17.TabIndex = 22;
            this.label17.Text = "Naziv stadiona:";
            // 
            // labelUtakmicaRezultat
            // 
            this.labelUtakmicaRezultat.AutoSize = true;
            this.labelUtakmicaRezultat.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelUtakmicaRezultat.Location = new System.Drawing.Point(37, 245);
            this.labelUtakmicaRezultat.Name = "labelUtakmicaRezultat";
            this.labelUtakmicaRezultat.Size = new System.Drawing.Size(60, 19);
            this.labelUtakmicaRezultat.TabIndex = 21;
            this.labelUtakmicaRezultat.Text = "label19";
            // 
            // labelUtakmicaKolo
            // 
            this.labelUtakmicaKolo.AutoSize = true;
            this.labelUtakmicaKolo.Location = new System.Drawing.Point(324, 181);
            this.labelUtakmicaKolo.Name = "labelUtakmicaKolo";
            this.labelUtakmicaKolo.Size = new System.Drawing.Size(41, 13);
            this.labelUtakmicaKolo.TabIndex = 20;
            this.labelUtakmicaKolo.Text = "label19";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(216, 181);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(31, 13);
            this.label19.TabIndex = 19;
            this.label19.Text = "Kolo:";
            // 
            // pictureBox9
            // 
            this.pictureBox9.Image = global::Olimpiada.Properties.Resources.Sport_football_pitch_icon;
            this.pictureBox9.Location = new System.Drawing.Point(11, 40);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(163, 151);
            this.pictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox9.TabIndex = 17;
            this.pictureBox9.TabStop = false;
            // 
            // labelUtakmicaVreme
            // 
            this.labelUtakmicaVreme.AutoSize = true;
            this.labelUtakmicaVreme.Location = new System.Drawing.Point(324, 145);
            this.labelUtakmicaVreme.Name = "labelUtakmicaVreme";
            this.labelUtakmicaVreme.Size = new System.Drawing.Size(41, 13);
            this.labelUtakmicaVreme.TabIndex = 11;
            this.labelUtakmicaVreme.Text = "label19";
            // 
            // labelUtakmicaDatum
            // 
            this.labelUtakmicaDatum.AutoSize = true;
            this.labelUtakmicaDatum.Location = new System.Drawing.Point(324, 111);
            this.labelUtakmicaDatum.Name = "labelUtakmicaDatum";
            this.labelUtakmicaDatum.Size = new System.Drawing.Size(35, 13);
            this.labelUtakmicaDatum.TabIndex = 10;
            this.labelUtakmicaDatum.Text = "label8";
            // 
            // labelUtakmicaLokacija
            // 
            this.labelUtakmicaLokacija.AutoSize = true;
            this.labelUtakmicaLokacija.Location = new System.Drawing.Point(324, 79);
            this.labelUtakmicaLokacija.Name = "labelUtakmicaLokacija";
            this.labelUtakmicaLokacija.Size = new System.Drawing.Size(35, 13);
            this.labelUtakmicaLokacija.TabIndex = 9;
            this.labelUtakmicaLokacija.Text = "label8";
            // 
            // labelUtakmicaGrad
            // 
            this.labelUtakmicaGrad.AutoSize = true;
            this.labelUtakmicaGrad.Location = new System.Drawing.Point(324, 46);
            this.labelUtakmicaGrad.Name = "labelUtakmicaGrad";
            this.labelUtakmicaGrad.Size = new System.Drawing.Size(35, 13);
            this.labelUtakmicaGrad.TabIndex = 8;
            this.labelUtakmicaGrad.Text = "label8";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(216, 145);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(40, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Vreme:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(216, 111);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(41, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Datum:";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(216, 79);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(50, 13);
            this.label38.TabIndex = 2;
            this.label38.Text = "Lokacija:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(216, 46);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(33, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Grad:";
            // 
            // pictureBox10
            // 
            this.pictureBox10.Image = global::Olimpiada.Properties.Resources.coollogo_com_18122022;
            this.pictureBox10.Location = new System.Drawing.Point(71, 3);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(291, 68);
            this.pictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox10.TabIndex = 22;
            this.pictureBox10.TabStop = false;
            // 
            // Pocetna
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1370, 749);
            this.Controls.Add(this.panelUtakmica);
            this.Controls.Add(this.panelTakmicenjePojedinacno);
            this.Controls.Add(this.panelSportista);
            this.Controls.Add(this.panelKlub);
            this.Controls.Add(this.panelSudija);
            this.Controls.Add(this.panelTakmicenje);
            this.Controls.Add(this.panelStadion);
            this.Controls.Add(this.navBarControl1);
            this.Controls.Add(this.adminGroupbox);
            this.Name = "Pocetna";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.adminGroupbox.ResumeLayout(false);
            this.adminGroupbox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.navBarControl1)).EndInit();
            this.panelStadion.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            this.groupStadionTabela.ResumeLayout(false);
            this.groupStadionInformacije.ResumeLayout(false);
            this.groupStadionInformacije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.panelTakmicenje.ResumeLayout(false);
            this.groupTrenutnoKolo.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupNaredno_kolo.ResumeLayout(false);
            this.groupTakmicenjeInformacije.ResumeLayout(false);
            this.groupTakmicenjeInformacije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panelSudija.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupInformacijeSudija.ResumeLayout(false);
            this.groupInformacijeSudija.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.groupSudijaTablica.ResumeLayout(false);
            this.panelKlub.ResumeLayout(false);
            this.groupKlubSvaKola.ResumeLayout(false);
            this.groupInformacijeKlub.ResumeLayout(false);
            this.groupInformacijeKlub.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupInformacijeIgraci.ResumeLayout(false);
            this.panelSportista.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupSportistaInformacije.ResumeLayout(false);
            this.groupSportistaInformacije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.panelTakmicenjePojedinacno.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            this.groupTakmicenjePojedinacnoInformacije.ResumeLayout(false);
            this.groupTakmicenjePojedinacnoInformacije.PerformLayout();
            this.groupTakmicenjePojedinacnoRezultati.ResumeLayout(false);
            this.panelUtakmica.ResumeLayout(false);
            this.groupUtakmicaInformacije.ResumeLayout(false);
            this.groupUtakmicaInformacije.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.GroupBox adminGroupbox;
        private System.Windows.Forms.Button buttonLoginAdmin;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraNavBar.NavBarControl navBarControl1;
        private System.Windows.Forms.Panel panelStadion;
        private System.Windows.Forms.Button buttonXStadion;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.GroupBox groupStadionTabela;
        private System.Windows.Forms.ListView listStadionLiga;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Button buttonStadionX;
        private System.Windows.Forms.GroupBox groupStadionInformacije;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.Label labelStadionGodIzgradnje;
        private System.Windows.Forms.Label labelStadionKapacitet;
        private System.Windows.Forms.Label labelStadionLokacija;
        private System.Windows.Forms.Label labelStadionGrad;
        private System.Windows.Forms.Label labelStadionNaziv;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.Panel panelTakmicenje;
        private System.Windows.Forms.GroupBox groupTrenutnoKolo;
        private System.Windows.Forms.ListView listTakmicenjeSvaKola;
        private System.Windows.Forms.ColumnHeader Kolo;
        private System.Windows.Forms.ColumnHeader Datum;
        private System.Windows.Forms.ColumnHeader Domacin_Kola;
        private System.Windows.Forms.ColumnHeader Domacin_Rez;
        private System.Windows.Forms.ColumnHeader Gost_Rez;
        private System.Windows.Forms.ColumnHeader Gost_Kola;
        private System.Windows.Forms.Button buttonXTakmicenje;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ListView listTakmicenjeTabela;
        private System.Windows.Forms.ColumnHeader R_br;
        private System.Windows.Forms.ColumnHeader OM;
        private System.Windows.Forms.ColumnHeader P;
        private System.Windows.Forms.ColumnHeader N;
        private System.Windows.Forms.ColumnHeader I;
        private System.Windows.Forms.ColumnHeader DG;
        private System.Windows.Forms.ColumnHeader PG;
        private System.Windows.Forms.ColumnHeader GR;
        private System.Windows.Forms.ColumnHeader PO;
        private System.Windows.Forms.ColumnHeader Naziv;
        private System.Windows.Forms.GroupBox groupNaredno_kolo;
        private System.Windows.Forms.ListView listView2;
        private System.Windows.Forms.ColumnHeader Domacin;
        private System.Windows.Forms.ColumnHeader Br_gol_Domacin;
        private System.Windows.Forms.ColumnHeader Br_gol_Gost;
        private System.Windows.Forms.ColumnHeader Gost;
        private System.Windows.Forms.GroupBox groupTakmicenjeInformacije;
        private System.Windows.Forms.Label label_Sponzor;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.ComboBox comboBox_Kolo;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label_Trenutno_kolo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label_Tip;
        private System.Windows.Forms.Label label_Pol;
        private System.Windows.Forms.Label label_Kup;
        private System.Windows.Forms.Label label_Osnovana;
        private System.Windows.Forms.Label label_Zemlja;
        private System.Windows.Forms.Label label_Asociacija;
        private System.Windows.Forms.Label label_Sport;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panelSudija;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Button buttonXSudija;
        private System.Windows.Forms.GroupBox groupInformacijeSudija;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.Label labelSudijaGrad;
        private System.Windows.Forms.Label labelSudijaDrzava;
        private System.Windows.Forms.Label labelSudijaGodSudjenja;
        private System.Windows.Forms.Label labelSudijaSport;
        private System.Windows.Forms.Label labelSudijaDatRodjenja;
        private System.Windows.Forms.Label labelSudijaPrezime;
        private System.Windows.Forms.Label labelSudijaIme;
        private System.Windows.Forms.Label label9943;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.GroupBox groupSudijaTablica;
        private System.Windows.Forms.ListView listSudijaEkipni;
        private System.Windows.Forms.ColumnHeader SudijaDatum;
        private System.Windows.Forms.ColumnHeader SudijaVreme;
        private System.Windows.Forms.ColumnHeader SudijaDomacin;
        private System.Windows.Forms.ColumnHeader SudijaDomacinBrGol;
        private System.Windows.Forms.ColumnHeader SudijaGostBrGol;
        private System.Windows.Forms.ColumnHeader SudijaGost;
        private System.Windows.Forms.Panel panelKlub;
        private System.Windows.Forms.Button buttonXKlub;
        private System.Windows.Forms.GroupBox groupInformacijeKlub;
        private System.Windows.Forms.Label labelKlubSajt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label labelKlubMenadzer;
        private System.Windows.Forms.Label labelKlubTrener;
        private System.Windows.Forms.Label labelKlubGlavniSponzor;
        private System.Windows.Forms.Label labelKlubDatOsnivanja;
        private System.Windows.Forms.Label labelKlubBrTitula;
        private System.Windows.Forms.Label labelKlubGrad;
        private System.Windows.Forms.Label labelKlubNaziv;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupInformacijeIgraci;
        private System.Windows.Forms.ListView listViewKlubIgraci;
        private System.Windows.Forms.ColumnHeader Broj_dresa;
        private System.Windows.Forms.ColumnHeader Ime;
        private System.Windows.Forms.ColumnHeader Prezime;
        private System.Windows.Forms.Panel panelSportista;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupSportistaInformacije;
        private System.Windows.Forms.Label labelSportistaBrTel;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label labelSportistaVisina;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label labelSportistaTezina;
        private System.Windows.Forms.Label labelSportistaDrzava;
        private System.Windows.Forms.Label labelSportistaDres;
        private System.Windows.Forms.Label labelSportistaKlub;
        private System.Windows.Forms.Label labelSportistaDatRodjenja;
        private System.Windows.Forms.Label labelSportistaPrezime;
        private System.Windows.Forms.Label labelSportistaIme;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button buttonXSportista;
        private System.Windows.Forms.Panel panelTakmicenjePojedinacno;
        private System.Windows.Forms.Button buttonXTakmicenjePojedinacno;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.GroupBox groupTakmicenjePojedinacnoInformacije;
        private System.Windows.Forms.LinkLabel linkLabelTakmicenjePojedinacnoSudija;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoSponzor;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoBrUcesnika;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoTip;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoPol;
        private System.Windows.Forms.Label labelTAkmicenjePojedinacnoDisciplina;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoDatum;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoZemlja;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoAsociacija;
        private System.Windows.Forms.Label labelTakmicenjePojedinacnoSport;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.GroupBox groupTakmicenjePojedinacnoRezultati;
        private System.Windows.Forms.ListView listTakmicenjePojedinacno;
        private System.Windows.Forms.ColumnHeader TakmicenjePojedinacnoMesto;
        private System.Windows.Forms.ColumnHeader TakmicenjePojedinacnoIme;
        private System.Windows.Forms.ColumnHeader TakmicenjePojedinacnoPrezime;
        private System.Windows.Forms.ColumnHeader TakmicenjePojedinacnoRezultat;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panelUtakmica;
        private System.Windows.Forms.Button buttonXUtakmicaa;
        private System.Windows.Forms.Button buttonXUtakmica;
        private System.Windows.Forms.GroupBox groupUtakmicaInformacije;
        private System.Windows.Forms.LinkLabel linkLabelUtakmicaSudija;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.LinkLabel linkLabelGost;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.LinkLabel linkLabelDomacin;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.LinkLabel linkLabelUtakmicaStadion;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label labelUtakmicaRezultat;
        private System.Windows.Forms.Label labelUtakmicaKolo;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.Label labelUtakmicaVreme;
        private System.Windows.Forms.Label labelUtakmicaDatum;
        private System.Windows.Forms.Label labelUtakmicaLokacija;
        private System.Windows.Forms.Label labelUtakmicaGrad;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.PictureBox pictureBox10;
        public System.Windows.Forms.ColumnHeader ID;
        private System.Windows.Forms.ColumnHeader KoloID;
        private System.Windows.Forms.GroupBox groupKlubSvaKola;
        private System.Windows.Forms.ListView listKlubKola;
        private System.Windows.Forms.ColumnHeader KlubKolo;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader KlubKolaID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel linkLabelKlubStadion;
        private System.Windows.Forms.ColumnHeader KlubSportistaID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListView listStadionTakmicenj;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader StadionID;
        private System.Windows.Forms.ListView listSudijaPojedinacni;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.LinkLabel linkLabelStadion;
        private System.Windows.Forms.Label label14;
    }
}

