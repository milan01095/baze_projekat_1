﻿namespace Olimpiada
{
    partial class TakmicenjeEkipnoForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBoxSponzor = new System.Windows.Forms.TextBox();
            this.textBoxDomaciKup = new System.Windows.Forms.TextBox();
            this.labelTelefon = new System.Windows.Forms.Label();
            this.labelDrzava = new System.Windows.Forms.Label();
            this.buttonIzmeni = new System.Windows.Forms.Button();
            this.buttonKreiraj = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.dateTimePickerDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.labelDatumRodjenja = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.textBoxZemlja = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBoxSponzor);
            this.groupBox1.Controls.Add(this.textBoxDomaciKup);
            this.groupBox1.Controls.Add(this.labelTelefon);
            this.groupBox1.Controls.Add(this.labelDrzava);
            this.groupBox1.Controls.Add(this.buttonIzmeni);
            this.groupBox1.Controls.Add(this.buttonKreiraj);
            this.groupBox1.Controls.Add(this.label);
            this.groupBox1.Controls.Add(this.dateTimePickerDatumRodjenja);
            this.groupBox1.Controls.Add(this.textBoxNaziv);
            this.groupBox1.Controls.Add(this.labelDatumRodjenja);
            this.groupBox1.Controls.Add(this.labelPrezime);
            this.groupBox1.Controls.Add(this.textBoxZemlja);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(368, 259);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Takmicenje";
            // 
            // textBoxSponzor
            // 
            this.textBoxSponzor.Location = new System.Drawing.Point(133, 163);
            this.textBoxSponzor.Name = "textBoxSponzor";
            this.textBoxSponzor.Size = new System.Drawing.Size(123, 20);
            this.textBoxSponzor.TabIndex = 29;
            // 
            // textBoxDomaciKup
            // 
            this.textBoxDomaciKup.Location = new System.Drawing.Point(133, 126);
            this.textBoxDomaciKup.Name = "textBoxDomaciKup";
            this.textBoxDomaciKup.Size = new System.Drawing.Size(123, 20);
            this.textBoxDomaciKup.TabIndex = 28;
            // 
            // labelTelefon
            // 
            this.labelTelefon.AutoSize = true;
            this.labelTelefon.Location = new System.Drawing.Point(16, 133);
            this.labelTelefon.Name = "labelTelefon";
            this.labelTelefon.Size = new System.Drawing.Size(64, 13);
            this.labelTelefon.TabIndex = 26;
            this.labelTelefon.Text = "Domaci kup";
            // 
            // labelDrzava
            // 
            this.labelDrzava.AutoSize = true;
            this.labelDrzava.Location = new System.Drawing.Point(15, 170);
            this.labelDrzava.Name = "labelDrzava";
            this.labelDrzava.Size = new System.Drawing.Size(46, 13);
            this.labelDrzava.TabIndex = 22;
            this.labelDrzava.Text = "Sponzor";
            // 
            // buttonIzmeni
            // 
            this.buttonIzmeni.Location = new System.Drawing.Point(165, 217);
            this.buttonIzmeni.Name = "buttonIzmeni";
            this.buttonIzmeni.Size = new System.Drawing.Size(91, 23);
            this.buttonIzmeni.TabIndex = 15;
            this.buttonIzmeni.Text = "Izmeni takmicenje";
            this.buttonIzmeni.UseVisualStyleBackColor = true;
            this.buttonIzmeni.Click += new System.EventHandler(this.buttonIzmeni_Click);
            // 
            // buttonKreiraj
            // 
            this.buttonKreiraj.Location = new System.Drawing.Point(60, 217);
            this.buttonKreiraj.Name = "buttonKreiraj";
            this.buttonKreiraj.Size = new System.Drawing.Size(99, 23);
            this.buttonKreiraj.TabIndex = 6;
            this.buttonKreiraj.Text = "Kreiraj takmicenje";
            this.buttonKreiraj.UseVisualStyleBackColor = true;
            this.buttonKreiraj.Click += new System.EventHandler(this.buttonKreiraj_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(15, 31);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(34, 13);
            this.label.TabIndex = 2;
            this.label.Text = "Naziv";
            // 
            // dateTimePickerDatumRodjenja
            // 
            this.dateTimePickerDatumRodjenja.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePickerDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDatumRodjenja.Location = new System.Drawing.Point(133, 89);
            this.dateTimePickerDatumRodjenja.Name = "dateTimePickerDatumRodjenja";
            this.dateTimePickerDatumRodjenja.Size = new System.Drawing.Size(123, 20);
            this.dateTimePickerDatumRodjenja.TabIndex = 5;
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(133, 32);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(123, 20);
            this.textBoxNaziv.TabIndex = 0;
            // 
            // labelDatumRodjenja
            // 
            this.labelDatumRodjenja.AutoSize = true;
            this.labelDatumRodjenja.Location = new System.Drawing.Point(15, 96);
            this.labelDatumRodjenja.Name = "labelDatumRodjenja";
            this.labelDatumRodjenja.Size = new System.Drawing.Size(86, 13);
            this.labelDatumRodjenja.TabIndex = 4;
            this.labelDatumRodjenja.Text = "Datum osnivanja";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(15, 63);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(38, 13);
            this.labelPrezime.TabIndex = 3;
            this.labelPrezime.Text = "Zemlja";
            // 
            // textBoxZemlja
            // 
            this.textBoxZemlja.Location = new System.Drawing.Point(133, 63);
            this.textBoxZemlja.Name = "textBoxZemlja";
            this.textBoxZemlja.Size = new System.Drawing.Size(123, 20);
            this.textBoxZemlja.TabIndex = 1;
            // 
            // TakmicenjeEkipnoForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(418, 288);
            this.Controls.Add(this.groupBox1);
            this.Name = "TakmicenjeEkipnoForm";
            this.Text = "TakmicenjeEkipnoForm";
            this.Load += new System.EventHandler(this.TakmicenjeEkipnoForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBoxSponzor;
        private System.Windows.Forms.TextBox textBoxDomaciKup;
        private System.Windows.Forms.Label labelTelefon;
        private System.Windows.Forms.Label labelDrzava;
        private System.Windows.Forms.Button buttonIzmeni;
        private System.Windows.Forms.Button buttonKreiraj;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.DateTimePicker dateTimePickerDatumRodjenja;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.Label labelDatumRodjenja;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.TextBox textBoxZemlja;
    }
}