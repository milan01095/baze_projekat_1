﻿namespace Olimpiada
{
    partial class SudijaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxUtakmica = new System.Windows.Forms.ComboBox();
            this.comboBoxTakmicenjePojedinacna = new System.Windows.Forms.ComboBox();
            this.radioButtonKreirajSudiju = new System.Windows.Forms.RadioButton();
            this.buttonIzmeni = new System.Windows.Forms.Button();
            this.buttonDodaj = new System.Windows.Forms.Button();
            this.dateTimePickerDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.textBoxGrad = new System.Windows.Forms.TextBox();
            this.textBoxDrzava = new System.Windows.Forms.TextBox();
            this.labelGrad = new System.Windows.Forms.Label();
            this.labelDrzava = new System.Windows.Forms.Label();
            this.labelGodinaSudjenja = new System.Windows.Forms.Label();
            this.labelDatumRodjenja = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.labelIme = new System.Windows.Forms.Label();
            this.textBoxGodinaSudjenja = new System.Windows.Forms.TextBox();
            this.textBoxPrezime = new System.Windows.Forms.TextBox();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxUtakmica);
            this.groupBox1.Controls.Add(this.comboBoxTakmicenjePojedinacna);
            this.groupBox1.Controls.Add(this.radioButtonKreirajSudiju);
            this.groupBox1.Controls.Add(this.buttonIzmeni);
            this.groupBox1.Controls.Add(this.buttonDodaj);
            this.groupBox1.Controls.Add(this.dateTimePickerDatumRodjenja);
            this.groupBox1.Controls.Add(this.textBoxGrad);
            this.groupBox1.Controls.Add(this.textBoxDrzava);
            this.groupBox1.Controls.Add(this.labelGrad);
            this.groupBox1.Controls.Add(this.labelDrzava);
            this.groupBox1.Controls.Add(this.labelGodinaSudjenja);
            this.groupBox1.Controls.Add(this.labelDatumRodjenja);
            this.groupBox1.Controls.Add(this.labelPrezime);
            this.groupBox1.Controls.Add(this.labelIme);
            this.groupBox1.Controls.Add(this.textBoxGodinaSudjenja);
            this.groupBox1.Controls.Add(this.textBoxPrezime);
            this.groupBox1.Controls.Add(this.textBoxIme);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(311, 357);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sudija";
            // 
            // comboBoxUtakmica
            // 
            this.comboBoxUtakmica.FormattingEnabled = true;
            this.comboBoxUtakmica.Location = new System.Drawing.Point(5, 18);
            this.comboBoxUtakmica.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxUtakmica.Name = "comboBoxUtakmica";
            this.comboBoxUtakmica.Size = new System.Drawing.Size(109, 21);
            this.comboBoxUtakmica.TabIndex = 24;
            this.comboBoxUtakmica.SelectedIndexChanged += new System.EventHandler(this.comboBoxUtakmica_SelectedIndexChanged);
            // 
            // comboBoxTakmicenjePojedinacna
            // 
            this.comboBoxTakmicenjePojedinacna.FormattingEnabled = true;
            this.comboBoxTakmicenjePojedinacna.Location = new System.Drawing.Point(118, 18);
            this.comboBoxTakmicenjePojedinacna.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.comboBoxTakmicenjePojedinacna.Name = "comboBoxTakmicenjePojedinacna";
            this.comboBoxTakmicenjePojedinacna.Size = new System.Drawing.Size(110, 21);
            this.comboBoxTakmicenjePojedinacna.TabIndex = 23;
            this.comboBoxTakmicenjePojedinacna.SelectedIndexChanged += new System.EventHandler(this.comboBoxTakmicenjePojedinacna_SelectedIndexChanged);
            // 
            // radioButtonKreirajSudiju
            // 
            this.radioButtonKreirajSudiju.AutoSize = true;
            this.radioButtonKreirajSudiju.Location = new System.Drawing.Point(25, 56);
            this.radioButtonKreirajSudiju.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.radioButtonKreirajSudiju.Name = "radioButtonKreirajSudiju";
            this.radioButtonKreirajSudiju.Size = new System.Drawing.Size(84, 17);
            this.radioButtonKreirajSudiju.TabIndex = 22;
            this.radioButtonKreirajSudiju.TabStop = true;
            this.radioButtonKreirajSudiju.Text = "Kreiraj sudiju";
            this.radioButtonKreirajSudiju.UseVisualStyleBackColor = true;
            this.radioButtonKreirajSudiju.CheckedChanged += new System.EventHandler(this.radioButtonKreirajSudiju_CheckedChanged);
            // 
            // buttonIzmeni
            // 
            this.buttonIzmeni.Location = new System.Drawing.Point(140, 305);
            this.buttonIzmeni.Name = "buttonIzmeni";
            this.buttonIzmeni.Size = new System.Drawing.Size(88, 23);
            this.buttonIzmeni.TabIndex = 21;
            this.buttonIzmeni.Text = "Izmeni sudiju";
            this.buttonIzmeni.UseVisualStyleBackColor = true;
            this.buttonIzmeni.Click += new System.EventHandler(this.buttonIzmeni_Click);
            // 
            // buttonDodaj
            // 
            this.buttonDodaj.Location = new System.Drawing.Point(4, 305);
            this.buttonDodaj.Name = "buttonDodaj";
            this.buttonDodaj.Size = new System.Drawing.Size(88, 23);
            this.buttonDodaj.TabIndex = 20;
            this.buttonDodaj.Text = "Kreiraj sudiju";
            this.buttonDodaj.UseVisualStyleBackColor = true;
            this.buttonDodaj.Click += new System.EventHandler(this.buttonDodaj_Click);
            // 
            // dateTimePickerDatumRodjenja
            // 
            this.dateTimePickerDatumRodjenja.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePickerDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDatumRodjenja.Location = new System.Drawing.Point(106, 149);
            this.dateTimePickerDatumRodjenja.Name = "dateTimePickerDatumRodjenja";
            this.dateTimePickerDatumRodjenja.Size = new System.Drawing.Size(122, 20);
            this.dateTimePickerDatumRodjenja.TabIndex = 16;
            // 
            // textBoxGrad
            // 
            this.textBoxGrad.Location = new System.Drawing.Point(106, 260);
            this.textBoxGrad.Name = "textBoxGrad";
            this.textBoxGrad.Size = new System.Drawing.Size(122, 20);
            this.textBoxGrad.TabIndex = 13;
            // 
            // textBoxDrzava
            // 
            this.textBoxDrzava.Location = new System.Drawing.Point(106, 224);
            this.textBoxDrzava.Name = "textBoxDrzava";
            this.textBoxDrzava.Size = new System.Drawing.Size(122, 20);
            this.textBoxDrzava.TabIndex = 12;
            // 
            // labelGrad
            // 
            this.labelGrad.AutoSize = true;
            this.labelGrad.Location = new System.Drawing.Point(6, 263);
            this.labelGrad.Name = "labelGrad";
            this.labelGrad.Size = new System.Drawing.Size(33, 13);
            this.labelGrad.TabIndex = 11;
            this.labelGrad.Text = "Grad:";
            // 
            // labelDrzava
            // 
            this.labelDrzava.AutoSize = true;
            this.labelDrzava.Location = new System.Drawing.Point(6, 227);
            this.labelDrzava.Name = "labelDrzava";
            this.labelDrzava.Size = new System.Drawing.Size(44, 13);
            this.labelDrzava.TabIndex = 10;
            this.labelDrzava.Text = "Drzava:";
            // 
            // labelGodinaSudjenja
            // 
            this.labelGodinaSudjenja.AutoSize = true;
            this.labelGodinaSudjenja.Location = new System.Drawing.Point(6, 190);
            this.labelGodinaSudjenja.Name = "labelGodinaSudjenja";
            this.labelGodinaSudjenja.Size = new System.Drawing.Size(86, 13);
            this.labelGodinaSudjenja.TabIndex = 7;
            this.labelGodinaSudjenja.Text = "Godina sudjenja:";
            // 
            // labelDatumRodjenja
            // 
            this.labelDatumRodjenja.AutoSize = true;
            this.labelDatumRodjenja.Location = new System.Drawing.Point(6, 156);
            this.labelDatumRodjenja.Name = "labelDatumRodjenja";
            this.labelDatumRodjenja.Size = new System.Drawing.Size(81, 13);
            this.labelDatumRodjenja.TabIndex = 6;
            this.labelDatumRodjenja.Text = "Datum rodjenja:";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(6, 121);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(47, 13);
            this.labelPrezime.TabIndex = 5;
            this.labelPrezime.Text = "Prezime:";
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(6, 87);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(27, 13);
            this.labelIme.TabIndex = 4;
            this.labelIme.Text = "Ime:";
            // 
            // textBoxGodinaSudjenja
            // 
            this.textBoxGodinaSudjenja.Location = new System.Drawing.Point(106, 187);
            this.textBoxGodinaSudjenja.Name = "textBoxGodinaSudjenja";
            this.textBoxGodinaSudjenja.Size = new System.Drawing.Size(122, 20);
            this.textBoxGodinaSudjenja.TabIndex = 3;
            // 
            // textBoxPrezime
            // 
            this.textBoxPrezime.Location = new System.Drawing.Point(106, 119);
            this.textBoxPrezime.Name = "textBoxPrezime";
            this.textBoxPrezime.Size = new System.Drawing.Size(122, 20);
            this.textBoxPrezime.TabIndex = 1;
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(106, 84);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(122, 20);
            this.textBoxIme.TabIndex = 0;
            // 
            // SudijaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(395, 396);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "SudijaForm";
            this.Text = "SudijaForm";
            this.Load += new System.EventHandler(this.SudijaForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonIzmeni;
        private System.Windows.Forms.Button buttonDodaj;
        private System.Windows.Forms.DateTimePicker dateTimePickerDatumRodjenja;
        private System.Windows.Forms.TextBox textBoxGrad;
        private System.Windows.Forms.TextBox textBoxDrzava;
        private System.Windows.Forms.Label labelGrad;
        private System.Windows.Forms.Label labelDrzava;
        private System.Windows.Forms.Label labelGodinaSudjenja;
        private System.Windows.Forms.Label labelDatumRodjenja;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.TextBox textBoxGodinaSudjenja;
        private System.Windows.Forms.TextBox textBoxPrezime;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.RadioButton radioButtonKreirajSudiju;
        private System.Windows.Forms.ComboBox comboBoxUtakmica;
        private System.Windows.Forms.ComboBox comboBoxTakmicenjePojedinacna;
    }
}