﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class DisciplinaForm : Form
    {
        GraphClient client;
        List<Disciplina> discipline;
        List<Takmicenje> takmicenja;
        Sport selektovaniSport = null;
        int selectedIndex= -1;

        public DisciplinaForm(Sport sport)
        {
            InitializeComponent();
            selektovaniSport = sport;
        }

        private void DisciplinaForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            var query = new Neo4jClient.Cypher.CypherQuery(" match(n:Takmicenje) return n",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);

            var query1 = new Neo4jClient.Cypher.CypherQuery(" match(n:Disciplina) return n",
                                                        new Dictionary<string, object>(), CypherResultMode.Set);

            takmicenja = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();
            discipline = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query1).ToList();

            foreach (Disciplina disciplina in discipline)
            {
                comboBoxOdaberiteDisciplinu.Items.Add(disciplina.Naziv+" "+disciplina.Pol);
                comboBoxDisciplineVeza.Items.Add(disciplina.Naziv + " " + disciplina.Pol);
            }

            foreach (Takmicenje tak in takmicenja)
                comboBoxTakmicenjaVeza.Items.Add(tak.Naziv + " " + tak.Zemlja);

            radioButtonMuski.Checked = true;
        }

        private void buttonKreirajDisciplinu_Click(object sender, EventArgs e)
        {
            if(textBoxNazivDiscipline.Text != "" && comboBoxTip.Text != "" &&(radioButtonMuski.Checked == true || radioButtonZenski.Checked == true))
            {
                string pol = "";
                if (radioButtonMuski.Checked)
                    pol = "M";
                else
                    pol = "Z";
                Disciplina disciplina = Disciplina.Create(client, textBoxNazivDiscipline.Text, pol, comboBoxTip.Text);

                Sport.CreateSportDisciplina(client, selektovaniSport, disciplina);

                comboBoxOdaberiteDisciplinu.Items.Add(disciplina.Naziv + " " + disciplina.Pol);
                comboBoxDisciplineVeza.Items.Add(disciplina.Naziv + " " + disciplina.Pol);
                discipline.Add(disciplina);
            }
        }

        private void buttonIzmeniDisciplinu_Click(object sender, EventArgs e)
        {
            if (textBoxNazivDiscipline.Text != "" && comboBoxTip.Text != "" && (radioButtonMuski.Checked == true || radioButtonZenski.Checked == true))
            {
                string pol = "";
                if (radioButtonMuski.Checked)
                    pol = "M";
                else
                    pol = "Z";
                Disciplina ds = discipline[selectedIndex].Modify(client, textBoxNazivDiscipline.Text, pol, comboBoxTip.Text);
                discipline[selectedIndex] = ds;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (selectedIndex != -1)
            {
                Disciplina sp = discipline[selectedIndex];
                discipline.RemoveAt(selectedIndex);
                sp.Delete(client);
                Ucitaj();
            }
        }

        private void Ucitaj()
        {
            comboBoxOdaberiteDisciplinu.Items.Clear();
            comboBoxDisciplineVeza.Items.Clear();
            comboBoxTakmicenjaVeza.Items.Clear();

            foreach (Disciplina disciplina in discipline)
            {
                comboBoxOdaberiteDisciplinu.Items.Add(disciplina.Naziv + " " + disciplina.Pol);
                comboBoxDisciplineVeza.Items.Add(disciplina.Naziv + " " + disciplina.Pol);
            }

            foreach (Takmicenje tak in takmicenja)
                comboBoxTakmicenjaVeza.Items.Add(tak.Naziv + " " + tak.Zemlja);

            radioButtonMuski.Checked = true;
        }

        private void comboBoxOdaberiteDisciplinu_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxNazivDiscipline.Text = discipline[comboBoxOdaberiteDisciplinu.SelectedIndex].Naziv;
            comboBoxTip.Text = discipline[comboBoxOdaberiteDisciplinu.SelectedIndex].Tip;
            if (discipline[comboBoxOdaberiteDisciplinu.SelectedIndex].Pol == "M")
                radioButtonMuski.Checked = true;
            else
                radioButtonZenski.Checked = true;
            selectedIndex = comboBoxOdaberiteDisciplinu.SelectedIndex;
            Ucitaj();
        }

        private void buttonDodajVezu_Click(object sender, EventArgs e)
        {
            if (comboBoxDisciplineVeza.SelectedIndex != -1 && comboBoxTakmicenjaVeza.SelectedIndex != -1)
            {
                Disciplina.CreateDisciplinaTakmicenje(client, discipline[comboBoxDisciplineVeza.SelectedIndex], takmicenja[comboBoxTakmicenjaVeza.SelectedIndex]);
            }
        }

        private void buttonIzbrisiVezu_Click(object sender, EventArgs e)
        {
            if (comboBoxDisciplineVeza.SelectedIndex != -1 && comboBoxTakmicenjaVeza.SelectedIndex != -1)
            {
                Disciplina.DeletSportDisciplina(client, discipline[comboBoxDisciplineVeza.SelectedIndex], takmicenja[comboBoxTakmicenjaVeza.SelectedIndex]);
            }
        }
    }
}
