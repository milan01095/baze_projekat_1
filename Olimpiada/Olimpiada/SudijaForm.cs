﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class SudijaForm : Form
    {
        private Sudija sudija;
        private Sport sport;
        private Disciplina disciplina;
        private GraphClient client;
        private Utakmica selektovanaUtakmica;
        private Takmicenje selektovanoTakmicenje;
        private List<Takmicenje> takmicenja = null;
        private List<Utakmica> utakmice = null;

        public SudijaForm(Sudija s, Sport sp, Disciplina d)
        {
            InitializeComponent();
            sudija = s;
            sport = sp;
            disciplina = d;
        }

        public void popuniKontrole()
        {
            textBoxIme.Text = sudija.Ime;
            textBoxPrezime.Text = sudija.Prezime;
            textBoxDrzava.Text = sudija.Drzava;
            textBoxGrad.Text = sudija.Grad;
            textBoxGodinaSudjenja.Text = sudija.Godina_sudjenja;

            dateTimePickerDatumRodjenja.Value = Convert.ToDateTime(sudija.Datum_rodjenja);



        }
        public void obrisiKontrole()
        {
            textBoxIme.Text = String.Empty;
            textBoxPrezime.Text = String.Empty;
            textBoxDrzava.Text = String.Empty;
            textBoxGrad.Text = String.Empty;
            textBoxGodinaSudjenja.Text = String.Empty;
            dateTimePickerDatumRodjenja.Value = DateTime.Now;

        }

        private void SudijaForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            comboBoxUtakmica.Text = "--Odaberite utakmicu--";
            comboBoxTakmicenjePojedinacna.Text = "--Odaberite takmicenje--";

            if (disciplina != null && disciplina.Tip == "Ekipna")
            {
                comboBoxTakmicenjePojedinacna.Enabled = false;
            }
            else
                comboBoxUtakmica.Enabled = false;

            if(disciplina!=null)
            popuniTakmicenje();
            else
                MessageBox.Show("Morate odabrati disciplinu ", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);


            if (sudija != null)
                popuniKontrole();
        }

        private void radioButtonKreirajSudiju_CheckedChanged(object sender, EventArgs e)
        {
            obrisiKontrole();
            sudija = null;
            
        }

        private void buttonIzmeni_Click(object sender, EventArgs e)
        {
            DateTime d = dateTimePickerDatumRodjenja.Value;
            d.ToString("dd.MM.yyyy");

            if (textBoxIme.Text != "" && textBoxPrezime.Text != "" && textBoxGodinaSudjenja.Text != "" && textBoxDrzava.Text != "" && textBoxGrad.Text != "")
            {
                var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Sudija) and n.ID = " + "'" + sudija.ID + "'" +
                                                            " set n.Ime = " + "'" + textBoxIme.Text + "'" +
                                                            ", n.Prezime= " + "'" + textBoxPrezime.Text + "'" +
                                                             ", n.Datum_rodjenja= " + "'" + d.ToString("dd.MM.yyyy") + "'" +
                                                               ", n.Drzava= " + "'" + textBoxDrzava.Text + "'" +
                                                               ", n.Grad= " + "'" + textBoxGrad.Text + "'" +
                                                               ", n.Godina_sudjenja= " + "'" + textBoxGodinaSudjenja.Text + "'" +
                                                            " return n",
                                                new Dictionary<string, object>(), CypherResultMode.Set);

                //start n=node(*) where (n:Sportista)  and n.ID ='4'  set n.IME = 'Stefana'   return n
                //start n=node(*) where (n:Sportista)  and n.ID ='4'  set n.IME = 'Stefana', n.Prezime='Nik'   return n

                List<Sudija> s = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).ToList();
                MessageBox.Show("Uspesno ste izmenili sudiju.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                MessageBox.Show("Morate popuniti sva polja.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            this.Close();

        }


        private int getMaxId()
        {

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sudija) RETURN n.ID",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            List<String> maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList();



            List<int> id = maxId.Select(s => int.Parse(s)).ToList();
            int max = id.Max();

            return max;
        }

        private Sudija dodajSudiju()
        {
            Sudija a = new Sudija();
            

            DateTime d = dateTimePickerDatumRodjenja.Value;
            
            a.Ime = textBoxIme.Text;
            a.Prezime = textBoxPrezime.Text;
            a.Datum_rodjenja = d.ToString("dd.MM.yyyy");
            a.Godina_sudjenja = textBoxGodinaSudjenja.Text;
            a.Drzava = textBoxDrzava.Text;
            a.Grad = textBoxGrad.Text;


            return a;
        }

        private void buttonDodaj_Click(object sender, EventArgs e)
        {
            Sudija s = dodajSudiju();
            int maxId = getMaxId();

            try
            {
                // int mId = Int32.Parse(maxId);
                s.ID = (++maxId).ToString();
            }
            catch (Exception exception)
            {
                s.ID = "";
            }


            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Ime", s.Ime);
            queryDict.Add("Prezime", s.Prezime);
            queryDict.Add("Datum_rodjenja", s.Datum_rodjenja);
            queryDict.Add("Godina_sudjenja", s.Godina_sudjenja);
            queryDict.Add("Drzava", s.Drzava);
            queryDict.Add("Grad", s.Grad);


            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Sudija {ID:'" + s.ID + "', Ime:'" + s.Ime
                                                            + "', Prezime:'" + s.Prezime + "', Datum_rodjenja:'" + s.Datum_rodjenja
                                                            + "', Godina_sudjenja:'" + s.Godina_sudjenja
                                                            + "', Drzava:'" + s.Drzava
                                                            + "', Grad:'" + s.Grad
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Sudija> sp = ((IRawGraphClient)client).ExecuteGetCypherResults<Sudija>(query).ToList();



            if (disciplina.Tip == "Ekipna")
            {
                var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sudija),(b:Sport)"
                                                + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + sport.ID + "'"
                                                + " CREATE (a)-[r:`IZ SPORTA`]->(b)"
                                                + " return a,r,b",
                                              new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query1);

                var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sudija),(b:Utakmica)"
                                                + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + selektovanaUtakmica.ID + "'"
                                                + " CREATE (a)-[r:SUDI]->(b)"
                                                + " return a,r,b",
                                              new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query2);
                MessageBox.Show("Uspesno ste dodali sudiju na utakmici. ", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var query1 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sudija),(b:Sport)"
                                               + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + sport.ID + "'"
                                               + " CREATE (a)-[r:`IZ SPORTA`]->(b)"
                                               + " return a,r,b",
                                             new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query1);


                var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sudija),(b:Takmicenje)"
                                                + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + selektovanoTakmicenje.ID + "'"
                                                + " CREATE (a)-[r:KOMANDUJE]->(b)"
                                                + " return a,r,b",
                                              new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query2);
                MessageBox.Show("Uspesno ste dodali sudiju na takmicenju " + selektovanoTakmicenje.Naziv, "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }



            this.Close();

           
        }

        private void popuniTakmicenje()
        {

            if (disciplina.Tip == "Ekipna")
            {
                
                var query = new CypherQuery("start n=node(*) where (n:Utakmica) return n",
                                             new Dictionary<string, object>(), CypherResultMode.Set);


                List<Utakmica> utakmice = ((IRawGraphClient)client).ExecuteGetCypherResults<Utakmica>(query).ToList();

                if (comboBoxUtakmica.Items.Count > 0)
                    comboBoxUtakmica.Items.Clear();
                foreach (Utakmica s in utakmice)
                {
                    comboBoxUtakmica.Items.Add(s);
                    comboBoxUtakmica.DisplayMember = "Datum";
                }
            }
            else
            {
                takmicenja = disciplina.GetTakmicenja();
                if (comboBoxTakmicenjePojedinacna.Items.Count > 0)
                    comboBoxTakmicenjePojedinacna.Items.Clear();
                foreach (Takmicenje tak in takmicenja)
                {
                    comboBoxTakmicenjePojedinacna.Items.Add(tak);
                    comboBoxTakmicenjePojedinacna.DisplayMember = "Naziv";
                }

                /* Dictionary<string, object> queryDict = new Dictionary<string, object>();
                 queryDict.Add("Naziv", disciplina.Naziv);
                 var query = new CypherQuery("match (n)-[r:POSEDUJE]->(d) where n.Naziv =~ {Naziv} return d",
                    queryDict, CypherResultMode.Set);
                 List<Takmicenje> tak = ((IRawGraphClient)client).ExecuteGetCypherResults<Takmicenje>(query).ToList();

                 if (comboBoxTakmicenjePojedinacna.Items.Count > 0)
                     comboBoxTakmicenjePojedinacna.Items.Clear();
                 foreach (Takmicenje s in tak)
                 {
                     comboBoxTakmicenjePojedinacna.Items.Add(s);
                     comboBoxTakmicenjePojedinacna.DisplayMember = "Naziv";


                 }*/


            }
        }

        private void comboBoxUtakmica_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovanaUtakmica = (Utakmica)comboBoxUtakmica.SelectedItem;
        }

        private void comboBoxTakmicenjePojedinacna_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovanoTakmicenje = (Takmicenje)comboBoxTakmicenjePojedinacna.SelectedItem;
        }
    }
}
