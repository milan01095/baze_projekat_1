﻿using Neo4jClient;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class KoloForm : Form
    {
        GraphClient client;
        Tabela selektovanaTabela = null;
        Kolo selektovanoKolo = null;
        Sport selektovanSport = null;
        List<Kolo> kola = null;
        List<Utakmica> utakmice= null;
        List<Klub> klubovi = null;
        List<Sudija> sudije = null;
        Takmicenje selektovanoTakmicenje = null;
        public KoloForm(Tabela tabela, Kolo kolo, List<Kolo> kola, Takmicenje takmicenje, Sport sport)
        {
            InitializeComponent();
            this.selektovanaTabela = tabela;
            this.selektovanoKolo = kolo;
            this.kola = kola;
            selektovanSport = sport;
            selektovanoTakmicenje = takmicenje;

        }

        private void KoloForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            sudije = selektovanSport.GetSudije();
            klubovi = selektovanoTakmicenje.GetKlubovi();
            if (selektovanoKolo != null)
            {
                textBoxRedniBrojKola.Text = selektovanoKolo.Redni_broj.ToString();
                labelNazivLige.Text = selektovanoTakmicenje.Naziv;

                int i = 1;
               utakmice = selektovanoKolo.GetUtakmice();
                foreach(Utakmica uk in utakmice)
                {
                    uk.GetGost();
                    uk.GetDomacin();
                    uk.GetSudija();
                    postavi(i++, uk);
                }
               
                groupBox1.Visible = true;
            }
            else
            {
                groupBox1.Visible = false;
            }
            foreach (Sudija sudija in sudije)
            {
                comboBox1.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox2.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox3.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox4.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox5.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox6.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox7.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox8.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox9.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox10.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox11.Items.Add(sudija.Ime + " " + sudija.Prezime);
                comboBox12.Items.Add(sudija.Ime + " " + sudija.Prezime);
            }

            foreach (Klub klub in klubovi)
            {
                comboBoxA1.Items.Add(klub.Naziv);
                comboBoxA2.Items.Add(klub.Naziv);
                comboBoxA3.Items.Add(klub.Naziv);
                comboBoxA4.Items.Add(klub.Naziv);
                comboBoxA5.Items.Add(klub.Naziv);
                comboBoxA6.Items.Add(klub.Naziv);
                comboBoxA7.Items.Add(klub.Naziv);
                comboBoxA8.Items.Add(klub.Naziv);
                comboBoxA9.Items.Add(klub.Naziv);
                comboBoxA10.Items.Add(klub.Naziv);
                comboBoxA11.Items.Add(klub.Naziv);
                comboBoxA12.Items.Add(klub.Naziv);

                comboBoxB1.Items.Add(klub.Naziv);
                comboBoxB2.Items.Add(klub.Naziv);
                comboBoxB3.Items.Add(klub.Naziv);
                comboBoxB4.Items.Add(klub.Naziv);
                comboBoxB5.Items.Add(klub.Naziv);
                comboBoxB6.Items.Add(klub.Naziv);
                comboBoxB7.Items.Add(klub.Naziv);
                comboBoxB8.Items.Add(klub.Naziv);
                comboBoxB9.Items.Add(klub.Naziv);
                comboBoxB10.Items.Add(klub.Naziv);
                comboBoxB11.Items.Add(klub.Naziv);
                comboBoxB12.Items.Add(klub.Naziv);
            }

        }

        private void postavi(int i, Utakmica uk)
        {
            String rez = uk.Rezultat;
            int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
            int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

            switch (i)
            {
                case 1:
                    comboBoxA1.Text = uk.Domacin.Naziv;
                    comboBoxB1.Text = uk.Gost.Naziv;
                    textBoxA1.Text = domacin.ToString();
                    textBoxB1.Text = gost.ToString();
                    comboBox1.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 2:
                    comboBoxA2.Text = uk.Domacin.Naziv;
                    comboBoxB2.Text = uk.Gost.Naziv;
                    textBoxA2.Text = domacin.ToString();
                    textBoxB2.Text = gost.ToString();
                    comboBox2.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 3:
                    comboBoxA3.Text = uk.Domacin.Naziv;
                    comboBoxB3.Text = uk.Gost.Naziv;
                    textBoxA3.Text = domacin.ToString();
                    textBoxB3.Text = gost.ToString();
                    comboBox3.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 4:
                    comboBoxA4.Text = uk.Domacin.Naziv;
                    comboBoxB4.Text = uk.Gost.Naziv;
                    textBoxA4.Text = domacin.ToString();
                    textBoxB4.Text = gost.ToString();
                    comboBox4.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 5:
                    comboBoxA5.Text = uk.Domacin.Naziv;
                    comboBoxB5.Text = uk.Gost.Naziv;
                    textBoxA5.Text = domacin.ToString();
                    textBoxB5.Text = gost.ToString();
                    comboBox5.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 6:
                    comboBoxA6.Text = uk.Domacin.Naziv;
                    comboBoxB6.Text = uk.Gost.Naziv;
                    textBoxA6.Text = domacin.ToString();
                    textBoxB6.Text = gost.ToString();
                    comboBox6.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 7:
                    comboBoxA7.Text = uk.Domacin.Naziv;
                    comboBoxB7.Text = uk.Gost.Naziv;
                    textBoxA7.Text = domacin.ToString();
                    textBoxB7.Text = gost.ToString();
                    comboBox7.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 8:
                    comboBoxA8.Text = uk.Domacin.Naziv;
                    comboBoxB8.Text = uk.Gost.Naziv;
                    textBoxA8.Text = domacin.ToString();
                    textBoxB8.Text = gost.ToString();
                    comboBox8.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 9:
                    comboBoxA9.Text = uk.Domacin.Naziv;
                    comboBoxB9.Text = uk.Gost.Naziv;
                    textBoxA9.Text = domacin.ToString();
                    textBoxB9.Text = gost.ToString();
                    comboBox9.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 10:
                    comboBoxA10.Text = uk.Domacin.Naziv;
                    comboBoxB10.Text = uk.Gost.Naziv;
                    textBoxA10.Text = domacin.ToString();
                    textBoxB10.Text = gost.ToString();
                    comboBox10.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 11:
                    comboBoxA11.Text = uk.Domacin.Naziv;
                    comboBoxB11.Text = uk.Gost.Naziv;
                    textBoxA11.Text = domacin.ToString();
                    textBoxB11.Text = gost.ToString();
                    comboBox11.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
                case 12:
                    comboBoxA12.Text = uk.Domacin.Naziv;
                    comboBoxB12.Text = uk.Gost.Naziv;
                    textBoxA12.Text = domacin.ToString();
                    textBoxB12.Text = gost.ToString();
                    comboBox12.Text = uk.Sudija.Ime + " " + uk.Sudija.Prezime;
                    break;
            }
        }
     
        private void Kreiraj_Click(object sender, EventArgs e)
        {
            if(textBoxRedniBrojKola.Text != "")
            {
                selektovanoKolo = Kolo.Create(client, textBoxRedniBrojKola.Text, dateTimePickerPocetak.Value.ToString("dd.MM.yyyy"), dateTimePickerKraj.Value.ToString("dd.MM.yyyy"));
                Tabela.CreateTabelaKolo(client, selektovanaTabela, selektovanoKolo);
                groupBox1.Visible = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(comboBoxA1.SelectedIndex != -1 && comboBoxB1.SelectedIndex != -1 && textBoxA1.Text != "" && textBoxB1.Text != "" && comboBox1.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA1.Text);
                int drugi = Convert.ToInt32(textBoxB1.Text);
                string pobednik="";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";

                if (utakmice.Count == 0)
                {
                    Utakmica ut = Utakmica.Create(client, date1.Value.ToString("HH:mm"), textBoxA1.Text + " " + textBoxB1.Text, date1.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA1.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB1.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox1.SelectedIndex]);
                    button1.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[0].GetGost();
                    Klub domacn = utakmice[0].GetDomacin();
                    Sudija sudija = utakmice[0].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[0], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[0], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[0], sudija);
                    utakmice[0].Modify(client, date1.Value.ToString("HH:mm"), textBoxA1.Text + " " + textBoxB1.Text, date1.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[0], klubovi[comboBoxA1.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[0], klubovi[comboBoxB1.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[0], sudije[comboBox1.SelectedIndex]);
                    button1.Enabled = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (comboBoxA2.SelectedIndex != -1 && comboBoxB2.SelectedIndex != -1 && textBoxA2.Text != "" && textBoxB2.Text != "" && comboBox2.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA2.Text);
                int drugi = Convert.ToInt32(textBoxB2.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";

                if (utakmice.Count < 2)
                {
                    Utakmica ut = Utakmica.Create(client, date2.Value.ToString("HH:mm"), textBoxA2.Text + " " + textBoxB2.Text, date2.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA2.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB2.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox2.SelectedIndex]);
                    button2.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[1].GetGost();
                    Klub domacn = utakmice[1].GetDomacin();
                    Sudija sudija = utakmice[1].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[1], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[1], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[1], sudija);
                    utakmice[1].Modify(client, date2.Value.ToString("HH:mm"), textBoxA2.Text + " " + textBoxB2.Text, date2.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[1], klubovi[comboBoxA2.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[1], klubovi[comboBoxB2.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[1], sudije[comboBox2.SelectedIndex]);
                    button2.Enabled = false;
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (comboBoxA3.SelectedIndex != -1 && comboBoxB3.SelectedIndex != -1 && textBoxA3.Text != "" && textBoxB3.Text != "" && comboBox3.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA3.Text);
                int drugi = Convert.ToInt32(textBoxB3.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";

                if (utakmice.Count < 3)
                {
                    Utakmica ut = Utakmica.Create(client, date3.Value.ToString("HH:mm"), textBoxA3.Text + " " + textBoxB3.Text, date3.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA3.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB3.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox3.SelectedIndex]);
                    button3.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[2].GetGost();
                    Klub domacn = utakmice[2].GetDomacin();
                    Sudija sudija = utakmice[2].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[2], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[2], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[2], sudija);
                    utakmice[2].Modify(client, date3.Value.ToString("HH:mm"), textBoxA3.Text + " " + textBoxB3.Text, date3.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[2], klubovi[comboBoxA3.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[2], klubovi[comboBoxB3.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[2], sudije[comboBox3.SelectedIndex]);
                    button3.Enabled = false;
                }
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (comboBoxA4.SelectedIndex != -1 && comboBoxB4.SelectedIndex != -1 && textBoxA4.Text != "" && textBoxB4.Text != "" && comboBox4.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA4.Text);
                int drugi = Convert.ToInt32(textBoxB4.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 4)
                {
                    Utakmica ut = Utakmica.Create(client, date4.Value.ToString("HH:mm"), textBoxA4.Text + " " + textBoxB4.Text, date4.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA4.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB4.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox4.SelectedIndex]);
                    button4.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[3].GetGost();
                    Klub domacn = utakmice[3].GetDomacin();
                    Sudija sudija = utakmice[3].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[3], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[3], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[3], sudija);
                    utakmice[3].Modify(client, date4.Value.ToString("HH:mm"), textBoxA4.Text + " " + textBoxB4.Text, date4.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[3], klubovi[comboBoxA4.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[3], klubovi[comboBoxB4.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[3], sudije[comboBox4.SelectedIndex]);
                    button4.Enabled = false;
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (comboBoxA5.SelectedIndex != -1 && comboBoxB5.SelectedIndex != -1 && textBoxA5.Text != "" && textBoxB5.Text != "" && comboBox5.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA5.Text);
                int drugi = Convert.ToInt32(textBoxB5.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 5)
                {
                    Utakmica ut = Utakmica.Create(client, date5.Value.ToString("HH:mm"), textBoxA5.Text + " " + textBoxB5.Text, date5.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA5.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB5.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox5.SelectedIndex]);
                    button5.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[4].GetGost();
                    Klub domacn = utakmice[4].GetDomacin();
                    Sudija sudija = utakmice[4].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[4], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[4], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[4], sudija);
                    utakmice[4].Modify(client, date5.Value.ToString("HH:mm"), textBoxA5.Text + " " + textBoxB5.Text, date5.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[4], klubovi[comboBoxA5.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[4], klubovi[comboBoxB5.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[4], sudije[comboBox5.SelectedIndex]);
                    button5.Enabled = false;
                }
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if (comboBoxA6.SelectedIndex != -1 && comboBoxB6.SelectedIndex != -1 && textBoxA6.Text != "" && textBoxB6.Text != "" && comboBox6.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA6.Text);
                int drugi = Convert.ToInt32(textBoxB6.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 6)
                {
                    Utakmica ut = Utakmica.Create(client, date6.Value.ToString("HH:mm"), textBoxA6.Text + " " + textBoxB6.Text, date6.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA6.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB6.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox6.SelectedIndex]);
                    button6.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[5].GetGost();
                    Klub domacn = utakmice[5].GetDomacin();
                    Sudija sudija = utakmice[5].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[5], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[5], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[5], sudija);
                    utakmice[5].Modify(client, date6.Value.ToString("HH:mm"), textBoxA6.Text + " " + textBoxB6.Text, date6.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[5], klubovi[comboBoxA6.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[5], klubovi[comboBoxB6.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[5], sudije[comboBox6.SelectedIndex]);
                    button6.Enabled = false;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (comboBoxA7.SelectedIndex != -1 && comboBoxB7.SelectedIndex != -1 && textBoxA7.Text != "" && textBoxB7.Text != "" && comboBox7.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA7.Text);
                int drugi = Convert.ToInt32(textBoxB7.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";

                if (utakmice.Count < 7)
                {
                    Utakmica ut = Utakmica.Create(client, date7.Value.ToString("HH:mm"), textBoxA7.Text + " " + textBoxB7.Text, date7.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA7.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB7.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox7.SelectedIndex]);
                    button7.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[6].GetGost();
                    Klub domacn = utakmice[6].GetDomacin();
                    Sudija sudija = utakmice[6].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[6], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[6], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[6], sudija);
                    utakmice[6].Modify(client, date7.Value.ToString("HH:mm"), textBoxA7.Text + " " + textBoxB7.Text, date7.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[6], klubovi[comboBoxA7.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[6], klubovi[comboBoxB7.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[6], sudije[comboBox7.SelectedIndex]);
                    button7.Enabled = false;
                }
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            if (comboBoxA8.SelectedIndex != -1 && comboBoxB8.SelectedIndex != -1 && textBoxA8.Text != "" && textBoxB8.Text != "" && comboBox8.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA8.Text);
                int drugi = Convert.ToInt32(textBoxB8.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";

                if (utakmice.Count < 8)
                {
                    Utakmica ut = Utakmica.Create(client, date8.Value.ToString("HH:mm"), textBoxA8.Text + " " + textBoxB8.Text, date8.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA8.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB8.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox8.SelectedIndex]);
                    button8.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[7].GetGost();
                    Klub domacn = utakmice[7].GetDomacin();
                    Sudija sudija = utakmice[7].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[7], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[7], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[7], sudija);
                    utakmice[7].Modify(client, date8.Value.ToString("HH:mm"), textBoxA8.Text + " " + textBoxB8.Text, date8.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[7], klubovi[comboBoxA8.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[7], klubovi[comboBoxB8.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[7], sudije[comboBox8.SelectedIndex]);
                    button8.Enabled = false;
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if (comboBoxA9.SelectedIndex != -1 && comboBoxB9.SelectedIndex != -1 && textBoxA9.Text != "" && textBoxB9.Text != "" && comboBox9.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA9.Text);
                int drugi = Convert.ToInt32(textBoxB9.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 9)
                {
                    Utakmica ut = Utakmica.Create(client, date9.Value.ToString("HH:mm"), textBoxA9.Text + " " + textBoxB9.Text, date9.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA9.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB9.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox9.SelectedIndex]);
                    button9.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[8].GetGost();
                    Klub domacn = utakmice[8].GetDomacin();
                    Sudija sudija = utakmice[8].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[8], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[8], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[8], sudija);
                    utakmice[8].Modify(client, date9.Value.ToString("HH:mm"), textBoxA9.Text + " " + textBoxB9.Text, date9.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[8], klubovi[comboBoxA9.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[8], klubovi[comboBoxB9.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[8], sudije[comboBox9.SelectedIndex]);
                    button9.Enabled = false;
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            if (comboBoxA10.SelectedIndex != -1 && comboBoxB10.SelectedIndex != -1 && textBoxA10.Text != "" && textBoxB10.Text != "" && comboBox10.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA10.Text);
                int drugi = Convert.ToInt32(textBoxB10.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 10)
                {
                    Utakmica ut = Utakmica.Create(client, date10.Value.ToString("HH:mm"), textBoxA10.Text + " " + textBoxB10.Text, date10.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA10.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB10.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox10.SelectedIndex]);
                    button10.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[9].GetGost();
                    Klub domacn = utakmice[9].GetDomacin();
                    Sudija sudija = utakmice[9].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[9], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[9], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[9], sudija);
                    utakmice[9].Modify(client, date10.Value.ToString("HH:mm"), textBoxA10.Text + " " + textBoxB10.Text, date10.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[9], klubovi[comboBoxA10.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[9], klubovi[comboBoxB10.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[9], sudije[comboBox10.SelectedIndex]);
                    button10.Enabled = false;
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            if (comboBoxA11.SelectedIndex != -1 && comboBoxB11.SelectedIndex != -1 && textBoxA11.Text != "" && textBoxB11.Text != "" && comboBox11.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA11.Text);
                int drugi = Convert.ToInt32(textBoxB11.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 11)
                {
                    Utakmica ut = Utakmica.Create(client, date11.Value.ToString("HH:mm"), textBoxA11.Text + " " + textBoxB11.Text, date11.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA11.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB11.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox11.SelectedIndex]);
                    button11.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[10].GetGost();
                    Klub domacn = utakmice[10].GetDomacin();
                    Sudija sudija = utakmice[10].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[10], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[10], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[10], sudija);
                    utakmice[10].Modify(client, date11.Value.ToString("HH:mm"), textBoxA11.Text + " " + textBoxB11.Text, date11.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[10], klubovi[comboBoxA11.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[10], klubovi[comboBoxB11.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[10], sudije[comboBox11.SelectedIndex]);
                    button11.Enabled = false;
                }
            }
        }

        private void button12_Click(object sender, EventArgs e)
        {
            if (comboBoxA12.SelectedIndex != -1 && comboBoxB12.SelectedIndex != -1 && textBoxA12.Text != "" && textBoxB12.Text != "" && comboBox12.SelectedIndex != -1)
            {
                int prvi = Convert.ToInt32(textBoxA12.Text);
                int drugi = Convert.ToInt32(textBoxB12.Text);
                string pobednik = "";

                if (prvi == drugi)
                    pobednik = "X";
                else if (prvi > drugi)
                    pobednik = "1";
                else
                    pobednik = "2";
                if (utakmice.Count < 12)
                {
                    Utakmica ut = Utakmica.Create(client, date12.Value.ToString("HH:mm"), textBoxA12.Text + " " + textBoxB12.Text, date12.Value.ToString("dd.MM.yyyy"), pobednik);
                    Kolo.CreateKoloUtakmica(client, selektovanoKolo, ut);
                    Utakmica.CreateUtakmicaDomacin(client, ut, klubovi[comboBoxA12.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, ut, klubovi[comboBoxB12.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, ut, sudije[comboBox12.SelectedIndex]);
                    button12.Enabled = false;
                }
                else
                {
                    Klub gost = utakmice[11].GetGost();
                    Klub domacn = utakmice[11].GetDomacin();
                    Sudija sudija = utakmice[11].GetSudija();
                    Utakmica.DeleteUtakmicaDomacin(client, utakmice[11], domacn);
                    Utakmica.DeleteUtakmicaGost(client, utakmice[11], gost);
                    Utakmica.DeleteSudijaUtakmica(client, utakmice[11], sudija);
                    utakmice[11].Modify(client, date12.Value.ToString("HH:mm"), textBoxA12.Text + " " + textBoxB12.Text, date12.Value.ToString("dd.MM.yyyy"), pobednik);
                    Utakmica.CreateUtakmicaDomacin(client, utakmice[11], klubovi[comboBoxA12.SelectedIndex]);
                    Utakmica.CreateUtakmicaGost(client, utakmice[11], klubovi[comboBoxB12.SelectedIndex]);
                    Utakmica.CreateUtakmicaSudija(client, utakmice[11], sudije[comboBox12.SelectedIndex]);
                    button12.Enabled = false;
                }
            }
        }
    }
}
