﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class Pocetna : Form
    {
        Stack<Panel> stekPanela;
        int IndexSport, IndexDisciplina, IndexTakmicenje, indexKola = -1;
        public GraphClient client;
        public GroupBox takmicenje, klubInfo, sportistaInfo;
        List<Sport> Sports;
        Takmicenje aktivnoTakmicenje;
        Kolo trenutnoKolo;
        Klub aktivniKlub;
        Utakmica aktivnaUtakmica;
        Stadion aktivniStadion;
        Sportista aktivniSportista;
        Sudija aktivniSudija;
       

        public Pocetna()
        {
            InitializeComponent();
            IndexDisciplina = IndexSport = IndexTakmicenje = -1;
            stekPanela = new Stack<Panel>();
        }

        public void Form1_Load(object sender, EventArgs e)
        {
            this.Size = new Size(620, 547);
            this.FormBorderStyle = FormBorderStyle.FixedSingle;

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            SetePanels();

            try
            {
                client.Connect();
                GetSports();


            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

        }

        public void GetSports()
        {
            var query = new CypherQuery("match(n:Sport) return n",
              new Dictionary<string, object>(), CypherResultMode.Set);

            Sports = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).ToList();

            int i = 0;
            foreach (Sport dis in Sports)
            {
                dis.GetDiscipline();
                int k = 0;
                foreach (Disciplina ds in dis.Discipline)
                {
                    DevExpress.XtraNavBar.NavBarGroup grup = new DevExpress.XtraNavBar.NavBarGroup(dis.Naziv + " " + ds.Naziv);
                    ds.GetTakmicenja();

                    int p = 0;
                    foreach (Models.Takmicenje takmicenja in ds.Takmicenja)
                    {
                        DevExpress.XtraNavBar.NavBarItem pp = new DevExpress.XtraNavBar.NavBarItem(takmicenja.Naziv);
                        pp.Name = i.ToString() + " " + k.ToString() + " " + p.ToString();
                        pp.LinkPressed += new DevExpress.XtraNavBar.NavBarLinkEventHandler(OdabirTakmicenja);
                        grup.ItemLinks.Add(pp);
                        p++;
                    }
                    navBarControl1.Groups.Add(grup);
                    k++;
                }
                i++;
            }
        }

        private void SetePanels()
        {
            panelTakmicenje.Visible = false;
            panelTakmicenje.Location = new Point(146, 0);
            panelTakmicenje.Size = new Size(454, 540);
            panelTakmicenje.VerticalScroll.Enabled = true;
            panelTakmicenjePojedinacno.Visible = false;
            panelTakmicenjePojedinacno.Location = new Point(146, 0);
            panelTakmicenjePojedinacno.Size = new Size(454, 540);
            panelKlub.Visible = false;
            panelKlub.Location = new Point(146, 0);
            panelKlub.Size = new Size(454, 540);
            panelUtakmica.Visible = false;
            panelUtakmica.Location = new Point(146, 0);
            panelUtakmica.Size = new Size(454, 540);
            panelSportista.Visible = false;
            panelSportista.Location = new Point(146, 0);
            panelSportista.Size = new Size(454, 540);
            panelStadion.Visible = false;
            panelStadion.Location = new Point(146, 0);
            panelStadion.Size = new Size(454, 540);
            panelSudija.Visible = false;
            panelSudija.Location = new Point(146, 0);
            panelSudija.Size = new Size(454, 540);
        }

        private void DisablePanels()
        {
            panelKlub.Visible = false;
            panelSportista.Visible = false;
            panelStadion.Visible = false;
            panelSudija.Visible = false;
            panelTakmicenje.Visible = false;
            panelTakmicenjePojedinacno.Visible = false;
            panelUtakmica.Visible = false;
        }


        private void comboBox_Kolo_SelectedIndexChanged(object sender, EventArgs e)
        {
            listTakmicenjeSvaKola.Items.Clear();
            Kolo trenutno = null;
            for (int i = 0; i < aktivnoTakmicenje.Tabela.Kola.Count; i++)
                if (Convert.ToInt32(comboBox_Kolo.Text) == aktivnoTakmicenje.Tabela.Kola[i].Redni_broj)
                    trenutno = aktivnoTakmicenje.Tabela.Kola[i];

            if (trenutno != null)
            {
                for (int i = 0; i < trenutno.Utakmice.Count; i++)
                {
                    String rez = trenutno.Utakmice[i].Rezultat;
                    int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                    int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                    ListViewItem item = new ListViewItem(new string[] { trenutno.Redni_broj.ToString(), trenutno.Utakmice[i].Datum+" "+ trenutno.Utakmice[i].Vreme,
                        trenutno.Utakmice[i].Domacin.Naziv,  domacin.ToString(), gost.ToString(), trenutno.Utakmice[i].Gost.Naziv, trenutno.Utakmice[i].ID.ToString()});

                    listTakmicenjeSvaKola.Items.Add(item);
                }
            }
            trenutnoKolo = trenutno;
            indexKola = Convert.ToInt32(comboBox_Kolo.Text);
        }

        private void buttonLoginAdmin_Click(object sender, EventArgs e)
        {
            var query = new CypherQuery("match(n:Admin) return n",
              new Dictionary<string, object>(), CypherResultMode.Set);

            List<Admin> admins = ((IRawGraphClient)client).ExecuteGetCypherResults<Admin>(query).ToList();

            int s = -1;
            for (int i = 0; i < admins.Count; i++)
            {
                if (admins[i].Korisnicko.Equals(textBoxUsername.Text) && admins[i].Sifra.Equals(textBoxPassword.Text))
                   s = i;
             }
            if (s != -1)
            {
                Form f = new AdminForm();
                f.BringToFront();
                f.ShowDialog();
            }
            else
            {
                MessageBox.Show("Korisnicko i sifra se ne poklapaju");
            }
        }
              
        private void buttonXTakmicenje_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXStadion_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXSudija_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXTakmicenjePojedinacno_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXKlub_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXSportista_Click(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void buttonXStadion_Click_1(object sender, EventArgs e)
        {
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                if (stekPanela.Count != 0)
                {
                    Panel v = stekPanela.Pop();
                    v.Visible = true;
                    stekPanela.Push(v);
                }
            }
        }

        private void listTakmicenjeTabela_DoubleClick(object sender, EventArgs e)
        {
            if (listTakmicenjeTabela.SelectedItems.Count == 1)
            {
                int ID =Convert.ToInt32(listTakmicenjeTabela.Items[listTakmicenjeTabela.SelectedIndices[0]].SubItems[10].Text);                     
                for(int i=0; i<aktivnoTakmicenje.Klubovi.Count; i++)
                {
                       if(ID == aktivnoTakmicenje.Klubovi[i].ID)
                        aktivniKlub = aktivnoTakmicenje.Klubovi[i];
                   
                }             
                SetPanelKlub();
            }
        }

        private void listTakmicenjeSvaKola_DoubleClick(object sender, EventArgs e)
        {
            if (listTakmicenjeSvaKola.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listTakmicenjeSvaKola.Items[listTakmicenjeSvaKola.SelectedIndices[0]].SubItems[6].Text);
                for (int i = 0; i < aktivnoTakmicenje.Tabela.Kola[indexKola -1].Utakmice.Count; i++)
                {
                    if (ID == aktivnoTakmicenje.Tabela.Kola[indexKola - 1].Utakmice[i].ID)
                        aktivnaUtakmica = aktivnoTakmicenje.Tabela.Kola[indexKola - 1].Utakmice[i];
                }
                SetPanelUtakmica();
            }
        }

        private void listViewKlubIgraci_DoubleClick(object sender, EventArgs e)
        {
            if (listViewKlubIgraci.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listViewKlubIgraci.Items[listViewKlubIgraci.SelectedIndices[0]].SubItems[3].Text);
                for (int i = 0; i < aktivniKlub.Igraci.Count; i++)
                {
                    if (ID == aktivniKlub.Igraci[i].ID)
                        aktivniSportista = aktivniKlub.Igraci[i];
                }
                SetPanelSportista();
            }
        }

        private void listKlubKola_DoubleClick(object sender, EventArgs e)
        {
            if (listKlubKola.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listKlubKola.Items[listKlubKola.SelectedIndices[0]].SubItems[6].Text);
                for (int i = 0; i < aktivniKlub.Utakmice.Count; i++)
                {
                    if (ID == aktivniKlub.Utakmice[i].ID)
                        aktivnaUtakmica = aktivniKlub.Utakmice[i];
                }
                SetPanelUtakmica();
            }
        }

        private void listStadionLiga_DoubleClick(object sender, EventArgs e)
        {
            if (listStadionLiga.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listStadionLiga.Items[listStadionLiga.SelectedIndices[0]].SubItems[6].Text);
                for (int i = 0; i < aktivniStadion.Utakmice.Count; i++)
                {
                    if (ID == aktivniStadion.Utakmice[i].ID)
                        aktivnaUtakmica = aktivniStadion.Utakmice[i];
                }
                SetPanelUtakmica();
            }
        }

        private void listStadionTakmicenj_DoubleClick(object sender, EventArgs e)
        {
            if (listStadionTakmicenj.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listStadionTakmicenj.Items[listStadionTakmicenj.SelectedIndices[0]].SubItems[4].Text);
                for (int i = 0; i < aktivniStadion.TakmicenjaPojedinacna.Count; i++)
                {
                    if (ID == aktivniStadion.TakmicenjaPojedinacna[i].ID)
                        aktivnoTakmicenje = aktivniStadion.TakmicenjaPojedinacna[i];
                }
                SetPanelTakmicenjePojedinacno(aktivnoTakmicenje, aktivnoTakmicenje.GetDisciplina());
            }
        }

        private void listSudijaEkipni_DoubleClick(object sender, EventArgs e)
        {
            if (listSudijaEkipni.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listSudijaEkipni.Items[listSudijaEkipni.SelectedIndices[0]].SubItems[6].Text);
                for (int i = 0; i < aktivniSudija.Utakmice.Count; i++)
                {
                    if (ID == aktivniSudija.Utakmice[i].ID)
                        aktivnaUtakmica = aktivniSudija.Utakmice[i];
                }
                SetPanelUtakmica();
            }
        }

        private void listSudijaPojedinacni_DoubleClick(object sender, EventArgs e)
        {
            if (listSudijaPojedinacni.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listSudijaPojedinacni.Items[listSudijaPojedinacni.SelectedIndices[0]].SubItems[4].Text);
                for (int i = 0; i < aktivniSudija.Takmicenja.Count; i++)
                {
                    if (ID == aktivniSudija.Takmicenja[i].ID)
                        aktivnoTakmicenje = aktivniSudija.Takmicenja[i];
                }
                SetPanelTakmicenjePojedinacno(aktivnoTakmicenje, aktivnoTakmicenje.GetDisciplina());
            }
        }

        private void listTakmicenjePojedinacno_DoubleClick(object sender, EventArgs e)
        {
            if (listTakmicenjePojedinacno.SelectedItems.Count == 1)
            {
                int ID = Convert.ToInt32(listTakmicenjePojedinacno.Items[listTakmicenjePojedinacno.SelectedIndices[0]].SubItems[4].Text);
                for (int i = 0; i < aktivnoTakmicenje.Takmicari.Count; i++)
                {
                    if (ID == aktivnoTakmicenje.Takmicari[i].IDSportista)                   
                        aktivniSportista = aktivnoTakmicenje.Takmicari[i].GetSportista();                   
                }
                SetPanelSportista();
            }
        }


        private void linkLabelKlubStadion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniStadion = aktivniKlub.Stadion;
            SetPanelStadion();
        }
       
        private void linkLabelUtakmicaStadion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivnaUtakmica.GetDomacin();
            aktivniStadion = aktivnaUtakmica.Domacin.GetStadion();
            SetPanelStadion();
        }

        private void linkLabelDomacin_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniKlub = aktivnaUtakmica.GetDomacin();
            SetPanelKlub();
        }

        private void linkLabelGost_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniKlub = aktivnaUtakmica.GetGost();
            SetPanelKlub();
        }

        private void linkLabelUtakmicaSudija_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniSudija = aktivnaUtakmica.GetSudija();
            SetPanelSudija();
        }

        private void linkLabelTakmicenjePojedinacnoSudija_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniSudija = aktivnoTakmicenje.GetSudija();
            SetPanelSudija();
        }

        private void linkLabelStadion_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            aktivniStadion = aktivnoTakmicenje.GetStadion();
            SetPanelStadion();
        }

        public void OdabirTakmicenja(object sender, EventArgs e)
        {
            DevExpress.XtraNavBar.NavBarItem selected = (DevExpress.XtraNavBar.NavBarItem)sender;
            int ind = selected.Name.IndexOf(' ');
            IndexSport = Convert.ToInt32(selected.Name.Substring(0, ind));
            ind += 1;
            int ind2 = selected.Name.IndexOf(' ', ind);
            IndexDisciplina = Convert.ToInt32(selected.Name.Substring(ind - 1, ind2).ToString());
            IndexTakmicenje = Convert.ToInt32(selected.Name.Substring(ind2 + 1));

            Takmicenje izabrano = Sports[IndexSport].Discipline[IndexDisciplina].Takmicenja[IndexTakmicenje];
            Disciplina disciplina = Sports[IndexSport].Discipline[IndexDisciplina];

            if (disciplina.Tip.Equals("Ekipna"))
            {
                DisablePanels();
                SetPanelTakmicenje(izabrano, disciplina);
            }
            else
            {
                DisablePanels();
                aktivnoTakmicenje = izabrano;           
                SetPanelTakmicenjePojedinacno(izabrano, disciplina);
            }
        }
                          
        private void SetPanelTakmicenje(Takmicenje takmicenje, Disciplina disciplina)
        {
            try
            {             
                panelTakmicenje.Visible = true;
                if (stekPanela.Count != 0)
                {
                    Panel p = stekPanela.Pop();
                    if (p != panelTakmicenje)
                    {
                        p.Visible = false;
                        stekPanela.Push(p);
                    }
                }
                stekPanela.Push(panelTakmicenje);

                label_Sport.Text = Sports[IndexSport].Naziv;
                label_Asociacija.Text = Sports[IndexSport].Sportska_asociacija;
                label_Zemlja.Text = takmicenje.Zemlja;
                label_Osnovana.Text = takmicenje.Datum_osnivanja;
                label_Kup.Text = takmicenje.Domaci_kup;
                label_Pol.Text = disciplina.Pol;
                label_Tip.Text = disciplina.Tip;
                label_Sponzor.Text = takmicenje.Sponzor;

                takmicenje.GetKlubovi();
                takmicenje.GetTabela();
                label_Trenutno_kolo.Text = takmicenje.Tabela.Trenutno_kolo.ToString();
                indexKola = takmicenje.Tabela.Trenutno_kolo;

                listTakmicenjeTabela.Items.Clear();
                for (int i = 1; i <= takmicenje.Tabela.Broj_kola; i++)
                    comboBox_Kolo.Items.Add(i.ToString());


                List<Models.Kolo> kola = takmicenje.Tabela.GetKola();
                int[] odigranih = new int[takmicenje.Klubovi.Count];
                int[] pobedjenih = new int[takmicenje.Klubovi.Count];
                int[] izgubljenih = new int[takmicenje.Klubovi.Count];
                int[] neresenih = new int[takmicenje.Klubovi.Count];
                int[] datihgolova = new int[takmicenje.Klubovi.Count];
                int[] primljenih = new int[takmicenje.Klubovi.Count];
                int[] indeksi = new int[takmicenje.Klubovi.Count];
                int[] bodovi = new int[takmicenje.Klubovi.Count];

                for (int i = 0; i < kola.Count; i++)
                {
                    kola[i].GetUtakmice();
                    for (int j = 0; j < kola[i].Utakmice.Count; j++)
                    {
                        kola[i].Utakmice[j].GetGost();
                        kola[i].Utakmice[j].GetDomacin();


                        int pobednikPrvi = 0;
                        int nereseno = 0;
                        int pobednikDrugi = 0;

                        String rez = kola[i].Utakmice[j].Rezultat;
                        int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                        int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                        if (domacin == gost)
                        {
                            nereseno = 1;

                        }
                        else if (domacin > gost)
                        {
                            pobednikPrvi = 1;
                        }
                        else
                        {
                            pobednikDrugi = 1;
                        }

                        int s = 0;

                        while (kola[i].Utakmice[j].Domacin.ID != takmicenje.Klubovi[s].ID)
                            s++;
                        odigranih[s]++;
                        pobedjenih[s] += pobednikPrvi;
                        neresenih[s] += nereseno;
                        izgubljenih[s] += pobednikDrugi;
                        datihgolova[s] += domacin;
                        primljenih[s] += gost;

                        s = 0;
                        while (kola[i].Utakmice[j].Gost.ID != takmicenje.Klubovi[s].ID)
                            s++;
                        odigranih[s]++;
                        pobedjenih[s] += pobednikDrugi;
                        neresenih[s] += nereseno;
                        izgubljenih[s] += pobednikPrvi;
                        datihgolova[s] += gost;
                        primljenih[s] += domacin;
                    }
                }
                for (int i = 0; i < takmicenje.Klubovi.Count; i++)
                {
                    indeksi[i] = takmicenje.Klubovi[i].ID;
                    bodovi[i] = pobedjenih[i] * 3 + neresenih[i];
                }
                for (int i = 0; i < takmicenje.Klubovi.Count; i++)
                {
                    for (int j = i; j < takmicenje.Klubovi.Count; j++)
                    {
                        if (bodovi[i] < bodovi[j])
                        {
                            int tmp = bodovi[i];
                            bodovi[i] = bodovi[j];
                            bodovi[j] = tmp;

                            tmp = indeksi[i];
                            indeksi[i] = indeksi[j];
                            indeksi[j] = tmp;

                            tmp = primljenih[i];
                            primljenih[i] = primljenih[j];
                            primljenih[j] = tmp;

                            tmp = datihgolova[i];
                            datihgolova[i] = datihgolova[j];
                            datihgolova[j] = tmp;

                            tmp = neresenih[i];
                            neresenih[i] = neresenih[j];
                            neresenih[j] = tmp;

                            tmp = izgubljenih[i];
                            izgubljenih[i] = izgubljenih[j];
                            izgubljenih[j] = tmp;

                            tmp = pobedjenih[i];
                            pobedjenih[i] = pobedjenih[j];
                            pobedjenih[j] = tmp;

                            tmp = odigranih[i];
                            odigranih[i] = odigranih[j];
                            odigranih[j] = tmp;
                        }
                    }
                    int s = 0;
                    while (indeksi[i] != takmicenje.Klubovi[s].ID)
                        s++;

                    ListViewItem item = new ListViewItem(new string[] { Convert.ToString(i+1), takmicenje.Klubovi[s].Naziv, odigranih[i].ToString(),
                    pobedjenih[i].ToString(), neresenih[i].ToString(), izgubljenih[i].ToString(), datihgolova[i].ToString(), primljenih[i].ToString(),
                    Convert.ToString(datihgolova[i] - primljenih[i]), bodovi[i].ToString(), indeksi[i].ToString() });

                    listTakmicenjeTabela.Items.Add(item);
                }

                listTakmicenjeSvaKola.Items.Clear();
                Kolo trenutno = null;
                for (int i = 0; i < takmicenje.Tabela.Kola.Count; i++)
                    if (takmicenje.Tabela.Trenutno_kolo == takmicenje.Tabela.Kola[i].Redni_broj)
                        trenutno = takmicenje.Tabela.Kola[i];
                if (trenutno != null)
                {
                    for (int i = 0; i < trenutno.Utakmice.Count; i++)
                    {
                        String rez = trenutno.Utakmice[i].Rezultat;
                        int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                        int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                        ListViewItem item = new ListViewItem(new string[] { trenutno.Redni_broj.ToString(), trenutno.Utakmice[i].Datum+" "+ trenutno.Utakmice[i].Vreme,
                        trenutno.Utakmice[i].Domacin.Naziv,  domacin.ToString(), gost.ToString(), trenutno.Utakmice[i].Gost.Naziv, trenutno.Utakmice[i].ID.ToString()});

                        listTakmicenjeSvaKola.Items.Add(item);
                    }
                }

                aktivnoTakmicenje = takmicenje;
                trenutnoKolo = trenutno;
               
              
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
      
        private void SetPanelKlub()
        {         
            panelKlub.Visible = true;
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                stekPanela.Push(p);
            }
            stekPanela.Push(panelKlub);

            listKlubKola.Items.Clear();
            listViewKlubIgraci.Items.Clear();
            
            labelKlubNaziv.Text = aktivniKlub.Naziv;
            labelKlubGrad.Text = aktivniKlub.Grad;
            labelKlubBrTitula.Text = aktivniKlub.Broj_titula.ToString();
            labelKlubDatOsnivanja.Text = aktivniKlub.Datum_osnivanja;
            labelKlubGlavniSponzor.Text = aktivniKlub.Sponzor;
            labelKlubTrener.Text = aktivniKlub.Trener;
            labelKlubMenadzer.Text = aktivniKlub.Menadzer;
            labelKlubSajt.Text = aktivniKlub.Sajt;            

            aktivniKlub.GetIgaci();
            aktivniKlub.GetStadion();
            aktivniKlub.GetUtakmice();
            linkLabelKlubStadion.Text = aktivniKlub.Stadion.Ime_stadiona;
            aktivniStadion = aktivniKlub.Stadion;

            for(int i=0; i <aktivniKlub.Utakmice.Count; i++)
            {
                aktivniKlub.Utakmice[i].GetGost();
                aktivniKlub.Utakmice[i].GetDomacin();

                String rez = aktivniKlub.Utakmice[i].Rezultat;
                int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                ListViewItem item = new ListViewItem(new string[] { aktivniKlub.Utakmice[i].ID.ToString(), aktivniKlub.Utakmice[i].Datum,
                aktivniKlub.Utakmice[i].Domacin.Naziv, domacin.ToString(), gost.ToString(), aktivniKlub.Utakmice[i].Gost.Naziv, aktivniKlub.Utakmice[i].ID.ToString()});

                listKlubKola.Items.Add(item);
            }

            for(int i=0; i<aktivniKlub.Igraci.Count; i++)
            {
                ListViewItem item = new ListViewItem(new string[] { aktivniKlub.Igraci[i].Broj_dresa.ToString(), aktivniKlub.Igraci[i].Ime,
                aktivniKlub.Igraci[i].Prezime, aktivniKlub.Igraci[i].ID.ToString() });

                listViewKlubIgraci.Items.Add(item);
            }                     
        }
       
        private void SetPanelUtakmica()
        {         
            panelUtakmica.Visible = true;
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                stekPanela.Push(p);
            }
            stekPanela.Push(panelUtakmica);

            aktivnaUtakmica.GetSudija();
            aktivnaUtakmica.GetGost();
            aktivnaUtakmica.GetDomacin();
            aktivnaUtakmica.GetKolo();
            Stadion stad =  aktivnaUtakmica.Domacin.GetStadion();

            linkLabelUtakmicaStadion.Text = stad.Ime_stadiona;
            linkLabelDomacin.Text = aktivnaUtakmica.Domacin.Naziv;
            linkLabelGost.Text = aktivnaUtakmica.Gost.Naziv;
            linkLabelUtakmicaSudija.Text = aktivnaUtakmica.Sudija.Ime + " " + aktivnaUtakmica.Sudija.Prezime;
            labelUtakmicaGrad.Text = stad.Grad;
            labelUtakmicaLokacija.Text = stad.Lokacija;
            labelUtakmicaDatum.Text = aktivnaUtakmica.Datum;
            labelUtakmicaVreme.Text = aktivnaUtakmica.Vreme;
            labelUtakmicaKolo.Text = aktivnaUtakmica.Kolo.Redni_broj.ToString();
            labelUtakmicaRezultat.Text = aktivnaUtakmica.Domacin.Naziv + " " + aktivnaUtakmica.Rezultat + " " + aktivnaUtakmica.Gost.Naziv;
        }

        private void SetPanelSportista()
        {          
            panelSportista.Visible = true;
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                stekPanela.Push(p);
            }
            stekPanela.Push(panelSportista);


            aktivniSportista.GetKlub();
            labelSportistaIme.Text = aktivniSportista.Ime;
            labelSportistaPrezime.Text = aktivniSportista.Prezime;
            labelSportistaDatRodjenja.Text = aktivniSportista.Datum_rodjenja;
            labelSportistaDres.Text = aktivniSportista.Broj_dresa.ToString();
            if (aktivniSportista.Klub != null)
            {
                labelSportistaKlub.Text = aktivniSportista.Klub.Naziv;
            }
            else
            {
                labelSportistaKlub.Text = "";
            }
            labelSportistaDrzava.Text = aktivniSportista.Drzava;
            labelSportistaVisina.Text = aktivniSportista.Visina;
            labelSportistaTezina.Text = aktivniSportista.Tezina;
            labelSportistaBrTel.Text = aktivniSportista.Broj_tel;
        }

        private void SetPanelStadion()
        {         
            panelStadion.Visible = true;
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                stekPanela.Push(p);
            }
            stekPanela.Push(panelStadion);
            listStadionLiga.Items.Clear();
            listStadionTakmicenj.Items.Clear();

            labelStadionNaziv.Text = aktivniStadion.Ime_stadiona;
            labelStadionGrad.Text = aktivniStadion.Grad;
            labelStadionLokacija.Text = aktivniStadion.Lokacija;
            labelStadionGodIzgradnje.Text = aktivniStadion.Godina_izgradnje;
            labelStadionKapacitet.Text = aktivniStadion.Kapacitet;

            if(aktivniStadion.GetUtakmice() != null)
            {
                for(int i=0; i <aktivniStadion.Utakmice.Count; i++)
                {
                    aktivniStadion.Utakmice[i].GetDomacin();
                    aktivniStadion.Utakmice[i].GetGost();

                    String rez = aktivniStadion.Utakmice[i].Rezultat;
                    int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                    int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                    ListViewItem item = new ListViewItem(new string[] { aktivniStadion.Utakmice[i].Datum, aktivniStadion.Utakmice[i].Vreme, aktivniStadion.Utakmice[i].Domacin.Naziv,
                     domacin.ToString(), gost.ToString(), aktivniStadion.Utakmice[i].Gost.Naziv, aktivniStadion.Utakmice[i].ID.ToString() });

                    listStadionLiga.Items.Add(item);
                }
            }

            if(aktivniStadion.GetTakmicenja() != null)
            {
                for(int i=0; i < aktivniStadion.TakmicenjaPojedinacna.Count; i++)
                {
                    aktivniStadion.TakmicenjaPojedinacna[i].GetDisciplina();

                    ListViewItem item = new ListViewItem(new string[] { aktivniStadion.TakmicenjaPojedinacna[i].Datum_osnivanja, aktivniStadion.TakmicenjaPojedinacna[i].Naziv,
                    aktivniStadion.TakmicenjaPojedinacna[i].Disciplina.Naziv, aktivniStadion.TakmicenjaPojedinacna[i].Disciplina.Pol, aktivniStadion.TakmicenjaPojedinacna[i].ID.ToString() });

                    listStadionTakmicenj.Items.Add(item);
                }
            }
        }

        private void SetPanelSudija()
        {
            panelSudija.Visible = true;
            if (stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                p.Visible = false;
                stekPanela.Push(p);
            }
            stekPanela.Push(panelSudija);
            listSudijaEkipni.Items.Clear();
            listSudijaPojedinacni.Items.Clear();

            aktivniSudija.GetSport();
            aktivniSudija.GetUtakmice();
            aktivniSudija.GetTakmicenja();

            labelSudijaIme.Text = aktivniSudija.Ime;
            labelSudijaPrezime.Text = aktivniSudija.Prezime;
            labelSudijaSport.Text = aktivniSudija.Sport.Naziv;
            labelSudijaGodSudjenja.Text = aktivniSudija.Godina_sudjenja;
            labelSudijaDatRodjenja.Text = aktivniSudija.Datum_rodjenja;
            labelSudijaDrzava.Text = aktivniSudija.Drzava;
            labelSudijaGrad.Text = aktivniSudija.Grad;

            listSudijaEkipni.Items.Clear();
            listSudijaPojedinacni.Items.Clear();
            if(aktivniSudija.Takmicenja != null && aktivniSudija.Takmicenja.Count > 0)
            {
                listSudijaEkipni.Visible = false;
                listSudijaPojedinacni.Visible = true;

                for (int i = 0; i < aktivniSudija.Takmicenja.Count; i++)
                {
                    aktivniSudija.Takmicenja[i].GetDisciplina();

                    ListViewItem item = new ListViewItem(new string[] { aktivniSudija.Takmicenja[i].Datum_osnivanja, aktivniSudija.Takmicenja[i].Naziv,
                    aktivniSudija.Takmicenja[i].Disciplina.Naziv, aktivniSudija.Takmicenja[i].Disciplina.Pol, aktivniSudija.Takmicenja[i].ID.ToString() });

                    listSudijaPojedinacni.Items.Add(item);
                }

            }
            else
            {
                listSudijaPojedinacni.Visible = false;
                listSudijaEkipni.Visible = true;

                for (int i = 0; i < aktivniSudija.Utakmice.Count; i++)
                {
                    aktivniSudija.Utakmice[i].GetDomacin();
                    aktivniSudija.Utakmice[i].GetGost();

                    String rez = aktivniSudija.Utakmice[i].Rezultat;
                    int domacin = Convert.ToInt32(rez.Substring(0, rez.IndexOf(' ')));
                    int gost = Convert.ToInt32(rez.Substring(rez.IndexOf(' ')));

                    ListViewItem item = new ListViewItem(new string[] { aktivniSudija.Utakmice[i].Datum, aktivniSudija.Utakmice[i].Vreme, aktivniSudija.Utakmice[i].Domacin.Naziv,
                     domacin.ToString(), gost.ToString(), aktivniSudija.Utakmice[i].Gost.Naziv, aktivniSudija.Utakmice[i].ID.ToString() });

                    listSudijaEkipni.Items.Add(item);
                }
            }
            
        }
    
        private void SetPanelTakmicenjePojedinacno(Takmicenje izabrano, Disciplina disciplina)
        {           
            panelTakmicenjePojedinacno.Visible = true;
            if(stekPanela.Count != 0)
            {
                Panel p = stekPanela.Pop();
                if (p != panelTakmicenjePojedinacno)
                {
                    p.Visible = false;
                    stekPanela.Push(p);
                }
            }     
            stekPanela.Push(panelTakmicenjePojedinacno);

            izabrano.GetTakmicari();
            izabrano.GetSudija();
            izabrano.GetStadion();

            listTakmicenjePojedinacno.Items.Clear();
            labelTakmicenjePojedinacnoSport.Text = Sports[IndexSport].Naziv;
            labelTakmicenjePojedinacnoAsociacija.Text = Sports[IndexSport].Sportska_asociacija;
            labelTakmicenjePojedinacnoZemlja.Text = izabrano.Zemlja;
            labelTakmicenjePojedinacnoDatum.Text = izabrano.Datum_osnivanja;
            labelTakmicenjePojedinacnoPol.Text = disciplina.Pol;
            labelTakmicenjePojedinacnoTip.Text = disciplina.Tip;
            labelTakmicenjePojedinacnoSponzor.Text = izabrano.Sponzor;
            labelTAkmicenjePojedinacnoDisciplina.Text = disciplina.Naziv;
            labelTakmicenjePojedinacnoBrUcesnika.Text = izabrano.Takmicari.Count.ToString();
            linkLabelTakmicenjePojedinacnoSudija.Text = izabrano.Sudija.Ime + " " + izabrano.Sudija.Prezime;
            linkLabelStadion.Text = izabrano.Stadion.Ime_stadiona;

            for(int i=0; i <izabrano.Takmicari.Count; i++)
            {
                izabrano.Takmicari[i].GetSportista();
                ListViewItem item = new ListViewItem(new string[] { izabrano.Takmicari[i].Mesto.ToString(), izabrano.Takmicari[i].Sportista.Ime, izabrano.Takmicari[i].Sportista.Prezime,
                izabrano.Takmicari[i].Rezultat, izabrano.Takmicari[i].IDSportista.ToString() });

                listTakmicenjePojedinacno.Items.Add(item);
            }

            aktivnoTakmicenje = izabrano;
        }
    }
}
