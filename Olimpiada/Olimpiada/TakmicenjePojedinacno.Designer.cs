﻿namespace Olimpiada
{
    partial class TakmicenjePojedinacno
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDomacin = new System.Windows.Forms.Label();
            this.labelRezultat = new System.Windows.Forms.Label();
            this.comboBoxA1 = new System.Windows.Forms.ComboBox();
            this.textBoxA1 = new System.Windows.Forms.TextBox();
            this.comboBoxA2 = new System.Windows.Forms.ComboBox();
            this.textBoxA2 = new System.Windows.Forms.TextBox();
            this.comboBoxA3 = new System.Windows.Forms.ComboBox();
            this.comboBoxSudija = new System.Windows.Forms.ComboBox();
            this.textBoxA3 = new System.Windows.Forms.TextBox();
            this.button8 = new System.Windows.Forms.Button();
            this.comboBoxA4 = new System.Windows.Forms.ComboBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBoxA4 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBoxA5 = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxA5 = new System.Windows.Forms.TextBox();
            this.comboBoxA6 = new System.Windows.Forms.ComboBox();
            this.textBoxA6 = new System.Windows.Forms.TextBox();
            this.comboBoxA7 = new System.Windows.Forms.ComboBox();
            this.textBoxA7 = new System.Windows.Forms.TextBox();
            this.comboBoxA8 = new System.Windows.Forms.ComboBox();
            this.textBoxA8 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxStadion = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.labelDrzava = new System.Windows.Forms.Label();
            this.buttonIzmeni = new System.Windows.Forms.Button();
            this.buttonKreiraj = new System.Windows.Forms.Button();
            this.label = new System.Windows.Forms.Label();
            this.dateTimePickerDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.textBoxNaziv = new System.Windows.Forms.TextBox();
            this.labelDatumRodjenja = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.textBoxZemlja = new System.Windows.Forms.TextBox();
            this.textBoxSponzor = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // labelDomacin
            // 
            this.labelDomacin.AutoSize = true;
            this.labelDomacin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDomacin.Location = new System.Drawing.Point(75, 30);
            this.labelDomacin.Name = "labelDomacin";
            this.labelDomacin.Size = new System.Drawing.Size(65, 16);
            this.labelDomacin.TabIndex = 294;
            this.labelDomacin.Text = "Takmicar";
            // 
            // labelRezultat
            // 
            this.labelRezultat.AutoSize = true;
            this.labelRezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRezultat.Location = new System.Drawing.Point(198, 30);
            this.labelRezultat.Name = "labelRezultat";
            this.labelRezultat.Size = new System.Drawing.Size(56, 16);
            this.labelRezultat.TabIndex = 296;
            this.labelRezultat.Text = "Rezultat";
            // 
            // comboBoxA1
            // 
            this.comboBoxA1.FormattingEnabled = true;
            this.comboBoxA1.Location = new System.Drawing.Point(45, 53);
            this.comboBoxA1.Name = "comboBoxA1";
            this.comboBoxA1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA1.TabIndex = 297;         
            // 
            // textBoxA1
            // 
            this.textBoxA1.Location = new System.Drawing.Point(192, 53);
            this.textBoxA1.Name = "textBoxA1";
            this.textBoxA1.Size = new System.Drawing.Size(75, 20);
            this.textBoxA1.TabIndex = 301;
            // 
            // comboBoxA2
            // 
            this.comboBoxA2.FormattingEnabled = true;
            this.comboBoxA2.Location = new System.Drawing.Point(45, 79);
            this.comboBoxA2.Name = "comboBoxA2";
            this.comboBoxA2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA2.TabIndex = 302;
            // 
            // textBoxA2
            // 
            this.textBoxA2.Location = new System.Drawing.Point(192, 79);
            this.textBoxA2.Name = "textBoxA2";
            this.textBoxA2.Size = new System.Drawing.Size(75, 20);
            this.textBoxA2.TabIndex = 306;
            // 
            // comboBoxA3
            // 
            this.comboBoxA3.FormattingEnabled = true;
            this.comboBoxA3.Location = new System.Drawing.Point(45, 105);
            this.comboBoxA3.Name = "comboBoxA3";
            this.comboBoxA3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA3.TabIndex = 307;
            // 
            // comboBoxSudija
            // 
            this.comboBoxSudija.FormattingEnabled = true;
            this.comboBoxSudija.Location = new System.Drawing.Point(133, 217);
            this.comboBoxSudija.Name = "comboBoxSudija";
            this.comboBoxSudija.Size = new System.Drawing.Size(121, 21);
            this.comboBoxSudija.TabIndex = 346;
            this.comboBoxSudija.SelectedIndexChanged += new System.EventHandler(this.comboBoxSudija_SelectedIndexChanged);
            // 
            // textBoxA3
            // 
            this.textBoxA3.Location = new System.Drawing.Point(192, 105);
            this.textBoxA3.Name = "textBoxA3";
            this.textBoxA3.Size = new System.Drawing.Size(75, 20);
            this.textBoxA3.TabIndex = 311;
            // 
            // button8
            // 
            this.button8.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button8.Location = new System.Drawing.Point(289, 233);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(39, 22);
            this.button8.TabIndex = 344;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // comboBoxA4
            // 
            this.comboBoxA4.FormattingEnabled = true;
            this.comboBoxA4.Location = new System.Drawing.Point(45, 131);
            this.comboBoxA4.Name = "comboBoxA4";
            this.comboBoxA4.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA4.TabIndex = 312;

            // 
            // button6
            // 
            this.button6.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button6.Location = new System.Drawing.Point(289, 181);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(39, 22);
            this.button6.TabIndex = 343;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button7.Location = new System.Drawing.Point(289, 207);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(39, 22);
            this.button7.TabIndex = 342;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button5.Location = new System.Drawing.Point(289, 155);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(39, 22);
            this.button5.TabIndex = 341;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button4.Location = new System.Drawing.Point(289, 130);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(39, 22);
            this.button4.TabIndex = 340;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxA4
            // 
            this.textBoxA4.Location = new System.Drawing.Point(192, 131);
            this.textBoxA4.Name = "textBoxA4";
            this.textBoxA4.Size = new System.Drawing.Size(75, 20);
            this.textBoxA4.TabIndex = 316;
            // 
            // button2
            // 
            this.button2.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button2.Location = new System.Drawing.Point(289, 78);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(39, 22);
            this.button2.TabIndex = 339;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button3.Location = new System.Drawing.Point(289, 104);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(39, 22);
            this.button3.TabIndex = 338;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBoxA5
            // 
            this.comboBoxA5.FormattingEnabled = true;
            this.comboBoxA5.Location = new System.Drawing.Point(45, 157);
            this.comboBoxA5.Name = "comboBoxA5";
            this.comboBoxA5.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA5.TabIndex = 317;
            // 
            // button1
            // 
            this.button1.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button1.Location = new System.Drawing.Point(289, 52);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(39, 22);
            this.button1.TabIndex = 337;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxA5
            // 
            this.textBoxA5.Location = new System.Drawing.Point(192, 157);
            this.textBoxA5.Name = "textBoxA5";
            this.textBoxA5.Size = new System.Drawing.Size(75, 20);
            this.textBoxA5.TabIndex = 321;
            // 
            // comboBoxA6
            // 
            this.comboBoxA6.FormattingEnabled = true;
            this.comboBoxA6.Location = new System.Drawing.Point(45, 183);
            this.comboBoxA6.Name = "comboBoxA6";
            this.comboBoxA6.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA6.TabIndex = 322;
            // 
            // textBoxA6
            // 
            this.textBoxA6.Location = new System.Drawing.Point(192, 183);
            this.textBoxA6.Name = "textBoxA6";
            this.textBoxA6.Size = new System.Drawing.Size(75, 20);
            this.textBoxA6.TabIndex = 326;
            // 
            // comboBoxA7
            // 
            this.comboBoxA7.FormattingEnabled = true;
            this.comboBoxA7.Location = new System.Drawing.Point(45, 209);
            this.comboBoxA7.Name = "comboBoxA7";
            this.comboBoxA7.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA7.TabIndex = 327;

            // 
            // textBoxA7
            // 
            this.textBoxA7.Location = new System.Drawing.Point(192, 209);
            this.textBoxA7.Name = "textBoxA7";
            this.textBoxA7.Size = new System.Drawing.Size(75, 20);
            this.textBoxA7.TabIndex = 331;
            // 
            // comboBoxA8
            // 
            this.comboBoxA8.FormattingEnabled = true;
            this.comboBoxA8.Location = new System.Drawing.Point(45, 235);
            this.comboBoxA8.Name = "comboBoxA8";
            this.comboBoxA8.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA8.TabIndex = 332;

            // 
            // textBoxA8
            // 
            this.textBoxA8.Location = new System.Drawing.Point(192, 235);
            this.textBoxA8.Name = "textBoxA8";
            this.textBoxA8.Size = new System.Drawing.Size(75, 20);
            this.textBoxA8.TabIndex = 336;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(13, 13);
            this.label1.TabIndex = 355;
            this.label1.Text = "1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 356;
            this.label2.Text = "2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 117);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(13, 13);
            this.label3.TabIndex = 357;
            this.label3.Text = "3";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(13, 13);
            this.label4.TabIndex = 358;
            this.label4.Text = "4";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 168);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(13, 13);
            this.label5.TabIndex = 359;
            this.label5.Text = "5";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 194);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(13, 13);
            this.label6.TabIndex = 360;
            this.label6.Text = "6";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(16, 220);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(13, 13);
            this.label7.TabIndex = 361;
            this.label7.Text = "7";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(16, 246);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 362;
            this.label8.Text = "8";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxA5);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxA8);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBoxA8);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxA7);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.comboBoxA7);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.textBoxA6);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxA6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.textBoxA5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.labelDomacin);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.labelRezultat);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.comboBoxA1);
            this.groupBox1.Controls.Add(this.textBoxA4);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.textBoxA1);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.comboBoxA2);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.textBoxA2);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.comboBoxA4);
            this.groupBox1.Controls.Add(this.comboBoxA3);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.textBoxA3);
            this.groupBox1.Location = new System.Drawing.Point(395, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(356, 272);
            this.groupBox1.TabIndex = 363;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Rezultati";
            // 
            // comboBoxStadion
            // 
            this.comboBoxStadion.FormattingEnabled = true;
            this.comboBoxStadion.Location = new System.Drawing.Point(133, 181);
            this.comboBoxStadion.Name = "comboBoxStadion";
            this.comboBoxStadion.Size = new System.Drawing.Size(121, 21);
            this.comboBoxStadion.TabIndex = 364;
            this.comboBoxStadion.SelectedIndexChanged += new System.EventHandler(this.comboBoxStadion_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.textBoxSponzor);
            this.groupBox2.Controls.Add(this.comboBoxStadion);
            this.groupBox2.Controls.Add(this.comboBoxSudija);
            this.groupBox2.Controls.Add(this.labelDrzava);
            this.groupBox2.Controls.Add(this.buttonIzmeni);
            this.groupBox2.Controls.Add(this.buttonKreiraj);
            this.groupBox2.Controls.Add(this.label);
            this.groupBox2.Controls.Add(this.dateTimePickerDatumRodjenja);
            this.groupBox2.Controls.Add(this.textBoxNaziv);
            this.groupBox2.Controls.Add(this.labelDatumRodjenja);
            this.groupBox2.Controls.Add(this.labelPrezime);
            this.groupBox2.Controls.Add(this.textBoxZemlja);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 310);
            this.groupBox2.TabIndex = 366;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Takmicenje";
            // 
            // labelDrzava
            // 
            this.labelDrzava.AutoSize = true;
            this.labelDrzava.Location = new System.Drawing.Point(15, 139);
            this.labelDrzava.Name = "labelDrzava";
            this.labelDrzava.Size = new System.Drawing.Size(46, 13);
            this.labelDrzava.TabIndex = 22;
            this.labelDrzava.Text = "Sponzor";
            // 
            // buttonIzmeni
            // 
            this.buttonIzmeni.Location = new System.Drawing.Point(168, 265);
            this.buttonIzmeni.Name = "buttonIzmeni";
            this.buttonIzmeni.Size = new System.Drawing.Size(91, 23);
            this.buttonIzmeni.TabIndex = 15;
            this.buttonIzmeni.Text = "Izmeni takmicenje";
            this.buttonIzmeni.UseVisualStyleBackColor = true;
            this.buttonIzmeni.Click += new System.EventHandler(this.buttonIzmeni_Click);
            // 
            // buttonKreiraj
            // 
            this.buttonKreiraj.Location = new System.Drawing.Point(63, 265);
            this.buttonKreiraj.Name = "buttonKreiraj";
            this.buttonKreiraj.Size = new System.Drawing.Size(99, 23);
            this.buttonKreiraj.TabIndex = 6;
            this.buttonKreiraj.Text = "Kreiraj takmicenje";
            this.buttonKreiraj.UseVisualStyleBackColor = true;
            this.buttonKreiraj.Click += new System.EventHandler(this.buttonKreiraj_Click);
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Location = new System.Drawing.Point(15, 31);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(34, 13);
            this.label.TabIndex = 2;
            this.label.Text = "Naziv";
            // 
            // dateTimePickerDatumRodjenja
            // 
            this.dateTimePickerDatumRodjenja.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePickerDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDatumRodjenja.Location = new System.Drawing.Point(133, 89);
            this.dateTimePickerDatumRodjenja.Name = "dateTimePickerDatumRodjenja";
            this.dateTimePickerDatumRodjenja.Size = new System.Drawing.Size(123, 20);
            this.dateTimePickerDatumRodjenja.TabIndex = 5;
            // 
            // textBoxNaziv
            // 
            this.textBoxNaziv.Location = new System.Drawing.Point(133, 32);
            this.textBoxNaziv.Name = "textBoxNaziv";
            this.textBoxNaziv.Size = new System.Drawing.Size(123, 20);
            this.textBoxNaziv.TabIndex = 0;
            // 
            // labelDatumRodjenja
            // 
            this.labelDatumRodjenja.AutoSize = true;
            this.labelDatumRodjenja.Location = new System.Drawing.Point(15, 96);
            this.labelDatumRodjenja.Name = "labelDatumRodjenja";
            this.labelDatumRodjenja.Size = new System.Drawing.Size(93, 13);
            this.labelDatumRodjenja.TabIndex = 4;
            this.labelDatumRodjenja.Text = "Datum odrzavanja";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(15, 63);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(38, 13);
            this.labelPrezime.TabIndex = 3;
            this.labelPrezime.Text = "Zemlja";
            // 
            // textBoxZemlja
            // 
            this.textBoxZemlja.Location = new System.Drawing.Point(133, 63);
            this.textBoxZemlja.Name = "textBoxZemlja";
            this.textBoxZemlja.Size = new System.Drawing.Size(123, 20);
            this.textBoxZemlja.TabIndex = 1;
            // 
            // textBoxSponzor
            // 
            this.textBoxSponzor.Location = new System.Drawing.Point(133, 132);
            this.textBoxSponzor.Name = "textBoxSponzor";
            this.textBoxSponzor.Size = new System.Drawing.Size(123, 20);
            this.textBoxSponzor.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(15, 190);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(43, 13);
            this.label9.TabIndex = 365;
            this.label9.Text = "Stadion";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 225);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(36, 13);
            this.label10.TabIndex = 366;
            this.label10.Text = "Sudija";
            // 
            // TakmicenjePojedinacno
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(759, 326);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "TakmicenjePojedinacno";
            this.Text = "TakmicenjePojedinacno";
            this.Load += new System.EventHandler(this.TakmicenjePojedinacno_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label labelDomacin;
        private System.Windows.Forms.Label labelRezultat;
        private System.Windows.Forms.ComboBox comboBoxA1;
        private System.Windows.Forms.TextBox textBoxA1;
        private System.Windows.Forms.ComboBox comboBoxA2;
        private System.Windows.Forms.TextBox textBoxA2;
        private System.Windows.Forms.ComboBox comboBoxA3;
        private System.Windows.Forms.ComboBox comboBoxSudija;
        private System.Windows.Forms.TextBox textBoxA3;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ComboBox comboBoxA4;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBoxA4;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBoxA5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxA5;
        private System.Windows.Forms.ComboBox comboBoxA6;
        private System.Windows.Forms.TextBox textBoxA6;
        private System.Windows.Forms.ComboBox comboBoxA7;
        private System.Windows.Forms.TextBox textBoxA7;
        private System.Windows.Forms.ComboBox comboBoxA8;
        private System.Windows.Forms.TextBox textBoxA8;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxStadion;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox textBoxSponzor;
        private System.Windows.Forms.Label labelDrzava;
        private System.Windows.Forms.Button buttonIzmeni;
        private System.Windows.Forms.Button buttonKreiraj;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.DateTimePicker dateTimePickerDatumRodjenja;
        private System.Windows.Forms.TextBox textBoxNaziv;
        private System.Windows.Forms.Label labelDatumRodjenja;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.TextBox textBoxZemlja;
    }
}