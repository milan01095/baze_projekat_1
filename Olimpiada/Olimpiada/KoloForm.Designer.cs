﻿namespace Olimpiada
{
    partial class KoloForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxA12 = new System.Windows.Forms.TextBox();
            this.textBoxB12 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBoxB12 = new System.Windows.Forms.ComboBox();
            this.comboBoxA12 = new System.Windows.Forms.ComboBox();
            this.date12 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA11 = new System.Windows.Forms.TextBox();
            this.textBoxB11 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.comboBoxB11 = new System.Windows.Forms.ComboBox();
            this.comboBoxA11 = new System.Windows.Forms.ComboBox();
            this.date11 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA10 = new System.Windows.Forms.TextBox();
            this.textBoxB10 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.comboBoxB10 = new System.Windows.Forms.ComboBox();
            this.comboBoxA10 = new System.Windows.Forms.ComboBox();
            this.date10 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA9 = new System.Windows.Forms.TextBox();
            this.textBoxB9 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.comboBoxB9 = new System.Windows.Forms.ComboBox();
            this.comboBoxA9 = new System.Windows.Forms.ComboBox();
            this.date9 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA8 = new System.Windows.Forms.TextBox();
            this.textBoxB8 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBoxB8 = new System.Windows.Forms.ComboBox();
            this.comboBoxA8 = new System.Windows.Forms.ComboBox();
            this.date8 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA7 = new System.Windows.Forms.TextBox();
            this.textBoxB7 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBoxB7 = new System.Windows.Forms.ComboBox();
            this.comboBoxA7 = new System.Windows.Forms.ComboBox();
            this.date7 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA6 = new System.Windows.Forms.TextBox();
            this.textBoxB6 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBoxB6 = new System.Windows.Forms.ComboBox();
            this.comboBoxA6 = new System.Windows.Forms.ComboBox();
            this.date6 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA5 = new System.Windows.Forms.TextBox();
            this.textBoxB5 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBoxB5 = new System.Windows.Forms.ComboBox();
            this.comboBoxA5 = new System.Windows.Forms.ComboBox();
            this.date5 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA4 = new System.Windows.Forms.TextBox();
            this.textBoxB4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxB4 = new System.Windows.Forms.ComboBox();
            this.comboBoxA4 = new System.Windows.Forms.ComboBox();
            this.date4 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA3 = new System.Windows.Forms.TextBox();
            this.textBoxB3 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBoxB3 = new System.Windows.Forms.ComboBox();
            this.comboBoxA3 = new System.Windows.Forms.ComboBox();
            this.date3 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA2 = new System.Windows.Forms.TextBox();
            this.textBoxB2 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBoxB2 = new System.Windows.Forms.ComboBox();
            this.comboBoxA2 = new System.Windows.Forms.ComboBox();
            this.date2 = new System.Windows.Forms.DateTimePicker();
            this.textBoxA1 = new System.Windows.Forms.TextBox();
            this.textBoxB1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBoxB1 = new System.Windows.Forms.ComboBox();
            this.comboBoxA1 = new System.Windows.Forms.ComboBox();
            this.date1 = new System.Windows.Forms.DateTimePicker();
            this.labelRezultat = new System.Windows.Forms.Label();
            this.labelGost = new System.Windows.Forms.Label();
            this.labelDomacin = new System.Windows.Forms.Label();
            this.labelDatumIVreme = new System.Windows.Forms.Label();
            this.labelNazivLige = new System.Windows.Forms.Label();
            this.labelBrojKola = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.textBoxRedniBrojKola = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dateTimePickerKraj = new System.Windows.Forms.DateTimePicker();
            this.dateTimePickerPocetak = new System.Windows.Forms.DateTimePicker();
            this.Kreiraj = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxA12
            // 
            this.textBoxA12.Location = new System.Drawing.Point(396, 337);
            this.textBoxA12.Name = "textBoxA12";
            this.textBoxA12.Size = new System.Drawing.Size(21, 20);
            this.textBoxA12.TabIndex = 268;
            // 
            // textBoxB12
            // 
            this.textBoxB12.Location = new System.Drawing.Point(439, 337);
            this.textBoxB12.Name = "textBoxB12";
            this.textBoxB12.Size = new System.Drawing.Size(21, 20);
            this.textBoxB12.TabIndex = 267;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(423, 339);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(10, 13);
            this.label9.TabIndex = 266;
            this.label9.Text = ":";
            // 
            // comboBoxB12
            // 
            this.comboBoxB12.FormattingEnabled = true;
            this.comboBoxB12.Location = new System.Drawing.Point(497, 335);
            this.comboBoxB12.Name = "comboBoxB12";
            this.comboBoxB12.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB12.TabIndex = 265;
            // 
            // comboBoxA12
            // 
            this.comboBoxA12.FormattingEnabled = true;
            this.comboBoxA12.Location = new System.Drawing.Point(233, 335);
            this.comboBoxA12.Name = "comboBoxA12";
            this.comboBoxA12.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA12.TabIndex = 264;
            // 
            // date12
            // 
            this.date12.Location = new System.Drawing.Point(6, 338);
            this.date12.Name = "date12";
            this.date12.Size = new System.Drawing.Size(200, 20);
            this.date12.TabIndex = 263;
            // 
            // textBoxA11
            // 
            this.textBoxA11.Location = new System.Drawing.Point(396, 311);
            this.textBoxA11.Name = "textBoxA11";
            this.textBoxA11.Size = new System.Drawing.Size(21, 20);
            this.textBoxA11.TabIndex = 262;
            // 
            // textBoxB11
            // 
            this.textBoxB11.Location = new System.Drawing.Point(439, 311);
            this.textBoxB11.Name = "textBoxB11";
            this.textBoxB11.Size = new System.Drawing.Size(21, 20);
            this.textBoxB11.TabIndex = 261;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(423, 313);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(10, 13);
            this.label10.TabIndex = 260;
            this.label10.Text = ":";
            // 
            // comboBoxB11
            // 
            this.comboBoxB11.FormattingEnabled = true;
            this.comboBoxB11.Location = new System.Drawing.Point(497, 309);
            this.comboBoxB11.Name = "comboBoxB11";
            this.comboBoxB11.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB11.TabIndex = 259;
            // 
            // comboBoxA11
            // 
            this.comboBoxA11.FormattingEnabled = true;
            this.comboBoxA11.Location = new System.Drawing.Point(233, 309);
            this.comboBoxA11.Name = "comboBoxA11";
            this.comboBoxA11.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA11.TabIndex = 258;
            // 
            // date11
            // 
            this.date11.Location = new System.Drawing.Point(6, 312);
            this.date11.Name = "date11";
            this.date11.Size = new System.Drawing.Size(200, 20);
            this.date11.TabIndex = 257;
            // 
            // textBoxA10
            // 
            this.textBoxA10.Location = new System.Drawing.Point(396, 285);
            this.textBoxA10.Name = "textBoxA10";
            this.textBoxA10.Size = new System.Drawing.Size(21, 20);
            this.textBoxA10.TabIndex = 256;
            // 
            // textBoxB10
            // 
            this.textBoxB10.Location = new System.Drawing.Point(439, 285);
            this.textBoxB10.Name = "textBoxB10";
            this.textBoxB10.Size = new System.Drawing.Size(21, 20);
            this.textBoxB10.TabIndex = 255;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(423, 287);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(10, 13);
            this.label11.TabIndex = 254;
            this.label11.Text = ":";
            // 
            // comboBoxB10
            // 
            this.comboBoxB10.FormattingEnabled = true;
            this.comboBoxB10.Location = new System.Drawing.Point(497, 283);
            this.comboBoxB10.Name = "comboBoxB10";
            this.comboBoxB10.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB10.TabIndex = 253;
            // 
            // comboBoxA10
            // 
            this.comboBoxA10.FormattingEnabled = true;
            this.comboBoxA10.Location = new System.Drawing.Point(233, 283);
            this.comboBoxA10.Name = "comboBoxA10";
            this.comboBoxA10.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA10.TabIndex = 252;
            // 
            // date10
            // 
            this.date10.Location = new System.Drawing.Point(6, 286);
            this.date10.Name = "date10";
            this.date10.Size = new System.Drawing.Size(200, 20);
            this.date10.TabIndex = 251;
            // 
            // textBoxA9
            // 
            this.textBoxA9.Location = new System.Drawing.Point(396, 259);
            this.textBoxA9.Name = "textBoxA9";
            this.textBoxA9.Size = new System.Drawing.Size(21, 20);
            this.textBoxA9.TabIndex = 250;
            // 
            // textBoxB9
            // 
            this.textBoxB9.Location = new System.Drawing.Point(439, 259);
            this.textBoxB9.Name = "textBoxB9";
            this.textBoxB9.Size = new System.Drawing.Size(21, 20);
            this.textBoxB9.TabIndex = 249;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(423, 261);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(10, 13);
            this.label12.TabIndex = 248;
            this.label12.Text = ":";
            // 
            // comboBoxB9
            // 
            this.comboBoxB9.FormattingEnabled = true;
            this.comboBoxB9.Location = new System.Drawing.Point(497, 257);
            this.comboBoxB9.Name = "comboBoxB9";
            this.comboBoxB9.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB9.TabIndex = 247;
            // 
            // comboBoxA9
            // 
            this.comboBoxA9.FormattingEnabled = true;
            this.comboBoxA9.Location = new System.Drawing.Point(233, 257);
            this.comboBoxA9.Name = "comboBoxA9";
            this.comboBoxA9.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA9.TabIndex = 246;
            // 
            // date9
            // 
            this.date9.Location = new System.Drawing.Point(6, 260);
            this.date9.Name = "date9";
            this.date9.Size = new System.Drawing.Size(200, 20);
            this.date9.TabIndex = 245;
            // 
            // textBoxA8
            // 
            this.textBoxA8.Location = new System.Drawing.Point(396, 233);
            this.textBoxA8.Name = "textBoxA8";
            this.textBoxA8.Size = new System.Drawing.Size(21, 20);
            this.textBoxA8.TabIndex = 244;
            // 
            // textBoxB8
            // 
            this.textBoxB8.Location = new System.Drawing.Point(439, 233);
            this.textBoxB8.Name = "textBoxB8";
            this.textBoxB8.Size = new System.Drawing.Size(21, 20);
            this.textBoxB8.TabIndex = 243;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(423, 235);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(10, 13);
            this.label5.TabIndex = 242;
            this.label5.Text = ":";
            // 
            // comboBoxB8
            // 
            this.comboBoxB8.FormattingEnabled = true;
            this.comboBoxB8.Location = new System.Drawing.Point(497, 231);
            this.comboBoxB8.Name = "comboBoxB8";
            this.comboBoxB8.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB8.TabIndex = 241;
            // 
            // comboBoxA8
            // 
            this.comboBoxA8.FormattingEnabled = true;
            this.comboBoxA8.Location = new System.Drawing.Point(233, 231);
            this.comboBoxA8.Name = "comboBoxA8";
            this.comboBoxA8.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA8.TabIndex = 240;
            // 
            // date8
            // 
            this.date8.Location = new System.Drawing.Point(6, 234);
            this.date8.Name = "date8";
            this.date8.Size = new System.Drawing.Size(200, 20);
            this.date8.TabIndex = 239;
            // 
            // textBoxA7
            // 
            this.textBoxA7.Location = new System.Drawing.Point(396, 207);
            this.textBoxA7.Name = "textBoxA7";
            this.textBoxA7.Size = new System.Drawing.Size(21, 20);
            this.textBoxA7.TabIndex = 238;
            // 
            // textBoxB7
            // 
            this.textBoxB7.Location = new System.Drawing.Point(439, 207);
            this.textBoxB7.Name = "textBoxB7";
            this.textBoxB7.Size = new System.Drawing.Size(21, 20);
            this.textBoxB7.TabIndex = 237;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(423, 209);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(10, 13);
            this.label6.TabIndex = 236;
            this.label6.Text = ":";
            // 
            // comboBoxB7
            // 
            this.comboBoxB7.FormattingEnabled = true;
            this.comboBoxB7.Location = new System.Drawing.Point(497, 205);
            this.comboBoxB7.Name = "comboBoxB7";
            this.comboBoxB7.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB7.TabIndex = 235;
            // 
            // comboBoxA7
            // 
            this.comboBoxA7.FormattingEnabled = true;
            this.comboBoxA7.Location = new System.Drawing.Point(233, 205);
            this.comboBoxA7.Name = "comboBoxA7";
            this.comboBoxA7.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA7.TabIndex = 234;
            // 
            // date7
            // 
            this.date7.Location = new System.Drawing.Point(6, 208);
            this.date7.Name = "date7";
            this.date7.Size = new System.Drawing.Size(200, 20);
            this.date7.TabIndex = 233;
            // 
            // textBoxA6
            // 
            this.textBoxA6.Location = new System.Drawing.Point(396, 181);
            this.textBoxA6.Name = "textBoxA6";
            this.textBoxA6.Size = new System.Drawing.Size(21, 20);
            this.textBoxA6.TabIndex = 232;
            // 
            // textBoxB6
            // 
            this.textBoxB6.Location = new System.Drawing.Point(439, 181);
            this.textBoxB6.Name = "textBoxB6";
            this.textBoxB6.Size = new System.Drawing.Size(21, 20);
            this.textBoxB6.TabIndex = 231;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(423, 183);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(10, 13);
            this.label7.TabIndex = 230;
            this.label7.Text = ":";
            // 
            // comboBoxB6
            // 
            this.comboBoxB6.FormattingEnabled = true;
            this.comboBoxB6.Location = new System.Drawing.Point(497, 179);
            this.comboBoxB6.Name = "comboBoxB6";
            this.comboBoxB6.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB6.TabIndex = 229;
            // 
            // comboBoxA6
            // 
            this.comboBoxA6.FormattingEnabled = true;
            this.comboBoxA6.Location = new System.Drawing.Point(233, 179);
            this.comboBoxA6.Name = "comboBoxA6";
            this.comboBoxA6.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA6.TabIndex = 228;
            // 
            // date6
            // 
            this.date6.Location = new System.Drawing.Point(6, 182);
            this.date6.Name = "date6";
            this.date6.Size = new System.Drawing.Size(200, 20);
            this.date6.TabIndex = 227;
            // 
            // textBoxA5
            // 
            this.textBoxA5.Location = new System.Drawing.Point(396, 155);
            this.textBoxA5.Name = "textBoxA5";
            this.textBoxA5.Size = new System.Drawing.Size(21, 20);
            this.textBoxA5.TabIndex = 226;
            // 
            // textBoxB5
            // 
            this.textBoxB5.Location = new System.Drawing.Point(439, 155);
            this.textBoxB5.Name = "textBoxB5";
            this.textBoxB5.Size = new System.Drawing.Size(21, 20);
            this.textBoxB5.TabIndex = 225;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(423, 157);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(10, 13);
            this.label8.TabIndex = 224;
            this.label8.Text = ":";
            // 
            // comboBoxB5
            // 
            this.comboBoxB5.FormattingEnabled = true;
            this.comboBoxB5.Location = new System.Drawing.Point(497, 153);
            this.comboBoxB5.Name = "comboBoxB5";
            this.comboBoxB5.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB5.TabIndex = 223;
            // 
            // comboBoxA5
            // 
            this.comboBoxA5.FormattingEnabled = true;
            this.comboBoxA5.Location = new System.Drawing.Point(233, 153);
            this.comboBoxA5.Name = "comboBoxA5";
            this.comboBoxA5.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA5.TabIndex = 222;
            // 
            // date5
            // 
            this.date5.Location = new System.Drawing.Point(6, 156);
            this.date5.Name = "date5";
            this.date5.Size = new System.Drawing.Size(200, 20);
            this.date5.TabIndex = 221;
            // 
            // textBoxA4
            // 
            this.textBoxA4.Location = new System.Drawing.Point(396, 129);
            this.textBoxA4.Name = "textBoxA4";
            this.textBoxA4.Size = new System.Drawing.Size(21, 20);
            this.textBoxA4.TabIndex = 220;
            // 
            // textBoxB4
            // 
            this.textBoxB4.Location = new System.Drawing.Point(439, 129);
            this.textBoxB4.Name = "textBoxB4";
            this.textBoxB4.Size = new System.Drawing.Size(21, 20);
            this.textBoxB4.TabIndex = 219;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(423, 131);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(10, 13);
            this.label3.TabIndex = 218;
            this.label3.Text = ":";
            // 
            // comboBoxB4
            // 
            this.comboBoxB4.FormattingEnabled = true;
            this.comboBoxB4.Location = new System.Drawing.Point(497, 127);
            this.comboBoxB4.Name = "comboBoxB4";
            this.comboBoxB4.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB4.TabIndex = 217;
            // 
            // comboBoxA4
            // 
            this.comboBoxA4.FormattingEnabled = true;
            this.comboBoxA4.Location = new System.Drawing.Point(233, 127);
            this.comboBoxA4.Name = "comboBoxA4";
            this.comboBoxA4.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA4.TabIndex = 216;
            // 
            // date4
            // 
            this.date4.Location = new System.Drawing.Point(6, 130);
            this.date4.Name = "date4";
            this.date4.Size = new System.Drawing.Size(200, 20);
            this.date4.TabIndex = 215;
            // 
            // textBoxA3
            // 
            this.textBoxA3.Location = new System.Drawing.Point(396, 103);
            this.textBoxA3.Name = "textBoxA3";
            this.textBoxA3.Size = new System.Drawing.Size(21, 20);
            this.textBoxA3.TabIndex = 214;
            // 
            // textBoxB3
            // 
            this.textBoxB3.Location = new System.Drawing.Point(439, 103);
            this.textBoxB3.Name = "textBoxB3";
            this.textBoxB3.Size = new System.Drawing.Size(21, 20);
            this.textBoxB3.TabIndex = 213;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(423, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(10, 13);
            this.label4.TabIndex = 212;
            this.label4.Text = ":";
            // 
            // comboBoxB3
            // 
            this.comboBoxB3.FormattingEnabled = true;
            this.comboBoxB3.Location = new System.Drawing.Point(497, 101);
            this.comboBoxB3.Name = "comboBoxB3";
            this.comboBoxB3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB3.TabIndex = 211;
            // 
            // comboBoxA3
            // 
            this.comboBoxA3.FormattingEnabled = true;
            this.comboBoxA3.Location = new System.Drawing.Point(233, 101);
            this.comboBoxA3.Name = "comboBoxA3";
            this.comboBoxA3.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA3.TabIndex = 210;
            // 
            // date3
            // 
            this.date3.Location = new System.Drawing.Point(6, 104);
            this.date3.Name = "date3";
            this.date3.Size = new System.Drawing.Size(200, 20);
            this.date3.TabIndex = 209;
            // 
            // textBoxA2
            // 
            this.textBoxA2.Location = new System.Drawing.Point(396, 77);
            this.textBoxA2.Name = "textBoxA2";
            this.textBoxA2.Size = new System.Drawing.Size(21, 20);
            this.textBoxA2.TabIndex = 208;
            // 
            // textBoxB2
            // 
            this.textBoxB2.Location = new System.Drawing.Point(439, 77);
            this.textBoxB2.Name = "textBoxB2";
            this.textBoxB2.Size = new System.Drawing.Size(21, 20);
            this.textBoxB2.TabIndex = 207;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(423, 79);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(10, 13);
            this.label2.TabIndex = 206;
            this.label2.Text = ":";
            // 
            // comboBoxB2
            // 
            this.comboBoxB2.FormattingEnabled = true;
            this.comboBoxB2.Location = new System.Drawing.Point(497, 75);
            this.comboBoxB2.Name = "comboBoxB2";
            this.comboBoxB2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB2.TabIndex = 205;
            // 
            // comboBoxA2
            // 
            this.comboBoxA2.FormattingEnabled = true;
            this.comboBoxA2.Location = new System.Drawing.Point(233, 75);
            this.comboBoxA2.Name = "comboBoxA2";
            this.comboBoxA2.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA2.TabIndex = 204;
            // 
            // date2
            // 
            this.date2.Location = new System.Drawing.Point(6, 78);
            this.date2.Name = "date2";
            this.date2.Size = new System.Drawing.Size(200, 20);
            this.date2.TabIndex = 203;
            // 
            // textBoxA1
            // 
            this.textBoxA1.Location = new System.Drawing.Point(396, 51);
            this.textBoxA1.Name = "textBoxA1";
            this.textBoxA1.Size = new System.Drawing.Size(21, 20);
            this.textBoxA1.TabIndex = 202;
            // 
            // textBoxB1
            // 
            this.textBoxB1.Location = new System.Drawing.Point(439, 51);
            this.textBoxB1.Name = "textBoxB1";
            this.textBoxB1.Size = new System.Drawing.Size(21, 20);
            this.textBoxB1.TabIndex = 201;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(423, 53);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(10, 13);
            this.label1.TabIndex = 200;
            this.label1.Text = ":";
            // 
            // comboBoxB1
            // 
            this.comboBoxB1.FormattingEnabled = true;
            this.comboBoxB1.Location = new System.Drawing.Point(497, 49);
            this.comboBoxB1.Name = "comboBoxB1";
            this.comboBoxB1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxB1.TabIndex = 199;
            // 
            // comboBoxA1
            // 
            this.comboBoxA1.FormattingEnabled = true;
            this.comboBoxA1.Location = new System.Drawing.Point(233, 49);
            this.comboBoxA1.Name = "comboBoxA1";
            this.comboBoxA1.Size = new System.Drawing.Size(121, 21);
            this.comboBoxA1.TabIndex = 198;
            // 
            // date1
            // 
            this.date1.Location = new System.Drawing.Point(6, 52);
            this.date1.Name = "date1";
            this.date1.Size = new System.Drawing.Size(200, 20);
            this.date1.TabIndex = 197;
            // 
            // labelRezultat
            // 
            this.labelRezultat.AutoSize = true;
            this.labelRezultat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRezultat.Location = new System.Drawing.Point(403, 26);
            this.labelRezultat.Name = "labelRezultat";
            this.labelRezultat.Size = new System.Drawing.Size(56, 16);
            this.labelRezultat.TabIndex = 196;
            this.labelRezultat.Text = "Rezultat";
            // 
            // labelGost
            // 
            this.labelGost.AutoSize = true;
            this.labelGost.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGost.Location = new System.Drawing.Point(536, 26);
            this.labelGost.Name = "labelGost";
            this.labelGost.Size = new System.Drawing.Size(36, 16);
            this.labelGost.TabIndex = 195;
            this.labelGost.Text = "Gost";
            // 
            // labelDomacin
            // 
            this.labelDomacin.AutoSize = true;
            this.labelDomacin.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDomacin.Location = new System.Drawing.Point(263, 26);
            this.labelDomacin.Name = "labelDomacin";
            this.labelDomacin.Size = new System.Drawing.Size(62, 16);
            this.labelDomacin.TabIndex = 194;
            this.labelDomacin.Text = "Domacin";
            // 
            // labelDatumIVreme
            // 
            this.labelDatumIVreme.AutoSize = true;
            this.labelDatumIVreme.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelDatumIVreme.Location = new System.Drawing.Point(53, 26);
            this.labelDatumIVreme.Name = "labelDatumIVreme";
            this.labelDatumIVreme.Size = new System.Drawing.Size(96, 16);
            this.labelDatumIVreme.TabIndex = 193;
            this.labelDatumIVreme.Text = "Datum | Vreme";
            // 
            // labelNazivLige
            // 
            this.labelNazivLige.AutoSize = true;
            this.labelNazivLige.Font = new System.Drawing.Font("Modern No. 20", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNazivLige.Location = new System.Drawing.Point(106, 64);
            this.labelNazivLige.Name = "labelNazivLige";
            this.labelNazivLige.Size = new System.Drawing.Size(97, 24);
            this.labelNazivLige.TabIndex = 192;
            this.labelNazivLige.Text = "Naziv lige";
            // 
            // labelBrojKola
            // 
            this.labelBrojKola.AutoSize = true;
            this.labelBrojKola.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelBrojKola.Location = new System.Drawing.Point(106, 25);
            this.labelBrojKola.Name = "labelBrojKola";
            this.labelBrojKola.Size = new System.Drawing.Size(135, 21);
            this.labelBrojKola.TabIndex = 190;
            this.labelBrojKola.Text = "Redni broj kola:";
            // 
            // button12
            // 
            this.button12.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button12.Location = new System.Drawing.Point(789, 330);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(23, 22);
            this.button12.TabIndex = 280;
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button10
            // 
            this.button10.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button10.Location = new System.Drawing.Point(789, 278);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(23, 22);
            this.button10.TabIndex = 279;
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button11
            // 
            this.button11.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button11.Location = new System.Drawing.Point(789, 304);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(23, 22);
            this.button11.TabIndex = 278;
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button9
            // 
            this.button9.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button9.Location = new System.Drawing.Point(790, 252);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(23, 22);
            this.button9.TabIndex = 277;
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button8
            // 
            this.button8.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button8.Location = new System.Drawing.Point(790, 227);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(23, 22);
            this.button8.TabIndex = 276;
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button6
            // 
            this.button6.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button6.Location = new System.Drawing.Point(790, 175);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(23, 22);
            this.button6.TabIndex = 275;
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button7.Location = new System.Drawing.Point(790, 201);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(23, 22);
            this.button7.TabIndex = 274;
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button5
            // 
            this.button5.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button5.Location = new System.Drawing.Point(790, 149);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(23, 22);
            this.button5.TabIndex = 273;
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button4.Location = new System.Drawing.Point(790, 124);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(23, 22);
            this.button4.TabIndex = 272;
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button2
            // 
            this.button2.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button2.Location = new System.Drawing.Point(790, 72);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(23, 22);
            this.button2.TabIndex = 271;
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button3.Location = new System.Drawing.Point(790, 98);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(23, 22);
            this.button3.TabIndex = 270;
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button1
            // 
            this.button1.Image = global::Olimpiada.Properties.Resources.edit_14;
            this.button1.Location = new System.Drawing.Point(790, 46);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(23, 22);
            this.button1.TabIndex = 269;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(644, 335);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(121, 21);
            this.comboBox12.TabIndex = 292;
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(644, 309);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(121, 21);
            this.comboBox11.TabIndex = 291;
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(644, 283);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(121, 21);
            this.comboBox10.TabIndex = 290;
            // 
            // comboBox9
            // 
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(644, 257);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(121, 21);
            this.comboBox9.TabIndex = 289;
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(644, 231);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(121, 21);
            this.comboBox8.TabIndex = 288;
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(644, 205);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(121, 21);
            this.comboBox7.TabIndex = 287;
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(644, 179);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(121, 21);
            this.comboBox6.TabIndex = 286;
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(644, 153);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(121, 21);
            this.comboBox5.TabIndex = 285;
            // 
            // comboBox4
            // 
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Location = new System.Drawing.Point(644, 127);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(121, 21);
            this.comboBox4.TabIndex = 284;
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Location = new System.Drawing.Point(644, 101);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(121, 21);
            this.comboBox3.TabIndex = 283;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(644, 75);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 21);
            this.comboBox2.TabIndex = 282;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(644, 49);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 281;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(679, 26);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 16);
            this.label13.TabIndex = 293;
            this.label13.Text = "Sudija";
            // 
            // textBoxRedniBrojKola
            // 
            this.textBoxRedniBrojKola.Location = new System.Drawing.Point(247, 28);
            this.textBoxRedniBrojKola.Name = "textBoxRedniBrojKola";
            this.textBoxRedniBrojKola.Size = new System.Drawing.Size(41, 20);
            this.textBoxRedniBrojKola.TabIndex = 294;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(391, 23);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(135, 21);
            this.label14.TabIndex = 295;
            this.label14.Text = "Redni broj kola:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Modern No. 20", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(391, 65);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(135, 21);
            this.label15.TabIndex = 296;
            this.label15.Text = "Redni broj kola:";
            // 
            // dateTimePickerKraj
            // 
            this.dateTimePickerKraj.Location = new System.Drawing.Point(535, 66);
            this.dateTimePickerKraj.Name = "dateTimePickerKraj";
            this.dateTimePickerKraj.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerKraj.TabIndex = 298;
            // 
            // dateTimePickerPocetak
            // 
            this.dateTimePickerPocetak.Location = new System.Drawing.Point(535, 23);
            this.dateTimePickerPocetak.Name = "dateTimePickerPocetak";
            this.dateTimePickerPocetak.Size = new System.Drawing.Size(200, 20);
            this.dateTimePickerPocetak.TabIndex = 297;
            // 
            // Kreiraj
            // 
            this.Kreiraj.Location = new System.Drawing.Point(764, 43);
            this.Kreiraj.Name = "Kreiraj";
            this.Kreiraj.Size = new System.Drawing.Size(75, 23);
            this.Kreiraj.TabIndex = 299;
            this.Kreiraj.Text = "Kreiraj";
            this.Kreiraj.UseVisualStyleBackColor = true;
            this.Kreiraj.Click += new System.EventHandler(this.Kreiraj_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxB5);
            this.groupBox1.Controls.Add(this.labelDatumIVreme);
            this.groupBox1.Controls.Add(this.labelDomacin);
            this.groupBox1.Controls.Add(this.labelGost);
            this.groupBox1.Controls.Add(this.labelRezultat);
            this.groupBox1.Controls.Add(this.date1);
            this.groupBox1.Controls.Add(this.comboBoxA1);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.comboBoxB1);
            this.groupBox1.Controls.Add(this.comboBox12);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.comboBox11);
            this.groupBox1.Controls.Add(this.textBoxB1);
            this.groupBox1.Controls.Add(this.comboBox10);
            this.groupBox1.Controls.Add(this.textBoxA1);
            this.groupBox1.Controls.Add(this.comboBox9);
            this.groupBox1.Controls.Add(this.date2);
            this.groupBox1.Controls.Add(this.comboBox8);
            this.groupBox1.Controls.Add(this.comboBoxA2);
            this.groupBox1.Controls.Add(this.comboBox7);
            this.groupBox1.Controls.Add(this.comboBoxB2);
            this.groupBox1.Controls.Add(this.comboBox6);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.comboBox5);
            this.groupBox1.Controls.Add(this.textBoxB2);
            this.groupBox1.Controls.Add(this.comboBox4);
            this.groupBox1.Controls.Add(this.textBoxA2);
            this.groupBox1.Controls.Add(this.comboBox3);
            this.groupBox1.Controls.Add(this.date3);
            this.groupBox1.Controls.Add(this.comboBox2);
            this.groupBox1.Controls.Add(this.comboBoxA3);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.comboBoxB3);
            this.groupBox1.Controls.Add(this.button12);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.textBoxB3);
            this.groupBox1.Controls.Add(this.button11);
            this.groupBox1.Controls.Add(this.textBoxA3);
            this.groupBox1.Controls.Add(this.button9);
            this.groupBox1.Controls.Add(this.date4);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.comboBoxA4);
            this.groupBox1.Controls.Add(this.button6);
            this.groupBox1.Controls.Add(this.comboBoxB4);
            this.groupBox1.Controls.Add(this.button7);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button5);
            this.groupBox1.Controls.Add(this.textBoxB4);
            this.groupBox1.Controls.Add(this.button4);
            this.groupBox1.Controls.Add(this.textBoxA4);
            this.groupBox1.Controls.Add(this.button2);
            this.groupBox1.Controls.Add(this.date5);
            this.groupBox1.Controls.Add(this.button3);
            this.groupBox1.Controls.Add(this.comboBoxA5);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.textBoxA12);
            this.groupBox1.Controls.Add(this.textBoxB5);
            this.groupBox1.Controls.Add(this.textBoxB12);
            this.groupBox1.Controls.Add(this.textBoxA5);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.date6);
            this.groupBox1.Controls.Add(this.comboBoxB12);
            this.groupBox1.Controls.Add(this.comboBoxA6);
            this.groupBox1.Controls.Add(this.comboBoxA12);
            this.groupBox1.Controls.Add(this.comboBoxB6);
            this.groupBox1.Controls.Add(this.date12);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.textBoxA11);
            this.groupBox1.Controls.Add(this.textBoxB6);
            this.groupBox1.Controls.Add(this.textBoxB11);
            this.groupBox1.Controls.Add(this.textBoxA6);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.date7);
            this.groupBox1.Controls.Add(this.comboBoxB11);
            this.groupBox1.Controls.Add(this.comboBoxA7);
            this.groupBox1.Controls.Add(this.comboBoxA11);
            this.groupBox1.Controls.Add(this.comboBoxB7);
            this.groupBox1.Controls.Add(this.date11);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.textBoxA10);
            this.groupBox1.Controls.Add(this.textBoxB7);
            this.groupBox1.Controls.Add(this.textBoxB10);
            this.groupBox1.Controls.Add(this.textBoxA7);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.date8);
            this.groupBox1.Controls.Add(this.comboBoxB10);
            this.groupBox1.Controls.Add(this.comboBoxA8);
            this.groupBox1.Controls.Add(this.comboBoxA10);
            this.groupBox1.Controls.Add(this.comboBoxB8);
            this.groupBox1.Controls.Add(this.date10);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.textBoxA9);
            this.groupBox1.Controls.Add(this.textBoxB8);
            this.groupBox1.Controls.Add(this.textBoxB9);
            this.groupBox1.Controls.Add(this.textBoxA8);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.date9);
            this.groupBox1.Controls.Add(this.comboBoxB9);
            this.groupBox1.Controls.Add(this.comboBoxA9);
            this.groupBox1.Location = new System.Drawing.Point(46, 108);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(853, 423);
            this.groupBox1.TabIndex = 300;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Utakmice";
            // 
            // KoloForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 543);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.Kreiraj);
            this.Controls.Add(this.dateTimePickerKraj);
            this.Controls.Add(this.dateTimePickerPocetak);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxRedniBrojKola);
            this.Controls.Add(this.labelNazivLige);
            this.Controls.Add(this.labelBrojKola);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "KoloForm";
            this.Text = "KoloForm";
            this.Load += new System.EventHandler(this.KoloForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxA12;
        private System.Windows.Forms.TextBox textBoxB12;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBoxB12;
        private System.Windows.Forms.ComboBox comboBoxA12;
        private System.Windows.Forms.DateTimePicker date12;
        private System.Windows.Forms.TextBox textBoxA11;
        private System.Windows.Forms.TextBox textBoxB11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox comboBoxB11;
        private System.Windows.Forms.ComboBox comboBoxA11;
        private System.Windows.Forms.DateTimePicker date11;
        private System.Windows.Forms.TextBox textBoxA10;
        private System.Windows.Forms.TextBox textBoxB10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox comboBoxB10;
        private System.Windows.Forms.ComboBox comboBoxA10;
        private System.Windows.Forms.DateTimePicker date10;
        private System.Windows.Forms.TextBox textBoxA9;
        private System.Windows.Forms.TextBox textBoxB9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox comboBoxB9;
        private System.Windows.Forms.ComboBox comboBoxA9;
        private System.Windows.Forms.DateTimePicker date9;
        private System.Windows.Forms.TextBox textBoxA8;
        private System.Windows.Forms.TextBox textBoxB8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox comboBoxB8;
        private System.Windows.Forms.ComboBox comboBoxA8;
        private System.Windows.Forms.DateTimePicker date8;
        private System.Windows.Forms.TextBox textBoxA7;
        private System.Windows.Forms.TextBox textBoxB7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBoxB7;
        private System.Windows.Forms.ComboBox comboBoxA7;
        private System.Windows.Forms.DateTimePicker date7;
        private System.Windows.Forms.TextBox textBoxA6;
        private System.Windows.Forms.TextBox textBoxB6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBoxB6;
        private System.Windows.Forms.ComboBox comboBoxA6;
        private System.Windows.Forms.DateTimePicker date6;
        private System.Windows.Forms.TextBox textBoxA5;
        private System.Windows.Forms.TextBox textBoxB5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBoxB5;
        private System.Windows.Forms.ComboBox comboBoxA5;
        private System.Windows.Forms.DateTimePicker date5;
        private System.Windows.Forms.TextBox textBoxA4;
        private System.Windows.Forms.TextBox textBoxB4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxB4;
        private System.Windows.Forms.ComboBox comboBoxA4;
        private System.Windows.Forms.DateTimePicker date4;
        private System.Windows.Forms.TextBox textBoxA3;
        private System.Windows.Forms.TextBox textBoxB3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBoxB3;
        private System.Windows.Forms.ComboBox comboBoxA3;
        private System.Windows.Forms.DateTimePicker date3;
        private System.Windows.Forms.TextBox textBoxA2;
        private System.Windows.Forms.TextBox textBoxB2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBoxB2;
        private System.Windows.Forms.ComboBox comboBoxA2;
        private System.Windows.Forms.DateTimePicker date2;
        private System.Windows.Forms.TextBox textBoxA1;
        private System.Windows.Forms.TextBox textBoxB1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBoxB1;
        private System.Windows.Forms.ComboBox comboBoxA1;
        private System.Windows.Forms.DateTimePicker date1;
        private System.Windows.Forms.Label labelRezultat;
        private System.Windows.Forms.Label labelGost;
        private System.Windows.Forms.Label labelDomacin;
        private System.Windows.Forms.Label labelDatumIVreme;
        private System.Windows.Forms.Label labelNazivLige;
        private System.Windows.Forms.Label labelBrojKola;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxRedniBrojKola;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DateTimePicker dateTimePickerKraj;
        private System.Windows.Forms.DateTimePicker dateTimePickerPocetak;
        private System.Windows.Forms.Button Kreiraj;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}