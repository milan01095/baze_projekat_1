﻿using Neo4jClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Olimpiada.Models;
using Neo4jClient.Cypher;

namespace Olimpiada
{
    public partial class SportForm : Form
    {
        GraphClient client;
        List<Sport> sports;
        List<Disciplina> discipline;
        int selectIndex = -1;
        public SportForm()
        {
            InitializeComponent();
        }

        private void SportForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            var query = new Neo4jClient.Cypher.CypherQuery(" match(n:Sport) return n",
                                                         new Dictionary<string, object>(), CypherResultMode.Set);

            var query1 = new Neo4jClient.Cypher.CypherQuery(" match(n:Disciplina) return n",
                                                        new Dictionary<string, object>(), CypherResultMode.Set);

            sports = ((IRawGraphClient)client).ExecuteGetCypherResults<Sport>(query).ToList();
            discipline = ((IRawGraphClient)client).ExecuteGetCypherResults<Disciplina>(query1).ToList();

            foreach (Sport sport in sports)
            {
                comboBoxOdaberiteSport.Items.Add(sport.Naziv);
                comboBoxSportVeza.Items.Add(sport.Naziv);
            }

            foreach (Disciplina dis in discipline)
                comboBoxDisciplinaVeza.Items.Add(dis.Naziv + " " + dis.Pol);
        }

        private void buttonKreirajSport_Click(object sender, EventArgs e)
        {
            if(textBoxNazivSporta.Text != "" && textBoxSportskaAsocijacija.Text != "")
            {
                Sport sport = Sport.Create(client, textBoxNazivSporta.Text, textBoxSportskaAsocijacija.Text);
                 comboBoxOdaberiteSport.Items.Add(sport.Naziv);
                 comboBoxSportVeza.Items.Add(sport.Naziv);
                sports.Add(sport);
            }
        }

        private void buttonIzmeniSport_Click(object sender, EventArgs e)
        {
            if (textBoxNazivSporta.Text != "" && textBoxSportskaAsocijacija.Text != "")
            {
                Sport sp = sports[selectIndex].Modify(client, textBoxNazivSporta.Text, textBoxSportskaAsocijacija.Text);
                sports[selectIndex] = sp;
            }

        }
        private void comboBoxOdaberiteSport_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBoxNazivSporta.Text = sports[comboBoxOdaberiteSport.SelectedIndex].Naziv;
            textBoxSportskaAsocijacija.Text = sports[comboBoxOdaberiteSport.SelectedIndex].Sportska_asociacija;         
            selectIndex = comboBoxOdaberiteSport.SelectedIndex;
            Ucitaj();
        }

        private void Ucitaj()
        {
            comboBoxDisciplinaVeza.Items.Clear();
            comboBoxSportVeza.Items.Clear();
            comboBoxOdaberiteSport.Items.Clear();
           

            foreach (Sport sport in sports)
            {
                comboBoxOdaberiteSport.Items.Add(sport.Naziv);
                comboBoxSportVeza.Items.Add(sport.Naziv);
            }

            foreach (Disciplina dis in discipline)
                comboBoxDisciplinaVeza.Items.Add(dis.Naziv + " " + dis.Pol);
        }

        private void buttonObrisiSport_Click(object sender, EventArgs e)
        {
            if(selectIndex != -1)
            {
                Sport sp = sports[selectIndex];
                sports.RemoveAt(selectIndex);
                sp.Delete(client);
                Ucitaj();
            }
        }

        private void buttonDodajVezu_Click(object sender, EventArgs e)
        {
            if(comboBoxDisciplinaVeza.SelectedIndex != -1 && comboBoxSportVeza.SelectedIndex != -1)
            {
                Sport.CreateSportDisciplina(client, sports[comboBoxSportVeza.SelectedIndex], discipline[comboBoxDisciplinaVeza.SelectedIndex]);
            }
        }

        private void buttonIzbrisiVezu_Click(object sender, EventArgs e)
        {
            if (comboBoxDisciplinaVeza.SelectedIndex != -1 && comboBoxSportVeza.SelectedIndex != -1)
            {
                Sport.DeletSportDisciplina(client, sports[comboBoxSportVeza.SelectedIndex], discipline[comboBoxDisciplinaVeza.SelectedIndex]);
            }
        }
    }
}
