﻿using Neo4jClient;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class TakmicenjeEkipnoForm : Form
    {
        Disciplina selektovanaDiscilina = null;
        Takmicenje selektovanoTakmicenje = null;
        GraphClient client;
        public TakmicenjeEkipnoForm(Disciplina disciplina, Takmicenje takmicenje)
        {
            InitializeComponent();
            selektovanaDiscilina = disciplina;
            selektovanoTakmicenje = takmicenje;
        }

        private void TakmicenjeEkipnoForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            if (selektovanoTakmicenje != null)
            {
                textBoxNaziv.Text = selektovanoTakmicenje.Naziv;
                textBoxZemlja.Text = selektovanoTakmicenje.Zemlja;
                textBoxDomaciKup.Text = selektovanoTakmicenje.Domaci_kup;            
                textBoxSponzor.Text = selektovanoTakmicenje.Sponzor;               
            }
            else
            {
                dateTimePickerDatumRodjenja.Value = DateTime.Now;
                buttonIzmeni.Enabled = false;
            }
        }

        private void buttonKreiraj_Click(object sender, EventArgs e)
        {
            if(textBoxDomaciKup.Text != "" && textBoxZemlja.Text != "" && textBoxNaziv.Text != "" && textBoxSponzor.Text != "")
            {              
              selektovanoTakmicenje =   Takmicenje.Create(client, textBoxNaziv.Text, textBoxZemlja.Text, dateTimePickerDatumRodjenja.Value.ToString("dd.MM.yyyy"), textBoxDomaciKup.Text, textBoxSponzor.Text);
                Disciplina.CreateDisciplinaTakmicenje(client, selektovanaDiscilina, selektovanoTakmicenje);
                buttonIzmeni.Enabled = true;
            }
        }

        private void buttonIzmeni_Click(object sender, EventArgs e)
        {
            if (textBoxDomaciKup.Text != "" && textBoxZemlja.Text != "" && textBoxNaziv.Text != "" && textBoxSponzor.Text != "")
            {
                selektovanoTakmicenje.Modify(client, textBoxNaziv.Text, textBoxZemlja.Text, dateTimePickerDatumRodjenja.Value.ToString("dd.MM.yyyy"), textBoxDomaciKup.Text, textBoxSponzor.Text);               
            }
        }
    }
}
