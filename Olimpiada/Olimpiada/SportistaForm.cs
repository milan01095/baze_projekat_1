﻿using Neo4jClient;
using Neo4jClient.Cypher;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class SportistaForm : Form
    {
        private Sportista sportista;
        private Takmicenje selektovanoTakmicenje;
        private Klub klub;
        private Disciplina disciplina;
        private GraphClient client;
        private List<Takmicenje> takmicenja;

        public SportistaForm(Sportista a, Klub b, Disciplina d)
        {
            InitializeComponent();
            sportista = a;
            klub = b;
            disciplina = d;
        }



        public void popuniKontrole()
        {
            textBoxIme.Text = sportista.Ime;
            textBoxPrezime.Text = sportista.Prezime;
            textBoxDrzava.Text = sportista.Drzava;
            textBoxTelefon.Text = sportista.Broj_tel;
            textBoxVisina.Text = sportista.Visina;
            textBoxTezina.Text = sportista.Tezina;
            numericUpDownBrojNaDresu.Value = sportista.Broj_dresa;

            dateTimePickerDatumRodjenja.Value = DateTime.ParseExact(sportista.Datum_rodjenja, "dd.MM.yyyy", CultureInfo.InvariantCulture);              
        }
        public void obrisiKontrole()
        {
            textBoxIme.Text = String.Empty;
            textBoxPrezime.Text = String.Empty;
            textBoxDrzava.Text = String.Empty;
            textBoxTelefon.Text = String.Empty;
            textBoxVisina.Text = String.Empty;
            textBoxTezina.Text = String.Empty;
            numericUpDownBrojNaDresu.Value = 0;
            dateTimePickerDatumRodjenja.Value = DateTime.Now;

        }

        private void SportistaForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }

            if (sportista != null)
                popuniKontrole();

            comboBoxTakmicenja.Text = "--Izaberi takmicenje--";

            if (disciplina.Tip == "Pojedinacna")
            {
                ucitajTakmicenjaPojedinacna();

            }
            else
                comboBoxTakmicenja.Enabled = false;
            
        }

        private void radioButtonKreirajSportistu_CheckedChanged(object sender, EventArgs e)
        {
            obrisiKontrole();
        }

        private void buttonIzmeni_Click(object sender, EventArgs e)
        {
            DateTime d = dateTimePickerDatumRodjenja.Value;
            d.ToString("dd.MM.yyyy");


            var query = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:Sportista) and n.ID = " + "'" + sportista.ID + "'" +
                                                            " set n.Ime = " + "'" + textBoxIme.Text + "'" +
                                                            ", n.Prezime= " + "'" + textBoxPrezime.Text + "'" +
                                                             ", n.Datum_rodjenja= " + "'" + d.ToString("dd.MM.yyyy") + "'" +
                                                              ", n.Broj_dresa= " + "'" + numericUpDownBrojNaDresu.Value + "'" +
                                                               ", n.Broj_tel= " + "'" + textBoxTelefon.Text + "'" +
                                                               ", n.Drzava= " + "'" + textBoxDrzava.Text + "'" +
                                                               ", n.Visina= " + "'" + textBoxVisina.Text + "'" +
                                                               ", n.Tezina= " + "'" + textBoxTezina.Text + "'" +

                                                            " return n",
                                                new Dictionary<string, object>(), CypherResultMode.Set);

            //start n=node(*) where (n:Sportista)  and n.ID ='4'  set n.IME = 'Stefana'   return n
            //start n=node(*) where (n:Sportista)  and n.ID ='4'  set n.IME = 'Stefana', n.Prezime='Nik'   return n

            List<Sportista> s = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).ToList();
            MessageBox.Show("Uspesno ste izmenili sportistu.", "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void buttonKreiraj_Click(object sender, EventArgs e)
        {
            Sportista s = dodajSportistu();
            int maxId = getMaxId();

            try
            {
                // int mId = Int32.Parse(maxId);
                s.ID = (++maxId);
            }
            catch (Exception exception)
            {
                s.ID = 0;
            }


            Dictionary<string, object> queryDict = new Dictionary<string, object>();
            queryDict.Add("Ime", s.Ime);
            queryDict.Add("Prezime", s.Prezime);
            queryDict.Add("Datum_rodjenja", s.Datum_rodjenja);
            queryDict.Add("Broj_dresa", s.Broj_dresa);
            queryDict.Add("Drzava", s.Drzava);
            queryDict.Add("Broj_tel", s.Broj_tel);
            queryDict.Add("Visina", s.Visina);
            queryDict.Add("Tezina", s.Tezina);

            var query = new Neo4jClient.Cypher.CypherQuery("CREATE (n:Sportista {ID:'" + s.ID + "', Ime:'" + s.Ime
                                                            + "', Prezime:'" + s.Prezime + "', Datum_rodjenja:'" + s.Datum_rodjenja
                                                            + "', Broj_dresa:'" + s.Broj_dresa
                                                            + "', Drzava:'" + s.Drzava
                                                            + "', Broj_tel:'" + s.Broj_tel
                                                            + "', Visina:'" + s.Visina
                                                            + "', Tezina:'" + s.Tezina
                                                            + "'}) return n",
                                                            queryDict, CypherResultMode.Set);

            List<Sportista> sp = ((IRawGraphClient)client).ExecuteGetCypherResults<Sportista>(query).ToList();


            if(disciplina.Tip=="Ekipna")
            {
                var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sportista),(b:Klub)"
                                                + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + klub.ID + "'"
                                                + " CREATE (a)-[r:UCESTVUJE]->(b)"
                                                + " return a,r,b",
                                              new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query2);
                MessageBox.Show("Uspesno ste dodali sportistu za klub " + klub.Naziv, "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                var query2 = new Neo4jClient.Cypher.CypherQuery("MATCH(a:Sportista),(b:Takmicenje)"
                                                + " WHERE a.ID=" + "'" + s.ID + "'" + " AND b.ID=" + "'" + selektovanoTakmicenje.ID + "'"
                                                + " CREATE (a)-[r:TAKMICI]->(b)"
                                                + " return a,r,b",
                                              new Dictionary<string, object>(), CypherResultMode.Projection);
                ((IRawGraphClient)client).ExecuteCypher(query2);
                MessageBox.Show("Uspesno ste dodali sportistu na takmicenju " + selektovanoTakmicenje.Naziv, "Obavestenje", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            this.Close();
           
        }

        private int getMaxId()
        {
            

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Sportista) RETURN n.ID",
                                                            new Dictionary<string, object>(), CypherResultMode.Set);

            List<String> maxId = ((IRawGraphClient)client).ExecuteGetCypherResults<String>(query).ToList();

            

            List<int> id = maxId.Select(s => int.Parse(s)).ToList();
            int max = id.Max();
            
            return max;
        }

        private Sportista dodajSportistu()
        {
            Sportista a = new Sportista();

            

            DateTime d = dateTimePickerDatumRodjenja.Value;
        
            a.Ime = textBoxIme.Text;
            a.Prezime = textBoxPrezime.Text;
            a.Datum_rodjenja = d.ToString("dd.MM.yyyy");
            a.Broj_dresa = (int)numericUpDownBrojNaDresu.Value;
            a.Broj_tel = textBoxTelefon.Text;
            a.Drzava = textBoxDrzava.Text;
            a.Tezina = textBoxTezina.Text;
            a.Visina = textBoxVisina.Text;

            return a;
        }
            

        public void ucitajTakmicenjaPojedinacna()
        {


            takmicenja = disciplina.GetTakmicenja();
            if (comboBoxTakmicenja.Items.Count > 0)
                comboBoxTakmicenja.Items.Clear();
            foreach (Takmicenje tak in takmicenja)
            {
                comboBoxTakmicenja.Items.Add(tak);
                comboBoxTakmicenja.DisplayMember = "Naziv";
            }
            

        }

        private void comboBoxTakmicenja_SelectedIndexChanged(object sender, EventArgs e)
        {
            selektovanoTakmicenje = (Takmicenje)comboBoxTakmicenja.SelectedItem;

        }
    }
}
