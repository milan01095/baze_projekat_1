﻿namespace Olimpiada
{
    partial class AdminForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxKlub = new System.Windows.Forms.GroupBox();
            this.buttonObrisiKlub = new System.Windows.Forms.Button();
            this.buttonKreirajKlub = new System.Windows.Forms.Button();
            this.comboBoxKlub = new System.Windows.Forms.ComboBox();
            this.labelOdaberiteKlub = new System.Windows.Forms.Label();
            this.groupBoxSportista = new System.Windows.Forms.GroupBox();
            this.comboBoxOdaberiteSportistu = new System.Windows.Forms.ComboBox();
            this.labelOdaberiteSportistu = new System.Windows.Forms.Label();
            this.buttonObrisiSportistu = new System.Windows.Forms.Button();
            this.buttonKreirajSportistu = new System.Windows.Forms.Button();
            this.groupBoxDisciplina = new System.Windows.Forms.GroupBox();
            this.buttonObrisiDisciplinu = new System.Windows.Forms.Button();
            this.buttonKreirajDisciplinu = new System.Windows.Forms.Button();
            this.labelOdaberiteDisciplinu = new System.Windows.Forms.Label();
            this.comboBoxOdaberiteDisciplinu = new System.Windows.Forms.ComboBox();
            this.groupBoxSportovi = new System.Windows.Forms.GroupBox();
            this.buttonObrisiSport = new System.Windows.Forms.Button();
            this.buttonKreirajSport = new System.Windows.Forms.Button();
            this.comboBoxOdaberiteSport = new System.Windows.Forms.ComboBox();
            this.labelOdaberiSport = new System.Windows.Forms.Label();
            this.groupBoxAdministratorskePrivilegije = new System.Windows.Forms.GroupBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBoxPassword = new System.Windows.Forms.TextBox();
            this.textBoxUsername = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonObrisiNalog = new System.Windows.Forms.Button();
            this.buttonDodajPrivilegije = new System.Windows.Forms.Button();
            this.listBoxAdministratorskiNalozi = new System.Windows.Forms.ListBox();
            this.labelAdministratorskiNalozi = new System.Windows.Forms.Label();
            this.groupBoxSudija = new System.Windows.Forms.GroupBox();
            this.buttonKreirajSudiju = new System.Windows.Forms.Button();
            this.buttonObrisiSudiju = new System.Windows.Forms.Button();
            this.comboBoxOdaberiteSudiju = new System.Windows.Forms.ComboBox();
            this.labelOdaberiteSudiju = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.buttonObrisiTakmicenje = new System.Windows.Forms.Button();
            this.buttonKreirajIzmeniTakmicenje = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBoxOdaberiTakmicenje = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.buttonKreirajIzmeniStadion = new System.Windows.Forms.Button();
            this.buttonObrisiStadion = new System.Windows.Forms.Button();
            this.comboBoxOdaberiteStadion = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBoxTabela = new System.Windows.Forms.GroupBox();
            this.buttonObrisiTabelu = new System.Windows.Forms.Button();
            this.buttonKreirajIzmeniTabelu = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBoxKola = new System.Windows.Forms.GroupBox();
            this.buttonObrisiKolo = new System.Windows.Forms.Button();
            this.buttonKreirajIzmeniKolo = new System.Windows.Forms.Button();
            this.comboBoxKola = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBoxUtakmica = new System.Windows.Forms.GroupBox();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxKlub.SuspendLayout();
            this.groupBoxSportista.SuspendLayout();
            this.groupBoxDisciplina.SuspendLayout();
            this.groupBoxSportovi.SuspendLayout();
            this.groupBoxAdministratorskePrivilegije.SuspendLayout();
            this.groupBoxSudija.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBoxTabela.SuspendLayout();
            this.groupBoxKola.SuspendLayout();
            this.groupBoxUtakmica.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxKlub
            // 
            this.groupBoxKlub.Controls.Add(this.buttonObrisiKlub);
            this.groupBoxKlub.Controls.Add(this.buttonKreirajKlub);
            this.groupBoxKlub.Controls.Add(this.comboBoxKlub);
            this.groupBoxKlub.Controls.Add(this.labelOdaberiteKlub);
            this.groupBoxKlub.Location = new System.Drawing.Point(401, 12);
            this.groupBoxKlub.Name = "groupBoxKlub";
            this.groupBoxKlub.Size = new System.Drawing.Size(325, 104);
            this.groupBoxKlub.TabIndex = 15;
            this.groupBoxKlub.TabStop = false;
            this.groupBoxKlub.Text = "Klub";
            // 
            // buttonObrisiKlub
            // 
            this.buttonObrisiKlub.Location = new System.Drawing.Point(241, 19);
            this.buttonObrisiKlub.Name = "buttonObrisiKlub";
            this.buttonObrisiKlub.Size = new System.Drawing.Size(75, 23);
            this.buttonObrisiKlub.TabIndex = 4;
            this.buttonObrisiKlub.Text = "Obrisi klub";
            this.buttonObrisiKlub.UseVisualStyleBackColor = true;
            this.buttonObrisiKlub.Click += new System.EventHandler(this.buttonObrisiKlub_Click);
            // 
            // buttonKreirajKlub
            // 
            this.buttonKreirajKlub.Location = new System.Drawing.Point(100, 69);
            this.buttonKreirajKlub.Name = "buttonKreirajKlub";
            this.buttonKreirajKlub.Size = new System.Drawing.Size(122, 23);
            this.buttonKreirajKlub.TabIndex = 2;
            this.buttonKreirajKlub.Text = "Kreiraj/Izmeni klub";
            this.buttonKreirajKlub.UseVisualStyleBackColor = true;
            this.buttonKreirajKlub.Click += new System.EventHandler(this.buttonKreirajKlub_Click);
            // 
            // comboBoxKlub
            // 
            this.comboBoxKlub.FormattingEnabled = true;
            this.comboBoxKlub.Location = new System.Drawing.Point(101, 19);
            this.comboBoxKlub.Name = "comboBoxKlub";
            this.comboBoxKlub.Size = new System.Drawing.Size(121, 21);
            this.comboBoxKlub.TabIndex = 1;
            this.comboBoxKlub.SelectedIndexChanged += new System.EventHandler(this.comboBoxKlub_SelectedIndexChanged);
            // 
            // labelOdaberiteKlub
            // 
            this.labelOdaberiteKlub.AutoSize = true;
            this.labelOdaberiteKlub.Location = new System.Drawing.Point(6, 22);
            this.labelOdaberiteKlub.Name = "labelOdaberiteKlub";
            this.labelOdaberiteKlub.Size = new System.Drawing.Size(76, 13);
            this.labelOdaberiteKlub.TabIndex = 0;
            this.labelOdaberiteKlub.Text = "Odaberite klub";
            // 
            // groupBoxSportista
            // 
            this.groupBoxSportista.Controls.Add(this.comboBoxOdaberiteSportistu);
            this.groupBoxSportista.Controls.Add(this.labelOdaberiteSportistu);
            this.groupBoxSportista.Controls.Add(this.buttonObrisiSportistu);
            this.groupBoxSportista.Controls.Add(this.buttonKreirajSportistu);
            this.groupBoxSportista.Location = new System.Drawing.Point(732, 20);
            this.groupBoxSportista.Name = "groupBoxSportista";
            this.groupBoxSportista.Size = new System.Drawing.Size(359, 119);
            this.groupBoxSportista.TabIndex = 14;
            this.groupBoxSportista.TabStop = false;
            this.groupBoxSportista.Text = "Sportista";
            // 
            // comboBoxOdaberiteSportistu
            // 
            this.comboBoxOdaberiteSportistu.FormattingEnabled = true;
            this.comboBoxOdaberiteSportistu.Location = new System.Drawing.Point(117, 16);
            this.comboBoxOdaberiteSportistu.Name = "comboBoxOdaberiteSportistu";
            this.comboBoxOdaberiteSportistu.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiteSportistu.TabIndex = 17;
            this.comboBoxOdaberiteSportistu.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteSportistu_SelectedIndexChanged);
            // 
            // labelOdaberiteSportistu
            // 
            this.labelOdaberiteSportistu.AutoSize = true;
            this.labelOdaberiteSportistu.Location = new System.Drawing.Point(6, 19);
            this.labelOdaberiteSportistu.Name = "labelOdaberiteSportistu";
            this.labelOdaberiteSportistu.Size = new System.Drawing.Size(95, 13);
            this.labelOdaberiteSportistu.TabIndex = 16;
            this.labelOdaberiteSportistu.Text = "Odaberite sportistu";
            // 
            // buttonObrisiSportistu
            // 
            this.buttonObrisiSportistu.Location = new System.Drawing.Point(256, 19);
            this.buttonObrisiSportistu.Name = "buttonObrisiSportistu";
            this.buttonObrisiSportistu.Size = new System.Drawing.Size(94, 23);
            this.buttonObrisiSportistu.TabIndex = 15;
            this.buttonObrisiSportistu.Text = "Obrisi sportistu";
            this.buttonObrisiSportistu.UseVisualStyleBackColor = true;
            this.buttonObrisiSportistu.Click += new System.EventHandler(this.buttonObrisiSportistu_Click);
            // 
            // buttonKreirajSportistu
            // 
            this.buttonKreirajSportistu.Location = new System.Drawing.Point(104, 61);
            this.buttonKreirajSportistu.Name = "buttonKreirajSportistu";
            this.buttonKreirajSportistu.Size = new System.Drawing.Size(106, 45);
            this.buttonKreirajSportistu.TabIndex = 13;
            this.buttonKreirajSportistu.Text = "Kreiraj/Izmeni sportistu";
            this.buttonKreirajSportistu.UseVisualStyleBackColor = true;
            this.buttonKreirajSportistu.Click += new System.EventHandler(this.buttonKreirajSportistu_Click);
            // 
            // groupBoxDisciplina
            // 
            this.groupBoxDisciplina.Controls.Add(this.buttonObrisiDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.buttonKreirajDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.labelOdaberiteDisciplinu);
            this.groupBoxDisciplina.Controls.Add(this.comboBoxOdaberiteDisciplinu);
            this.groupBoxDisciplina.Location = new System.Drawing.Point(2, 373);
            this.groupBoxDisciplina.Name = "groupBoxDisciplina";
            this.groupBoxDisciplina.Size = new System.Drawing.Size(351, 110);
            this.groupBoxDisciplina.TabIndex = 13;
            this.groupBoxDisciplina.TabStop = false;
            this.groupBoxDisciplina.Text = "Disciplina";
            // 
            // buttonObrisiDisciplinu
            // 
            this.buttonObrisiDisciplinu.Location = new System.Drawing.Point(255, 18);
            this.buttonObrisiDisciplinu.Name = "buttonObrisiDisciplinu";
            this.buttonObrisiDisciplinu.Size = new System.Drawing.Size(75, 44);
            this.buttonObrisiDisciplinu.TabIndex = 10;
            this.buttonObrisiDisciplinu.Text = "Obrisi disciplinu";
            this.buttonObrisiDisciplinu.UseVisualStyleBackColor = true;
            this.buttonObrisiDisciplinu.Click += new System.EventHandler(this.buttonObrisiDisciplinu_Click);
            // 
            // buttonKreirajDisciplinu
            // 
            this.buttonKreirajDisciplinu.Location = new System.Drawing.Point(104, 61);
            this.buttonKreirajDisciplinu.Name = "buttonKreirajDisciplinu";
            this.buttonKreirajDisciplinu.Size = new System.Drawing.Size(122, 39);
            this.buttonKreirajDisciplinu.TabIndex = 9;
            this.buttonKreirajDisciplinu.Text = "Kreiraj/Izmeni disciplinu";
            this.buttonKreirajDisciplinu.UseVisualStyleBackColor = true;
            this.buttonKreirajDisciplinu.Click += new System.EventHandler(this.buttonKreirajDisciplinu_Click);
            // 
            // labelOdaberiteDisciplinu
            // 
            this.labelOdaberiteDisciplinu.AutoSize = true;
            this.labelOdaberiteDisciplinu.Location = new System.Drawing.Point(6, 21);
            this.labelOdaberiteDisciplinu.Name = "labelOdaberiteDisciplinu";
            this.labelOdaberiteDisciplinu.Size = new System.Drawing.Size(99, 13);
            this.labelOdaberiteDisciplinu.TabIndex = 4;
            this.labelOdaberiteDisciplinu.Text = "Odaberite disciplinu";
            // 
            // comboBoxOdaberiteDisciplinu
            // 
            this.comboBoxOdaberiteDisciplinu.FormattingEnabled = true;
            this.comboBoxOdaberiteDisciplinu.Location = new System.Drawing.Point(118, 18);
            this.comboBoxOdaberiteDisciplinu.Name = "comboBoxOdaberiteDisciplinu";
            this.comboBoxOdaberiteDisciplinu.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiteDisciplinu.TabIndex = 0;
            this.comboBoxOdaberiteDisciplinu.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteDisciplinu_SelectedIndexChanged);
            // 
            // groupBoxSportovi
            // 
            this.groupBoxSportovi.Controls.Add(this.buttonObrisiSport);
            this.groupBoxSportovi.Controls.Add(this.buttonKreirajSport);
            this.groupBoxSportovi.Controls.Add(this.comboBoxOdaberiteSport);
            this.groupBoxSportovi.Controls.Add(this.labelOdaberiSport);
            this.groupBoxSportovi.Location = new System.Drawing.Point(0, 258);
            this.groupBoxSportovi.Name = "groupBoxSportovi";
            this.groupBoxSportovi.Size = new System.Drawing.Size(353, 111);
            this.groupBoxSportovi.TabIndex = 12;
            this.groupBoxSportovi.TabStop = false;
            this.groupBoxSportovi.Text = "Sportovi";
            // 
            // buttonObrisiSport
            // 
            this.buttonObrisiSport.Location = new System.Drawing.Point(257, 17);
            this.buttonObrisiSport.Name = "buttonObrisiSport";
            this.buttonObrisiSport.Size = new System.Drawing.Size(75, 44);
            this.buttonObrisiSport.TabIndex = 9;
            this.buttonObrisiSport.Text = "Obrisi sport";
            this.buttonObrisiSport.UseVisualStyleBackColor = true;
            this.buttonObrisiSport.Click += new System.EventHandler(this.buttonObrisiSport_Click);
            // 
            // buttonKreirajSport
            // 
            this.buttonKreirajSport.Location = new System.Drawing.Point(104, 67);
            this.buttonKreirajSport.Name = "buttonKreirajSport";
            this.buttonKreirajSport.Size = new System.Drawing.Size(106, 23);
            this.buttonKreirajSport.TabIndex = 1;
            this.buttonKreirajSport.Text = "Kreiraj/Izmeni sport";
            this.buttonKreirajSport.UseVisualStyleBackColor = true;
            this.buttonKreirajSport.Click += new System.EventHandler(this.buttonKreirajSport_Click);
            // 
            // comboBoxOdaberiteSport
            // 
            this.comboBoxOdaberiteSport.FormattingEnabled = true;
            this.comboBoxOdaberiteSport.Location = new System.Drawing.Point(98, 18);
            this.comboBoxOdaberiteSport.Name = "comboBoxOdaberiteSport";
            this.comboBoxOdaberiteSport.Size = new System.Drawing.Size(140, 21);
            this.comboBoxOdaberiteSport.TabIndex = 8;
            this.comboBoxOdaberiteSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteSport_SelectedIndexChanged);
            // 
            // labelOdaberiSport
            // 
            this.labelOdaberiSport.AutoSize = true;
            this.labelOdaberiSport.Location = new System.Drawing.Point(6, 22);
            this.labelOdaberiSport.Name = "labelOdaberiSport";
            this.labelOdaberiSport.Size = new System.Drawing.Size(79, 13);
            this.labelOdaberiSport.TabIndex = 7;
            this.labelOdaberiSport.Text = "Odaberite sport";
            // 
            // groupBoxAdministratorskePrivilegije
            // 
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.label2);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.textBoxPassword);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.textBoxUsername);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.label1);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.buttonObrisiNalog);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.buttonDodajPrivilegije);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.listBoxAdministratorskiNalozi);
            this.groupBoxAdministratorskePrivilegije.Controls.Add(this.labelAdministratorskiNalozi);
            this.groupBoxAdministratorskePrivilegije.Location = new System.Drawing.Point(0, 1);
            this.groupBoxAdministratorskePrivilegije.Name = "groupBoxAdministratorskePrivilegije";
            this.groupBoxAdministratorskePrivilegije.Size = new System.Drawing.Size(395, 251);
            this.groupBoxAdministratorskePrivilegije.TabIndex = 16;
            this.groupBoxAdministratorskePrivilegije.TabStop = false;
            this.groupBoxAdministratorskePrivilegije.Text = "Administratorske privilegije";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(230, 125);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 11;
            this.label2.Text = "Password";
            // 
            // textBoxPassword
            // 
            this.textBoxPassword.Location = new System.Drawing.Point(233, 141);
            this.textBoxPassword.Name = "textBoxPassword";
            this.textBoxPassword.PasswordChar = '*';
            this.textBoxPassword.Size = new System.Drawing.Size(145, 20);
            this.textBoxPassword.TabIndex = 10;
            // 
            // textBoxUsername
            // 
            this.textBoxUsername.Location = new System.Drawing.Point(233, 86);
            this.textBoxUsername.Name = "textBoxUsername";
            this.textBoxUsername.Size = new System.Drawing.Size(145, 20);
            this.textBoxUsername.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(230, 69);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(55, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Username";
            // 
            // buttonObrisiNalog
            // 
            this.buttonObrisiNalog.Location = new System.Drawing.Point(38, 191);
            this.buttonObrisiNalog.Name = "buttonObrisiNalog";
            this.buttonObrisiNalog.Size = new System.Drawing.Size(101, 39);
            this.buttonObrisiNalog.TabIndex = 7;
            this.buttonObrisiNalog.Text = "Obrisi admina";
            this.buttonObrisiNalog.UseVisualStyleBackColor = true;
            this.buttonObrisiNalog.Click += new System.EventHandler(this.buttonObrisiNalog_Click);
            // 
            // buttonDodajPrivilegije
            // 
            this.buttonDodajPrivilegije.Location = new System.Drawing.Point(247, 190);
            this.buttonDodajPrivilegije.Name = "buttonDodajPrivilegije";
            this.buttonDodajPrivilegije.Size = new System.Drawing.Size(106, 40);
            this.buttonDodajPrivilegije.TabIndex = 5;
            this.buttonDodajPrivilegije.Text = "Dodaj admina";
            this.buttonDodajPrivilegije.UseVisualStyleBackColor = true;
            this.buttonDodajPrivilegije.Click += new System.EventHandler(this.buttonDodajPrivilegije_Click);
            // 
            // listBoxAdministratorskiNalozi
            // 
            this.listBoxAdministratorskiNalozi.FormattingEnabled = true;
            this.listBoxAdministratorskiNalozi.Location = new System.Drawing.Point(38, 51);
            this.listBoxAdministratorskiNalozi.Name = "listBoxAdministratorskiNalozi";
            this.listBoxAdministratorskiNalozi.Size = new System.Drawing.Size(169, 134);
            this.listBoxAdministratorskiNalozi.TabIndex = 2;
            // 
            // labelAdministratorskiNalozi
            // 
            this.labelAdministratorskiNalozi.AutoSize = true;
            this.labelAdministratorskiNalozi.Location = new System.Drawing.Point(35, 35);
            this.labelAdministratorskiNalozi.Name = "labelAdministratorskiNalozi";
            this.labelAdministratorskiNalozi.Size = new System.Drawing.Size(57, 13);
            this.labelAdministratorskiNalozi.TabIndex = 4;
            this.labelAdministratorskiNalozi.Text = "Admin lista";
            // 
            // groupBoxSudija
            // 
            this.groupBoxSudija.Controls.Add(this.buttonKreirajSudiju);
            this.groupBoxSudija.Controls.Add(this.buttonObrisiSudiju);
            this.groupBoxSudija.Controls.Add(this.comboBoxOdaberiteSudiju);
            this.groupBoxSudija.Controls.Add(this.labelOdaberiteSudiju);
            this.groupBoxSudija.Location = new System.Drawing.Point(728, 145);
            this.groupBoxSudija.Name = "groupBoxSudija";
            this.groupBoxSudija.Size = new System.Drawing.Size(363, 96);
            this.groupBoxSudija.TabIndex = 18;
            this.groupBoxSudija.TabStop = false;
            this.groupBoxSudija.Text = "Sudija";
            // 
            // buttonKreirajSudiju
            // 
            this.buttonKreirajSudiju.Location = new System.Drawing.Point(115, 55);
            this.buttonKreirajSudiju.Name = "buttonKreirajSudiju";
            this.buttonKreirajSudiju.Size = new System.Drawing.Size(113, 23);
            this.buttonKreirajSudiju.TabIndex = 3;
            this.buttonKreirajSudiju.Text = "Kreiraj/Izmeni sudiju";
            this.buttonKreirajSudiju.UseVisualStyleBackColor = true;
            this.buttonKreirajSudiju.Click += new System.EventHandler(this.buttonKreirajSudiju_Click);
            // 
            // buttonObrisiSudiju
            // 
            this.buttonObrisiSudiju.Location = new System.Drawing.Point(254, 20);
            this.buttonObrisiSudiju.Name = "buttonObrisiSudiju";
            this.buttonObrisiSudiju.Size = new System.Drawing.Size(94, 23);
            this.buttonObrisiSudiju.TabIndex = 2;
            this.buttonObrisiSudiju.Text = "Obrisi sudiju";
            this.buttonObrisiSudiju.UseVisualStyleBackColor = true;
            this.buttonObrisiSudiju.Click += new System.EventHandler(this.buttonObrisiSudiju_Click);
            // 
            // comboBoxOdaberiteSudiju
            // 
            this.comboBoxOdaberiteSudiju.FormattingEnabled = true;
            this.comboBoxOdaberiteSudiju.Location = new System.Drawing.Point(115, 22);
            this.comboBoxOdaberiteSudiju.Name = "comboBoxOdaberiteSudiju";
            this.comboBoxOdaberiteSudiju.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiteSudiju.TabIndex = 1;
            this.comboBoxOdaberiteSudiju.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteSudiju_SelectedIndexChanged);
            // 
            // labelOdaberiteSudiju
            // 
            this.labelOdaberiteSudiju.AutoSize = true;
            this.labelOdaberiteSudiju.Location = new System.Drawing.Point(6, 25);
            this.labelOdaberiteSudiju.Name = "labelOdaberiteSudiju";
            this.labelOdaberiteSudiju.Size = new System.Drawing.Size(86, 13);
            this.labelOdaberiteSudiju.TabIndex = 0;
            this.labelOdaberiteSudiju.Text = "Odaberite sudiju:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.buttonObrisiTakmicenje);
            this.groupBox1.Controls.Add(this.buttonKreirajIzmeniTakmicenje);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.comboBoxOdaberiTakmicenje);
            this.groupBox1.Location = new System.Drawing.Point(2, 489);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(351, 111);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Takmicenja";
            // 
            // buttonObrisiTakmicenje
            // 
            this.buttonObrisiTakmicenje.Location = new System.Drawing.Point(255, 18);
            this.buttonObrisiTakmicenje.Name = "buttonObrisiTakmicenje";
            this.buttonObrisiTakmicenje.Size = new System.Drawing.Size(75, 44);
            this.buttonObrisiTakmicenje.TabIndex = 11;
            this.buttonObrisiTakmicenje.Text = "Obrisi takmicenje";
            this.buttonObrisiTakmicenje.UseVisualStyleBackColor = true;
            this.buttonObrisiTakmicenje.Click += new System.EventHandler(this.buttonObrisiTakmicenje_Click);
            // 
            // buttonKreirajIzmeniTakmicenje
            // 
            this.buttonKreirajIzmeniTakmicenje.Location = new System.Drawing.Point(104, 61);
            this.buttonKreirajIzmeniTakmicenje.Name = "buttonKreirajIzmeniTakmicenje";
            this.buttonKreirajIzmeniTakmicenje.Size = new System.Drawing.Size(122, 39);
            this.buttonKreirajIzmeniTakmicenje.TabIndex = 9;
            this.buttonKreirajIzmeniTakmicenje.Text = "Kreiraj/Izmeni takmicenje";
            this.buttonKreirajIzmeniTakmicenje.UseVisualStyleBackColor = true;
            this.buttonKreirajIzmeniTakmicenje.Click += new System.EventHandler(this.buttonKreirajIzmeniTakmicenje_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(107, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Odaberite takmicenje";
            // 
            // comboBoxOdaberiTakmicenje
            // 
            this.comboBoxOdaberiTakmicenje.FormattingEnabled = true;
            this.comboBoxOdaberiTakmicenje.Location = new System.Drawing.Point(118, 18);
            this.comboBoxOdaberiTakmicenje.Name = "comboBoxOdaberiTakmicenje";
            this.comboBoxOdaberiTakmicenje.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiTakmicenje.TabIndex = 0;
            this.comboBoxOdaberiTakmicenje.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiTakmicenje_SelectedIndexChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.buttonKreirajIzmeniStadion);
            this.groupBox2.Controls.Add(this.buttonObrisiStadion);
            this.groupBox2.Controls.Add(this.comboBoxOdaberiteStadion);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(732, 247);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(363, 96);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Stadion";
            // 
            // buttonKreirajIzmeniStadion
            // 
            this.buttonKreirajIzmeniStadion.Location = new System.Drawing.Point(102, 58);
            this.buttonKreirajIzmeniStadion.Name = "buttonKreirajIzmeniStadion";
            this.buttonKreirajIzmeniStadion.Size = new System.Drawing.Size(180, 23);
            this.buttonKreirajIzmeniStadion.TabIndex = 3;
            this.buttonKreirajIzmeniStadion.Text = "Kreiraj/Izmeni stadion";
            this.buttonKreirajIzmeniStadion.UseVisualStyleBackColor = true;
            this.buttonKreirajIzmeniStadion.Click += new System.EventHandler(this.buttonKreirajIzmeniStadion_Click);
            // 
            // buttonObrisiStadion
            // 
            this.buttonObrisiStadion.Location = new System.Drawing.Point(254, 20);
            this.buttonObrisiStadion.Name = "buttonObrisiStadion";
            this.buttonObrisiStadion.Size = new System.Drawing.Size(94, 23);
            this.buttonObrisiStadion.TabIndex = 2;
            this.buttonObrisiStadion.Text = "Obrisi stadion";
            this.buttonObrisiStadion.UseVisualStyleBackColor = true;
            this.buttonObrisiStadion.Click += new System.EventHandler(this.buttonObrisiStadion_Click);
            // 
            // comboBoxOdaberiteStadion
            // 
            this.comboBoxOdaberiteStadion.FormattingEnabled = true;
            this.comboBoxOdaberiteStadion.Location = new System.Drawing.Point(115, 22);
            this.comboBoxOdaberiteStadion.Name = "comboBoxOdaberiteStadion";
            this.comboBoxOdaberiteStadion.Size = new System.Drawing.Size(121, 21);
            this.comboBoxOdaberiteStadion.TabIndex = 1;
            this.comboBoxOdaberiteStadion.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteStadion_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "Odaberite stadion:";
            // 
            // groupBoxTabela
            // 
            this.groupBoxTabela.Controls.Add(this.buttonObrisiTabelu);
            this.groupBoxTabela.Controls.Add(this.buttonKreirajIzmeniTabelu);
            this.groupBoxTabela.Controls.Add(this.label5);
            this.groupBoxTabela.Location = new System.Drawing.Point(401, 122);
            this.groupBoxTabela.Name = "groupBoxTabela";
            this.groupBoxTabela.Size = new System.Drawing.Size(325, 99);
            this.groupBoxTabela.TabIndex = 21;
            this.groupBoxTabela.TabStop = false;
            this.groupBoxTabela.Text = "Tabela";
            // 
            // buttonObrisiTabelu
            // 
            this.buttonObrisiTabelu.Location = new System.Drawing.Point(241, 19);
            this.buttonObrisiTabelu.Name = "buttonObrisiTabelu";
            this.buttonObrisiTabelu.Size = new System.Drawing.Size(75, 23);
            this.buttonObrisiTabelu.TabIndex = 4;
            this.buttonObrisiTabelu.Text = "Obrisi tabelu";
            this.buttonObrisiTabelu.UseVisualStyleBackColor = true;
            this.buttonObrisiTabelu.Click += new System.EventHandler(this.buttonObrisiTabelu_Click);
            // 
            // buttonKreirajIzmeniTabelu
            // 
            this.buttonKreirajIzmeniTabelu.Location = new System.Drawing.Point(101, 20);
            this.buttonKreirajIzmeniTabelu.Name = "buttonKreirajIzmeniTabelu";
            this.buttonKreirajIzmeniTabelu.Size = new System.Drawing.Size(132, 23);
            this.buttonKreirajIzmeniTabelu.TabIndex = 2;
            this.buttonKreirajIzmeniTabelu.Text = "Kreiraj/Izmeni tabelu";
            this.buttonKreirajIzmeniTabelu.UseVisualStyleBackColor = true;
            this.buttonKreirajIzmeniTabelu.Click += new System.EventHandler(this.buttonKreirajIzmeniTabelu_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 22);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(85, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Odaberite tabelu";
            // 
            // groupBoxKola
            // 
            this.groupBoxKola.Controls.Add(this.buttonObrisiKolo);
            this.groupBoxKola.Controls.Add(this.buttonKreirajIzmeniKolo);
            this.groupBoxKola.Controls.Add(this.comboBoxKola);
            this.groupBoxKola.Controls.Add(this.label6);
            this.groupBoxKola.Location = new System.Drawing.Point(401, 227);
            this.groupBoxKola.Name = "groupBoxKola";
            this.groupBoxKola.Size = new System.Drawing.Size(325, 116);
            this.groupBoxKola.TabIndex = 22;
            this.groupBoxKola.TabStop = false;
            this.groupBoxKola.Text = "Kola";
            // 
            // buttonObrisiKolo
            // 
            this.buttonObrisiKolo.Location = new System.Drawing.Point(241, 19);
            this.buttonObrisiKolo.Name = "buttonObrisiKolo";
            this.buttonObrisiKolo.Size = new System.Drawing.Size(75, 23);
            this.buttonObrisiKolo.TabIndex = 4;
            this.buttonObrisiKolo.Text = "Obrisi kolo";
            this.buttonObrisiKolo.UseVisualStyleBackColor = true;
            this.buttonObrisiKolo.Click += new System.EventHandler(this.buttonObrisiKolo_Click);
            // 
            // buttonKreirajIzmeniKolo
            // 
            this.buttonKreirajIzmeniKolo.Location = new System.Drawing.Point(100, 69);
            this.buttonKreirajIzmeniKolo.Name = "buttonKreirajIzmeniKolo";
            this.buttonKreirajIzmeniKolo.Size = new System.Drawing.Size(148, 23);
            this.buttonKreirajIzmeniKolo.TabIndex = 2;
            this.buttonKreirajIzmeniKolo.Text = "Kreiraj/Izmeni kolo";
            this.buttonKreirajIzmeniKolo.UseVisualStyleBackColor = true;
            this.buttonKreirajIzmeniKolo.Click += new System.EventHandler(this.buttonKreirajIzmeniKolo_Click);
            // 
            // comboBoxKola
            // 
            this.comboBoxKola.FormattingEnabled = true;
            this.comboBoxKola.Location = new System.Drawing.Point(101, 19);
            this.comboBoxKola.Name = "comboBoxKola";
            this.comboBoxKola.Size = new System.Drawing.Size(121, 21);
            this.comboBoxKola.TabIndex = 1;
            this.comboBoxKola.SelectedIndexChanged += new System.EventHandler(this.comboBoxKola_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Odaberite kolo";
            // 
            // groupBoxUtakmica
            // 
            this.groupBoxUtakmica.Controls.Add(this.button3);
            this.groupBoxUtakmica.Controls.Add(this.button4);
            this.groupBoxUtakmica.Controls.Add(this.comboBox2);
            this.groupBoxUtakmica.Controls.Add(this.label7);
            this.groupBoxUtakmica.Location = new System.Drawing.Point(401, 373);
            this.groupBoxUtakmica.Name = "groupBoxUtakmica";
            this.groupBoxUtakmica.Size = new System.Drawing.Size(430, 93);
            this.groupBoxUtakmica.TabIndex = 23;
            this.groupBoxUtakmica.TabStop = false;
            this.groupBoxUtakmica.Text = "Utakmice";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(327, 11);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(75, 34);
            this.button3.TabIndex = 4;
            this.button3.Text = "Obrisi utakmicu";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(111, 64);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(148, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Kreiraj/Izmeni utakmicu";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(111, 19);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(194, 21);
            this.comboBox2.TabIndex = 1;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(99, 13);
            this.label7.TabIndex = 0;
            this.label7.Text = "Odaberite utakmicu";
            // 
            // AdminForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1103, 597);
            this.Controls.Add(this.groupBoxUtakmica);
            this.Controls.Add(this.groupBoxKola);
            this.Controls.Add(this.groupBoxTabela);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxSudija);
            this.Controls.Add(this.groupBoxAdministratorskePrivilegije);
            this.Controls.Add(this.groupBoxSportista);
            this.Controls.Add(this.groupBoxKlub);
            this.Controls.Add(this.groupBoxDisciplina);
            this.Controls.Add(this.groupBoxSportovi);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "AdminForm";
            this.Text = "AdminForm";
            this.Load += new System.EventHandler(this.AdminForm_Load);
            this.groupBoxKlub.ResumeLayout(false);
            this.groupBoxKlub.PerformLayout();
            this.groupBoxSportista.ResumeLayout(false);
            this.groupBoxSportista.PerformLayout();
            this.groupBoxDisciplina.ResumeLayout(false);
            this.groupBoxDisciplina.PerformLayout();
            this.groupBoxSportovi.ResumeLayout(false);
            this.groupBoxSportovi.PerformLayout();
            this.groupBoxAdministratorskePrivilegije.ResumeLayout(false);
            this.groupBoxAdministratorskePrivilegije.PerformLayout();
            this.groupBoxSudija.ResumeLayout(false);
            this.groupBoxSudija.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBoxTabela.ResumeLayout(false);
            this.groupBoxTabela.PerformLayout();
            this.groupBoxKola.ResumeLayout(false);
            this.groupBoxKola.PerformLayout();
            this.groupBoxUtakmica.ResumeLayout(false);
            this.groupBoxUtakmica.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxKlub;
        private System.Windows.Forms.Button buttonObrisiKlub;
        private System.Windows.Forms.Button buttonKreirajKlub;
        private System.Windows.Forms.ComboBox comboBoxKlub;
        private System.Windows.Forms.Label labelOdaberiteKlub;
        private System.Windows.Forms.GroupBox groupBoxSportista;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteSportistu;
        private System.Windows.Forms.Label labelOdaberiteSportistu;
        private System.Windows.Forms.Button buttonObrisiSportistu;
        private System.Windows.Forms.Button buttonKreirajSportistu;
        private System.Windows.Forms.GroupBox groupBoxDisciplina;
        private System.Windows.Forms.Button buttonKreirajDisciplinu;
        private System.Windows.Forms.Label labelOdaberiteDisciplinu;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteDisciplinu;
        private System.Windows.Forms.GroupBox groupBoxSportovi;
        private System.Windows.Forms.Button buttonKreirajSport;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteSport;
        private System.Windows.Forms.Label labelOdaberiSport;
        private System.Windows.Forms.GroupBox groupBoxAdministratorskePrivilegije;
        private System.Windows.Forms.Button buttonObrisiNalog;
        private System.Windows.Forms.Button buttonDodajPrivilegije;
        private System.Windows.Forms.ListBox listBoxAdministratorskiNalozi;
        private System.Windows.Forms.Label labelAdministratorskiNalozi;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBoxPassword;
        private System.Windows.Forms.TextBox textBoxUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxSudija;
        private System.Windows.Forms.Button buttonKreirajSudiju;
        private System.Windows.Forms.Button buttonObrisiSudiju;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteSudiju;
        private System.Windows.Forms.Label labelOdaberiteSudiju;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button buttonKreirajIzmeniTakmicenje;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBoxOdaberiTakmicenje;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button buttonKreirajIzmeniStadion;
        private System.Windows.Forms.Button buttonObrisiStadion;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteStadion;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBoxTabela;
        private System.Windows.Forms.Button buttonObrisiTabelu;
        private System.Windows.Forms.Button buttonKreirajIzmeniTabelu;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBoxKola;
        private System.Windows.Forms.Button buttonObrisiKolo;
        private System.Windows.Forms.Button buttonKreirajIzmeniKolo;
        private System.Windows.Forms.ComboBox comboBoxKola;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBoxUtakmica;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button buttonObrisiDisciplinu;
        private System.Windows.Forms.Button buttonObrisiSport;
        private System.Windows.Forms.Button buttonObrisiTakmicenje;
    }
}