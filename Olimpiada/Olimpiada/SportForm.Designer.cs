﻿namespace Olimpiada
{
    partial class SportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBoxSportovi = new System.Windows.Forms.GroupBox();
            this.buttonObrisiSport = new System.Windows.Forms.Button();
            this.comboBoxOdaberiteSport = new System.Windows.Forms.ComboBox();
            this.buttonIzmeniSport = new System.Windows.Forms.Button();
            this.buttonKreirajSport = new System.Windows.Forms.Button();
            this.labelOdaberiSport = new System.Windows.Forms.Label();
            this.textBoxSportskaAsocijacija = new System.Windows.Forms.TextBox();
            this.textBoxNazivSporta = new System.Windows.Forms.TextBox();
            this.labelSportskaAsocijacija = new System.Windows.Forms.Label();
            this.labelNazivSporta = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.comboBoxDisciplinaVeza = new System.Windows.Forms.ComboBox();
            this.comboBoxSportVeza = new System.Windows.Forms.ComboBox();
            this.buttonIzbrisiVezu = new System.Windows.Forms.Button();
            this.buttonDodajVezu = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBoxSportovi.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxSportovi
            // 
            this.groupBoxSportovi.Controls.Add(this.buttonObrisiSport);
            this.groupBoxSportovi.Controls.Add(this.comboBoxOdaberiteSport);
            this.groupBoxSportovi.Controls.Add(this.buttonIzmeniSport);
            this.groupBoxSportovi.Controls.Add(this.buttonKreirajSport);
            this.groupBoxSportovi.Controls.Add(this.labelOdaberiSport);
            this.groupBoxSportovi.Controls.Add(this.textBoxSportskaAsocijacija);
            this.groupBoxSportovi.Controls.Add(this.textBoxNazivSporta);
            this.groupBoxSportovi.Controls.Add(this.labelSportskaAsocijacija);
            this.groupBoxSportovi.Controls.Add(this.labelNazivSporta);
            this.groupBoxSportovi.Location = new System.Drawing.Point(10, 11);
            this.groupBoxSportovi.Name = "groupBoxSportovi";
            this.groupBoxSportovi.Size = new System.Drawing.Size(285, 235);
            this.groupBoxSportovi.TabIndex = 5;
            this.groupBoxSportovi.TabStop = false;
            this.groupBoxSportovi.Text = "Sportovi";
            // 
            // buttonObrisiSport
            // 
            this.buttonObrisiSport.Location = new System.Drawing.Point(172, 164);
            this.buttonObrisiSport.Name = "buttonObrisiSport";
            this.buttonObrisiSport.Size = new System.Drawing.Size(75, 23);
            this.buttonObrisiSport.TabIndex = 11;
            this.buttonObrisiSport.Text = "Obrisi sport";
            this.buttonObrisiSport.UseVisualStyleBackColor = true;
            this.buttonObrisiSport.Click += new System.EventHandler(this.buttonObrisiSport_Click);
            // 
            // comboBoxOdaberiteSport
            // 
            this.comboBoxOdaberiteSport.FormattingEnabled = true;
            this.comboBoxOdaberiteSport.Location = new System.Drawing.Point(125, 26);
            this.comboBoxOdaberiteSport.Name = "comboBoxOdaberiteSport";
            this.comboBoxOdaberiteSport.Size = new System.Drawing.Size(140, 21);
            this.comboBoxOdaberiteSport.TabIndex = 10;
            this.comboBoxOdaberiteSport.SelectedIndexChanged += new System.EventHandler(this.comboBoxOdaberiteSport_SelectedIndexChanged);
            // 
            // buttonIzmeniSport
            // 
            this.buttonIzmeniSport.Location = new System.Drawing.Point(91, 164);
            this.buttonIzmeniSport.Name = "buttonIzmeniSport";
            this.buttonIzmeniSport.Size = new System.Drawing.Size(75, 23);
            this.buttonIzmeniSport.TabIndex = 6;
            this.buttonIzmeniSport.Text = "Izmeni sport";
            this.buttonIzmeniSport.UseVisualStyleBackColor = true;
            this.buttonIzmeniSport.Click += new System.EventHandler(this.buttonIzmeniSport_Click);
            // 
            // buttonKreirajSport
            // 
            this.buttonKreirajSport.Location = new System.Drawing.Point(10, 164);
            this.buttonKreirajSport.Name = "buttonKreirajSport";
            this.buttonKreirajSport.Size = new System.Drawing.Size(75, 23);
            this.buttonKreirajSport.TabIndex = 1;
            this.buttonKreirajSport.Text = "Kreiraj sport";
            this.buttonKreirajSport.UseVisualStyleBackColor = true;
            this.buttonKreirajSport.Click += new System.EventHandler(this.buttonKreirajSport_Click);
            // 
            // labelOdaberiSport
            // 
            this.labelOdaberiSport.AutoSize = true;
            this.labelOdaberiSport.Location = new System.Drawing.Point(6, 30);
            this.labelOdaberiSport.Name = "labelOdaberiSport";
            this.labelOdaberiSport.Size = new System.Drawing.Size(79, 13);
            this.labelOdaberiSport.TabIndex = 9;
            this.labelOdaberiSport.Text = "Odaberite sport";
            // 
            // textBoxSportskaAsocijacija
            // 
            this.textBoxSportskaAsocijacija.Location = new System.Drawing.Point(125, 105);
            this.textBoxSportskaAsocijacija.Name = "textBoxSportskaAsocijacija";
            this.textBoxSportskaAsocijacija.Size = new System.Drawing.Size(140, 20);
            this.textBoxSportskaAsocijacija.TabIndex = 3;
            // 
            // textBoxNazivSporta
            // 
            this.textBoxNazivSporta.Location = new System.Drawing.Point(125, 67);
            this.textBoxNazivSporta.Name = "textBoxNazivSporta";
            this.textBoxNazivSporta.Size = new System.Drawing.Size(140, 20);
            this.textBoxNazivSporta.TabIndex = 2;
            // 
            // labelSportskaAsocijacija
            // 
            this.labelSportskaAsocijacija.AutoSize = true;
            this.labelSportskaAsocijacija.Location = new System.Drawing.Point(7, 105);
            this.labelSportskaAsocijacija.Name = "labelSportskaAsocijacija";
            this.labelSportskaAsocijacija.Size = new System.Drawing.Size(101, 13);
            this.labelSportskaAsocijacija.TabIndex = 1;
            this.labelSportskaAsocijacija.Text = "Sportska asocijacija";
            // 
            // labelNazivSporta
            // 
            this.labelNazivSporta.AutoSize = true;
            this.labelNazivSporta.Location = new System.Drawing.Point(7, 67);
            this.labelNazivSporta.Name = "labelNazivSporta";
            this.labelNazivSporta.Size = new System.Drawing.Size(34, 13);
            this.labelNazivSporta.TabIndex = 0;
            this.labelNazivSporta.Text = "Naziv";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.comboBoxDisciplinaVeza);
            this.groupBox1.Controls.Add(this.comboBoxSportVeza);
            this.groupBox1.Controls.Add(this.buttonIzbrisiVezu);
            this.groupBox1.Controls.Add(this.buttonDodajVezu);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Location = new System.Drawing.Point(301, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(285, 235);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Povezivanje sporta i discipline";
            // 
            // comboBoxDisciplinaVeza
            // 
            this.comboBoxDisciplinaVeza.FormattingEnabled = true;
            this.comboBoxDisciplinaVeza.Location = new System.Drawing.Point(125, 67);
            this.comboBoxDisciplinaVeza.Name = "comboBoxDisciplinaVeza";
            this.comboBoxDisciplinaVeza.Size = new System.Drawing.Size(140, 21);
            this.comboBoxDisciplinaVeza.TabIndex = 11;
            // 
            // comboBoxSportVeza
            // 
            this.comboBoxSportVeza.FormattingEnabled = true;
            this.comboBoxSportVeza.Location = new System.Drawing.Point(125, 26);
            this.comboBoxSportVeza.Name = "comboBoxSportVeza";
            this.comboBoxSportVeza.Size = new System.Drawing.Size(140, 21);
            this.comboBoxSportVeza.TabIndex = 10;
            // 
            // buttonIzbrisiVezu
            // 
            this.buttonIzbrisiVezu.Location = new System.Drawing.Point(158, 142);
            this.buttonIzbrisiVezu.Name = "buttonIzbrisiVezu";
            this.buttonIzbrisiVezu.Size = new System.Drawing.Size(75, 23);
            this.buttonIzbrisiVezu.TabIndex = 6;
            this.buttonIzbrisiVezu.Text = "Izbrisi vezu";
            this.buttonIzbrisiVezu.UseVisualStyleBackColor = true;
            this.buttonIzbrisiVezu.Click += new System.EventHandler(this.buttonIzbrisiVezu_Click);
            // 
            // buttonDodajVezu
            // 
            this.buttonDodajVezu.Location = new System.Drawing.Point(45, 142);
            this.buttonDodajVezu.Name = "buttonDodajVezu";
            this.buttonDodajVezu.Size = new System.Drawing.Size(75, 23);
            this.buttonDodajVezu.TabIndex = 1;
            this.buttonDodajVezu.Text = "Dodaj vezu";
            this.buttonDodajVezu.UseVisualStyleBackColor = true;
            this.buttonDodajVezu.Click += new System.EventHandler(this.buttonDodajVezu_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Odaberite sport";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Discipline";
            // 
            // SportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(609, 294);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBoxSportovi);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SportForm";
            this.Text = "SportForm";
            this.Load += new System.EventHandler(this.SportForm_Load);
            this.groupBoxSportovi.ResumeLayout(false);
            this.groupBoxSportovi.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxSportovi;
        private System.Windows.Forms.ComboBox comboBoxOdaberiteSport;
        private System.Windows.Forms.Button buttonIzmeniSport;
        private System.Windows.Forms.Button buttonKreirajSport;
        private System.Windows.Forms.Label labelOdaberiSport;
        private System.Windows.Forms.TextBox textBoxSportskaAsocijacija;
        private System.Windows.Forms.TextBox textBoxNazivSporta;
        private System.Windows.Forms.Label labelSportskaAsocijacija;
        private System.Windows.Forms.Label labelNazivSporta;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox comboBoxDisciplinaVeza;
        private System.Windows.Forms.ComboBox comboBoxSportVeza;
        private System.Windows.Forms.Button buttonIzbrisiVezu;
        private System.Windows.Forms.Button buttonDodajVezu;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button buttonObrisiSport;
    }
}