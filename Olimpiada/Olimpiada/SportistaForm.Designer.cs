﻿namespace Olimpiada
{
    partial class SportistaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButtonKreirajSportistu = new System.Windows.Forms.RadioButton();
            this.labelCm = new System.Windows.Forms.Label();
            this.labelKg = new System.Windows.Forms.Label();
            this.textBoxTezina = new System.Windows.Forms.TextBox();
            this.textBoxVisina = new System.Windows.Forms.TextBox();
            this.textBoxDrzava = new System.Windows.Forms.TextBox();
            this.textBoxTelefon = new System.Windows.Forms.TextBox();
            this.numericUpDownBrojNaDresu = new System.Windows.Forms.NumericUpDown();
            this.labelTelefon = new System.Windows.Forms.Label();
            this.labelDres = new System.Windows.Forms.Label();
            this.labelTezina = new System.Windows.Forms.Label();
            this.labelVisina = new System.Windows.Forms.Label();
            this.labelDrzava = new System.Windows.Forms.Label();
            this.comboBoxTakmicenja = new System.Windows.Forms.ComboBox();
            this.buttonIzmeni = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonKreiraj = new System.Windows.Forms.Button();
            this.labelIme = new System.Windows.Forms.Label();
            this.dateTimePickerDatumRodjenja = new System.Windows.Forms.DateTimePicker();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.labelDatumRodjenja = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.textBoxPrezime = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojNaDresu)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonKreirajSportistu);
            this.groupBox1.Controls.Add(this.labelCm);
            this.groupBox1.Controls.Add(this.labelKg);
            this.groupBox1.Controls.Add(this.textBoxTezina);
            this.groupBox1.Controls.Add(this.textBoxVisina);
            this.groupBox1.Controls.Add(this.textBoxDrzava);
            this.groupBox1.Controls.Add(this.textBoxTelefon);
            this.groupBox1.Controls.Add(this.numericUpDownBrojNaDresu);
            this.groupBox1.Controls.Add(this.labelTelefon);
            this.groupBox1.Controls.Add(this.labelDres);
            this.groupBox1.Controls.Add(this.labelTezina);
            this.groupBox1.Controls.Add(this.labelVisina);
            this.groupBox1.Controls.Add(this.labelDrzava);
            this.groupBox1.Controls.Add(this.comboBoxTakmicenja);
            this.groupBox1.Controls.Add(this.buttonIzmeni);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.buttonKreiraj);
            this.groupBox1.Controls.Add(this.labelIme);
            this.groupBox1.Controls.Add(this.dateTimePickerDatumRodjenja);
            this.groupBox1.Controls.Add(this.textBoxIme);
            this.groupBox1.Controls.Add(this.labelDatumRodjenja);
            this.groupBox1.Controls.Add(this.labelPrezime);
            this.groupBox1.Controls.Add(this.textBoxPrezime);
            this.groupBox1.Location = new System.Drawing.Point(10, 11);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(315, 474);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sportista";
            // 
            // radioButtonKreirajSportistu
            // 
            this.radioButtonKreirajSportistu.AutoSize = true;
            this.radioButtonKreirajSportistu.Location = new System.Drawing.Point(10, 32);
            this.radioButtonKreirajSportistu.Name = "radioButtonKreirajSportistu";
            this.radioButtonKreirajSportistu.Size = new System.Drawing.Size(96, 17);
            this.radioButtonKreirajSportistu.TabIndex = 0;
            this.radioButtonKreirajSportistu.Text = "Kreiraj sportistu";
            this.radioButtonKreirajSportistu.UseVisualStyleBackColor = true;
            this.radioButtonKreirajSportistu.CheckedChanged += new System.EventHandler(this.radioButtonKreirajSportistu_CheckedChanged);
            // 
            // labelCm
            // 
            this.labelCm.AutoSize = true;
            this.labelCm.Location = new System.Drawing.Point(160, 317);
            this.labelCm.Name = "labelCm";
            this.labelCm.Size = new System.Drawing.Size(21, 13);
            this.labelCm.TabIndex = 17;
            this.labelCm.Text = "cm";
            // 
            // labelKg
            // 
            this.labelKg.AutoSize = true;
            this.labelKg.Location = new System.Drawing.Point(160, 352);
            this.labelKg.Name = "labelKg";
            this.labelKg.Size = new System.Drawing.Size(19, 13);
            this.labelKg.TabIndex = 20;
            this.labelKg.Text = "kg";
            // 
            // textBoxTezina
            // 
            this.textBoxTezina.Location = new System.Drawing.Point(124, 345);
            this.textBoxTezina.Name = "textBoxTezina";
            this.textBoxTezina.Size = new System.Drawing.Size(30, 20);
            this.textBoxTezina.TabIndex = 19;
            // 
            // textBoxVisina
            // 
            this.textBoxVisina.Location = new System.Drawing.Point(124, 310);
            this.textBoxVisina.Name = "textBoxVisina";
            this.textBoxVisina.Size = new System.Drawing.Size(30, 20);
            this.textBoxVisina.TabIndex = 16;
            // 
            // textBoxDrzava
            // 
            this.textBoxDrzava.Location = new System.Drawing.Point(124, 276);
            this.textBoxDrzava.Name = "textBoxDrzava";
            this.textBoxDrzava.Size = new System.Drawing.Size(123, 20);
            this.textBoxDrzava.TabIndex = 14;
            // 
            // textBoxTelefon
            // 
            this.textBoxTelefon.Location = new System.Drawing.Point(124, 239);
            this.textBoxTelefon.Name = "textBoxTelefon";
            this.textBoxTelefon.Size = new System.Drawing.Size(123, 20);
            this.textBoxTelefon.TabIndex = 12;
            // 
            // numericUpDownBrojNaDresu
            // 
            this.numericUpDownBrojNaDresu.Location = new System.Drawing.Point(124, 203);
            this.numericUpDownBrojNaDresu.Name = "numericUpDownBrojNaDresu";
            this.numericUpDownBrojNaDresu.Size = new System.Drawing.Size(39, 20);
            this.numericUpDownBrojNaDresu.TabIndex = 10;
            // 
            // labelTelefon
            // 
            this.labelTelefon.AutoSize = true;
            this.labelTelefon.Location = new System.Drawing.Point(7, 246);
            this.labelTelefon.Name = "labelTelefon";
            this.labelTelefon.Size = new System.Drawing.Size(43, 13);
            this.labelTelefon.TabIndex = 11;
            this.labelTelefon.Text = "Telefon";
            // 
            // labelDres
            // 
            this.labelDres.AutoSize = true;
            this.labelDres.Location = new System.Drawing.Point(6, 210);
            this.labelDres.Name = "labelDres";
            this.labelDres.Size = new System.Drawing.Size(69, 13);
            this.labelDres.TabIndex = 9;
            this.labelDres.Text = "Broj na dresu";
            // 
            // labelTezina
            // 
            this.labelTezina.AutoSize = true;
            this.labelTezina.Location = new System.Drawing.Point(7, 352);
            this.labelTezina.Name = "labelTezina";
            this.labelTezina.Size = new System.Drawing.Size(39, 13);
            this.labelTezina.TabIndex = 18;
            this.labelTezina.Text = "Tezina";
            // 
            // labelVisina
            // 
            this.labelVisina.AutoSize = true;
            this.labelVisina.Location = new System.Drawing.Point(6, 317);
            this.labelVisina.Name = "labelVisina";
            this.labelVisina.Size = new System.Drawing.Size(35, 13);
            this.labelVisina.TabIndex = 15;
            this.labelVisina.Text = "Visina";
            // 
            // labelDrzava
            // 
            this.labelDrzava.AutoSize = true;
            this.labelDrzava.Location = new System.Drawing.Point(6, 283);
            this.labelDrzava.Name = "labelDrzava";
            this.labelDrzava.Size = new System.Drawing.Size(41, 13);
            this.labelDrzava.TabIndex = 13;
            this.labelDrzava.Text = "Drzava";
            // 
            // comboBoxTakmicenja
            // 
            this.comboBoxTakmicenja.FormattingEnabled = true;
            this.comboBoxTakmicenja.Location = new System.Drawing.Point(124, 85);
            this.comboBoxTakmicenja.Name = "comboBoxTakmicenja";
            this.comboBoxTakmicenja.Size = new System.Drawing.Size(123, 21);
            this.comboBoxTakmicenja.TabIndex = 2;
            this.comboBoxTakmicenja.SelectedIndexChanged += new System.EventHandler(this.comboBoxTakmicenja_SelectedIndexChanged);
            // 
            // buttonIzmeni
            // 
            this.buttonIzmeni.Location = new System.Drawing.Point(156, 403);
            this.buttonIzmeni.Name = "buttonIzmeni";
            this.buttonIzmeni.Size = new System.Drawing.Size(91, 23);
            this.buttonIzmeni.TabIndex = 22;
            this.buttonIzmeni.Text = "Izmeni sportistu";
            this.buttonIzmeni.UseVisualStyleBackColor = true;
            this.buttonIzmeni.Click += new System.EventHandler(this.buttonIzmeni_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(230, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Odaberite takmicenje za pojedinacnog sportistu";
            // 
            // buttonKreiraj
            // 
            this.buttonKreiraj.Location = new System.Drawing.Point(19, 403);
            this.buttonKreiraj.Name = "buttonKreiraj";
            this.buttonKreiraj.Size = new System.Drawing.Size(99, 23);
            this.buttonKreiraj.TabIndex = 21;
            this.buttonKreiraj.Text = "Kreiraj sportistu";
            this.buttonKreiraj.UseVisualStyleBackColor = true;
            this.buttonKreiraj.Click += new System.EventHandler(this.buttonKreiraj_Click);
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(6, 110);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(24, 13);
            this.labelIme.TabIndex = 3;
            this.labelIme.Text = "Ime";
            // 
            // dateTimePickerDatumRodjenja
            // 
            this.dateTimePickerDatumRodjenja.CustomFormat = "dd-MMM-yyyy";
            this.dateTimePickerDatumRodjenja.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePickerDatumRodjenja.Location = new System.Drawing.Point(124, 168);
            this.dateTimePickerDatumRodjenja.Name = "dateTimePickerDatumRodjenja";
            this.dateTimePickerDatumRodjenja.Size = new System.Drawing.Size(123, 20);
            this.dateTimePickerDatumRodjenja.TabIndex = 8;
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(124, 111);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(123, 20);
            this.textBoxIme.TabIndex = 4;
            // 
            // labelDatumRodjenja
            // 
            this.labelDatumRodjenja.AutoSize = true;
            this.labelDatumRodjenja.Location = new System.Drawing.Point(6, 175);
            this.labelDatumRodjenja.Name = "labelDatumRodjenja";
            this.labelDatumRodjenja.Size = new System.Drawing.Size(78, 13);
            this.labelDatumRodjenja.TabIndex = 7;
            this.labelDatumRodjenja.Text = "Datum rodjenja";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(6, 142);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(44, 13);
            this.labelPrezime.TabIndex = 5;
            this.labelPrezime.Text = "Prezime";
            // 
            // textBoxPrezime
            // 
            this.textBoxPrezime.Location = new System.Drawing.Point(124, 142);
            this.textBoxPrezime.Name = "textBoxPrezime";
            this.textBoxPrezime.Size = new System.Drawing.Size(123, 20);
            this.textBoxPrezime.TabIndex = 6;
            // 
            // SportistaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(368, 504);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "SportistaForm";
            this.Text = "SportistaForm";
            this.Load += new System.EventHandler(this.SportistaForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBrojNaDresu)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButtonKreirajSportistu;
        private System.Windows.Forms.Label labelCm;
        private System.Windows.Forms.Label labelKg;
        private System.Windows.Forms.TextBox textBoxTezina;
        private System.Windows.Forms.TextBox textBoxVisina;
        private System.Windows.Forms.TextBox textBoxDrzava;
        private System.Windows.Forms.TextBox textBoxTelefon;
        private System.Windows.Forms.NumericUpDown numericUpDownBrojNaDresu;
        private System.Windows.Forms.Label labelTelefon;
        private System.Windows.Forms.Label labelDres;
        private System.Windows.Forms.Label labelTezina;
        private System.Windows.Forms.Label labelVisina;
        private System.Windows.Forms.Label labelDrzava;
        private System.Windows.Forms.ComboBox comboBoxTakmicenja;
        private System.Windows.Forms.Button buttonIzmeni;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonKreiraj;
        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.DateTimePicker dateTimePickerDatumRodjenja;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.Label labelDatumRodjenja;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.TextBox textBoxPrezime;
    }
}