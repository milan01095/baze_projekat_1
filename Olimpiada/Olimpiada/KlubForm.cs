﻿using Neo4jClient;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class KlubForm : Form
    {
        GraphClient client;
        Takmicenje selektovanoTakmicenje = null;
        Klub selektovaniKlub = null;

        public KlubForm(Takmicenje takmicenje, Klub klub)
        {
            InitializeComponent();
            selektovanoTakmicenje = takmicenje;
            selektovaniKlub = klub;
        }

        private void KlubForm_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            if (selektovaniKlub != null)
            {
                textBoxNaziv.Text = selektovaniKlub.Naziv;
                textBoxTrener.Text = selektovaniKlub.Trener;
                textBoxSponzor.Text = selektovaniKlub.Sponzor;
                textBoxSajt.Text = selektovaniKlub.Sajt;
                textBoxMenadzer.Text = selektovaniKlub.Menadzer;
                textBoxGrad.Text = selektovaniKlub.Grad;
                textBoxGodina.Text = selektovaniKlub.Datum_osnivanja;
                numericUpDownBrojTitula.Value = Convert.ToDecimal(selektovaniKlub.Broj_titula);
            }
            else                      
                buttonIzmeniKlub.Enabled = false;
            
        }

        private void buttonKreirajKlub_Click(object sender, EventArgs e)
        {
            if (textBoxNaziv.Text != "" && textBoxTrener.Text != "" && textBoxSponzor.Text != "" && textBoxSajt.Text != "" && textBoxMenadzer.Text != "" 
                && textBoxGrad.Text != "" && textBoxGodina.Text != "" && numericUpDownBrojTitula.Value >= 0)
            {
                selektovaniKlub = Klub.Create(client, textBoxNaziv.Text, textBoxGodina.Text, textBoxGrad.Text, textBoxMenadzer.Text, textBoxTrener.Text, numericUpDownBrojTitula.Value.ToString(),
                    textBoxSajt.Text, textBoxSponzor.Text);
                Takmicenje.CreateTakmicenjeKlub(client, selektovanoTakmicenje, selektovaniKlub);
                buttonIzmeniKlub.Enabled = true;
            }

            this.Close();
        }

        private void buttonIzmeniKlub_Click(object sender, EventArgs e)
        {
            if (textBoxNaziv.Text != "" && textBoxTrener.Text != "" && textBoxSponzor.Text != "" && textBoxSajt.Text != "" && textBoxMenadzer.Text != ""
                           && textBoxGrad.Text != "" && textBoxGodina.Text != "" && numericUpDownBrojTitula.Value >= 0)
            {
                selektovaniKlub.Modify(client, textBoxNaziv.Text, textBoxGodina.Text, textBoxGrad.Text, textBoxMenadzer.Text, textBoxTrener.Text, numericUpDownBrojTitula.Value.ToString(),
                    textBoxSajt.Text, textBoxSponzor.Text);           
            }

            this.Close();
        }
    }
}
