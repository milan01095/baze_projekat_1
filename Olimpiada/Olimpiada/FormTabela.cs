﻿using Neo4jClient;
using Olimpiada.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Olimpiada
{
    public partial class FormTabela : Form
    {
        GraphClient client;
        Tabela selektovanaTabela = null;
        Takmicenje selektovanoTakmicenje = null;
        public FormTabela(Takmicenje selektovanoTakmicenje,Tabela tabela)
        {
            InitializeComponent();
            selektovanaTabela = tabela;
            this.selektovanoTakmicenje = selektovanoTakmicenje;
        }

        private void FormTabela_Load(object sender, EventArgs e)
        {
            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "nosql");
            try
            {
                client.Connect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message);
            }
            if(selektovanaTabela != null)
            {
                textBox1.Text = selektovanaTabela.Trenutno_kolo.ToString();
                textBox2.Text = selektovanaTabela.Broj_kola.ToString();
                button1.Enabled = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
           if(textBox1.Text != "" && textBox2.Text != "")
            {
                int trenutno = Convert.ToInt32(textBox1.Text);
                int kola = Convert.ToInt32(textBox2.Text);
                if(trenutno <= kola)
                {
                    selektovanaTabela = Tabela.Create(client, textBox1.Text, textBox2.Text);
                    Takmicenje.CreateTakmicenjeTabela(client, selektovanoTakmicenje, selektovanaTabela);
                    button1.Enabled = false;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                int trenutno = Convert.ToInt32(textBox1.Text);
                int kola = Convert.ToInt32(textBox2.Text);
                if (trenutno <= kola)
                {
                  selektovanaTabela.Modify(client, textBox1.Text, textBox2.Text);                    
               }
            }
        }
    }
}
